package com.rusoft.chequepay.data.entities.info.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.info.Country;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class CountryListResponse extends BaseResponse {

    @SerializedName("countries")
    @Expose @Getter @Setter
    private List<Country> countries;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}
