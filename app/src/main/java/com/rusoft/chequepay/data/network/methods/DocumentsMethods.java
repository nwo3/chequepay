package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.document.request.DocumentListRequest;
import com.rusoft.chequepay.data.entities.document.request.DocumentPdfRequest;
import com.rusoft.chequepay.data.entities.document.response.DocumentListResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.documents.DocsFilterModel;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.ResponseBody;

public class DocumentsMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public DocumentsMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<ResponseBody> getPdf(String token, long senderId, Long documentId) {
        return chequepayApi.getPdf(token, DocumentPdfRequest.builder()
                .senderId(senderId)
                .documentId(documentId)
                .documentType(BusinessConstants.DocType.PDF.getVal())
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<DocumentListResponse> getDocumentList(String token, long senderId, DocsFilterModel model) {
        return chequepayApi.getDocumentList(token, DocumentListRequest.builder()
                .senderId(senderId)
                .currentTime(System.currentTimeMillis())
                .dateStart(model.getDateStart())
                .dateFinish(model.getDateFinish())
                .docCategory(model.getDocCategory().name())
                .docStatus(model.getDocStatus().name())
                .pageNumber(model.getCurrentPageNumber())
                .language(model.getLanguage())
                .sortField(model.getSortFields())
                .build());
    }
}
