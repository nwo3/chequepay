package com.rusoft.chequepay.data.repository.account;


import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;

import lombok.Getter;
import lombok.Setter;

public class NewAccountModel {
    @Getter @Setter String name;
    @Getter @Setter String currency;
    @Getter @Setter Bank bank;
    @Getter @Setter Issuer issuer;

    public boolean isAllDataReceived(){
        return name != null && currency != null && bank != null && issuer != null;
    }
}
