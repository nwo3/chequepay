package com.rusoft.chequepay.data.repository.contractors;

import com.rusoft.chequepay.data.entities.correspondents.request.Account;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CorrespondentModel {
    @Getter @Setter private boolean isEdit = false;
    @Getter @Setter private String correspondentName = "";
    @Getter @Setter private String accountNumber = "";
    @Getter @Setter private String inn = "";
    @Getter @Setter private String comment = "";
    @Getter @Setter private List<Account> accounts = new ArrayList<>();

    public void clear() {
        setEdit(false);
        setCorrespondentName("");
        setAccountNumber("");
        setInn("");
        setComment("");
        getAccounts().clear();
    }

    public boolean isAllDataReceived(){
        return getCorrespondentName().length() > 0 && getAccounts().size() > 0;
    }
}
