package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class GetWalletsResponse extends BaseResponse {
    @SerializedName("currentTime")
    @Expose @Setter @Getter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Setter @Getter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Setter @Getter
    private String phone;

    @SerializedName("signature")
    @Expose @Setter @Getter
    private String signature;

    @SerializedName("wallets")
    @Expose @Setter @Getter
    private List<Wallet> wallets = null;
}
