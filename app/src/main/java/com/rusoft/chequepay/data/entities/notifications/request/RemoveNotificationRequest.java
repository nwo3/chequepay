package com.rusoft.chequepay.data.entities.notifications.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class RemoveNotificationRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("entityId")
    @Expose @Setter
    private String entityId;

    @SerializedName("entityType")
    @Expose @Setter
    private String entityType;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
