package com.rusoft.chequepay.data.entities.accounts.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.accounts.Account;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CheckAccountResponse extends BaseResponse {

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("accountsData")
    @Expose @Getter @Setter
    private List<Account> accountsData;
}
