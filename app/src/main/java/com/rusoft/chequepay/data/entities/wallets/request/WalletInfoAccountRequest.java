package com.rusoft.chequepay.data.entities.wallets.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Setter;

@Builder
@AllArgsConstructor
public class WalletInfoAccountRequest extends BodyRequest {

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("email")
    @Expose @Setter
    private String email;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("phone")
    @Expose @Setter
    private String phone;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("wallet")
    @Expose @Setter
    private String wallet;
}
