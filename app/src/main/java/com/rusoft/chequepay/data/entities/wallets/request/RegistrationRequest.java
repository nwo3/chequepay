package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.wallets.Account;
import com.rusoft.chequepay.data.entities.wallets.Address;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.Company;
import com.rusoft.chequepay.data.entities.wallets.PersonBase;
import com.rusoft.chequepay.data.entities.wallets.UrAddress;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class RegistrationRequest extends BodyRequest {

    @SerializedName("account")
    @Expose @Setter
    private Account account;

    @SerializedName("allowCertReg")
    @Expose @Setter
    private Boolean allowCertReg;

    @SerializedName("allowedIpAddresses")
    @Expose @Setter
    private List<String> allowedIpAddresses;

    @SerializedName("bankAccount")
    @Expose @Setter
    private BankAccount bankAccount;

    @SerializedName("cashAvailable")
    @Expose @Setter
    private Boolean cashAvailable;

    @SerializedName("company")
    @Expose @Setter
    private Company company;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("factAddress")
    @Expose @Setter
    private Address factAddress;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("password")
    @Expose @Setter
    private String password;

    @SerializedName("paymentType")
    @Expose @Setter
    private int paymentType;

    @SerializedName("person")
    @Expose @Setter
    private PersonBase person;

    @SerializedName("regType")
    @Expose @Setter
    private Integer regType;

    @SerializedName("residencyCountryId")
    @Expose @Setter
    private Integer residencyCountryId;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("urAddress")
    @Expose @Setter
    private UrAddress urAddress;

}
