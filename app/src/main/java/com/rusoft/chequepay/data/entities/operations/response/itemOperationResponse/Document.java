package com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Document {
    @SerializedName("id")
    @Expose @Getter @Setter
    private String id;

    @SerializedName("idDownload")
    @Expose @Getter @Setter
    private long idDownload;

    @SerializedName("link")
    @Expose @Getter @Setter
    private Boolean link;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("type")
    @Expose @Getter @Setter
    private long type;
}
