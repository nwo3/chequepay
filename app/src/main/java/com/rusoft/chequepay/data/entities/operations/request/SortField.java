package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Setter;

public class SortField {
    @SerializedName("column")
    @Expose @Setter
    private String column;
    @SerializedName("direction")
    @Expose @Setter
    private String direction;

    public SortField(String column, String direction) {
        this.column = column;
        this.direction = direction;
    }
}
