package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;

public class MessageMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public MessageMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<Integer> getMessage(long senderId, long currentTime) {
        Map<String, Long> params = new HashMap<>();
        params.put("id", currentTime);
        params.put("walletId", senderId);
        return chequepayApi.getMessage(params);
    }

    public Single<String> disconnectMessage(long senderId, long currentTime) {
        Map<String, Long> params = new HashMap<>();
        params.put("id", currentTime);
        params.put("walletId", senderId);
        return chequepayApi.disconnectMessage(params);
    }
}
