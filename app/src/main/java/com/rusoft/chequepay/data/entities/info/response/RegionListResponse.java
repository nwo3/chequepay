package com.rusoft.chequepay.data.entities.info.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.info.Region;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class RegionListResponse extends BaseResponse {

    @SerializedName("regionList")
    @Expose @Getter @Setter
    private List<Region> regionList = null;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}
