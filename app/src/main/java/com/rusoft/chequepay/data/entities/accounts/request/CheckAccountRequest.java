package com.rusoft.chequepay.data.entities.accounts.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public class CheckAccountRequest extends BodyRequest {

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
