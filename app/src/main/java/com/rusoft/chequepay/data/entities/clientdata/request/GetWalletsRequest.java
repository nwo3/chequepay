package com.rusoft.chequepay.data.entities.clientdata.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetWalletsRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("regType")
    @Expose @Setter
    private String regType;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
