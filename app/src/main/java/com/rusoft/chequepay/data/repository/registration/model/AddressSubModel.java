package com.rusoft.chequepay.data.repository.registration.model;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;

import lombok.Getter;
import lombok.Setter;

public class AddressSubModel {
    private RegistrationModel registrationModel;
    private boolean isRegAddress;
    @Getter @Setter private Country country;
    @Getter @Setter private Region region;
    @Getter @Setter private String postCode;
    @Getter @Setter private String settlement;
    @Getter @Setter private String street;

    public AddressSubModel(RegistrationModel registrationModel, boolean isRegAddress) {
        this.registrationModel = registrationModel;
        this.isRegAddress = isRegAddress;
    }

    public boolean isFromRussia(){
        return country.getCountryId() == BusinessConstants.COUNTRY_CODE_RUSSIA;
    }

    public boolean isRequiredDataReceived() {
        if (!registrationModel.isInProfileEditingMode()) {
            return country != null && (!isFromRussia() || region != null) &&
                    postCode != null && settlement != null && street != null;
        } else {
            return isCountryChanged(country != null ? country.getName() : "") ||
                    isRegionChanged(region != null ? region.getName() : "") ||
                    isPostCodeChanged(postCode) ||
                    isSettlementChanged(settlement) ||
                    isStreetChanged(street);
        }
    }

    public void copy(AddressSubModel source) {
        country = source.getCountry();
        region = source.getRegion();
        postCode = source.getPostCode();
        settlement = source.getSettlement();
        street = source.getStreet();
    }

    public void clear() {
        //country = null;
        region = null;
        postCode = null;
        settlement = null;
        street = null;
    }

    void updateByProfile() {
        if (registrationModel.comparedProfile == null) throw new InternalError();

        com.rusoft.chequepay.data.entities.wallets.Address address = isRegAddress ? registrationModel.comparedProfile.getUrAddress() : registrationModel.comparedProfile.getFactAddress();
        setCountry(new Country(address.getCountryId(), address.getCountryNameEn(), address.getCountryName()));
        setRegion(new Region(address.getRegionId(), address.getRegionNameEn(), address.getRegionName()));
        setPostCode(address.getPostcode());
        setSettlement(address.getSettlement());
        setStreet(address.getStreet());
    }

    private boolean isCountryChanged(String newCountry) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldCountry = isRegAddress ? registrationModel.comparedProfile.getUrAddress().getCountryName()
                : registrationModel.comparedProfile.getFactAddress().getCountryName();
        if (oldCountry == null) {
            return newCountry != null;
        } else {
            return newCountry != null && !oldCountry.equals(newCountry);
        }
    }

    private boolean isRegionChanged(String newRegion) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldRegion = isRegAddress ? registrationModel.comparedProfile.getUrAddress().getRegionName()
                : registrationModel.comparedProfile.getFactAddress().getRegionName();
        if (oldRegion == null) {
            return newRegion != null;
        } else {
            return newRegion != null && !oldRegion.equals(newRegion);
        }
    }

    private boolean isPostCodeChanged(String newPostCode) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldPostCode = isRegAddress ? registrationModel.comparedProfile.getUrAddress().getPostcode()
                : registrationModel.comparedProfile.getFactAddress().getPostcode();
        if (oldPostCode == null) {
            return newPostCode != null;
        } else {
            return newPostCode != null && !oldPostCode.equals(newPostCode);
        }
    }

    private boolean isSettlementChanged(String newSettlement) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldSettlement = isRegAddress ? registrationModel.comparedProfile.getUrAddress().getSettlement()
                : registrationModel.comparedProfile.getFactAddress().getSettlement();
        if (oldSettlement == null) {
            return newSettlement != null;
        } else {
            return newSettlement != null && !oldSettlement.equals(newSettlement);
        }
    }

    private boolean isStreetChanged(String newStreet) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldStreet = isRegAddress ? registrationModel.comparedProfile.getUrAddress().getStreet()
                : registrationModel.comparedProfile.getFactAddress().getStreet();
        if (oldStreet == null) {
            return newStreet != null;
        } else {
            return newStreet != null && !oldStreet.equals(newStreet);
        }
    }

    @Override
    public String toString() {
        return "AddressSubModel{" +
                "isRegAddress=" + isRegAddress +
                ", country=" + country +
                ", region=" + region +
                ", postCode='" + postCode + '\'' +
                ", settlement='" + settlement + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}
