package com.rusoft.chequepay.data.repository.registration.model;


import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;

import java.util.Calendar;

import javax.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

public class RegistrationModel {
    @Getter @Setter String flowToken;

    //region profile edit
    @Getter @Setter boolean isInProfileEditingMode;
    @Setter @Getter @Nullable FullProfileResponse comparedProfile; //NonNull only in profile edit mode
    @Getter @Setter boolean isActionDone;
    //endregion

    public RootSubModel root = new RootSubModel(this);
    public PersonSubModel person = new PersonSubModel(this);
    public PassportSubModel passport = new PassportSubModel(this);
    public AddressSubModel regAddress = new AddressSubModel(this, true);
    public AddressSubModel factAddress = new AddressSubModel(this, false);
    public BankSubModel bank = new BankSubModel(this);
    public AccountSubModel account = new AccountSubModel(this);

    //Category organization is not implemented yet

    public boolean isResident(){
        return root.getResidencyCountry().equals(BusinessConstants.COUNTRY_RUSSIA);
    }

    public boolean isAllDataReceived(){
        return person.isRequiredDataReceived() &&
                passport.isRequiredDataReceived() &&
                regAddress.isRequiredDataReceived() &&
                factAddress.isRequiredDataReceived() &&
                bank.isRequiredDataReceived() &&
                account.isRequiredDataReceived();
    }

    public void clearBankSubModel(){
        bank = new BankSubModel(this);
    }

    public void clearFlowToken() {
        flowToken = null;
    }

    /**
     * Method used only in profile editing screen,
     * parses profile data to profile model.
     */
    public void updateModelByProfile(FullProfileResponse profile) {
        setComparedProfile(profile);
        root.updateByProfile();
        person.updateByProfile();
        passport.updateByProfile();
        regAddress.updateByProfile();
        factAddress.updateByProfile();
        bank.updateByProfile();
    }

    public static RegistrationModel MOCK() {
        RegistrationModel model = new RegistrationModel();

        model.root.setRegType(BusinessConstants.RegType.STANDART);
        model.root.setResidencyCountry(BusinessConstants.COUNTRY_RUSSIA);
        model.root.setCategory(BusinessConstants.ClientCategory.INDIVIDUAL);
        model.root.setPass("password123Q");
        model.root.setPassConfirm("password123Q");

        model.person.setLastName("Иванов");
        model.person.setFirstName("Тест");
        model.person.setPatronymic("Иванович");
        model.person.setBirthDate(Calendar.getInstance());
        model.person.setMobPhone("+79507777707");
        model.person.setEmail("testov.t.7@chequepay.ru");
        model.person.setWebSite("www.chequepay.ru");

        model.passport.setSeries("1111");
        model.passport.setNumber("169666");
        model.passport.setIssuer("ТП #77 УФМС г. Санкт-Петербург");
        model.passport.setIssueDate((long) 1300654);
        model.passport.setDepartmentCode("444-000");

        model.regAddress.setCountry(BusinessConstants.COUNTRY_RUSSIA);
        model.regAddress.setRegion(new Region(78, "Leningradskaya oblast", "Ленинградская обл"));
        model.regAddress.setPostCode("196777");
        model.regAddress.setSettlement("г Санкт-Петербург");
        model.regAddress.setStreet("Выборгская наб.55");

        model.bank.setBik("041203711");
        model.bank.setName("ФИЛИАЛ \"ЦЕНТРАЛЬНЫЙ\" БАНКА ВТБ (ПАО)");
        model.bank.setCorrAcc("30101810145250000411");
        model.bank.setAccNum("01234567890123456789");
        model.bank.setInn("0123456789");
        model.bank.setKpp("770943002");
        model.bank.setReceiver("Тестов Т");

        return model;
    }

    @Override
    public String toString() {
        return "\nROOT: " + root +
                "\nPERSON: " + person +
                "\nPASSPORT: " + passport +
                "\nREG.ADDRESS: " + regAddress +
                "\nHOME.ADDRESS: " + factAddress +
                "\nBANK: " + bank +
                "\nACCOUNT: " + account;
    }
}