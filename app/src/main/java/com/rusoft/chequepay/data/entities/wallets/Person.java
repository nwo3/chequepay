package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@AllArgsConstructor
public class Person extends PersonBase{

    @SerializedName("birthDate")
    @Expose @Getter @Setter
    private Long birthDate;

    @SerializedName("email")
    @Expose @Getter @Setter
    private String email;

    @SerializedName("firstName")
    @Expose @Getter @Setter
    private String firstName;

    @SerializedName("firstNameEn")
    @Expose @Getter @Setter
    private String firstNameEn;

    @SerializedName("lastName")
    @Expose @Getter @Setter
    private String lastName;

    @SerializedName("lastNameEn")
    @Expose @Getter @Setter
    private String lastNameEn;

    @SerializedName("mobilePhone")
    @Expose @Getter @Setter
    private String mobilePhone;

    @SerializedName("passport")
    @Expose @Getter @Setter
    private Passport passport;

    @SerializedName("patronymic")
    @Expose @Getter @Setter
    private String patronymic;

    @SerializedName("patronymicEn")
    @Expose @Getter @Setter
    private String patronymicEn;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("taxNumber")
    @Expose @Getter @Setter
    private String taxNumber;

    @SerializedName("website")
    @Expose @Getter @Setter
    private String website;

    public Person getCopy() {
        return new Person(
                this.getBirthDate(),
                this.getEmail(),
                this.getFirstName(),
                this.getFirstNameEn(),
                this.getLastName(),
                this.getLastNameEn(),
                this.getMobilePhone(),
                this.getPassport(),
                this.getPatronymic(),
                this.getPatronymicEn(),
                this.getPhone(),
                this.getTaxNumber(),
                this.getWebsite());
    }
}
