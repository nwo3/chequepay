package com.rusoft.chequepay.data.entities.info.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public class GetBankInfoRequest extends BodyRequest {

    @SerializedName("bic")
    @Expose @Setter
    private String bic;
}
