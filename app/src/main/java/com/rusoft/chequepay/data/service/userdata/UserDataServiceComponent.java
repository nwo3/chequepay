package com.rusoft.chequepay.data.service.userdata;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ChequepayApiModule.class})
public interface UserDataServiceComponent {
    void inject(UserDataService userDataService);
}
