package com.rusoft.chequepay.data.service.update;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.data.repository.user.UserRepository;

import javax.inject.Inject;

public class UpdateDataService extends IntentService {

    @Inject
    UserRepository userRepository;

    public UpdateDataService() {
        super("UpdateDataService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDaggerComponent();
    }

    private void initializeDaggerComponent() {
        DaggerUpdateDataServiceComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


    }
}
