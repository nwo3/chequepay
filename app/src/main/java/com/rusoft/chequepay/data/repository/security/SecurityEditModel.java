package com.rusoft.chequepay.data.repository.security;


import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class SecurityEditModel {
    @Getter @Setter private String newPass, repeatPass, oldPass;
    @Getter @Setter private boolean isIpRestrictionEnabled;
    @Getter @Setter private List<String> allowedIpAddresses;
    @Getter @Setter private boolean authByOtpEnabled;
    @Getter @Setter private boolean smsNotificationEnabled;
    @Getter @Setter private String flowToken;
    @Getter @Setter private boolean isEditingDone;

    public enum SecurityScreenActiveTab {PASS, IP, SMS}
    @Getter @Setter private SecurityScreenActiveTab activeTab = SecurityScreenActiveTab.PASS;

    private SecurityEditModel initialModel;

    public void update(FullProfileResponse profile) {
        allowedIpAddresses = profile.getAllowedIpAddresses();
        isIpRestrictionEnabled = allowedIpAddresses != null && allowedIpAddresses.size() > 0;

        authByOtpEnabled = profile.getAuthByOtp();
        smsNotificationEnabled = profile.getSmsNotification();

        initialModel = new SecurityEditModel(this);
    }

    private SecurityEditModel(SecurityEditModel modelToCopy) {
        this.isIpRestrictionEnabled = modelToCopy.isIpRestrictionEnabled();
        this.allowedIpAddresses = new ArrayList<>(modelToCopy.getAllowedIpAddresses());
        this.authByOtpEnabled = modelToCopy.isAuthByOtpEnabled();
        this.smsNotificationEnabled = modelToCopy.isSmsNotificationEnabled();
    }

    public boolean isPassConfirmed() {
        return newPass != null && repeatPass != null && newPass.equals(repeatPass);
    }

    private boolean isPassChangeDataReceived() {
        return isPassConfirmed() && oldPass != null;
    }

    private boolean isIpRestrictionDataChanged() {
        return !(isIpRestrictionEnabled && allowedIpAddresses.size() == 0) &&
                ((isIpRestrictionEnabled && !allowedIpAddresses.equals(initialModel.getAllowedIpAddresses())) || (initialModel.isIpRestrictionEnabled && !isIpRestrictionEnabled));
    }

    private boolean isSmsSettingsChanged() {
        return authByOtpEnabled != initialModel.isAuthByOtpEnabled()
                || smsNotificationEnabled != initialModel.isSmsNotificationEnabled();
    }

    public boolean isProceedShouldBeEnabled() {
        switch (activeTab) {
            case PASS:
                return isPassChangeDataReceived();

            case IP:
                return isIpRestrictionDataChanged();

            case SMS:
                return isSmsSettingsChanged();
        }

        return false;
    }

    public void clearFlowToken() {
        flowToken = null;
    }
}
