package com.rusoft.chequepay.data.entities.recover.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class RecoverStartResponse extends BaseResponse {

    @SerializedName("delay")
    @Expose @Getter @Setter
    public long delay;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

}
