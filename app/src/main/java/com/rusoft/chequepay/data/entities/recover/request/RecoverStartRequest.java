package com.rusoft.chequepay.data.entities.recover.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class RecoverStartRequest extends BodyRequest {

    @SerializedName("confirmType")
    @Expose @Setter
    public BusinessConstants.PassRecoveryType confirmType;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

    @SerializedName("source")
    @Expose @Setter
    public String source;

}
