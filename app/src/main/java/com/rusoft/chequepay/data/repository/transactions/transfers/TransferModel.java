package com.rusoft.chequepay.data.repository.transactions.transfers;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceItemResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class TransferModel extends BaseTransactionModel {
    @Getter Account senderAccount;@Getter String currency;
    @Getter @Setter boolean inputModeManual;
    @Getter @Setter Correspondent chosenContact;
    private String receiverAccountIdManual, receiverAccountIdChosen;
    @Getter @Setter String comment;

    @Getter @Setter Double amount;
    @Getter @Setter Double commission;
    @Getter Double commissionAmount;
    @Getter Double total;

    @Getter @Setter String operationUID;
    @Getter @Setter boolean isTransactionDone;

    @Getter private Application shortInvoiceData;
    @Getter private InvoiceItemResponse fullInvoiceData;

    public void applyShortInvoiceData(Application shortInvoiceData){
        this.shortInvoiceData = shortInvoiceData;

        inputModeManual = false;
        setReceiverAccountId(shortInvoiceData.getAccountNumber());
        comment = shortInvoiceData.getComment();
        amount = shortInvoiceData.getAmount();
    }

    public void applyFullInvoiceData(InvoiceItemResponse fullInvoiceData){
        this.fullInvoiceData = fullInvoiceData;
    }

    public void setSenderAccount(Account senderAccount) {
        if (senderAccount != null) {
            String newStrCode = CurrencyUtils.getStrCode(senderAccount.getCurrency());
            if (!inputModeManual && isCurrencyChanged(newStrCode)) {
                receiverAccountIdChosen = null;
            }

            this.senderAccount = senderAccount;
            this.currency = newStrCode;

        } else {
            this.senderAccount = null;
            this.currency = null;
        }
    }

    private boolean isCurrencyChanged(String newStrCode) {
        return this.currency != null && newStrCode != null && !newStrCode.equals(this.currency);
    }

    public String getReceiverAccountId() {
        return inputModeManual ? receiverAccountIdManual : receiverAccountIdChosen;
    }

    public void setReceiverAccountId(String receiverAccountId) {
        if (inputModeManual) {
            receiverAccountIdManual = receiverAccountId;
        } else {
            receiverAccountIdChosen = receiverAccountId;
        }
    }

    public boolean updateNumericData() {
        boolean isSuccess;
        if (amount != null && this.commission != null) {
            total = CommonUtils.round(amount * (1 + commission / 100), 2);
            commissionAmount = total - amount;
            isSuccess = true;
        } else {
            commissionAmount = null;
            total = null;
            isSuccess = false;
        }
        return isSuccess;
    }

    public boolean isAllDataReceived() {
        return senderAccount != null &&
                getReceiverAccountId() != null && !getReceiverAccountId().isEmpty() &&
                amount != null && commission != null &&
                currency != null && total != null;
    }
}
