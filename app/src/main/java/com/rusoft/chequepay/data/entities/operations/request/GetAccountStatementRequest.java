package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetAccountStatementRequest extends BodyRequest {
    @SerializedName("currency")
    @Expose @Setter
    private List<String> currency;

    @SerializedName("operationType")
    @Expose @Setter
    private List<Integer> operationTypeList;

    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("dateFinish")
    @Expose @Setter
    private long dateFinish;

    @SerializedName("dateStart")
    @Expose @Setter
    private long dateStart;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("contractorId")
    @Expose @Setter
    private Long contractorId;

    @SerializedName("pageNumber")
    @Expose @Setter
    private long pageNumber;

    @SerializedName("paymentType")
    @Expose @Setter
    private String paymentType;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("sortField")
    @Expose @Setter
    private List<SortField> sortField;
}
