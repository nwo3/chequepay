package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class PasswordChangeRequest extends BodyRequest {

    @SerializedName("clientId")
    @Expose @Setter
    public Long clientId;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("newPassword")
    @Expose @Setter
    public String newPassword;

    @SerializedName("oldPassword")
    @Expose @Setter
    public String oldPassword;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
