package com.rusoft.chequepay.data.entities.document.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class DocumentPdfRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("documentId")
    @Expose @Setter
    private Long documentId;

    @SerializedName("documentType")
    @Expose @Setter
    private int documentType;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;
}
