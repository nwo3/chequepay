package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class BankRus {
    @SerializedName("bic")
    @Expose @Setter @Getter
    private String bic;

    @SerializedName("corAccount")
    @Expose @Setter @Getter
    private String corAccount;

    @SerializedName("deleted")
    @Expose @Setter @Getter
    private Boolean deleted;

    @SerializedName("name")
    @Expose @Setter @Getter
    private String name;
}
