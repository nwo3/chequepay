package com.rusoft.chequepay.data.repository.messages;

import com.rusoft.chequepay.data.event.UpdateDataEvent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.methods.MessageMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.rusoft.chequepay.AppConstants.SUCCESS_RESPONSE;

public class MessageRepository extends BaseRepository implements IMessageRepository {

    private CacheManager cache;
    private Preferences preferences;
    private MessageMethods messageMethods;
    private boolean stopLongPolling = false;

    @Inject
    public MessageRepository(CacheManager cacheManager, Preferences preferences, MessageMethods messageMethods) {
        this.cache = cacheManager;
        this.preferences = preferences;
        this.messageMethods = messageMethods;
        }

    public void initLongPolling() {
        getMessage();
    }

    public void removeLongPolling() {
        stopLongPolling = true;
    }

    private void getMessage() {
        messageMethods.getMessage(preferences.getClientId(), preferences.getTimestamp())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        if (integer == SUCCESS_RESPONSE) {
                            flagsManager.updateAll();
                            EventBus.getDefault().post(new UpdateDataEvent());
                        }
                        if (!stopLongPolling)
                            getMessage();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (!stopLongPolling)
                            getMessage();
                    }
                });
    }

    public void disconnectMessage(RequestCallbackListener listener) {
        messageMethods.disconnectMessage(preferences.getClientId(), preferences.getTimestamp())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String sting) {
                        listener.onSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onError(e);
                    }
                });
    }
}
