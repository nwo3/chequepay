package com.rusoft.chequepay.data.entities.clientdata.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class BankRequisitesRequest extends BodyRequest {
    @SerializedName("bankId")
    @Expose @Setter
    private long bankId;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("defineCurrencyAccount")
    @Expose @Setter
    private Boolean defineCurrencyAccount;

    @SerializedName("issuerId")
    @Expose @Setter
    private long issuerId;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
