package com.rusoft.chequepay.data.repository.transactions.withdrawal;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.application.selltoissuer_15.response.SellToIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.cheques.Cheque;
import com.rusoft.chequepay.data.entities.cheques.response.GetChequeResponse;
import com.rusoft.chequepay.data.entities.tariffs.response.GetTariffResponse;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.ApplicationMethods;
import com.rusoft.chequepay.data.network.methods.ChequesMethods;
import com.rusoft.chequepay.data.network.methods.TariffsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class WithdrawalRepository extends BaseRepository implements IWithdrawalRepository {

    private CacheManager cache;
    private Preferences preferences;

    private UserRepository userRepository;
    private ChequesMethods chequesMethods;
    private TariffsMethods tariffsMethods;
    private ApplicationMethods applicationMethods;

    @Getter private WithdrawalModel model;

    @Inject
    public WithdrawalRepository(CacheManager cache, Preferences preferences,
                                UserRepository userRepository, ChequesMethods chequesMethods,
                                TariffsMethods tariffsMethods, ApplicationMethods applicationMethods) {
        this.cache = cache;
        this.preferences = preferences;

        this.userRepository = userRepository;
        this.chequesMethods = chequesMethods;
        this.tariffsMethods = tariffsMethods;
        this.applicationMethods = applicationMethods;

        this.model = cache.getWithdrawalModel();
    }

    @Override
    public void updateEmitterData(RequestCallbackListener listener) {
        Account account = model.getAccount();
        if (account == null) {
            return;

        } else if (account.getIssuerId() != null && account.getIssuerShortName() != null){
            model.setIssuerId(account.getIssuerId());
            model.setIssuerShortName(account.getIssuerShortName());
            listener.onSuccess();
            return;
        }

        Disposable disposable = chequesMethods.getCheque(preferences.getAuthToken(), preferences.getClientId(), model.getAccount().getAccountNumber())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getTariff(RequestCallbackListener listener) {
        if (model.getAccount() == null || model.getIssuerId() == null){
            return;
        }

        Disposable disposable =  tariffsMethods.getTariff(preferences.getAuthToken(), preferences.getClientId(), model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void createWithdraw(String otp, RequestCallbackListener listener) {
        String authToken = preferences.getAuthToken();
        long clientId = preferences.getClientId();
        FullProfileResponse profile = userRepository.getUser().getFullProfile();
        model.setIsResident(profile.isResident());
        model.setBankAccount(profile.getBankAccount());

        Disposable disposable = applicationMethods.createWithdraw(authToken, clientId, otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof GetChequeResponse) {
                Cheque cheque = ((GetChequeResponse) response).getCheque();
                model.setIssuerId(cheque.getIssuerId());
                model.setIssuerShortName(cheque.getIssuerShortName());
                listener.onSuccess();

            } else if (response instanceof GetTariffResponse) {
                double tariff = ((GetTariffResponse) response).getTariff();
                listener.onSuccess(tariff);

            } else if (response instanceof SellToIssuerCreateResponse){
                SellToIssuerCreateResponse res = (SellToIssuerCreateResponse) response;
                model.setApplicationCoreId(res.getApplicationCoreId());
                model.setFlowToken(res.getFlowToken());
                flagsManager.turnOn(Flags.ACCOUNTS);
                listener.onSuccess();
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    @Override
    public void clearModel() {
        cache.clearWithdrawalModel();
    }

}
