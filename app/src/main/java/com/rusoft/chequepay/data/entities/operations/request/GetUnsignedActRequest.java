package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetUnsignedActRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
