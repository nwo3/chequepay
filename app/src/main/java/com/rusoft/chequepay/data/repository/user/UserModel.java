package com.rusoft.chequepay.data.repository.user;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LongSparseArray;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.accounts.AccountShort;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.entities.cheques.Cheque;
import com.rusoft.chequepay.data.entities.clientdata.response.Contractor;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.document.response.Document;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.BankIssuer;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.entities.info.response.BankIssuersListResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.entities.signin.response.SignInOpenApiResponse;
import com.rusoft.chequepay.data.entities.templates.BaseTemplate;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.data.entities.wallets.Passport;
import com.rusoft.chequepay.data.entities.wallets.Person;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.ShortProfileResponse;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.LogUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class UserModel {

    @Getter @Setter private long clientId;
    @Getter @Setter private String phone;
    @Getter @Setter private String operatorLogin;
    @Getter @Setter private String accessToken;
    @Getter @Setter private long accessTokenExpired;
    @Getter @Setter private String refreshToken;
    @Getter @Setter private long refreshTokenExpired;
    @Getter @Setter private String flowToken;

    @Getter List<Account> accounts = new ArrayList<>();
    @Getter @Setter List<AccountShort> shortAccounts = new ArrayList<>();
    @Getter @Setter Map<String, Cheque> accountChequesMap = new HashMap<>();

    FullProfileResponse fullProfile;

    @Getter @Setter ShortProfileResponse shortProfile;

    @Setter List<Operation> shortOperations = new ArrayList<>();

    @Setter List<Operation> filteredOperations = new ArrayList<>();
    @Getter @Setter List<Correspondent> correspondents = new ArrayList<>();

    @Getter @Setter List<Contractor> contractors = new ArrayList<>();

    @Getter @Setter List<Document> documents = new ArrayList<>();

    @Getter @Setter List<Application> invoices = new ArrayList<>();

    @Getter private String login;

    @Getter private String password;
    private List<BankIssuer> bankIssuerList;

    private Map<String, Map<Issuer, Bank>> bankIssuersMap;
    @Getter @Setter private List<ShortTemplate> templatesList = new ArrayList<>();

    private LongSparseArray<BaseTemplate> detailedTemplatesMap = new LongSparseArray<>();

    public FullProfileResponse getFullProfile() {
        return fullProfile;
    }

    public void setFullProfile(FullProfileResponse fullProfile) {
        this.fullProfile = fixProfileIfSomeDataIsEmpty(fullProfile);

    }

    /*
     * Костыль необходимый во избежание крашей, связанных с отсутствием данных,
     * в случае если юзер был зарегестрирован напрямую в бд или через консоль.
     * */
    private FullProfileResponse fixProfileIfSomeDataIsEmpty(FullProfileResponse profile) {

        if (profile.getPerson() == null){
            profile.setPerson(new Person(null, "", "",
                            "", "", "", "",
                            new Passport("", null, "", "", "", null),
                            "", "", "", "", ""));
        }

        return profile;
    }

    public void setAuthData(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void clearAuthData() {
        this.login = null;
        this.password = null;
    }

    public void updateUserData(SignInOpenApiResponse response) {
        clientId = response.getClientId();
        phone = response.getPhone();
        operatorLogin = response.getOperatorLogin();
        accessToken = response.getAccessToken();
        accessTokenExpired = response.getAccessTokenExpired();
        refreshToken = response.getRefreshToken();
        refreshTokenExpired = response.getRefreshTokenExpired();
        flowToken = response.getFlowToken();
    }

    public double getCurrencyTotalBalance(int currencyCode) {
        double total = 0;
        for (Account account : accounts) {
            if (account.getCurrency() == currencyCode) {
                try {
                    total += account.getAmount();
                } catch (Exception e) {
                    LogUtil.e(e.getMessage());
                }
            }
        }
        return total;
    }

    public boolean isAccountsNullOrEmpty() {
        return accounts == null || accounts.isEmpty();
    }

    public void setAccounts(List<Account> accounts){
        Collections.sort(accounts, (a1, a2) -> a1.getRegDate().compareTo(a2.getRegDate()));
        this.accounts = accounts;
    }

    public void addCheque(Cheque cheque){
        accountChequesMap.put(cheque.getHolderAccount(), cheque);
    }

    @Nullable
    public BaseTemplate getTemplate(long templateId){
        return detailedTemplatesMap.get(templateId);
    }

    public void addTemplate(BaseTemplate template){
        detailedTemplatesMap.put(template.getTemplateId(), template);
    }

    @Nullable
    public Cheque getCheque(String accountNumber){
        return accountChequesMap.get(accountNumber);
    }

    @Nullable
    public Account getOldestAccount() {
        if (isAccountsNullOrEmpty())
            return null;

        for (Account account : accounts){
            CurrencyUtils.Currency currency = CurrencyUtils.getCurrency(account.getCurrency());
            if (currency != null) {
                boolean isEnabledForResident = currency.isEnabledForResident();
                if (isResident() && isEnabledForResident) return account;
                if (!isResident() && !isEnabledForResident) return account;
            }
        }

        return accounts.get(0);
    }

    @Nullable
    public List<Account> getAccountsWithPositiveBalance(){
        if (accounts == null){
            return null;
        }

        List<Account> tempList = new ArrayList<>();
        for (Account account : accounts){
            if (account.getAmount() > 0){
                tempList.add(account);
            }
        }
        return tempList;
    }

    public void setBankIssuersMap(String currencyStrCode, BankIssuersListResponse response){
        if (bankIssuersMap == null) bankIssuersMap = new HashMap<>();

        Map<Issuer, Bank> map = new HashMap<>();
        bankIssuerList = response.getAvailableBanksAndIssuersData();
        for (BankIssuer bankIssuer : bankIssuerList){
            map.put(bankIssuer.getIssuer(), bankIssuer.getBank());
        }

        bankIssuersMap.put(currencyStrCode, map);
    }

    @NonNull
    public List<Issuer> getIssuers(String currency){
        List<Issuer> result = new ArrayList<>();

        Map<Issuer, Bank> map = bankIssuersMap.get(currency);
        if (map == null) return new ArrayList<>();

        for (Map.Entry<Issuer, Bank> entry : map.entrySet()){
            Issuer issuer = entry.getKey();
            if (!result.contains(issuer)){
                result.add(issuer);
            }
        }

        return result;
    }

    @Nullable
    public Issuer getIssuerById(String currency, String issuerWalletId) {
        Issuer result = null;

        for (Issuer issuer : getIssuers(currency)){
            if (issuer.getIssuerWalletId().equals(issuerWalletId)){
                result = issuer;
            }
        }

        return result;
    }

    @NonNull
    public List<Bank> getBanks(String currency){
        List<Bank> result = new ArrayList<>();

        Map<Issuer, Bank> map = bankIssuersMap.get(currency);
        for (Map.Entry<Issuer, Bank> entry : map.entrySet()){
            Bank bank = entry.getValue();
            if (!result.contains(bank)){
                result.add(bank);
            }
        }

        return result;
    }

    @Nullable
    public Bank getBankById(String currency, String bankWalletId) {
        Bank result = null;

        for (Bank bank : getBanks(currency)){
            if (bank.getBankWalletId().equals(bankWalletId)){
                result = bank;
            }
        }

        return result;
    }

    @NonNull
    public List<Bank> getBankByIssuer(String currency, Issuer issuer){
        List<Bank> result = new ArrayList<>();

        Map<Issuer, Bank> map = bankIssuersMap.get(currency);
        for (Map.Entry<Issuer, Bank> entry : map.entrySet()){
            if (entry.getKey().equals(issuer)){
                Bank bank = entry.getValue();
                if (!result.contains(bank)){
                    result.add(bank);
                }
            }
        }

        return result;
    }

    @NonNull
    public List<Issuer> getIssuersByBank(String currency, Bank bank) {
        List<Issuer> result = new ArrayList<>();

        Map<Issuer, Bank> map = bankIssuersMap.get(currency);
        for (Map.Entry<Issuer, Bank> entry : map.entrySet()){
            if (entry.getValue().equals(bank)){
                Issuer issuer = entry.getKey();
                if (!result.contains(issuer)){
                    result.add(issuer);
                }
            }
        }

        return result;
    }

    public boolean isBankAndIssuerHasRelations(Issuer issuer, Bank bank){
        if (bankIssuerList == null || issuer == null || bank == null){
            LogUtil.e(ErrorUtils.INTERNAL_ERROR);
            return false;
        }

        for (BankIssuer ba : bankIssuerList){
            if (ba.getIssuerWalletId().equals(issuer.getIssuerWalletId())
                    && ba.getBankWalletId().equals(bank.getBankWalletId())){
                return true;
            }
        }

        return false;
    }

    @Nullable
    public Account getAccountByNum(String accountNumber) {
        Account result = null;
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                result = account;
            }
        }
        return result;
    }

    @Nullable
    public Correspondent getCorrespondentByAccountNumber(String accountNumber) {
        Correspondent result = null;
        for (Correspondent correspondent : correspondents) {
            for (com.rusoft.chequepay.data.entities.correspondents.response.Account account : correspondent.getAccounts()) {
                if (account.getNumber().equals(accountNumber)) {
                    result = correspondent;
                }
            }
        }
        return result;
    }


    @Nullable
    public Correspondent getCorrespondentByWalletId(String walletId) {
        Correspondent result = null;
        for (Correspondent correspondent : correspondents) {
            if (Long.toString(correspondent.getWalletId()).equals(walletId)) {
                result = correspondent;
            }
        }
        return result;
    }

    /**
     * @return a selection of operations by account for last month
     */
    @NonNull
    public List<Operation> getAccountOperationsList(String accountNumber) {
        List<Operation> result = new ArrayList<>();

        for (Operation operation : getShortOperations()) {
            if (operation.getAccountNumber().equals(accountNumber)) {
                result.add(operation);
            }
        }

        return result;
    }

    @NonNull
    public List<Account> getActiveAccounts() {
        List<Account> accounts = new ArrayList<>();
        for (Account account : getAccounts()) {
            if (!account.isActiveAccount() && account.getSpecialProperty() == 0) {
                accounts.add(account);
            }
        }
        return accounts;
    }

    public Long getWalletId() {
        return Long.valueOf(getFullProfile().getWalletId());
    }

    public boolean checkSumOfAccountsGreaterThanZero() {
        double sumAmount = 0;
        for (Account account : getAccounts()) {
            if (account.getAmount() >= 0) {
                sumAmount += account.getAmount();
            }
        }
        return sumAmount > 0;
    }

    public List<Operation> getShortOperations() {
        return updateOperations(shortOperations);
    }

    public List<Operation> getFilteredOperations() {
        return updateOperations(filteredOperations);
    }

    private List<Operation> updateOperations(List<Operation> operations) {
        List<Operation> operationList = new ArrayList<>();
        for (Operation operation : operations) {
            for (Account account : getAccounts()) {
                if (operation.getAccountNumber().equals(account.getAccountNumber())) {
                    operation.setAccountName(account.getName());
                    break;
                }
            }
            operationList.add(operation);
        }
        return operationList;
    }

    public boolean isResident() {
        return fullProfile.getResidencyCountryId() == BusinessConstants.COUNTRY_CODE_RUSSIA;
    }
}
