package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class WalletInfoProfileRequest extends BodyRequest {

    @SerializedName("clientId")
    @Expose @Setter
    private Long clientId;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

}
