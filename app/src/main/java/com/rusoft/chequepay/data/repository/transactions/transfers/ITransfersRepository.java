package com.rusoft.chequepay.data.repository.transactions.transfers;

import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.templates.TransferTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface ITransfersRepository {

    void getWalletInfo(ValidationPatterns dataType, String data, RequestCallbackListener listener);

    void getCommission(RequestCallbackListener listener);
    void sendTransfer(String otp, RequestCallbackListener listener);

    void sendInvoiceTransfer(String otp, RequestCallbackListener listener);

    void applyTemplate(TransferTemplate template);
    void clearModel();

}
