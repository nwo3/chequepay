package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.BusinessConstants;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class ShortTemplate extends BaseTemplate {

    @SerializedName("templateType")
    @Expose @Getter @Setter
    private BusinessConstants.TemplateType templateType;
}
