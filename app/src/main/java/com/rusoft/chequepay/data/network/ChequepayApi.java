package com.rusoft.chequepay.data.network;

import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.accounts.request.CheckAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.CheckShortAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.CloseAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.OpenAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.response.CheckAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CheckShortAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CloseAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.OpenAccountResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceCreateRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceItemRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceUpdateRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.IvoicesListRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceCreateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceItemResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceUpdateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoicesListResponse;
import com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.request.PurchaseFromIssuerCreateRequest;
import com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.response.PurchaseFromIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.application.selltoissuer_15.request.SellToIssuerCreateRequest;
import com.rusoft.chequepay.data.entities.application.selltoissuer_15.response.SellToIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.check.request.CheckLoginParamsRequest;
import com.rusoft.chequepay.data.entities.check.response.CheckLoginParamsResponse;
import com.rusoft.chequepay.data.entities.cheques.request.GetChequeRequest;
import com.rusoft.chequepay.data.entities.cheques.response.GetChequeResponse;
import com.rusoft.chequepay.data.entities.clientdata.request.BankRequisitesRequest;
import com.rusoft.chequepay.data.entities.clientdata.request.ContractorListRequest;
import com.rusoft.chequepay.data.entities.clientdata.request.GetWalletsRequest;
import com.rusoft.chequepay.data.entities.clientdata.request.ListOfBrokerRequest;
import com.rusoft.chequepay.data.entities.clientdata.response.BankRequisitesResponse;
import com.rusoft.chequepay.data.entities.clientdata.response.ContractorListResponse;
import com.rusoft.chequepay.data.entities.clientdata.response.GetWalletsResponse;
import com.rusoft.chequepay.data.entities.clientdata.response.ListOfBrokerResponse;
import com.rusoft.chequepay.data.entities.commission.request.CommissionTransferRequest;
import com.rusoft.chequepay.data.entities.commission.response.CommissionTransferResponse;
import com.rusoft.chequepay.data.entities.correspondents.request.AddAccountRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.AddCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.DeleteCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.EditCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.InfoCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.ListCorrespondentsRequest;
import com.rusoft.chequepay.data.entities.correspondents.response.AddAccountResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.AddCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.DeleteCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.EditCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.InfoCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.ListCorrespondentsResponse;
import com.rusoft.chequepay.data.entities.document.request.DocumentListRequest;
import com.rusoft.chequepay.data.entities.document.request.DocumentPdfRequest;
import com.rusoft.chequepay.data.entities.document.response.DocumentListResponse;
import com.rusoft.chequepay.data.entities.info.request.BankIssuersListRequest;
import com.rusoft.chequepay.data.entities.info.request.GetBankInfoRequest;
import com.rusoft.chequepay.data.entities.info.response.BankIssuersListResponse;
import com.rusoft.chequepay.data.entities.info.response.CountryListResponse;
import com.rusoft.chequepay.data.entities.info.response.GetBankInfoResponse;
import com.rusoft.chequepay.data.entities.info.response.RegionListResponse;
import com.rusoft.chequepay.data.entities.notifications.request.GetNotificationRequest;
import com.rusoft.chequepay.data.entities.notifications.request.RemoveNotificationRequest;
import com.rusoft.chequepay.data.entities.notifications.response.GetNotificationsResponse;
import com.rusoft.chequepay.data.entities.notifications.response.RemoveNotificationResponse;
import com.rusoft.chequepay.data.entities.operations.request.GetAccountStatementRequest;
import com.rusoft.chequepay.data.entities.operations.request.GetItemOperationRequest;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.OperationListResponse;
import com.rusoft.chequepay.data.entities.recover.request.ChangePasswordRequest;
import com.rusoft.chequepay.data.entities.recover.request.RecoverConfirmRequest;
import com.rusoft.chequepay.data.entities.recover.request.RecoverStartRequest;
import com.rusoft.chequepay.data.entities.recover.response.ChangePasswordResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverConfirmResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverStartResponse;
import com.rusoft.chequepay.data.entities.signin.request.AuthRequest;
import com.rusoft.chequepay.data.entities.signin.request.ConfirmAuthRequest;
import com.rusoft.chequepay.data.entities.signin.request.RefreshTokenRequest;
import com.rusoft.chequepay.data.entities.signin.response.SessionTokensOpenApiResponse;
import com.rusoft.chequepay.data.entities.signin.response.SignInOpenApiResponse;
import com.rusoft.chequepay.data.entities.tariffs.request.GetTariffRequest;
import com.rusoft.chequepay.data.entities.tariffs.response.GetTariffResponse;
import com.rusoft.chequepay.data.entities.templates.BuyTemplate;
import com.rusoft.chequepay.data.entities.templates.InvoiceTemplate;
import com.rusoft.chequepay.data.entities.templates.SaleTemplate;
import com.rusoft.chequepay.data.entities.templates.TransferTemplate;
import com.rusoft.chequepay.data.entities.templates.request.AllTemplatesRequest;
import com.rusoft.chequepay.data.entities.templates.request.GetOrDeleteTemplateRequest;
import com.rusoft.chequepay.data.entities.templates.request.SaveTemplateRequest;
import com.rusoft.chequepay.data.entities.templates.response.AllTemplatesResponse;
import com.rusoft.chequepay.data.entities.templates.response.GetTemplateResponse;
import com.rusoft.chequepay.data.entities.templates.response.SaveOrDeleteTemplateResponse;
import com.rusoft.chequepay.data.entities.transfers.request.TransferSendMoneyRequest;
import com.rusoft.chequepay.data.entities.transfers.response.TransferSendMoneyResponse;
import com.rusoft.chequepay.data.entities.wallets.request.PasswordChangeRequest;
import com.rusoft.chequepay.data.entities.wallets.request.RegistrationConfirmRequest;
import com.rusoft.chequepay.data.entities.wallets.request.RegistrationRequest;
import com.rusoft.chequepay.data.entities.wallets.request.SecurityEditRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletEditRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoAccountRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoByAccountRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoProfileRequest;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationConfirmResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationResponse;
import com.rusoft.chequepay.data.entities.wallets.response.ShortProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletEditResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoAccountResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoByAccountResponse;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface ChequepayApi {

    //region Signin
    @POST("open_api/signin/")
    Single<SignInOpenApiResponse> signIn(
            @Body AuthRequest request);

    @POST("open_api/signin/confirm")
    Single<SignInOpenApiResponse> signInConfirm(
            @Body ConfirmAuthRequest request);

    @POST("open_api/signin/refresh")
    Single<SessionTokensOpenApiResponse> signInRefresh(
            @Body RefreshTokenRequest request);
    //endregion

    //region Accounts
    @POST("check_account")
    Single<CheckAccountResponse> checkAccount(
            @Header("Authorization") String token,
            @Body CheckAccountRequest request);

    @POST("check_short_account")
    Single<CheckShortAccountResponse> checkShortAccount(
            @Header("Authorization") String token,
            @Body CheckShortAccountRequest request);

    @POST("open_account")
    Single<OpenAccountResponse> openAccount(
            @Header("Authorization") String token,
            @Body OpenAccountRequest request);

    @POST("close_account")
    Single<CloseAccountResponse> closeAccount(
            @Header("Authorization") String token,
            @Body CloseAccountRequest request);
    //endregion

    //region Operations
    @POST("operation/get_account_statement")
    Single<OperationListResponse> getAccountStatement(
            @Header("Authorization") String token,
            @Body GetAccountStatementRequest request);

    @POST("operation/get_contractor_list")
    Single<ContractorListResponse> getContractorList(
            @Header("Authorization") String token,
            @Body ContractorListRequest request);

    @POST("operation/get_item")
    Single<ItemOperationResponse> getItemOperation(
            @Header("Authorization") String token,
            @Body GetItemOperationRequest request);


    @POST("info/get_wallets")
    Single<GetWalletsResponse> getWallets(
            @Header("Authorization") String token,
            @Body GetWalletsRequest request);

    @POST("issuer/bank_requisites")
    Single<BankRequisitesResponse> bankRequisites(
            @Header("Authorization") String token,
            @Body BankRequisitesRequest request);

    @POST("list_of_broker")
    Single<ListOfBrokerResponse> listOfBroker(
            @Header("Authorization") String token,
            @Body ListOfBrokerRequest request);
    //endregion

    //region Wallets
    @POST("wallets/edit")
    Single<WalletEditResponse> editProfile(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body WalletEditRequest request);

    @POST("wallets/info/account")
    Single<WalletInfoAccountResponse> getWalletInfo(
            @Header("Authorization") String token,
            @Body WalletInfoAccountRequest request);

    @POST("wallets/info/by_account")
    Single<WalletInfoByAccountResponse> getWalletInfoByAccount(
            @Header("Authorization") String token,
            @Body WalletInfoByAccountRequest request);

    @POST("wallets/info/full_profile")
    Single<FullProfileResponse> getFullProfile(
            @Header("Authorization") String token,
            @Body WalletInfoProfileRequest request);

    @POST("wallets/info/short_profile")
    Single<ShortProfileResponse> getShortProfile(
            @Header("Authorization") String token,
            @Body WalletInfoProfileRequest request);

    @POST("wallets/registration")
    Single<RegistrationResponse> registration(
            @Body RegistrationRequest request);

    @POST("wallets/registration/confirm")
    Single<RegistrationConfirmResponse> registrationConfirm(
            @Body RegistrationConfirmRequest request);

    @POST("wallets/security/change_password")
    Single<WalletEditResponse> changePassword(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body PasswordChangeRequest request);

    @POST("wallets/security/edit")
    Single<WalletEditResponse> editSecuritySettings(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body SecurityEditRequest request);
    //endregion

    //region Commission
    @POST("comission/transfer")
    Single<CommissionTransferResponse> getTransferCommission(
            @Header("Authorization") String token,
            @Body CommissionTransferRequest request);
    //endregion

    //region Tariffs
    @POST("tariffs/get_tariffs")
    Single<GetTariffResponse> getTarif(
            @Header("Authorization") String token,
            @Body GetTariffRequest request);
    //endregion

    //region Transfers
    @POST("send_transfer")
    Single<TransferSendMoneyResponse> sendTransfer(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body TransferSendMoneyRequest request);
    //endregion

    //region Info
    @POST("open_api/check/login_params")
    Single<CheckLoginParamsResponse> checkLoginParams(
            @Body CheckLoginParamsRequest request);

    @POST("open_api/info/bank_issuer_id")
    Single<BankIssuersListResponse> getBankIssuersList(
            @Body BankIssuersListRequest request);

    @POST("open_api/info/get_bank_info")
    Single<GetBankInfoResponse> getBankInfo(
            @Body GetBankInfoRequest request);

    @POST("open_api/info/get_country_list")
    Single<CountryListResponse> getCountries(
            @Body BodyRequest request);

    @POST("open_api/info/get_region_list")
    Single<RegionListResponse> getRegions(
            @Body BodyRequest request);
    //endregion

    //region Application
    @POST("application/create/3")
    Single<InvoiceCreateResponse> createInvoice(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body InvoiceCreateRequest request);

    @POST("application/create/7")
    Single<PurchaseFromIssuerCreateResponse> createDeposit(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body PurchaseFromIssuerCreateRequest request);

    @POST("application/create/15")
    Single<SellToIssuerCreateResponse> createWithdraw(
            @Header("Authorization") String token,
            @Header("Confirmation-Type") String confirmationType,
            @Header("Flow-Token") String flowToken,
            @Header("One-Time-Password") String otp,
            @Body SellToIssuerCreateRequest request);

    @POST("application/get_list/3")
    Single<InvoicesListResponse> getInvoicesList(
            @Header("Authorization") String token,
            @Body IvoicesListRequest request);

    @POST("application/get_item/3")
    Single<InvoiceItemResponse> getInvoiceDetail(
            @Header("Authorization") String token,
            @Body InvoiceItemRequest request);

    @POST("application/update/3")
    Single<InvoiceUpdateResponse> updateInvoiceState(
            @Header("Authorization") String token,
            @Body InvoiceUpdateRequest request);

    //endregion

    //region Correspondents
    @POST("correspondents/add")
    Single<AddCorrespondentResponse> addCorrespondent(
            @Header("Authorization") String token,
            @Body AddCorrespondentRequest request);

    @POST("correspondents/edit")
    Single<EditCorrespondentResponse> editCorrespondent(
            @Header("Authorization") String token,
            @Body EditCorrespondentRequest request);

    @POST("correspondents/delete")
    Single<DeleteCorrespondentResponse> deleteCorrespondent(
            @Header("Authorization") String token,
            @Body DeleteCorrespondentRequest request);

    @POST("correspondents/info")
    Single<InfoCorrespondentResponse> getInfoCorrespondent(
            @Header("Authorization") String token,
            @Body InfoCorrespondentRequest request);

    @POST("correspondents/list")
    Single<ListCorrespondentsResponse> getListCorrespondents(
            @Header("Authorization") String token,
            @Body ListCorrespondentsRequest request);

    @POST("correspondents/accounts/add")
    Single<AddAccountResponse> addAccount(
            @Header("Authorization") String token,
            @Body AddAccountRequest request);
    //endregion

    //region Cheques
    @POST("get_cheque")
    Single<GetChequeResponse> getCheque(
            @Header("Authorization") String token,
            @Body GetChequeRequest request);
    //endregion

    //region Notification
    @POST("info/get_notifications")
    Single<GetNotificationsResponse> getNotification(
            @Header("Authorization") String token,
            @Body GetNotificationRequest request);

    @POST("notification/remove")
    Single<RemoveNotificationResponse> removeNotification(
            @Header("Authorization") String token,
            @Body RemoveNotificationRequest request);
    //endregion

    //region Messages
    @GET("message/get")
    Single<Integer> getMessage(
            @QueryMap Map<String, Long> params);

    @GET("message/disconnect")
    Single<String> disconnectMessage(
            @QueryMap Map<String, Long> params);
    //endregion

    //region Templates
    @POST("templates/all")
    Single<AllTemplatesResponse> getAllTemplates(
            @Header("Authorization") String token,
            @Body AllTemplatesRequest request);

    @POST("templates/buy/delete")
    Single<SaveOrDeleteTemplateResponse> deleteBuyTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/buy/get")
    Single<GetTemplateResponse<BuyTemplate>> getBuyTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/buy/save")
    Single<SaveOrDeleteTemplateResponse> saveBuyTemplate(
            @Header("Authorization") String token,
            @Body SaveTemplateRequest<BuyTemplate> request);

    @POST("templates/invoice/delete")
    Single<SaveOrDeleteTemplateResponse> deleteInvoiceTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/invoice/get")
    Single<GetTemplateResponse<InvoiceTemplate>> getInvoiceTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/invoice/save")
    Single<SaveOrDeleteTemplateResponse> saveInvoiceTemplate(
            @Header("Authorization") String token,
            @Body SaveTemplateRequest<InvoiceTemplate> request);

    @POST("templates/sale/delete")
    Single<SaveOrDeleteTemplateResponse> deleteSaleTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/sale/get")
    Single<GetTemplateResponse<SaleTemplate>> getSaleTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/sale/save")
    Single<SaveOrDeleteTemplateResponse> saveSaleTemplate(
            @Header("Authorization") String token,
            @Body SaveTemplateRequest<SaleTemplate> request);

    @POST("templates/transfer/delete")
    Single<SaveOrDeleteTemplateResponse> deleteTransferTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/transfer/get")
    Single<GetTemplateResponse<TransferTemplate>> getTransferTemplate(
            @Header("Authorization") String token,
            @Body GetOrDeleteTemplateRequest request);

    @POST("templates/transfer/save")
    Single<SaveOrDeleteTemplateResponse> saveTransferTemplate(
            @Header("Authorization") String token,
            @Body SaveTemplateRequest<TransferTemplate> request);
    //endregion

    //region Documents
    @POST("document/download")
    Single<ResponseBody> getPdf(
            @Header("Authorization") String token,
            @Body DocumentPdfRequest request);

    @POST("document/list")
    Single<DocumentListResponse> getDocumentList(
            @Header("Authorization") String token,
            @Body DocumentListRequest request);
    //endregion

    //region Recover
    @POST("open_api/recover/change/password")
    Single<ChangePasswordResponse> changePassword(
            @Header("Authorization") String token,
            @Body ChangePasswordRequest request);

    @POST("open_api/recover/confirm")
    Single<RecoverConfirmResponse> recoveryConfirm(
            @Body RecoverConfirmRequest request);

    @POST("open_api/recover/start")
    Single<RecoverStartResponse> recoveryStart(
            @Body RecoverStartRequest request);
    //endregion
}
