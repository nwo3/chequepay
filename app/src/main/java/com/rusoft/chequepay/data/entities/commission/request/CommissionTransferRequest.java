package com.rusoft.chequepay.data.entities.commission.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Setter;

@Builder
@AllArgsConstructor
public class CommissionTransferRequest extends BodyRequest{

    @SerializedName("assetAccountNumber")
    @Expose @Setter
    private String assetAccountNumber;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("locale")
    @Expose @Setter
    private String locale;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("providerCode")
    @Expose @Setter
    private String providerCode;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("targetAccountNumber")
    @Expose @Setter
    private String targetAccountNumber;

    @SerializedName("tradeOperation")
    @Expose @Setter
    private Boolean tradeOperation;
}
