package com.rusoft.chequepay.data.entities.accounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class AccountShort {

    @SerializedName("accountName")
    @Expose @Getter @Setter
    private String accountName;

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private Integer currency;

    @SerializedName("deleted")
    @Expose @Getter @Setter
    private boolean deleted;
}
