package com.rusoft.chequepay.data.entities.templates.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.templates.BaseTemplate;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public class SaveTemplateRequest <T extends BaseTemplate> extends BodyRequest {

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("template")
    @Expose @Setter
    private T template;

    public SaveTemplateRequest(Long currentTime, Long senderId, T template) {
        this.currentTime = currentTime;
        this.senderId = senderId;
        this.template = template;
    }
}
