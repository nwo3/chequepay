package com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Statistics {
    @SerializedName("eur")
    @Expose @Getter @Setter
    private Eur eur;

    @SerializedName("rub")
    @Expose @Getter @Setter
    private Rub rub;

    @SerializedName("usd")
    @Expose @Getter @Setter
    private Usd usd;
}
