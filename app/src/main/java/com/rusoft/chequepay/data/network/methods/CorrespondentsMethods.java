package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.data.entities.correspondents.request.AddAccountRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.AddCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.DeleteCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.EditCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.InfoCorrespondentRequest;
import com.rusoft.chequepay.data.entities.correspondents.request.ListCorrespondentsRequest;
import com.rusoft.chequepay.data.entities.correspondents.response.AddAccountResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.AddCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.DeleteCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.EditCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.InfoCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.ListCorrespondentsResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Correspondents : Работа с корреспондентами
 */
public class CorrespondentsMethods {
    private ChequepayApi chequepayApi;

    @Inject
    public CorrespondentsMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<AddCorrespondentResponse> addCorrespondents(String token, long senderId, long correspondentId, String name, String comment, List<Account> accounts) {
        return chequepayApi.addCorrespondent(token, AddCorrespondentRequest.builder()
                .senderId(senderId)
                .comment(comment)
                .correspondentId(correspondentId)
                .accounts(accounts)
                .currentTime(System.currentTimeMillis())
                .name(name)
                .build());
    }

    public Single<EditCorrespondentResponse> editCorrespondents(String token, long senderId, long correspondentId, String name, String comment, List<Account> accounts) {
        return chequepayApi.editCorrespondent(token, EditCorrespondentRequest.builder()
                .senderId(senderId)
                .comment(comment)
                .correspondentId(correspondentId)
                .accounts(accounts)
                .currentTime(System.currentTimeMillis())
                .name(name)
                .build());
    }

    public Single<DeleteCorrespondentResponse> deleteCorrespondent(String token, long senderId, long correspondentId) {
        return chequepayApi.deleteCorrespondent(token, DeleteCorrespondentRequest.builder()
                .senderId(senderId)
                .correspondentId(correspondentId)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<InfoCorrespondentResponse> getInfoCorrespondent(String token, long senderId, long correspondentId) {
        return chequepayApi.getInfoCorrespondent(token, InfoCorrespondentRequest.builder()
                .senderId(senderId)
                .correspondentId(correspondentId)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<ListCorrespondentsResponse> getListCorrespondents(String token, long senderId) {
        return chequepayApi.getListCorrespondents(token, ListCorrespondentsRequest.builder()
                .senderId(senderId)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<AddAccountResponse> addAccount(String token, long senderId, long correspondentId) {
        return chequepayApi.addAccount(token, AddAccountRequest.builder()
                .senderId(senderId)
                .correspondentId(correspondentId)
                .currentTime(System.currentTimeMillis())
                .build());
    }
}
