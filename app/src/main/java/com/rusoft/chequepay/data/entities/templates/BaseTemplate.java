package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class BaseTemplate implements Serializable {

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("rate")
    @Expose @Getter @Setter
    private Long rate;

    @SerializedName("templateId")
    @Expose @Getter @Setter
    private Long templateId;

    @SerializedName("whenDate")
    @Expose @Getter @Setter
    private Long whenDate;

}
