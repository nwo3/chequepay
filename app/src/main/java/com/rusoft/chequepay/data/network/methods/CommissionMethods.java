package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.commission.request.CommissionTransferRequest;
import com.rusoft.chequepay.data.entities.commission.response.CommissionTransferResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Commission : Комиссия
 */
public class CommissionMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public CommissionMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<CommissionTransferResponse> getTransferCommission(String token, long senderId, TransferModel model) {
        return chequepayApi.getTransferCommission(token, CommissionTransferRequest.builder()
                .senderId(senderId)
                .assetAccountNumber(model.getSenderAccount().getAccountNumber())
                .targetAccountNumber(model.getReceiverAccountId())
                .providerCode(BusinessConstants.PROVIDER_CODE_TRANSFER)
                .currentTime(System.currentTimeMillis())
                .tradeOperation(false)
                .locale("") //todo без "" - возвращается ошибка 2025, на бэке завели баг
                .build());
    }
}
