package com.rusoft.chequepay.data.entities.templates.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class AllTemplatesResponse extends BaseResponse {

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("templates")
    @Expose @Getter @Setter
    private List<ShortTemplate> templates;

}
