package com.rusoft.chequepay.data.entities.correspondents.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import lombok.Getter;
import lombok.Setter;

public class Correspondent implements Serializable {
    @SerializedName("registrationType")
    @Expose @Getter @Setter
    private Integer registrationType;

    @SerializedName("accounts")
    @Expose @Getter @Setter
    private List<Account> accounts;

    @SerializedName("lastOperation")
    @Expose @Getter @Setter
    private LastOperation lastOperation;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("walletId")
    @Expose @Getter @Setter
    private long walletId;

    @Getter @Setter
    private boolean fromContact = false;

    @Override
    public String toString() {
        return name;
    }

    public List<Account> getAccountsByCurrency(String strCode){
        if (strCode == null) return null;

        List<Account> result = new ArrayList<>();

        for (Account account : accounts) {
            String currency = CurrencyUtils.getCurrencyFromAccountNumber(account.getNumber()).getStrCode();
            if (currency.equals(strCode)){
                result.add(account);
            }
        }

        return result;
    }

    @Nullable
    public Account getAccountByNumber(String accNumber) {

        for (Account account : accounts) {
            if (account.getNumber().equals(accNumber)) {
                return account;
            }
        }

        return null;
    }

    @Nullable
    public String getAccountNameByNumber(String accNumber){

        for (Account account : accounts) {
            if (account.getNumber().equals(accNumber)) {
                return account.getName();
            }
        }

        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Correspondent that = (Correspondent) o;
        return walletId == that.walletId;
    }
}
