package com.rusoft.chequepay.data.repository.notifications;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface INotificationRepository {
    void getCountNotification(RequestCallbackListener listener);
    void removeNotification(RequestCallbackListener listener);
}
