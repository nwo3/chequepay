package com.rusoft.chequepay.data.repository.transactions.withdrawal;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.application.NotResidentBankDetails;
import com.rusoft.chequepay.data.entities.application.ResidentBankDetails;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class WithdrawalModel extends BaseTransactionModel {
    @Getter @Setter Account account;
    @Getter @Setter String issuerId;
    @Getter @Setter String issuerShortName;
    @Getter @Setter BusinessConstants.RefundMode mode;
    @Getter @Setter String currency;

    @Setter Boolean isResident;
    @Getter BankAccount bankAccount;
    @Getter ResidentBankDetails residentBankDetails;
    @Getter NotResidentBankDetails notResidentBankDetails;

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;

        if (bankAccount == null) {
            residentBankDetails = null;
            notResidentBankDetails = null;
        } else {
            if (isResident != null) {
                if (isResident) { //RefundMode.CASH not implemented yet
                    residentBankDetails = getMode() != BusinessConstants.RefundMode.TRANSFER ? null : ResidentBankDetails.builder()
                            .bankAccount(bankAccount.getNumber())
                            .bicRus(bankAccount.getCurrentBic())
                            .inn(bankAccount.getBankInn())
                            .kpp(bankAccount.getBankKpp())
                            .build();
                    notResidentBankDetails = null;

                } else {
                    notResidentBankDetails = getMode() != BusinessConstants.RefundMode.TRANSFER ? null : NotResidentBankDetails.builder()
                            .bankName(bankAccount.getBankForeign().getName())
                            .bicInternational(bankAccount.getBankForeign().getBic())
                            .iban(bankAccount.getNumber())
                            .build();
                    residentBankDetails = null;
                }
            }
        }
    }

    @Getter @Setter Double amount;
    @Getter @Setter Double tariff;
    @Getter Double commissionAmount;
    @Getter Double price;

    //third-party account data

    @Getter @Setter String applicationCoreId;
    @Getter @Setter boolean isTransactionDone;

    public boolean updateNumericData() {
        boolean isSuccess;

        if (amount != null && this.tariff != null) {
            commissionAmount = amount * tariff / 100;
            price = amount + commissionAmount;
            isSuccess = true;
        } else {
            commissionAmount = null;
            price = null;
            isSuccess = false;
        }

        return isSuccess;
    }

    public boolean isAllDataReceived(){
        return account != null && mode != null &&
                amount != null && tariff != null &&
                currency != null && price != null;
    }
}
