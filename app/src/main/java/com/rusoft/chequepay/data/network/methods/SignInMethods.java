package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.signin.request.AuthRequest;
import com.rusoft.chequepay.data.entities.signin.request.ConfirmAuthRequest;
import com.rusoft.chequepay.data.entities.signin.request.RefreshTokenRequest;
import com.rusoft.chequepay.data.entities.signin.response.SessionTokensOpenApiResponse;
import com.rusoft.chequepay.data.entities.signin.response.SignInOpenApiResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * SignIn : Аутентификация пользователя
 */
public class SignInMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public SignInMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<SignInOpenApiResponse> signIn(String login, String password) {
        return chequepayApi.signIn(new AuthRequest(login, password));
    }

    public Single<SignInOpenApiResponse> signInConfirm(String flowToken, String login, String opt, String password, long senderId) {
        return chequepayApi.signInConfirm(new ConfirmAuthRequest(flowToken, login, opt, password, senderId));
    }

    public Single<SessionTokensOpenApiResponse> signInRefresh(String refreshToken, long senderId) {
        return chequepayApi.signInRefresh(new RefreshTokenRequest(refreshToken, senderId));
    }
}
