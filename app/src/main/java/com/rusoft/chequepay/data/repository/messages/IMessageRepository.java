package com.rusoft.chequepay.data.repository.messages;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IMessageRepository {
    void initLongPolling();
    void removeLongPolling();

    void disconnectMessage(RequestCallbackListener listener);
}
