package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.utils.CommonUtils;

import lombok.Getter;
import lombok.Setter;

public class Contractor {
    @SerializedName("contractorId")
    @Expose @Getter @Setter
    private long contractorId;

    @SerializedName("contractorName")
    @Expose @Getter @Setter
    private String contractorName;

    @SerializedName("receiver")
    @Expose @Getter @Setter
    private Boolean receiver;

    @SerializedName("sender")
    @Expose @Getter @Setter
    private Boolean sender;

    @Override //Needed for PickerDialog
    public String toString() {
        return CommonUtils.splitContragent(contractorName);
    }
}
