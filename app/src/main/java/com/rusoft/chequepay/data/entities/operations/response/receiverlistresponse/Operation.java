package com.rusoft.chequepay.data.entities.operations.response.receiverlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Operation {
    @SerializedName("amount")
    @Expose @Getter @Setter
    private String amount;

    @SerializedName("chequeChargeOff")
    @Expose @Getter @Setter
    private Boolean chequeChargeOff;

    @SerializedName("chequeChargeOffStatus")
    @Expose @Getter @Setter
    private String chequeChargeOffStatus;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("contractorAccount")
    @Expose @Getter @Setter
    private String contractorAccount;

    @SerializedName("contractorId")
    @Expose @Getter @Setter
    private long contractorId;

    @SerializedName("contractorName")
    @Expose @Getter @Setter
    private String contractorName;

    @SerializedName("currencyCheque")
    @Expose @Getter @Setter
    private long currencyCheque;

    @SerializedName("date")
    @Expose @Getter @Setter
    private long date;

    @SerializedName("newOperation")
    @Expose @Getter @Setter
    private Boolean newOperation;

    @SerializedName("operationType")
    @Expose @Getter @Setter
    private long operationType;

    @SerializedName("operationTypeName")
    @Expose @Getter @Setter
    private String operationTypeName;

    @SerializedName("operationUID")
    @Expose @Getter @Setter
    private String operationUID;

    @SerializedName("paymentType")
    @Expose @Getter @Setter
    private long paymentType;

    @SerializedName("paymentTypeName")
    @Expose @Getter @Setter
    private String paymentTypeName;

    @SerializedName("price")
    @Expose @Getter @Setter
    private String price;

    @SerializedName("receiverAccount")
    @Expose @Getter @Setter
    private String receiverAccount;

    @SerializedName("refundMode")
    @Expose @Getter @Setter
    private String refundMode;

    @SerializedName("senderAccount")
    @Expose @Getter @Setter
    private String senderAccount;

    @SerializedName("senderId")
    @Expose @Getter @Setter
    private long senderId;

    @SerializedName("senderName")
    @Expose @Getter @Setter
    private String senderName;

    @SerializedName("stateId")
    @Expose @Getter @Setter
    private long stateId;

    @SerializedName("stateName")
    @Expose @Getter @Setter
    private String stateName;

    @SerializedName("transferAmount")
    @Expose @Getter @Setter
    private String transferAmount;

    @SerializedName("transferCheque")
    @Expose @Getter @Setter
    private String transferCheque;

    @SerializedName("transferChequeState")
    @Expose @Getter @Setter
    private long transferChequeState;

    @SerializedName("verificationRequestStatus")
    @Expose @Getter @Setter
    private String verificationRequestStatus;

    @SerializedName("verificationRequestStatusId")
    @Expose @Getter @Setter
    private long verificationRequestStatusId;
}
