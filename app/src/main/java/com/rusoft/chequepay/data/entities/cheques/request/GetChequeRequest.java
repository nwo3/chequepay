package com.rusoft.chequepay.data.entities.cheques.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetChequeRequest extends BodyRequest{

    @SerializedName("accountNumber")
    @Expose @Setter
    public String accountNumber;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("language")
    @Expose @Setter
    public String language;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
