package com.rusoft.chequepay.data.entities.application.invoice_3.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class InvoiceItemRequest extends BodyRequest {

    @SerializedName("applicationCoreId")
    @Expose @Setter
    public String applicationCoreId;

    @SerializedName("applicationType")
    @Expose @Setter
    public String applicationType;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("language")
    @Expose @Setter
    public String language;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
