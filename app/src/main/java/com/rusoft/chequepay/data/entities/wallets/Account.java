package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Setter;

@Builder
public class Account {

    @SerializedName("bankId")
    @Expose @Setter
    private String bankId;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("issuerId")
    @Expose @Setter
    private String issuerId;

}
