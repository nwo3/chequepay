package com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class ResidentBankDetails {
    @SerializedName("bankAccount")
    @Expose @Getter @Setter
    private String bankAccount;

    @SerializedName("bankInn")
    @Expose @Getter @Setter
    private String bankInn;

    @SerializedName("bankKpp")
    @Expose @Getter @Setter
    private String bankKpp;

    @SerializedName("bankName")
    @Expose @Getter @Setter
    private String bankName;

    @SerializedName("bicRus")
    @Expose @Getter @Setter
    private String bicRus;

    @SerializedName("inn")
    @Expose @Getter @Setter
    private String inn;

    @SerializedName("korAccount")
    @Expose @Getter @Setter
    private String korAccount;

    @SerializedName("kpp")
    @Expose @Getter @Setter
    private String kpp;

    @SerializedName("receiver")
    @Expose @Getter @Setter
    private String receiver;

    @SerializedName("receiverEn")
    @Expose @Getter @Setter
    private String receiverEn;

    @SerializedName("registrationCode")
    @Expose @Getter @Setter
    private String registrationCode;
}
