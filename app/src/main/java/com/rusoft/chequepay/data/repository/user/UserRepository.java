package com.rusoft.chequepay.data.repository.user;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CheckAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CheckShortAccountResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoicesListResponse;
import com.rusoft.chequepay.data.entities.cheques.response.GetChequeResponse;
import com.rusoft.chequepay.data.entities.clientdata.response.ContractorListResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.ListCorrespondentsResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.OperationListResponse;
import com.rusoft.chequepay.data.entities.signin.response.SignInOpenApiResponse;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.ShortProfileResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.AccountsMethods;
import com.rusoft.chequepay.data.network.methods.ApplicationMethods;
import com.rusoft.chequepay.data.network.methods.ChequesMethods;
import com.rusoft.chequepay.data.network.methods.CorrespondentsMethods;
import com.rusoft.chequepay.data.network.methods.DataMethods;
import com.rusoft.chequepay.data.network.methods.OperationMethods;
import com.rusoft.chequepay.data.network.methods.SignInMethods;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class UserRepository extends BaseRepository implements IUserRepository {

    private CacheManager cache;
    private Preferences preferences;

    private SignInMethods signInMethods;
    private AccountsMethods accountsMethods;
    private WalletsMethods walletsMethods;
    private OperationMethods operationMethods;
    private DataMethods dataMethods;
    private CorrespondentsMethods correspondentsMethods;
    private ChequesMethods chequesMethods;
    private ApplicationMethods applicationMethods;

    @Getter private UserModel user;

    @Inject
    public UserRepository(CacheManager cache, Preferences preferences, SignInMethods signInMethods,
                          AccountsMethods accountsMethods, WalletsMethods walletsMethods,
                          OperationMethods operationMethods, DataMethods dataMethods,
                          ChequesMethods chequesMethods, CorrespondentsMethods correspondentsMethods,
                          ApplicationMethods applicationMethods) {

        this.cache = cache;
        this.preferences = preferences;

        this.signInMethods = signInMethods;
        this.accountsMethods = accountsMethods;
        this.walletsMethods = walletsMethods;
        this.operationMethods = operationMethods;
        this.correspondentsMethods = correspondentsMethods;
        this.dataMethods = dataMethods;
        this.chequesMethods = chequesMethods;
        this.applicationMethods = applicationMethods;

        this.user = cache.getUser();
    }

    @Override
    public void auth(String login, String password, RequestCallbackListener listener) {
        user.setAuthData(login, password);
        Disposable disposable = signInMethods.signIn(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void authConfirm(String login, String password, String otp, RequestCallbackListener listener) {
        Disposable disposable = signInMethods.signInConfirm(cache.getUser().getFlowToken(), login, otp, password, preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateAccounts(boolean forced, RequestCallbackListener listener) {
        if (!forced && flagsManager.isFlagOff(Flags.ACCOUNTS)) {
            listener.onSuccess();
            listener.onPostExecute();
            return;
        }

        Disposable disposable = accountsMethods.checkAccount(preferences.getAuthToken(), user.getOperatorLogin(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void updateAccounts(RequestCallbackListener listener) {
        updateAccounts(false, listener);
    }

    @Override
    public void updateShortAccounts(RequestCallbackListener listener) {
        Disposable disposable = accountsMethods.checkShortAccount(preferences.getAuthToken(), user.getOperatorLogin(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateFullProfile(RequestCallbackListener listener) {
        if (flagsManager.isFlagOff(Flags.PROFILE)) {
            listener.onSuccess();
            listener.onPostExecute();
            return;
        }

        Disposable disposable = walletsMethods.getFullProfile(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateShortProfile(RequestCallbackListener listener) {
        Disposable disposable = walletsMethods.getShortProfile(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateUserData(RequestCallbackListener listener) {
        Disposable disposable = Single.zip(
                accountsMethods.checkAccount(preferences.getAuthToken(), user.getOperatorLogin(), preferences.getClientId()),
                walletsMethods.getFullProfile(preferences.getAuthToken(), preferences.getClientId()),
                operationMethods.getAccountStatementShort(preferences.getAuthToken(), preferences.getClientId()),
                correspondentsMethods.getListCorrespondents(preferences.getAuthToken(), preferences.getClientId()),
                dataMethods.getContractorList(preferences.getAuthToken(), preferences.getClientId()),
                applicationMethods.getInvoicesList(preferences.getAuthToken(), preferences.getClientId()),

                (accountListResponse, profileResponse, operationListResponse, correspondentsResponse, contractorListResponse, invoicesListResponse) -> {
                    handleResponses(listener,
                            accountListResponse,
                            profileResponse,
                            operationListResponse,
                            correspondentsResponse,
                            contractorListResponse,
                            invoicesListResponse);
                    return true;
                })

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(result -> listener.onSuccess(), listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getChequeData(String accountNumber, RequestCallbackListener listener) {
        if (user.getCheque(accountNumber) != null) {
            listener.onSuccess();
            return;
        }

        Disposable disposable = chequesMethods.getCheque(preferences.getAuthToken(), preferences.getClientId(), accountNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void logout() {
        preferences.clear();
        cache.clearUserData();
    }

    @Override
    public boolean isAuthorized() {
        return preferences.isAuthorized();
    }

    @Override
    public boolean isPinAlreadySet() {
        return !preferences.getPinCode().isEmpty() ||
                (preferences.isAuthenticatedByFingerprint() && preferences.isFingerprintAuthenticationEnabled());
    }

    @Override
    public String getPinCode() {
        return preferences.getPinCode();
    }

    @Override
    public void storePinCode(String pinCode) {
        preferences.storePinCode(pinCode);
    }

    @Override
    public void setAuthenticatedByFingerprint(boolean isAuthenticatedByFingerprint) {
        preferences.setAuthenticatedByFingerprint(isAuthenticatedByFingerprint);
    }

    @Override
    public void setFingerprintAuthenticationEnabled(boolean isFingerprintAuthenticationEnabled) {
        preferences.setFingerprintAuthenticationEnabled(isFingerprintAuthenticationEnabled);
    }

    @Override
    public void setIsFingerprintAuthenticationDialogShown(boolean IsFingerprintAuthenticationDialogShown) {
        preferences.setIsFingerprintAuthenticationDialogShown(IsFingerprintAuthenticationDialogShown);
    }

    @Override
    public boolean IsFingerprintAuthenticationDialogShown() {
        return preferences.IsFingerprintAuthenticationDialogShown();
    }

    @Override
    public boolean isFingerprintAuthenticationEnabled() {
        return preferences.isFingerprintAuthenticationEnabled();
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);
        handleResponse(listener, response, false);
    }

    private void handleResponse(RequestCallbackListener listener, BaseResponse response, boolean doNotInvokeCallbacks) {
        Integer errorCode = response.getError();
        if (errorCode == 0) {

            if (response instanceof SignInOpenApiResponse) {
                SignInOpenApiResponse authResponse = (SignInOpenApiResponse) response;
                user.updateUserData(authResponse);

                if (authResponse.getAccessToken() != null) {
                    user.clearAuthData();
                    preferences.storeToken(authResponse.getAccessToken());
                    preferences.storeClientId(authResponse.getClientId());
                    preferences.storeTimestamp(System.currentTimeMillis());
                }

            } else if (response instanceof CheckAccountResponse) {
                user.setAccounts(((CheckAccountResponse) response).getAccountsData());
                flagsManager.turnOff(Flags.ACCOUNTS);

            } else if (response instanceof CheckShortAccountResponse) {
                user.setShortAccounts(((CheckShortAccountResponse) response).getAccountsData());

            } else if (response instanceof FullProfileResponse) {
                user.setFullProfile((FullProfileResponse) response);
                flagsManager.turnOff(Flags.PROFILE);

            } else if (response instanceof ShortProfileResponse) {
                user.setShortProfile((ShortProfileResponse) response);

            } else if (response instanceof OperationListResponse) {
                user.setShortOperations(((OperationListResponse) response).getOperations());
                flagsManager.turnOff(Flags.EVENT);

            } else if (response instanceof ListCorrespondentsResponse) {
                flagsManager.turnOff(Flags.CONTACTS);
                cache.getUser().setCorrespondents(((ListCorrespondentsResponse) response).getCorrespondents());

            } else if (response instanceof ContractorListResponse) {
                cache.getUser().setContractors(((ContractorListResponse) response).getContractors());

            } else if (response instanceof GetChequeResponse) {
                user.addCheque(((GetChequeResponse) response).getCheque());

            } else if (response instanceof InvoicesListResponse) {
                user.setInvoices(((InvoicesListResponse) response).applications);
                flagsManager.turnOff(Flags.INVOICES);
            }

            if (!doNotInvokeCallbacks) listener.onSuccess();

        } else {

            if (response instanceof CheckAccountResponse && errorCode == ErrorUtils.ChequePayError.ERROR_2308.getCode()) {
                user.setAccounts(new ArrayList<>());
                return; //Не показываем ошибку
            }

            if (!doNotInvokeCallbacks) listener.onError(new ResponseError(errorCode));
        }
    }

    private void handleResponses(RequestCallbackListener listener, BaseResponse... responses) {
        for (BaseResponse response : responses) {
            handleResponse(listener, response, true);
        }

        Integer error = ErrorUtils.getError(responses);
        if (error == 0) {
            listener.onSuccess();
        } else {
            listener.onError(new ResponseError(error));
        }
    }

}