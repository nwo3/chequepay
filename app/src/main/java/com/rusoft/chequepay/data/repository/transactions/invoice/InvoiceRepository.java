package com.rusoft.chequepay.data.repository.transactions.invoice;


import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceCreateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceItemResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceUpdateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoicesListResponse;
import com.rusoft.chequepay.data.entities.templates.InvoiceTemplate;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoAccountResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoByAccountResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.ApplicationMethods;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class InvoiceRepository extends BaseRepository implements IInvoiceRepository {

    private CacheManager cache;
    private Preferences preferences;
    private WalletsMethods walletsMethods;
    private ApplicationMethods applicationMethods;
    private UserRepository userRepository;

    @Getter private InvoiceModel model;

    @Inject
    public InvoiceRepository(CacheManager cache, Preferences preferences, WalletsMethods walletsMethods, ApplicationMethods applicationMethods, UserRepository userRepository) {
        this.cache = cache;
        this.preferences = preferences;
        this.walletsMethods = walletsMethods;
        this.applicationMethods = applicationMethods;
        this.userRepository = userRepository;

        this.compositeDisposable = new CompositeDisposable();
        this.model = cache.getInvoiceModel();
    }

    @Override
    public void getWalletInfo(ValidationPatterns dataType, String data, RequestCallbackListener listener) {
        Disposable disposable = walletsMethods.getWalletInfo(preferences.getAuthToken(), preferences.getClientId(), model.getCurrency(), dataType, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getWalletInfoByAccount(String accountId, RequestCallbackListener listener) {
        Disposable disposable = walletsMethods.getWalletInfoByAccount(preferences.getAuthToken(), preferences.getClientId(), accountId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void createInvoice(String otp, RequestCallbackListener listener) {
        Disposable disposable = applicationMethods.createInvoice(preferences.getAuthToken(), preferences.getClientId(), otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void updateInvoicesList(boolean forced, RequestCallbackListener listener) {
        if (!forced && FlagsManager.getInstance().isFlagOff(Flags.INVOICES)) {
            listener.onSuccess();
            return;
        }

        Disposable disposable = applicationMethods.getInvoicesList(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getInvoiceDetail(String applicationCoreId, String applicationType, RequestCallbackListener listener) {
        Disposable disposable = applicationMethods.getInvoiceDetail(preferences.getAuthToken(), preferences.getClientId(), applicationCoreId, applicationType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void refuseInvoice(String applicationCoreId, String applicationType, RequestCallbackListener listener) {
        Disposable disposable = applicationMethods.refuseInvoice(preferences.getAuthToken(), preferences.getClientId(), applicationCoreId, applicationType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof WalletInfoAccountResponse) {
                String walletId = ((WalletInfoAccountResponse) response).getWalletId();
                model.setContractorWalletId(walletId);
                listener.onSuccess();

            } else if (response instanceof WalletInfoByAccountResponse) {
                String walletId = ((WalletInfoByAccountResponse) response).getWalletId();
                model.setContractorWalletId(walletId);
                listener.onSuccess();

            } else if (response instanceof InvoiceCreateResponse) {
                InvoiceCreateResponse res = (InvoiceCreateResponse) response;
                model.setApplicationCoreId(res.getApplicationCoreId());
                model.setFlowToken(res.getFlowToken());
                listener.onSuccess();

            } else if (response instanceof InvoicesListResponse) {
                userRepository.getUser().setInvoices(((InvoicesListResponse) response).applications);
                flagsManager.turnOff(Flags.INVOICES);
                listener.onSuccess();

            } else if (response instanceof InvoiceItemResponse) {
                listener.onSuccess(response);

            } else if (response instanceof InvoiceUpdateResponse) {
                FlagsManager.getInstance().turnOn(Flags.INVOICES);
                listener.onSuccess();
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    @Override
    public void applyTemplate(InvoiceTemplate template) {
        UserModel user = userRepository.getUser();
        model.setAccount(user.getAccountByNum(template.getAccountNumber()));
        model.setCurrency(template.getCurrency());
        model.setAmount(template.getAmount());
        model.setContractorWalletId(template.getReceiverId());
        model.setLifetime(template.getLifetime());
        model.setComment(template.getComment());
        model.setTemplateUsed(true);
    }

    @Override
    public void clearModel() {
        cache.clearInvoiceModel();
    }

}
