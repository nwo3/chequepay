package com.rusoft.chequepay.data.repository.registration.model;

import com.rusoft.chequepay.utils.CommonUtils;

import java.util.Calendar;

import lombok.Getter;
import lombok.Setter;

//region Category Individual
public class PersonSubModel {
    private RegistrationModel registrationModel;
    @Getter @Setter private String lastName;
    @Getter @Setter private String firstName;
    @Getter @Setter private String patronymic;
    @Getter private Calendar birthDate;
    @Getter @Setter private Long birthDateTs;
    @Getter @Setter private String inn;
    @Getter @Setter private String mobPhone;
    @Getter @Setter private String email;
    @Getter @Setter private String webSite;

    public PersonSubModel(RegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
        this.birthDateTs = birthDate != null ? birthDate.getTimeInMillis() : null;
    }

    public boolean isRequiredDataReceived(){
        if (!registrationModel.isInProfileEditingMode()) {
            return lastName != null && firstName != null &&
                    birthDate != null && mobPhone != null &&
                    email != null;
        } else {
            return isPhoneChanged(mobPhone) ||
                    isEmailChanged(email) ||
                    isWebsiteChanged(webSite);
        }
    }

    //region Profile edit mode:
    void updateByProfile() {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        com.rusoft.chequepay.data.entities.wallets.Person person = registrationModel.comparedProfile.getPerson().getCopy();
        setLastName(person.getLastName());
        setFirstName(person.getFirstName());
        setPatronymic(person.getPatronymic());
        setBirthDateTs(person.getBirthDate());
        setInn(person.getTaxNumber());
        setMobPhone(CommonUtils.isStringNullOrEmpty(person.getMobilePhone()) ? person.getPhone() : person.getMobilePhone());
        setEmail(person.getEmail());
        setWebSite(person.getWebsite());
    }

    public boolean isPhoneChanged(String newPhone) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldPhone = registrationModel.comparedProfile.getPerson().getPhone();
        if (oldPhone == null) {
            return newPhone != null;
        } else {
            return newPhone != null && !oldPhone.equals(newPhone);
        }
    }

    public boolean isEmailChanged(String newEmail) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldEmail = registrationModel.comparedProfile.getPerson().getEmail();
        if (oldEmail == null) {
            return newEmail != null;
        } else {
            return newEmail != null && !oldEmail.equals(newEmail);
        }
    }

    private boolean isWebsiteChanged(String newWebSite) {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        String oldWebsite = registrationModel.comparedProfile.getPerson().getWebsite();
        if (oldWebsite == null) {
            return newWebSite != null;
        } else {
            return newWebSite != null && !oldWebsite.equals(newWebSite);
        }
    }
    //endregion

    @Override
    public String toString() {
        return "PersonSubModel{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthDate=" + birthDate +
                ", birthDateTs=" + birthDateTs +
                ", inn='" + inn + '\'' +
                ", mobPhone='" + mobPhone + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                '}';
    }
}
