package com.rusoft.chequepay.data.repository.passrecovery;

import com.rusoft.chequepay.BusinessConstants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class PassRecoveryModel {
    @Getter @Setter private BusinessConstants.PassRecoveryType type;
    @Getter @Setter private String source;
    @Getter @Setter private String pass, passConfirm;
    @Getter @Setter private String flowToken;
    @Getter @Setter private String accessToken;
    @Getter @Setter private Long clientId;

    public PassRecoveryModel() {
        //TODO уточнить, необходимо ли восстановление по электронной почте
        type = BusinessConstants.PassRecoveryType.PHONE; //default
    }

    public boolean isPassConfirmed() {
        return pass != null && passConfirm != null && pass.equals(passConfirm);
    }

    public boolean isAccessConfirmed() {
        return accessToken != null && clientId != null;
    }
}
