package com.rusoft.chequepay.data.entities.info;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Bank {

    @SerializedName("bankDeleted")
    @Expose @Getter @Setter
    private Boolean bankDeleted;

    @SerializedName("bankEnabled")
    @Expose @Getter @Setter
    private Boolean bankEnabled;

    @SerializedName("bankShortName")
    @Expose @Getter @Setter
    private String bankShortName;

    @SerializedName("bankShortNameEn")
    @Expose @Getter @Setter
    private String bankShortNameEn;

    @SerializedName("bankWalletId")
    @Expose @Getter @Setter
    private String bankWalletId;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Bank other = (Bank) obj;
        return bankWalletId.equals(other.bankWalletId);
    }

    @Override
    public String toString() {
        return bankShortName;
    }
}
