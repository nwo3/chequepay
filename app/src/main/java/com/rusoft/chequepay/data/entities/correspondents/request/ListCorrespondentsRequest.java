package com.rusoft.chequepay.data.entities.correspondents.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;

@Builder
public class ListCorrespondentsRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose
    private long currentTime;

    @SerializedName("senderId")
    @Expose
    private long senderId;
}
