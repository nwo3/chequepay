package com.rusoft.chequepay.data.entities.transfers.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class TransferSendMoneyResponse extends BaseResponse {

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("operationDate")
    @Expose @Getter @Setter
    private Long operationDate;

    @SerializedName("operationUID")
    @Expose @Getter @Setter
    private String operationUID;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

}
