package com.rusoft.chequepay.data.entities.notifications.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class TrayMessage {
    @SerializedName("entityType")
    @Expose @Getter @Setter
    private String entityType;

    @SerializedName("header")
    @Expose @Getter @Setter
    private String header;

    @SerializedName("message")
    @Expose @Getter @Setter
    private String message;
}
