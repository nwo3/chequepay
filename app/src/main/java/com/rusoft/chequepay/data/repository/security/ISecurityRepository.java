package com.rusoft.chequepay.data.repository.security;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface ISecurityRepository {

    void changeSettings(String otp, RequestCallbackListener listener);

    void clearModel();

}
