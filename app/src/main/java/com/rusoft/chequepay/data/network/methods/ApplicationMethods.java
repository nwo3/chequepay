package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceCreateRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceItemRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.InvoiceUpdateRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.request.IvoicesListRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceCreateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceItemResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceUpdateResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoicesListResponse;
import com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.request.PurchaseFromIssuerCreateRequest;
import com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.response.PurchaseFromIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.application.selltoissuer_15.request.SellToIssuerCreateRequest;
import com.rusoft.chequepay.data.entities.application.selltoissuer_15.response.SellToIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.operations.request.SortField;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Application[7] Purchase to issuer : Покупка у эмитента
 * Application[15] Sell to issuer : Продажа эмитенту
 */
public class ApplicationMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public ApplicationMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<PurchaseFromIssuerCreateResponse> createDeposit(String token, long clientId, String otp, DepositModel model) {
        return chequepayApi.createDeposit(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp,
                PurchaseFromIssuerCreateRequest.builder()
                        .senderId(clientId)
                        .accountNumber(model.getAccount().getAccountNumber())
                        .receiverId(model.getIssuer().getIssuerWalletId())
                        .bankId(model.getBank().getBankWalletId())
                        .currency(model.getCurrency())
                        .amount(model.getAmount())
                        .price(model.getTotal())
                        .refundMode(model.getMode().getVal())
                        .applicationType(BusinessConstants.APPLICATION_TYPE_PURCHASE)
                        .paymentType(BusinessConstants.PAYMENT_TYPE)
                        .repeatableApplication(false)
                        .currentTime(System.currentTimeMillis())
                        .build());
    }

    public Single<SellToIssuerCreateResponse> createWithdraw(String token, long clientId, String otp, WithdrawalModel model) {
        return chequepayApi.createWithdraw(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp,
                SellToIssuerCreateRequest.builder()
                        .senderId(clientId)
                        .accountNumber(model.getAccount().getAccountNumber())
                        .currency(model.getCurrency())
                        .amount(model.getAmount())
                        .commission(model.getCommissionAmount())
                        .price(model.getPrice())
                        .receiverId(model.getIssuerId())
                        .refundMode(model.getMode().getVal())
                        .applicationType(BusinessConstants.APPLICATION_TYPE_SELL)
                        .paymentType(BusinessConstants.PAYMENT_TYPE)
                        .currentTime(System.currentTimeMillis())
                        .residentBankDetails(model.getResidentBankDetails())
                        .notResidentBankDetails(model.getNotResidentBankDetails())
                        .build());
    }

    public Single<InvoiceCreateResponse> createInvoice(String token, long clientId, String otp, InvoiceModel model) {
        return  chequepayApi.createInvoice(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp,
                InvoiceCreateRequest.builder()
                        .senderId(clientId)
                        .accountNumber(model.getAccount().getAccountNumber())
                        .receiverId(model.getContractorWalletId())
                        .currency(model.getCurrency())
                        .lifetime(model.getLifetime())
                        .amount(model.getAmount())
                        .comment(model.getComment())
                        .paymentType(BusinessConstants.PAYMENT_TYPE)
                        .applicationType(BusinessConstants.APPLICATION_TYPE_INVOICE)
                        .currentTime(System.currentTimeMillis())
                        .build());
    }

    public Single<InvoicesListResponse> getInvoicesList(String token, long senderId) {
        List<SortField> sortFields = new ArrayList<>();
        SortField sortFieldDate = new SortField("DATE", "DESC");
        sortFields.add(sortFieldDate);

        Calendar calendar = Calendar.getInstance();
        long dateFinish = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH, -1);
        long dateStart = calendar.getTimeInMillis();

        return chequepayApi.getInvoicesList(token, IvoicesListRequest.builder()
                .senderId(senderId)
                .applicationType(BusinessConstants.APPLICATION_TYPE_INVOICE)
                .sortField(sortFields)
                .dateStart(dateStart)
                .dateFinish(dateFinish)
                .pageNumber(BusinessConstants.PAGE_NUMBER_START) // TODO PAGINATION
                .senderTab(false)
                .language(Locale.getDefault().getLanguage())
                .currentTime(System.currentTimeMillis())
        .build());
    }

    public Single<InvoiceItemResponse> getInvoiceDetail(String token, long senderId, String applicationCoreId, String applicationType) {
        return chequepayApi.getInvoiceDetail(token, InvoiceItemRequest.builder()
                .applicationCoreId(applicationCoreId)
                .applicationType(applicationType)
                .currentTime(System.currentTimeMillis())
                .language(Locale.getDefault().getLanguage())
                .senderId(senderId)
        .build());
    }

    public Single<InvoiceUpdateResponse> refuseInvoice(String token, long senderId, String applicationCoreId, String applicationType) {
        return chequepayApi.updateInvoiceState(token, InvoiceUpdateRequest.builder()
                .senderId(senderId)
                .applicationCoreId(applicationCoreId)
                .applicationType(applicationType)
                .state("2") // "2" - Отклонено
                .currentTime(System.currentTimeMillis())
                .build());
    }
}
