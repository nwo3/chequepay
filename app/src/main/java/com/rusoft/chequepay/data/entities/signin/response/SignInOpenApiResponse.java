
package com.rusoft.chequepay.data.entities.signin.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true)
public class SignInOpenApiResponse extends BaseResponse {

    @SerializedName("accessToken")
    @Expose @Getter @Setter
    private String accessToken;

    @SerializedName("accessTokenExpired")
    @Expose @Getter @Setter
    private long accessTokenExpired;

    @SerializedName("clientId")
    @Expose @Getter @Setter
    private long clientId;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("operatorLogin ")
    @Expose @Getter @Setter
    private String operatorLogin ;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("refreshToken")
    @Expose @Getter @Setter
    private String refreshToken;

    @SerializedName("refreshTokenExpired")
    @Expose @Getter @Setter
    private long refreshTokenExpired;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}