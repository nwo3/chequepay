package com.rusoft.chequepay.data.entities.accounts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

public class Account extends BaseResponse implements Serializable{

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("activeAccount")
    @Expose @Getter @Setter
    private boolean activeAccount;

    @SerializedName("amount")
    @Expose @Getter @Setter
    private Double amount;

    @SerializedName("bankId")
    @Expose @Getter @Setter
    private String bankId;

    @SerializedName("bankShortName")
    @Expose @Getter @Setter
    private String bankShortName;

    @SerializedName("bankShortNameEn")
    @Expose @Getter @Setter
    private String bankShortNameEn;

    @SerializedName("brokerShortName")
    @Expose @Getter @Setter
    private String brokerShortName;

    @SerializedName("brokerShortNameEn")
    @Expose @Getter @Setter
    private String brokerShortNameEn;

    @SerializedName("brokerWalletId")
    @Expose @Getter @Setter
    private long brokerWalletId;

    @SerializedName("chequeId")
    @Expose @Getter @Setter
    private String chequeId;

    @SerializedName("closeDate")
    @Expose @Getter @Setter
    private long closeDate;

    @SerializedName("contractDate")
    @Expose @Getter @Setter
    private long contractDate;

    @SerializedName("contractNumber")
    @Expose @Getter @Setter
    private String contractNumber;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private Integer currency;

    @SerializedName("deleted")
    @Expose @Getter @Setter
    private boolean deleted;

    @SerializedName("issuerId")
    @Expose @Getter @Setter
    private String issuerId;

    @SerializedName("issuerShortName")
    @Expose @Getter @Setter
    private String issuerShortName;

    @SerializedName("issuerShortNameEn")
    @Expose @Getter @Setter
    private String issuerShortNameEn;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("regDate")
    @Expose @Getter @Setter
    private Long regDate;

    @SerializedName("specialProperty")
    @Expose @Getter @Setter
    private Integer specialProperty;

    @Override //Needed for PickerDialog
    public String toString() {
        return name + " (" + CurrencyUtils.getSymbol(currency) + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountNumber, account.accountNumber);
    }
}
