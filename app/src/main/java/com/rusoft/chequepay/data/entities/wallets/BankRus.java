package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@AllArgsConstructor
public class BankRus {

    @SerializedName("bic")
    @Expose @Getter @Setter
    private String bic;

    @SerializedName("corAccount")
    @Expose @Getter @Setter
    private String corAccount;

    @SerializedName("deleted")
    @Expose @Getter @Setter
    private Boolean deleted;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    public BankRus getCopy() {
        return new BankRus(
                this.getBic(),
                this.getCorAccount(),
                this.getDeleted(),
                this.getName());
    }
}
