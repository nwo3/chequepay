package com.rusoft.chequepay.data.repository.user;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IUserRepository {
    void auth(String login, String password, RequestCallbackListener listener);
    void authConfirm(String login, String password, String otp, RequestCallbackListener listener);

    void updateAccounts(RequestCallbackListener listener);
    void updateAccounts(boolean forced, RequestCallbackListener listener);

    void updateShortAccounts(RequestCallbackListener listener);
    void updateFullProfile(RequestCallbackListener listener);
    void updateShortProfile(RequestCallbackListener listener);
    void updateUserData(RequestCallbackListener listener);

    void getChequeData(String accountNumber, RequestCallbackListener listener);
    String getPinCode();

    void logout();

    boolean isAuthorized();
    boolean isPinAlreadySet();
    void storePinCode(String pinCode);

    UserModel getUser();

    boolean IsFingerprintAuthenticationDialogShown();
    boolean isFingerprintAuthenticationEnabled();

    void setIsFingerprintAuthenticationDialogShown(boolean IsFingerprintAuthenticationDialogShown);
    void setAuthenticatedByFingerprint(boolean isAuthenticationByFingerprint);
    void setFingerprintAuthenticationEnabled(boolean isFingerprintAuthenticationEnabled);
}
