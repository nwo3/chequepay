package com.rusoft.chequepay.data.entities.info.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public class BankIssuersListRequest extends BodyRequest {

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

}
