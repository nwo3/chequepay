package com.rusoft.chequepay.data.entities.application.invoice_3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class CommentField {

    @SerializedName("available")
    @Expose @Getter @Setter
    public Boolean available;

    @SerializedName("value")
    @Expose @Getter @Setter
    public String value;

}
