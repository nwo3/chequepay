package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.transfers.request.TransferSendMoneyRequest;
import com.rusoft.chequepay.data.entities.transfers.response.TransferSendMoneyResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Transfers : Переводы
 */
public class TransfersMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public TransfersMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<TransferSendMoneyResponse> sendTransfer(String token, long senderId, String otp, TransferModel model) {
        return chequepayApi.sendTransfer(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp, TransferSendMoneyRequest.builder()
                .senderId(senderId)
                .senderAccount(model.getSenderAccount().getAccountNumber())
                .receiverAccount(model.getReceiverAccountId())
                .currency(model.getCurrency())
                .commission(model.getCommissionAmount())
                .chequeValue(model.getTotal())
                .comment(model.getComment())
                .providerCode(BusinessConstants.PROVIDER_CODE_TRANSFER)
                .paymentType(BusinessConstants.PAYMENT_TYPE)
                .payOrderNum(0)
                .currentTime(System.currentTimeMillis())
                .tradeOperation(false)
                .build());
    }

    public Single<TransferSendMoneyResponse> sendInvoiceTransfer(String token, long senderId, String otp, TransferModel model) {
        return chequepayApi.sendTransfer(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp, TransferSendMoneyRequest.builder()
                .senderId(senderId)
                .senderAccount(model.getSenderAccount().getAccountNumber())
                .receiverAccount(model.getReceiverAccountId())
                .currency(model.getCurrency())
                .comment(model.getComment())
                .commission(model.getCommissionAmount())
                .chequeValue(model.getTotal())
                .providerCode(BusinessConstants.PROVIDER_CODE_TRANSFER)
                .paymentType(BusinessConstants.PAYMENT_TYPE)
                .payOrderNum(0)
                .currentTime(System.currentTimeMillis())
                .tradeOperation(false)
                .applicationCoreId(model.getShortInvoiceData().getApplicationCoreId())
                .applicationCoreType(BusinessConstants.OperationType.RECEIVING_INKASSO_CHEQUE.getCode())
                .build());
    }
}
