package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class BrokersDatum {
    @SerializedName("brokerId")
    @Expose @Setter @Getter
    private long brokerId;

    @SerializedName("brokerShortName")
    @Expose @Setter @Getter
    private String brokerShortName;

    @SerializedName("brokerShortNameEn")
    @Expose @Setter @Getter
    private String brokerShortNameEn;
}
