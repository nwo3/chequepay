package com.rusoft.chequepay.data.entities.correspondents.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class AddAccountRequest extends BodyRequest {
    @SerializedName("accounts")
    @Expose @Setter
    private Account account;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("correspondentId")
    @Expose @Setter
    private long correspondentId;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;
}
