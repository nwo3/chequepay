package com.rusoft.chequepay.data.repository.account;


import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CloseAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.OpenAccountResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.AccountsMethods;
import com.rusoft.chequepay.data.network.methods.InfoMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class AccountRepository extends BaseRepository implements IAccountRepository {

    private CacheManager cache;
    private Preferences preferences;
    private UserRepository userRepository;
    private AccountsMethods accountsMethods;
    private InfoMethods infoMethods;

    @Getter private NewAccountModel model;

    @Inject
    public AccountRepository(CacheManager cache, Preferences preferences,UserRepository userRepository, AccountsMethods accountsMethods, InfoMethods infoMethods) {
        this.cache = cache;
        this.preferences = preferences;
        this.userRepository = userRepository;
        this.accountsMethods = accountsMethods;
        this.infoMethods = infoMethods;

        this.model = cache.getNewAccountModel();
    }

    @Override
    public void getBankIssuers(String currency, RequestCallbackListener listener) {
        if (currency == null || currency.isEmpty()) return;

        Disposable disposable = infoMethods.getBankIssuersList(preferences.getClientId(), currency)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> {
                            if (response.getError() == 0) {
                                userRepository.getUser().setBankIssuersMap(currency, response);
                                listener.onSuccess();
                            } else {
                                listener.onError(new ResponseError(response.getError()));
                            }
                        },
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void createAccount(RequestCallbackListener listener) {
        Disposable disposable = accountsMethods.createAccount(preferences.getAuthToken(), preferences.getClientId(), model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void closeAccount(String accountNumber, RequestCallbackListener listener) {
        Disposable disposable = accountsMethods.closeAccount(preferences.getAuthToken(), preferences.getClientId(), accountNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof OpenAccountResponse || response instanceof CloseAccountResponse) {
                flagsManager.turnOn(Flags.ACCOUNTS);
            }

            listener.onSuccess();
        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    @Override
    public void clearModel(){
        cache.clearNewAccountModel();
    }

}
