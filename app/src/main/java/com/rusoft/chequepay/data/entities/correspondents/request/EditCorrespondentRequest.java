package com.rusoft.chequepay.data.entities.correspondents.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class EditCorrespondentRequest extends BodyRequest {
    @SerializedName("accounts")
    @Expose @Setter
    private List<Account> accounts;

    @SerializedName("comment")
    @Expose @Setter
    private String comment;

    @SerializedName("correspondentId")
    @Expose @Setter
    private long correspondentId;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("name")
    @Expose @Setter
    private String name;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
