package com.rusoft.chequepay.data.entities.operations.response.senderlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Usd {
    @SerializedName("totalAmount")
    @Expose @Getter @Setter
    private String totalAmount;

    @SerializedName("totalCommission")
    @Expose @Getter @Setter
    private String totalCommission;

    @SerializedName("totalTransferAmount")
    @Expose @Getter @Setter
    private String totalTransferAmount;
}
