package com.rusoft.chequepay.data.network;

import lombok.Getter;

public class ResponseError extends Throwable {
    @Getter private int errorCode;

    public ResponseError(int errorCode){
        this.errorCode = errorCode;
    }
}
