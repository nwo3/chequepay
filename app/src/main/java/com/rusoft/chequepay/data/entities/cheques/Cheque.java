package com.rusoft.chequepay.data.entities.cheques;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Cheque {

    @SerializedName("bankAccount")
    @Expose @Getter @Setter
    public String bankAccount;

    @SerializedName("bankFullName")
    @Expose @Getter @Setter
    public String bankFullName;

    @SerializedName("bankId")
    @Expose @Getter @Setter
    public Long bankId;

    @SerializedName("bankIsResident")
    @Expose @Getter @Setter
    public Boolean bankIsResident;

    @SerializedName("bankKpp")
    @Expose @Getter @Setter
    public String bankKpp;

    @SerializedName("bankName")
    @Expose @Getter @Setter
    public String bankName;

    @SerializedName("bankOgrn")
    @Expose @Getter @Setter
    public String bankOgrn;

    @SerializedName("bankShortName")
    @Expose @Getter @Setter
    public String bankShortName;

    @SerializedName("bankTaxNumber")
    @Expose @Getter @Setter
    public String bankTaxNumber;

    @SerializedName("chequeId")
    @Expose @Getter @Setter
    public String chequeId;

    @SerializedName("currency")
    @Expose @Getter @Setter
    public Long currency;

    @SerializedName("holderAccount")
    @Expose @Getter @Setter
    public String holderAccount;

    @SerializedName("issueDate")
    @Expose @Getter @Setter
    public Long issueDate;

    @SerializedName("issuerAddress")
    @Expose @Getter @Setter
    public String issuerAddress;

    @SerializedName("issuerDirectorName")
    @Expose @Getter @Setter
    public String issuerDirectorName;

    @SerializedName("issuerDirectorPost")
    @Expose @Getter @Setter
    public String issuerDirectorPost;

    @SerializedName("issuerEmail")
    @Expose @Getter @Setter
    public String issuerEmail;

    @SerializedName("issuerId")
    @Expose @Getter @Setter
    public String issuerId;

    @SerializedName("issuerIsResident")
    @Expose @Getter @Setter
    public Boolean issuerIsResident;

    @SerializedName("issuerKpp")
    @Expose @Getter @Setter
    public String issuerKpp;

    @SerializedName("issuerName")
    @Expose @Getter @Setter
    public String issuerName;

    @SerializedName("issuerOrgn")
    @Expose @Getter @Setter
    public String issuerOrgn;

    @SerializedName("issuerPhone")
    @Expose @Getter @Setter
    public String issuerPhone;

    @SerializedName("issuerShortName")
    @Expose @Getter @Setter
    public String issuerShortName;

    @SerializedName("issuerTaxNumber")
    @Expose @Getter @Setter
    public String issuerTaxNumber;

    @SerializedName("stateId")
    @Expose @Getter @Setter
    public Long stateId;

    @SerializedName("stateName")
    @Expose @Getter @Setter
    public String stateName;

    @SerializedName("value")
    @Expose @Getter @Setter
    public String value;

    public String getIssuerInfoText(){
        return issuerShortName + "\n" + issuerAddress;
    }

    @Override
    public String toString() {
        return "Cheque{" +
                "bankAccount='" + bankAccount + '\'' +
                ", bankFullName='" + bankFullName + '\'' +
                ", bankId=" + bankId +
                ", bankIsResident=" + bankIsResident +
                ", bankKpp='" + bankKpp + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankOgrn='" + bankOgrn + '\'' +
                ", bankShortName='" + bankShortName + '\'' +
                ", bankTaxNumber='" + bankTaxNumber + '\'' +
                ", chequeId='" + chequeId + '\'' +
                ", currency=" + currency +
                ", holderAccount='" + holderAccount + '\'' +
                ", issueDate=" + issueDate +
                ", issuerAddress='" + issuerAddress + '\'' +
                ", issuerDirectorName='" + issuerDirectorName + '\'' +
                ", issuerDirectorPost='" + issuerDirectorPost + '\'' +
                ", issuerEmail='" + issuerEmail + '\'' +
                ", issuerId='" + issuerId + '\'' +
                ", issuerIsResident=" + issuerIsResident +
                ", issuerKpp='" + issuerKpp + '\'' +
                ", issuerName='" + issuerName + '\'' +
                ", issuerOrgn='" + issuerOrgn + '\'' +
                ", issuerPhone='" + issuerPhone + '\'' +
                ", issuerShortName='" + issuerShortName + '\'' +
                ", issuerTaxNumber='" + issuerTaxNumber + '\'' +
                ", stateId=" + stateId +
                ", stateName='" + stateName + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
