package com.rusoft.chequepay.data.entities.signin.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true)
public class RefreshTokenRequest extends BodyRequest {

    @SerializedName("refreshToken")
    @Expose @Setter
    private String refreshToken;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    public RefreshTokenRequest(String refreshToken, long senderId) {
        this.refreshToken = refreshToken;
        this.senderId = senderId;
    }
}
