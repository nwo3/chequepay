package com.rusoft.chequepay.data.repository.transactions.transfers;

import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.commission.response.CommissionTransferResponse;
import com.rusoft.chequepay.data.entities.templates.TransferTemplate;
import com.rusoft.chequepay.data.entities.transfers.response.TransferSendMoneyResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoAccountResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.CommissionMethods;
import com.rusoft.chequepay.data.network.methods.TransfersMethods;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class TransfersRepository extends BaseRepository implements ITransfersRepository {

    private CacheManager cache;
    private Preferences preferences;
    private WalletsMethods walletsMethods;
    private CommissionMethods commissionMethods;
    private TransfersMethods transfersMethods;

    @Getter private TransferModel model;

    @Inject
    public TransfersRepository(CacheManager cache, Preferences preferences, WalletsMethods walletsMethods, CommissionMethods commissionMethods, TransfersMethods transfersMethods) {
        this.cache = cache;
        this.preferences = preferences;

        this.walletsMethods = walletsMethods;
        this.commissionMethods = commissionMethods;
        this.transfersMethods = transfersMethods;

        this.compositeDisposable = new CompositeDisposable();
        this.model = cache.getTransferModel();
    }

    @Override
    public void getWalletInfo(ValidationPatterns dataType, String data, RequestCallbackListener listener) {
        Disposable disposable =  walletsMethods.getWalletInfo(preferences.getAuthToken(), preferences.getClientId(), model.getCurrency(), dataType, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getCommission(RequestCallbackListener listener) {
        if (model.getSenderAccount() == null || model.getReceiverAccountId() == null || model.getReceiverAccountId().isEmpty()){
            return;
        }

        Disposable disposable =  commissionMethods.getTransferCommission(preferences.getAuthToken(), preferences.getClientId(), model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void sendTransfer(String otp, RequestCallbackListener listener) {
        Disposable disposable =  transfersMethods.sendTransfer(preferences.getAuthToken(), preferences.getClientId(), otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void sendInvoiceTransfer(String otp, RequestCallbackListener listener) {
        Disposable disposable =  transfersMethods.sendInvoiceTransfer(preferences.getAuthToken(), preferences.getClientId(), otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void applyTemplate(TransferTemplate template) {
        UserModel user = cache.getUser();
        model.setSenderAccount(user.getAccountByNum(template.getSenderAccount()));
        model.setReceiverAccountId(template.getReceiverAccount());
        model.setAmount(template.getChequeValue());
        model.setComment(template.getComment());
        model.setTemplateUsed(true);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof WalletInfoAccountResponse) {
                String accountNumber = ((WalletInfoAccountResponse) response).getAccountNumber();
                model.setReceiverAccountId(accountNumber);

            } else if (response instanceof CommissionTransferResponse) {
                Double commission = ((CommissionTransferResponse) response).getComission();
                model.setCommission(commission);

            } else if (response instanceof TransferSendMoneyResponse) {
                TransferSendMoneyResponse transferResponse = (TransferSendMoneyResponse) response;
                model.setOperationUID(transferResponse.getOperationUID());
                model.setFlowToken(transferResponse.getFlowToken());
                flagsManager.turnOn(Flags.ACCOUNTS);
            }

            listener.onSuccess();
        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    @Override
    public void clearModel(){
        cache.clearTransferModel();
    }

}
