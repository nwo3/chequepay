package com.rusoft.chequepay.data.entities.correspondents.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class LastOperation implements Serializable {
    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("commentEn")
    @Expose @Getter @Setter
    private String commentEn;

    @SerializedName("contractorAccount")
    @Expose @Getter @Setter
    private String contractorAccount;

    @SerializedName("contractorNameEn")
    @Expose @Getter @Setter
    private String contractorNameEn;

    @SerializedName("contractorNameRu")
    @Expose @Getter @Setter
    private String contractorNameRu;

    @SerializedName("dateTime")
    @Expose @Getter @Setter
    private long dateTime;

    @SerializedName("depositAmount")
    @Expose @Getter @Setter
    private String depositAmount;

    @SerializedName("operationType")
    @Expose @Getter @Setter
    private String operationType;

    @SerializedName("operationUID")
    @Expose @Getter @Setter
    private String operationUID;

    @SerializedName("withdrawAmount")
    @Expose @Getter @Setter
    private String withdrawAmount;
}
