package com.rusoft.chequepay.data.event;


import com.rusoft.chequepay.AppConstants;

import lombok.Getter;
import lombok.ToString;

@ToString
public class UserDataServiceEvent {
    @Getter private String message;
    @Getter private Throwable throwable;

    public UserDataServiceEvent(String message) {
        this.message = message;
    }

    public UserDataServiceEvent(Throwable throwable) {
        this.message = AppConstants.EVENT_USER_DATA_RECEIVING_ERROR;
        this.throwable = throwable;
    }
}
