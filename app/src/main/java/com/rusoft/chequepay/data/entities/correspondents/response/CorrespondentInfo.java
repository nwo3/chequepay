package com.rusoft.chequepay.data.entities.correspondents.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CorrespondentInfo {
    @SerializedName("accounts")
    @Expose @Getter @Setter
    private List<Account> accounts = null;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("residencyCountryId")
    @Expose @Getter @Setter
    private long residencyCountryId;

    @SerializedName("taxNumber")
    @Expose @Getter @Setter
    private String taxNumber;

    @SerializedName("urStatus")
    @Expose @Getter @Setter
    private String urStatus;

    @SerializedName("walletId")
    @Expose @Getter @Setter
    private String walletId;
}
