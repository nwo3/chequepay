package com.rusoft.chequepay.data.entities.notifications.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class AdditionalDatum {
    @SerializedName("additionalInfo")
    @Expose @Getter @Setter
    private String additionalInfo;

    @SerializedName("entityId")
    @Expose @Getter @Setter
    private String entityId;

    @SerializedName("entityType")
    @Expose @Getter @Setter
    private String entityType;
}
