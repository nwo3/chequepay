package com.rusoft.chequepay.data.entities.recover.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class RecoverConfirmResponse extends BaseResponse {

    @SerializedName("accessToken")
    @Expose @Getter @Setter
    public String accessToken;

    @SerializedName("accessTokenExpired")
    @Expose @Getter @Setter
    public long accessTokenExpired;

    @SerializedName("clientId")
    @Expose @Getter @Setter
    public long clientId;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("operatorLogin")
    @Expose @Getter @Setter
    public String operatorLogin;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("refreshToken")
    @Expose @Getter @Setter
    public String refreshToken;

    @SerializedName("refreshTokenExpired")
    @Expose @Getter @Setter
    public long refreshTokenExpired;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

}
