package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.application.ResidentBankDetails;
import com.rusoft.chequepay.data.entities.templates.BuyTemplate;
import com.rusoft.chequepay.data.entities.templates.InvoiceTemplate;
import com.rusoft.chequepay.data.entities.templates.SaleTemplate;
import com.rusoft.chequepay.data.entities.templates.TransferTemplate;
import com.rusoft.chequepay.data.entities.templates.request.AllTemplatesRequest;
import com.rusoft.chequepay.data.entities.templates.request.GetOrDeleteTemplateRequest;
import com.rusoft.chequepay.data.entities.templates.request.SaveTemplateRequest;
import com.rusoft.chequepay.data.entities.templates.response.AllTemplatesResponse;
import com.rusoft.chequepay.data.entities.templates.response.GetTemplateResponse;
import com.rusoft.chequepay.data.entities.templates.response.SaveOrDeleteTemplateResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;
import com.rusoft.chequepay.utils.ErrorUtils;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Templates : шаблоны операций
 */
public class TemplatesMethods {

    ChequepayApi chequepayApi;

    @Inject
    public TemplatesMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<AllTemplatesResponse> getAllTemplates(String token, long senderId) {
        return chequepayApi.getAllTemplates(token, AllTemplatesRequest
                .builder()
                .senderId(senderId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<SaveOrDeleteTemplateResponse> deleteBuyTemplate(String token, long senderId, long templateId) {
        return chequepayApi.deleteBuyTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<GetTemplateResponse<BuyTemplate>> getBuyTemplate(String token, long senderId, long templateId) {
        return chequepayApi.getBuyTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<SaveOrDeleteTemplateResponse> deleteInvoiceTemplate(String token, long senderId, long templateId) {
        return chequepayApi.deleteInvoiceTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<GetTemplateResponse<InvoiceTemplate>> getInvoiceTemplate(String token, long senderId, long templateId) {
        return chequepayApi.getInvoiceTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<SaveOrDeleteTemplateResponse> deleteSaleTemplate(String token, long senderId, long templateId) {
        return chequepayApi.deleteSaleTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<GetTemplateResponse<SaleTemplate>> getSaleTemplate(String token, long senderId, long templateId) {
        return chequepayApi.getSaleTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<SaveOrDeleteTemplateResponse> deleteTransferTemplate(String token, long senderId, long templateId) {
        return chequepayApi.deleteTransferTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<GetTemplateResponse<TransferTemplate>> getTransferTemplate(String token, long senderId, long templateId) {
        return chequepayApi.getTransferTemplate(token, GetOrDeleteTemplateRequest
                .builder()
                .senderId(senderId)
                .templateId(templateId)
                .currentTime(System.currentTimeMillis())
                .build()
        );
    }

    public Single<SaveOrDeleteTemplateResponse> saveTemplate(String token, long senderId,
                                                             BusinessConstants.TemplateType type,
                                                             BaseTransactionModel model) {
        switch (type) {
            case BUY:
                if (!(model instanceof DepositModel))
                    throw new IllegalArgumentException(ErrorUtils.WRONG_MODEL_TYPE);

                DepositModel deposit = (DepositModel) model;
                BuyTemplate buyTemplate = BuyTemplate.builder()
                        .accountNumber(deposit.getAccount().getAccountNumber())
                        .receiverId(deposit.getIssuer().getIssuerWalletId())
                        .bankId(deposit.getBank().getBankWalletId())
                        .currency(deposit.getCurrency())
                        .amount(deposit.getAmount())
                        .refundMode(deposit.getMode().getVal())
                        .applicationType(BusinessConstants.APPLICATION_TYPE_PURCHASE)
                        .build();

                buyTemplate.setName(model.getName());

                return chequepayApi.saveBuyTemplate(token,
                        new SaveTemplateRequest<>(System.currentTimeMillis(), senderId,
                                buyTemplate));

            case SALE:
                if (!(model instanceof WithdrawalModel))
                    throw new IllegalArgumentException(ErrorUtils.WRONG_MODEL_TYPE);

                WithdrawalModel withdrawal = (WithdrawalModel) model;
                SaleTemplate saleTemplate = SaleTemplate.builder()
                        .accountNumber(withdrawal.getAccount().getAccountNumber())
                        .currency(withdrawal.getCurrency())
                        .amount(withdrawal.getAmount())
                        .receiverId(withdrawal.getIssuerId())
                        .refundMode(withdrawal.getMode().getVal())
                        .applicationType(BusinessConstants.APPLICATION_TYPE_SELL)
                        .residentBankDetails(
                                withdrawal.getMode() != BusinessConstants.RefundMode.TRANSFER ?
                                        null : ResidentBankDetails.builder()
                                        .bankAccount(withdrawal.getBankAccount().getNumber())
                                        .bicRus(withdrawal.getBankAccount().getBankRus().getBic())
                                        .inn(withdrawal.getBankAccount().getBankInn())
                                        .kpp(withdrawal.getBankAccount().getBankKpp())
                                        .build())
                        .build();

                saleTemplate.setName(model.getName());

                return chequepayApi.saveSaleTemplate(token,
                        new SaveTemplateRequest<>(System.currentTimeMillis(), senderId,
                                saleTemplate));

            case TRANSFER:
                if (!(model instanceof TransferModel))
                    throw new IllegalArgumentException(ErrorUtils.WRONG_MODEL_TYPE);

                TransferModel transfer = (TransferModel) model;
                TransferTemplate transferTemplate = TransferTemplate.builder()
                        .senderAccount(transfer.getSenderAccount().getAccountNumber())
                        .receiverAccount(transfer.getReceiverAccountId())
                        .currency(transfer.getCurrency())
                        .chequeValue(transfer.getTotal())
                        .providerCode(BusinessConstants.PROVIDER_CODE_TRANSFER)
                        .build();

                transferTemplate.setName(model.getName());
                return chequepayApi.saveTransferTemplate(token,
                        new SaveTemplateRequest<>(System.currentTimeMillis(), senderId,
                                transferTemplate));

            case INVOICE:
                if (!(model instanceof InvoiceModel))
                    throw new IllegalArgumentException(ErrorUtils.WRONG_MODEL_TYPE);

                InvoiceModel invoice = (InvoiceModel) model;
                InvoiceTemplate invoiceTemplate = InvoiceTemplate.builder()
                        .accountNumber(invoice.getAccount().getAccountNumber())
                        .receiverId(invoice.getContractorWalletId())
                        .currency(invoice.getCurrency())
                        .lifetime(invoice.getLifetime())
                        .amount(invoice.getAmount())
                        .comment(invoice.getComment())
                        .applicationType(BusinessConstants.APPLICATION_TYPE_INVOICE)
                        .build();

                invoiceTemplate.setName(model.getName());
                return chequepayApi.saveInvoiceTemplate(token,
                        new SaveTemplateRequest<>(System.currentTimeMillis(), senderId,
                                invoiceTemplate));


            default:
                throw new IllegalArgumentException(ErrorUtils.WRONG_TEMPLATE_TYPE);
        }
    }
}
