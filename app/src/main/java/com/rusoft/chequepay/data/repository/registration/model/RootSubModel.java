package com.rusoft.chequepay.data.repository.registration.model;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.utils.CommonUtils;

import lombok.Getter;
import lombok.Setter;

//region ViewModels
public class RootSubModel {
    private RegistrationModel registrationModel;
    @Getter @Setter private BusinessConstants.RegType regType;
    @Getter @Setter private Country residencyCountry;
    @Getter @Setter private BusinessConstants.ClientCategory category;
    @Getter @Setter private String pass;
    @Getter @Setter private String passConfirm;

    public RootSubModel(RegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public boolean isPassMatches() {
        return !CommonUtils.isStringNullOrEmpty(pass) && !CommonUtils.isStringNullOrEmpty(passConfirm)
                && pass.equals(passConfirm);
    }

    public boolean isAllDataReceived(){
        return regType != null && residencyCountry != null &&
                category != null && !CommonUtils.isStringNullOrEmpty(pass) && isPassMatches();
    }

    void updateByProfile() {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        setRegType(BusinessConstants.RegType.get(registrationModel.comparedProfile.getRegType()));
        setResidencyCountry(new Country(registrationModel.comparedProfile.getResidencyCountryId(),
                registrationModel.comparedProfile.getResidencyCountryNameEn(),
                registrationModel.comparedProfile.getResidencyCountryName()));

        //Organizations not implemented!
        setCategory(BusinessConstants.ClientCategory.INDIVIDUAL);
    }

    @Override
    public String toString() {
        return "RootSubModel{" +
                "regType=" + regType +
                ", residencyCountry=" + residencyCountry +
                ", category=" + category +
                ", pass='" + pass + '\'' +
                ", passConfirm='" + passConfirm + '\'' +
                '}';
    }
}
