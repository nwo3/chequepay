package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.wallets.Account;
import com.rusoft.chequepay.data.entities.wallets.Address;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.BankForeign;
import com.rusoft.chequepay.data.entities.wallets.BankRus;
import com.rusoft.chequepay.data.entities.wallets.Passport;
import com.rusoft.chequepay.data.entities.wallets.Person;
import com.rusoft.chequepay.data.entities.wallets.UrAddress;
import com.rusoft.chequepay.data.entities.wallets.request.PasswordChangeRequest;
import com.rusoft.chequepay.data.entities.wallets.request.RegistrationConfirmRequest;
import com.rusoft.chequepay.data.entities.wallets.request.RegistrationRequest;
import com.rusoft.chequepay.data.entities.wallets.request.SecurityEditRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletEditRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoAccountRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoByAccountRequest;
import com.rusoft.chequepay.data.entities.wallets.request.WalletInfoProfileRequest;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationConfirmResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationResponse;
import com.rusoft.chequepay.data.entities.wallets.response.ShortProfileResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletEditResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoAccountResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoByAccountResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.security.SecurityEditModel;
import com.rusoft.chequepay.utils.ErrorUtils;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Wallets : Работа с регистрационными данными участников системы
 */
public class WalletsMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public WalletsMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<WalletInfoAccountResponse> getWalletInfo(String token, long senderId, String currency, ValidationPatterns dataType, String data) {
        WalletInfoAccountRequest.WalletInfoAccountRequestBuilder builder = WalletInfoAccountRequest.builder();

        switch (dataType) {
            case EMAIL:
                builder.email(data);
                break;

            case PHONE_RUS:
            case PHONE_OTHER:
                builder.phone(data);
                break;

            case WALLET_ID:
                builder.wallet(data);
                break;
        }


        return chequepayApi.getWalletInfo(token,
                builder
                        .currentTime(System.currentTimeMillis())
                        .currency(currency)
                        .senderId(senderId)
                        .build());
    }

    public Single<WalletInfoByAccountResponse> getWalletInfoByAccount(String token, long senderId, String accountNumber) {
        return chequepayApi.getWalletInfoByAccount(token, WalletInfoByAccountRequest.builder()
                .senderId(senderId)
                .accountNumber(accountNumber)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<FullProfileResponse> getFullProfile(String token, long clientId) {
        return chequepayApi.getFullProfile(token, WalletInfoProfileRequest.builder()
                .senderId(clientId)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<ShortProfileResponse> getShortProfile(String token, long clientId) {
        return chequepayApi.getShortProfile(token, WalletInfoProfileRequest.builder()
                .senderId(clientId)
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<RegistrationResponse> registration(RegistrationModel model) {
        return chequepayApi.registration(RegistrationRequest.builder()

                .regType(model.root.getRegType().getVal())
                .paymentType(BusinessConstants.PAYMENT_TYPE) // wtf?
                .allowCertReg(true)
                .residencyCountryId(model.root.getResidencyCountry().getCountryId())
                .password(model.root.getPass())

                /*
                для юр. лиц использовать PersonEmployee (не реализовано)
                для физ. лиц использовать Person
                */
                .person(Person.builder()
                        .firstName(model.person.getFirstName())
                        .lastName(model.person.getLastName())
                        .patronymic(model.person.getPatronymic())
                        .birthDate(model.person.getBirthDate().getTimeInMillis())
                        .phone(model.person.getMobPhone())
                        .mobilePhone(model.person.getMobPhone())
                        .email(model.person.getEmail())
                        .website(model.person.getWebSite())
                        .taxNumber(model.person.getInn())
                        .passport(Passport.builder()
                                .number(model.passport.getSeries() + model.passport.getNumber())
                                .issuer(model.passport.getIssuer())
                                .issueDate(model.passport.getIssueDate())
                                .departmentCode(model.passport.getDepartmentCode())
                                .type("1") //todo wtf?
                                .build())
                        .build())

                .urAddress(UrAddress.builder()
                        .countryId(model.regAddress.getCountry().getCountryId())
                        .countryName(model.regAddress.getCountry().getName())
                        .postcode(model.regAddress.getPostCode())
                        .regionId(model.isResident() ? model.regAddress.getRegion().getCode() : null)
                        .regionName(model.isResident() ? model.regAddress.getRegion().getName() : null)
                        .settlement(model.regAddress.getSettlement())
                        .street(model.regAddress.getStreet())
                        .state(model.isResident() ? null : model.regAddress.getRegion().getName())
                        .build())

                .factAddress(Address.builder()
                        .countryId(model.factAddress.getCountry().getCountryId())
                        .countryName(model.factAddress.getCountry().getName())
                        .postcode(model.factAddress.getPostCode())
                        .regionId(model.isResident() ? model.factAddress.getRegion().getCode() : null)
                        .regionName(model.isResident() ? model.factAddress.getRegion().getName() : null)
                        .settlement(model.factAddress.getSettlement())
                        .street(model.factAddress.getStreet())
                        .state(model.isResident() ? null : model.factAddress.getRegion().getName())
                        .build())

                .bankAccount(BankAccount.builder()
                        .bankForeign(model.isResident() ? null : BankForeign.builder()
                                .bic(model.bank.getBik())
                                .swift(model.bank.getSwift())
                                .name(model.bank.getName())
                                .address(model.bank.getAddress())
                                .additionalInfo(model.bank.getAdditionalInfo())
                                .build())

                        .bankRus(!model.isResident() ? null : BankRus.builder()
                                .bic(model.bank.getBik())
                                .name(model.bank.getName())
                                .corAccount(model.bank.getCorrAcc())
                                .build())
                        .bankInn(model.isResident() ? model.bank.getInn() : null)
                        .bankKpp(model.isResident() ? model.bank.getKpp() : null)

                        .number(model.bank.getAccNum())
                        .owner(model.bank.getReceiver())
                        .build())

                .account(Account.builder()
                        .currency(model.account.getCurrency().getStrCode())
                        .bankId(model.account.getBank().getBankWalletId())
                        .issuerId(model.account.getIssuer().getIssuerWalletId())
                        .build())

                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<RegistrationConfirmResponse> registrationConfirm(String flowToken, String otp) {
        return chequepayApi.registrationConfirm(RegistrationConfirmRequest.builder()
                .token(flowToken)
                .otp(otp)
                .build());
    }

    public Single<WalletEditResponse> editProfile(String otp, String token, long clientId, RegistrationModel model) {
        return chequepayApi.editProfile(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp,
                WalletEditRequest.builder()
                        .senderId(clientId)
                        .regType(model.root.getRegType().getVal())
                        .paymentType(BusinessConstants.PAYMENT_TYPE) // wtf?
                        .residencyCountryId(model.root.getResidencyCountry().getCountryId())

                        /*
                        для юр. лиц использовать PersonEmployee (не реализовано)
                        для физ. лиц использовать Person
                        */
                        .person(Person.builder()
                                .firstName(model.person.getFirstName())
                                .lastName(model.person.getLastName())
                                .patronymic(model.person.getPatronymic())
                                .birthDate(model.person.getBirthDateTs())
                                .phone(model.person.getMobPhone())
                                .mobilePhone(model.person.getMobPhone())
                                .email(model.person.getEmail())
                                .website(model.person.getWebSite())
                                .taxNumber(model.person.getInn())
                                .passport(Passport.builder()
                                        .number(model.passport.getFullNumber())
                                        .issuer(model.passport.getIssuer())
                                        .issueDate(model.passport.getIssueDate())
                                        .departmentCode(model.passport.getDepartmentCode())
                                        .type("1") //todo wtf?
                                        .build())
                                .build())

                        .urAddress(UrAddress.builder()
                                .countryId(model.regAddress.getCountry().getCountryId())
                                .countryName(model.regAddress.getCountry().getName())
                                .postcode(model.regAddress.getPostCode())
                                .regionId(model.isResident() ? model.regAddress.getRegion().getCode() : null)
                                .regionName(model.isResident() ? model.regAddress.getRegion().getName() : null)
                                .settlement(model.regAddress.getSettlement())
                                .street(model.regAddress.getStreet())
                                .state(model.isResident() ? null : model.regAddress.getRegion().getName())
                                .build())

                        .factAddress(Address.builder()
                                .countryId(model.factAddress.getCountry().getCountryId())
                                .countryName(model.factAddress.getCountry().getName())
                                .postcode(model.factAddress.getPostCode())
                                .regionId(model.isResident() ? model.factAddress.getRegion().getCode() : null)
                                .regionName(model.isResident() ? model.factAddress.getRegion().getName() : null)
                                .settlement(model.factAddress.getSettlement())
                                .street(model.factAddress.getStreet())
                                .state(model.isResident() ? null : model.factAddress.getRegion().getName())
                                .build())

                        .bankAccount(BankAccount.builder()
                                .bankForeign(model.isResident() ? null : BankForeign.builder()
                                        .bic(model.bank.getBik())
                                        .swift(model.bank.getSwift())
                                        .name(model.bank.getName())
                                        .address(model.bank.getAddress())
                                        .additionalInfo(model.bank.getAdditionalInfo())
                                        .build())

                                .bankRus(!model.isResident() ? null : BankRus.builder()
                                        .bic(model.bank.getBik())
                                        .name(model.bank.getName())
                                        .corAccount(model.bank.getCorrAcc())
                                        .build())
                                .bankInn(model.isResident() ? model.bank.getInn() : null)
                                .bankKpp(model.isResident() ? model.bank.getKpp() : null)

                                .number(model.bank.getAccNum())
                                .owner(model.bank.getReceiver())
                                .build())

                        .currentTime(System.currentTimeMillis())
                        .build());
    }

    public Single<WalletEditResponse> changePassword(String otp, String token, long clientId, SecurityEditModel model) {
        return chequepayApi.changePassword(token, BusinessConstants.HEADER_CONFIRMATION_TYPE, model.getFlowToken(), otp,
                PasswordChangeRequest.builder()
                        .senderId(clientId)
                        .newPassword(model.getNewPass())
                        .oldPassword(model.getOldPass())
                        .currentTime(System.currentTimeMillis())
                        .build());
    }

    public Single<WalletEditResponse> editSecuritySettings(String otp, String token, long clientId, SecurityEditModel model) {
        SecurityEditRequest request = null;
        switch (model.getActiveTab()) {
            case PASS:
                throw new InternalError(ErrorUtils.INTERNAL_ERROR);

            case IP:
                request = SecurityEditRequest.builder()
                        .senderId(clientId)
                        .allowedIpAddresses(model.isIpRestrictionEnabled() ? model.getAllowedIpAddresses() : null)
                        .currentTime(System.currentTimeMillis())
                        .build();
                break;

            case SMS:
                request = SecurityEditRequest.builder()
                        .senderId(clientId)
                        .authByOtp(model.isAuthByOtpEnabled())
                        .smsNotification(model.isSmsNotificationEnabled())
                        .currentTime(System.currentTimeMillis())
                        .build();
                break;
        }

        return chequepayApi.editSecuritySettings(token, BusinessConstants.HEADER_CONFIRMATION_TYPE,
                model.getFlowToken(), otp, request);
    }
}
