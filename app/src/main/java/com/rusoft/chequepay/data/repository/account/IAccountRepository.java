package com.rusoft.chequepay.data.repository.account;


import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IAccountRepository {

    void getBankIssuers(String currency, RequestCallbackListener listener);
    void createAccount(RequestCallbackListener listener);
    void closeAccount(String accountNumber, RequestCallbackListener listener);

    void clearModel();

}
