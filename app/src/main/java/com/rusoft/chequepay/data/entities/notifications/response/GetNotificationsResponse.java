package com.rusoft.chequepay.data.entities.notifications.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class GetNotificationsResponse extends BaseResponse {
    @SerializedName("additionalData")
    @Expose @Getter @Setter
    private List<AdditionalDatum> additionalData;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("highPriorityUpdatedEntities")
    @Expose @Getter @Setter
    private HighPriorityUpdatedEntities highPriorityUpdatedEntities;

    @SerializedName("lowPriorityUpdatedEntities")
    @Expose @Getter @Setter
    private List<String> lowPriorityUpdatedEntities;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("trayMessages")
    @Expose @Getter @Setter
    private List<TrayMessage> trayMessages;
}
