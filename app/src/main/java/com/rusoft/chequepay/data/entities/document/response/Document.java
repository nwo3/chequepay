package com.rusoft.chequepay.data.entities.document.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Document {
    @SerializedName("contractor")
    @Expose @Getter @Setter
    private String contractor;

    @SerializedName("contractorId")
    @Expose @Getter @Setter
    private Long contractorId;

    @SerializedName("docId")
    @Expose @Getter @Setter
    private Long docId;

    @SerializedName("docStatus")
    @Expose @Getter @Setter
    private String docStatus;

    @SerializedName("docTime")
    @Expose @Getter @Setter
    private Long docTime;

    @SerializedName("documentNumber")
    @Expose @Getter @Setter
    private String documentNumber;

    @SerializedName("documentType")
    @Expose @Getter @Setter
    private int documentType;

    @SerializedName("extension")
    @Expose @Getter @Setter
    private String extension;

    @SerializedName("signDocId")
    @Expose @Getter @Setter
    private String signDocId;

    @SerializedName("signDocType")
    @Expose @Getter @Setter
    private String signDocType;
}
