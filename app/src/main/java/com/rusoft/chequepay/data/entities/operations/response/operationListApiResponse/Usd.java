package com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Usd {
    @SerializedName("endOfPeriodBalance")
    @Expose @Getter @Setter
    private String endOfPeriodBalance;

    @SerializedName("startOfPeriodBalance")
    @Expose @Getter @Setter
    private String startOfPeriodBalance;

    @SerializedName("totalDepositAmount")
    @Expose @Getter @Setter
    private String totalDepositAmount;

    @SerializedName("totalTransferAmount")
    @Expose @Getter @Setter
    private String totalTransferAmount;

    @SerializedName("totalWithdrawAmount")
    @Expose @Getter @Setter
    private String totalWithdrawAmount;
}
