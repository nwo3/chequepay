package com.rusoft.chequepay.data.repository;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.event.IpRestrictionEvent;
import com.rusoft.chequepay.data.event.TokenExpiredEvent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseRepository {
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected FlagsManager flagsManager = FlagsManager.getInstance();

    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        if (response.getError() == ErrorUtils.ChequePayError.ERROR_43.getCode()) {
            EventBus.getDefault().post(new TokenExpiredEvent());

        } else if (response.getError() == ErrorUtils.ChequePayError.ERROR_12.getCode()) {
            EventBus.getDefault().post(new IpRestrictionEvent());
        }

    }

    public void clearDisposable() {
        compositeDisposable.clear();
    }
}
