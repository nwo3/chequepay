package com.rusoft.chequepay.data.entities.wallets.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;

public class WalletEditResponse extends BaseResponse {

    @SerializedName("currentTime")
    @Expose @Getter
    public Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter
    public String flowToken;

    @SerializedName("phone")
    @Expose @Getter
    public String phone;

    @SerializedName("signature")
    @Expose @Getter
    public String signature;
}
