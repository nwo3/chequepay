package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.tariffs.request.GetTariffRequest;
import com.rusoft.chequepay.data.entities.tariffs.response.GetTariffResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Tariffs : Информация о тарифах
 */
public class TariffsMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public TariffsMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<GetTariffResponse> getTariff(String token, long clientId, DepositModel model) {
        return chequepayApi.getTarif(token, GetTariffRequest.builder()
                .senderId(clientId)
                .currency(model.getCurrency())
                .providerCode(BusinessConstants.PROVIDER_CODE_PURCHASE_CHEQUE_EMITTER)
                .tariffOwner(model.getIssuer().getIssuerWalletId())
                .receiverId(model.getIssuer().getIssuerWalletId())
                .currentTime(System.currentTimeMillis())
                .tradeOperation(false)
                .build());
    }

    public Single<GetTariffResponse> getTariff(String token, long clientId, WithdrawalModel model) {
        return chequepayApi.getTarif(token, GetTariffRequest.builder()
                .senderId(clientId)
                .currency(model.getCurrency())
                .providerCode(BusinessConstants.PROVIDER_CODE_SALE_CHEQUE_TO_EMITTER)
                .tariffOwner(model.getIssuerId())
                .receiverId(model.getIssuerId())
                .currentTime(System.currentTimeMillis())
                .tradeOperation(false)
                .build());
    }
}
