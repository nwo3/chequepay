package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Wallet {
    @SerializedName("shortName")
    @Expose @Setter @Getter
    private String shortName;

    @SerializedName("walletDeleted")
    @Expose @Setter @Getter
    private Boolean walletDeleted;

    @SerializedName("walletEnabled")
    @Expose @Setter @Getter
    private Boolean walletEnabled;

    @SerializedName("walletId")
    @Expose @Setter @Getter
    private long walletId;
}
