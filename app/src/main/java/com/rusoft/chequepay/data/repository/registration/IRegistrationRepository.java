package com.rusoft.chequepay.data.repository.registration;


import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IRegistrationRepository {
    void getCountriesAndRegions(RequestCallbackListener listener);
    void checkLoginParams(String phone, String email, RequestCallbackListener listener);
    void getBankInfo(String bic, RequestCallbackListener listener);
    void registration(RequestCallbackListener listener);
    void registrationConfirm (String flowToken, String otp, RequestCallbackListener listener);

    void editProfile(String otp, RequestCallbackListener listener);

    void clearDisposable();
    void clearModel();
}
