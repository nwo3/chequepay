package com.rusoft.chequepay.data.entities.recover.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class ChangePasswordRequest extends BodyRequest {

    @SerializedName("currentTime")
    @Expose @Setter
    public long currentTime;

    @SerializedName("newPassword")
    @Expose @Setter
    public String newPassword;

    @SerializedName("newPasswordConfirm")
    @Expose @Setter
    public String newPasswordConfirm;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("recoverFlowToken")
    @Expose @Setter
    public String recoverFlowToken;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
