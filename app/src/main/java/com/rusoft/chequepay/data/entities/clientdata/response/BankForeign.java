package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class BankForeign {
    @SerializedName("additionalInfo")
    @Expose @Setter @Getter
    private String additionalInfo;

    @SerializedName("address")
    @Expose @Setter @Getter
    private String address;

    @SerializedName("bic")
    @Expose @Setter @Getter
    private String bic;

    @SerializedName("name")
    @Expose @Setter @Getter
    private String name;

    @SerializedName("swift")
    @Expose @Setter @Getter
    private String swift;
}
