package com.rusoft.chequepay.data.repository.documents;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IDocumentsRepository {
    void getPdf(Long documentId, RequestCallbackListener listener);
}
