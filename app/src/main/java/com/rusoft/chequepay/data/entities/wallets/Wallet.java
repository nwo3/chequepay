package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Wallet {

    @SerializedName("allowedIpAddresses")
    @Expose @Getter @Setter
    private List<String> allowedIpAddresses = null;

    @SerializedName("authByOtp")
    @Expose @Getter @Setter
    private Boolean authByOtp;

    @SerializedName("directorFullName")
    @Expose @Getter @Setter
    private String directorFullName;

    @SerializedName("directorFullNameEn")
    @Expose @Getter @Setter
    private String directorFullNameEn;

    @SerializedName("directorShortName")
    @Expose @Getter @Setter
    private String directorShortName;

    @SerializedName("directorShortNameEn")
    @Expose @Getter @Setter
    private String directorShortNameEn;

    @SerializedName("email")
    @Expose @Getter @Setter
    private String email;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("regType")
    @Expose @Getter @Setter
    private String regType;

    @SerializedName("residencyCountryId")
    @Expose @Getter @Setter
    private Long residencyCountryId;

    @SerializedName("residencyCountryName")
    @Expose @Getter @Setter
    private String residencyCountryName;

    @SerializedName("residencyCountryNameEn")
    @Expose @Getter @Setter
    private String residencyCountryNameEn;

    @SerializedName("smsNotification")
    @Expose @Getter @Setter
    private Boolean smsNotification;

    @SerializedName("taxNumber")
    @Expose @Getter @Setter
    private String taxNumber;

    @SerializedName("urStatus")
    @Expose @Getter @Setter
    private String urStatus;

    @SerializedName("walletFullName")
    @Expose @Getter @Setter
    private String walletFullName;

    @SerializedName("walletFullNameEn")
    @Expose @Getter @Setter
    private String walletFullNameEn;

    @SerializedName("walletId")
    @Expose @Getter @Setter
    private Long walletId;

    @SerializedName("walletShortName")
    @Expose @Getter @Setter
    private String walletShortName;

    @SerializedName("walletShortNameEn")
    @Expose @Getter @Setter
    private String walletShortNameEn;

}
