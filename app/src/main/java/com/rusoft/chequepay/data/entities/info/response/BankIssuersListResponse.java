package com.rusoft.chequepay.data.entities.info.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.info.BankIssuer;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class BankIssuersListResponse extends BaseResponse {

    @SerializedName("availableBanksAndIssuersData")
    @Expose @Getter @Setter
    private List<BankIssuer> availableBanksAndIssuersData;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

}
