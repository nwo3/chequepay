package com.rusoft.chequepay.data.entities.operations.response.chequeregisterresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Operation {
    @SerializedName("accountCheques")
    @Expose @Getter @Setter
    private List<String> accountCheques = null;

    @SerializedName("chequeId")
    @Expose @Getter @Setter
    private String chequeId;

    @SerializedName("contractorAccountNumber")
    @Expose @Getter @Setter
    private String contractorAccountNumber;

    @SerializedName("contractorName")
    @Expose @Getter @Setter
    private String contractorName;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private long currency;

    @SerializedName("date")
    @Expose @Getter @Setter
    private long date;

    @SerializedName("depositAmount")
    @Expose @Getter @Setter
    private String depositAmount;

    @SerializedName("operationId")
    @Expose @Getter @Setter
    private long operationId;

    @SerializedName("operationTypeId")
    @Expose @Getter @Setter
    private long operationTypeId;

    @SerializedName("operationTypeName")
    @Expose @Getter @Setter
    private String operationTypeName;

    @SerializedName("paymentTypeId")
    @Expose @Getter @Setter
    private long paymentTypeId;

    @SerializedName("paymentTypeName")
    @Expose @Getter @Setter
    private String paymentTypeName;

    @SerializedName("rest")
    @Expose @Getter @Setter
    private String rest;

    @SerializedName("withdrawAmount")
    @Expose @Getter @Setter
    private String withdrawAmount;
}
