package com.rusoft.chequepay.data.entities.check.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class CheckLoginParamsRequest extends BodyRequest {

    @SerializedName("email")
    @Expose @Setter
    private String email;

    @SerializedName("login")
    @Expose @Setter
    private String login;

    @SerializedName("phone")
    @Expose @Setter
    private String phone;

    @SerializedName("senderId")
    @Expose @Setter
    private Integer senderId;


}
