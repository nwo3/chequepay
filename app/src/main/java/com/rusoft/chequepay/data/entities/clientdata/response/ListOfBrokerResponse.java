package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ListOfBrokerResponse extends BaseResponse {
    @SerializedName("brokersData")
    @Expose @Setter @Getter
    private List<BrokersDatum> brokersData = null;

    @SerializedName("currentTime")
    @Expose @Setter @Getter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Setter @Getter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Setter @Getter
    private String phone;

    @SerializedName("signature")
    @Expose @Setter @Getter
    private String signature;
}
