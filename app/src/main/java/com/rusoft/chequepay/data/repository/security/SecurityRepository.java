package com.rusoft.chequepay.data.repository.security;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletEditResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class SecurityRepository extends BaseRepository implements ISecurityRepository{

    private CacheManager cache;
    private Preferences preferences;

    private WalletsMethods walletsMethods;

    @Getter private SecurityEditModel model;

    @Inject
    public SecurityRepository(CacheManager cache, Preferences preferences, WalletsMethods walletsMethods) {
        this.cache = cache;
        this.preferences = preferences;
        this.walletsMethods = walletsMethods;

        model = cache.getSecurityEditModel();
    }

    @Override
    public void changeSettings(String otp, RequestCallbackListener listener) {
        Disposable disposable;
        switch (model.getActiveTab()){
            case PASS:
                disposable = walletsMethods.changePassword(otp, preferences.getAuthToken(), preferences.getClientId(), model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(listener::onPreExecute)
                        .doFinally(listener::onPostExecute)
                        .subscribe(response -> handleResponse(listener, response),
                                listener::onError);
                compositeDisposable.add(disposable);
                break;

            case IP:
            case SMS:
                disposable = walletsMethods.editSecuritySettings(otp, preferences.getAuthToken(), preferences.getClientId(), model)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(listener::onPreExecute)
                        .doFinally(listener::onPostExecute)
                        .subscribe(response -> handleResponse(listener, response),
                                listener::onError);
                compositeDisposable.add(disposable);
                break;
        }
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);
        if (response.getError() == 0) {

            if (response instanceof WalletEditResponse) {
                flagsManager.turnOn(Flags.PROFILE);
                model.setFlowToken(((WalletEditResponse) response).getFlowToken());
            }

            listener.onSuccess();

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }


    @Override
    public void clearModel() {
        cache.clearSecurityEditModel();
    }

}
