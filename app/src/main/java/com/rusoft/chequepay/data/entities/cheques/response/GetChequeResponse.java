package com.rusoft.chequepay.data.entities.cheques.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.cheques.Cheque;

import lombok.Getter;
import lombok.Setter;

public class GetChequeResponse extends BaseResponse {

    @SerializedName("cheque")
    @Expose @Getter @Setter
    public Cheque cheque;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    public Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

}
