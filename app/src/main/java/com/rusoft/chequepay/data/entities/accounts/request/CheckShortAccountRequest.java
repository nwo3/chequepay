package com.rusoft.chequepay.data.entities.accounts.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public class CheckShortAccountRequest extends BodyRequest {

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("includeDeleted")
    @Expose @Setter
    private boolean includeDeleted;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
