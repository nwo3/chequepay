package com.rusoft.chequepay.data.entities.document.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.operations.request.SortField;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class DocumentListRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("dateStart")
    @Expose @Setter
    private Long dateStart;

    @SerializedName("dateFinish")
    @Expose @Setter
    private Long dateFinish;

    @SerializedName("docCategory")
    @Expose @Setter
    private String docCategory;

    @SerializedName("docStatus")
    @Expose @Setter
    private String docStatus;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("pageNumber")
    @Expose @Setter
    private Integer pageNumber;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("sortField")
    @Expose @Setter
    private List<SortField> sortField;
}
