package com.rusoft.chequepay.data.entities.operations.response.senderlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class SenderListResponse extends BaseResponse {
    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("operations")
    @Expose @Getter @Setter
    private List<Operation> operations = null;

    @SerializedName("operationsCount")
    @Expose @Getter @Setter
    private long operationsCount;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("statistics")
    @Expose @Getter @Setter
    private Statistics statistics;
}
