package com.rusoft.chequepay.data.entities.signin.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true)
public class AuthRequest extends BodyRequest {

    @SerializedName("login")
    @Expose @Setter
    private String login;

    @SerializedName("password")
    @Expose @Setter
    private String password;

    public AuthRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
