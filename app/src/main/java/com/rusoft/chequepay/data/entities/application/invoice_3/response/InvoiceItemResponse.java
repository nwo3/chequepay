package com.rusoft.chequepay.data.entities.application.invoice_3.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.CommentField;
import com.rusoft.chequepay.data.entities.application.invoice_3.Document;
import com.rusoft.chequepay.data.entities.application.invoice_3.InvoiceDetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class InvoiceItemResponse extends BaseResponse {

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    public String accountNumber;

    @SerializedName("amount")
    @Expose @Getter @Setter
    public Double amount;

    @SerializedName("comment")
    @Expose @Getter @Setter
    public String comment;

    @SerializedName("comment1")
    @Expose @Getter @Setter
    public String comment1;

    @SerializedName("comment1En")
    @Expose @Getter @Setter
    public String comment1En;

    @SerializedName("commentField")
    @Expose @Getter @Setter
    public CommentField commentField;

    @SerializedName("currency")
    @Expose @Getter @Setter
    public Integer currency;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    public Long currentTime;

    @SerializedName("date")
    @Expose @Getter @Setter
    public Long date;

    @SerializedName("documents")
    @Expose @Getter @Setter
    public List<Document> documents;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("invoiceDetails")
    @Expose @Getter @Setter
    public List<InvoiceDetail> invoiceDetails;

    @SerializedName("lifetime")
    @Expose @Getter @Setter
    public Long lifetime;

    @SerializedName("number")
    @Expose @Getter @Setter
    public String number;

    @SerializedName("params")
    @Expose @Getter @Setter
    public String params;

    @SerializedName("paymentType")
    @Expose @Getter @Setter
    public Integer paymentType;

    @SerializedName("paymentTypeName")
    @Expose @Getter @Setter
    public String paymentTypeName;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("price")
    @Expose @Getter @Setter
    public String price;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    public Long receiverId;

    @SerializedName("receiverName")
    @Expose @Getter @Setter
    public String receiverName;

    @SerializedName("receiverNameEn")
    @Expose @Getter @Setter
    public String receiverNameEn;

    @SerializedName("receiverType")
    @Expose @Getter @Setter
    public Integer receiverType;

    @SerializedName("senderId")
    @Expose @Getter @Setter
    public Long senderId;

    @SerializedName("senderName")
    @Expose @Getter @Setter
    public String senderName;

    @SerializedName("senderNameEn")
    @Expose @Getter @Setter
    public String senderNameEn;

    @SerializedName("senderType")
    @Expose @Getter @Setter
    public Integer senderType;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;


    @SerializedName("stateId")
    @Expose @Getter @Setter
    public Integer stateId;

    @SerializedName("stateName")
    @Expose @Getter @Setter
    public String stateName;

    @SerializedName("statusAvailable")
    @Expose @Getter @Setter
    public List<Integer> statusAvailable;

    @SerializedName("type")
    @Expose @Getter @Setter
    public String type;

}
