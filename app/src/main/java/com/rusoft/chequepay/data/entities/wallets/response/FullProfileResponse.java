package com.rusoft.chequepay.data.entities.wallets.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.wallets.Address;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.Company;
import com.rusoft.chequepay.data.entities.wallets.Person;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class FullProfileResponse extends BaseResponse {

    @SerializedName("allowedIpAddresses")
    @Expose @Getter @Setter
    private List<String> allowedIpAddresses = null;

    @SerializedName("authByOtp")
    @Expose @Getter @Setter
    private Boolean authByOtp;

    @SerializedName("bankAccount")
    @Expose @Getter @Setter
    private BankAccount bankAccount;

    @SerializedName("company")
    @Expose @Getter @Setter
    private Company company;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("factAddress")
    @Expose @Getter @Setter
    private Address factAddress;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("person")
    @Expose @Getter @Setter
    private Person person;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("regType")
    @Expose @Getter @Setter
    private int regType;

    @SerializedName("residencyCountryId")
    @Expose @Getter @Setter
    private Integer residencyCountryId;

    @SerializedName("residencyCountryName")
    @Expose @Getter @Setter
    private String residencyCountryName;

    @SerializedName("residencyCountryNameEn")
    @Expose @Getter @Setter
    private String residencyCountryNameEn;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("smsNotification")
    @Expose @Getter @Setter
    private Boolean smsNotification;

    @SerializedName("urAddress")
    @Expose @Getter @Setter
    private Address urAddress;

    @SerializedName("urStatus")
    @Expose @Getter @Setter
    private String urStatus;

    @SerializedName("walletId")
    @Expose @Getter @Setter
    private String walletId;

    public boolean isResident(){
        return residencyCountryName.equalsIgnoreCase(BusinessConstants.COUNTRY_RUSSIA.getName());
    }
}
