package com.rusoft.chequepay.data.entities.application.selltoissuer_15.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class SellToIssuerCreateResponse extends BaseResponse {

    @SerializedName("applicationCoreId")
    @Expose @Getter @Setter
    public String applicationCoreId;

    @SerializedName("applicationCoreNumber")
    @Expose @Getter @Setter
    public String applicationCoreNumber;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    public Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

}
