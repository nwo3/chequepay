package com.rusoft.chequepay.data.entities.templates.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.templates.BaseTemplate;

import lombok.Getter;
import lombok.Setter;


public class GetTemplateResponse <T extends BaseTemplate> extends BaseResponse {

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("template")
    @Expose @Getter @Setter
    private T template;

}
