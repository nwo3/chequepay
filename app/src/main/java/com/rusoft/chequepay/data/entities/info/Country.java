package com.rusoft.chequepay.data.entities.info;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Country {

    @SerializedName("countryId")
    @Expose @Getter @Setter
    private Integer countryId;

    @SerializedName("internationalName")
    @Expose @Getter @Setter
    private String internationalName;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        if (!countryId.equals(country.countryId)) return false;
        if (!internationalName.equals(country.internationalName)) return false;
        return name.equals(country.name);
    }
}
