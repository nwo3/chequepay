package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ContractorListResponse extends BaseResponse {
    @SerializedName("contractors")
    @Expose @Getter @Setter
    private List<Contractor> contractors = null;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}
