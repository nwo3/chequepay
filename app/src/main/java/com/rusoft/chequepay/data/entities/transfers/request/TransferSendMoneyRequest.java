package com.rusoft.chequepay.data.entities.transfers.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class TransferSendMoneyRequest extends BodyRequest {

    @SerializedName("applicationCoreId")
    @Expose @Setter
    private String applicationCoreId;

    @SerializedName("applicationCoreType")
    @Expose @Setter
    private Integer applicationCoreType;

    @SerializedName("chequeValue")
    @Expose @Setter
    private Double chequeValue;

    @SerializedName("comment")
    @Expose @Setter
    private String comment;

    @SerializedName("comment2")
    @Expose @Setter
    private String comment2;

    @SerializedName("commentEn")
    @Expose @Setter
    private String commentEn;

    @SerializedName("commission")
    @Expose @Setter
    private Double commission;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("payOrderDate")
    @Expose @Setter
    private Long payOrderDate;

    @SerializedName("payOrderNum")
    @Expose @Setter
    private int payOrderNum;

    @SerializedName("paymentType")
    @Expose @Setter
    private int paymentType;

    @SerializedName("providerCode")
    @Expose @Setter
    private String providerCode;

    @SerializedName("receiverAccount")
    @Expose @Setter
    private String receiverAccount;

    @SerializedName("senderAccount")
    @Expose @Setter
    private String senderAccount;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;
    @SerializedName("tradeOperation")

    @Expose @Setter
    private Boolean tradeOperation;

}
