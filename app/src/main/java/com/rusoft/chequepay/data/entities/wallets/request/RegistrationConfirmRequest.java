package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class RegistrationConfirmRequest extends BodyRequest {

    @SerializedName("otp")
    @Expose @Setter
    private String otp;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("token")
    @Expose @Setter
    private String token;

}
