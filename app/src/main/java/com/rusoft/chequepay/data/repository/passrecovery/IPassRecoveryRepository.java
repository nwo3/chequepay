package com.rusoft.chequepay.data.repository.passrecovery;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IPassRecoveryRepository {
    void recoveryStart(RequestCallbackListener listener);
    void recoveryConfirm(String otp, RequestCallbackListener listener);
    void changePassword(RequestCallbackListener listener);
}
