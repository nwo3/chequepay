package com.rusoft.chequepay.data.entities.accounts.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class CloseAccountRequest extends BodyRequest {

    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("withdrawApplication")
    @Expose @Setter
    private Boolean withdrawApplication;
}
