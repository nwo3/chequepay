package com.rusoft.chequepay.data.repository.operations;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.Document;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.OperationListResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.OperationMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;


public class OperationRepository  extends BaseRepository implements IOperationRepository {

    private CacheManager cache;
    private Preferences preferences;
    private OperationMethods operationMethods;

    @Getter private FilterDataModel filterDataModel;
    @Getter private DetailOperationModel detailOperationModel;

    @Inject
    public OperationRepository(CacheManager cacheManager, Preferences preferences, OperationMethods operationMethods) {
        this.cache = cacheManager;
        this.preferences = preferences;
        this.operationMethods = operationMethods;

        filterDataModel = cache.getFilterDataModel();
        detailOperationModel = cache.getDetailOperationModel();
    }

    @Override
    public List<Operation> getUniqueOperationList(long contractorId) {
        Map<String, Operation> operationsMap = new HashMap<>();
        for (Operation item : cache.getUser().getShortOperations()) {
            if (contractorId == item.getContractorId()) {
                continue;
            }
            item.setIsSelectedAccount(false);
            operationsMap.put(item.getContractorAccount(), item);
        }
        return new ArrayList<>(operationsMap.values());
    }

    @Override
    public void getOperationShortList(RequestCallbackListener listener) {
        if (cache.getUser().getShortOperations().size() > 0
                && flagsManager.isFlagOff(Flags.EVENT)){
            listener.onPostExecute();
            listener.onSuccess();
            return;
        }

        Calendar calendar = Calendar.getInstance();
        long dateEnd = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH, -1);
        long dateStart = calendar.getTimeInMillis();

        if (cache.getUser().getActiveAccounts().size() > 0) {
            Disposable disposable = operationMethods.getAccountStatement(null, dateStart, dateEnd, null, null, BusinessConstants.OperationType.getOperationTypeCodeList(new ArrayList<>()), preferences.getAuthToken(), preferences.getClientId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(listener::onPreExecute)
                    .doFinally(listener::onPostExecute)
                    .subscribe(response -> handleResponse(listener, response),
                    listener::onError);
            compositeDisposable.add(disposable);
        }
    }

    @Override
    public void getOperationList(RequestCallbackListener listener) {
        if (!filterDataModel.isUpdated()) {
            String accountName = null;

            if (filterDataModel.getAccount() != null) {
                accountName = filterDataModel.getAccount().getNumber();
            }

            List<Integer> operationTypeList = new ArrayList<>();
            for (BusinessConstants.OperationGroupType item : filterDataModel.getOperationTypes()) {
                if (item.getCode() == BusinessConstants.OperationGroupType.GROUP_TRANSFER.getCode()) {
                    operationTypeList.add(BusinessConstants.OperationType.RECEIVING_TRANSFER.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.SENDING_TRANSFER.getCode());
                } else if (item.getCode() == BusinessConstants.OperationGroupType.GROUP_REFILL.getCode()) {
                    operationTypeList.add(BusinessConstants.OperationType.ENROLLING_CHEQUE_ENROLL.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.ENROLLING_CHEQUE_WITHDRAW.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.PURCHASE_CHEQUE_ENROLL.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.PURCHASE_CHEQUE_WITHDRAW.getCode());
                } else if (item.getCode() == BusinessConstants.OperationGroupType.GROUP_WITHDRAWAL.getCode()) {
                    operationTypeList.add(BusinessConstants.OperationType.RECEIVING_SELL_CHEQUE.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.SENDING_SELL_CHEQUE.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.CHECK_RETURN.getCode());
                    operationTypeList.add(BusinessConstants.OperationType.RETURNING_CHEQUE_WITHDRAW.getCode());
                }
            }

            Long dateStart = filterDataModel.getDateStart();
            Long dateEnd = filterDataModel.getDateEnd();
            List<String> currencyList = CurrencyUtils.getCurrencyStrCodeList(filterDataModel.getCurrency());

            Disposable disposable = operationMethods.getAccountStatement(accountName,
                    dateStart,
                    dateEnd,
                    filterDataModel.getContractorId(),
                    currencyList,
                    operationTypeList,
                    preferences.getAuthToken(),
                    preferences.getClientId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(listener::onPreExecute)
                    .doFinally(listener::onPostExecute)
                    .subscribe(response -> handleResponseForFilteredOperations(listener, response),
                            listener::onError);
            compositeDisposable.add(disposable);
        } else {
            listener.onSuccess();
        }
    }

    @Override
    public void getOperation(long operationId, RequestCallbackListener listener) {
        Operation operation = null;
        List<Operation> operationList = new ArrayList<>();
        operationList.addAll(cache.getUser().getShortOperations());
        operationList.addAll(cache.getUser().getFilteredOperations());
        for(Operation item : operationList) {
            if (item.getOperationId() == operationId) {
                operation = item;
                break;
            }
        }
        if (operation != null) {
            Disposable disposable = operationMethods.getItemOperation(
                    preferences.getAuthToken(),
                    preferences.getClientId(),
                    operation.getOperationUID(),
                    operation.getOperationTypeId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(listener::onPreExecute)
                    .doFinally(listener::onPostExecute)
                    .subscribe(response -> handleResponse(listener, response),
                            listener::onError);
            compositeDisposable.add(disposable);
        }
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0){
            if (response instanceof OperationListResponse) {
                flagsManager.turnOff(Flags.EVENT);
                cache.getUser().setShortOperations(((OperationListResponse) response).getOperations());
                listener.onSuccess();

            } else if (response instanceof ItemOperationResponse) {
                ItemOperationResponse itemOperationResponse = (ItemOperationResponse) response;

                Operation operation = null;
                List<Operation> operations = cache.getUser().getShortOperations();
                operations.addAll(cache.getUser().getFilteredOperations());

                for (Operation item : operations) {
                    if (item.getOperationId().equals(detailOperationModel.getOperationId())) {
                        operation = item;
                        break;
                    }
                }

                if (operation != null && operation.getSaldo() != null) {

                    if (BusinessConstants.OperationType.getOperationTypeTransfer().contains(operation.getOperationTypeId())) {
                        boolean isTransferOut = Double.valueOf(operation.getSaldoPrevious()) > Double.valueOf(operation.getSaldo());
                        if (isTransferOut) itemOperationResponse.setDocument(getDocument(BusinessConstants.DocType.APPLICATION_TRANSFER_CHEQUE.getVal(), itemOperationResponse));
                        else itemOperationResponse.setDocument(getDocument(BusinessConstants.DocType.CHEQUE_ACCEPTANCE_PROTOCOL.getVal(), itemOperationResponse));

                    } else if (BusinessConstants.OperationType.getOperationTypeWithdrawal().contains(operation.getOperationTypeId())) {
                        itemOperationResponse.setDocument(getDocument(BusinessConstants.DocType.APPLICATION_PURCHASE_CHEQUE.getVal(), itemOperationResponse));

                    } else if (BusinessConstants.OperationType.getOperationTypeReplenishment().contains(operation.getOperationTypeId())) {
                        itemOperationResponse.setDocument(getDocument(BusinessConstants.DocType.APPLICATION_SELL_CHEQUE.getVal(), itemOperationResponse));
                    }
                }
                listener.onSuccess(itemOperationResponse, operation);
            }
        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    private void handleResponseForFilteredOperations(RequestCallbackListener listener, BaseResponse response) {
        if (response.getError() == 0) {
            if (response instanceof OperationListResponse) {
                cache.getUser().setFilteredOperations(((OperationListResponse) response).getOperations());
                listener.onSuccess();
            }
        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    private Document getDocument(int type, ItemOperationResponse itemOperationResponse) {
        for (Document document : itemOperationResponse.getDocuments()) {
            if (document.getType() == type && document.getLink()) {
                return document;
            }
        }
        return null;
    }
}
