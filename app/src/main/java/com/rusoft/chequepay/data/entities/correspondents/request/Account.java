package com.rusoft.chequepay.data.entities.correspondents.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Account implements Serializable {
    @SerializedName("name")
    @Expose @Setter @Getter
    private String name;

    @SerializedName("number")
    @Expose @Setter @Getter
    private String number;

}
