package com.rusoft.chequepay.data.repository.operations;

import lombok.Getter;
import lombok.Setter;

public class DetailOperationModel {
    @Getter @Setter private Long operationId;

    public void clear() {
        operationId = null;
    }
}
