package com.rusoft.chequepay.data.entities.recover.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class ChangePasswordResponse extends BaseResponse {

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    public long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

}
