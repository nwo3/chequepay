package com.rusoft.chequepay.data.repository.transactions;


import lombok.Getter;
import lombok.Setter;

public class BaseTransactionModel {
    @Getter @Setter String name; //template title
    @Getter @Setter boolean isTemplateUsed;

    @Getter @Setter protected String flowToken;

    public void clearFlowToken() {
        flowToken = null;
    }
}
