package com.rusoft.chequepay.data.entities.correspondents.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Account implements Serializable {
    @SerializedName("lastOperation")
    @Expose @Getter @Setter
    private LastOperation lastOperation;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("number")
    @Expose @Getter @Setter
    private String number;

    @Override //Needed for PickerDialog
    public String toString() {
        return name;
    }
}
