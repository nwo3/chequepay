package com.rusoft.chequepay.data.entities.check.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class CheckLoginParamsResponse extends BaseResponse {

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

}
