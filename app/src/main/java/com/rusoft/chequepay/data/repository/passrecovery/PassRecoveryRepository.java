package com.rusoft.chequepay.data.repository.passrecovery;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.recover.response.ChangePasswordResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverConfirmResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverStartResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.RecoverMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class PassRecoveryRepository extends BaseRepository implements IPassRecoveryRepository {

    private CacheManager cache;
    private RecoverMethods recoverMethods;

    @Getter private PassRecoveryModel model;

    @Inject
    public PassRecoveryRepository(CacheManager cache, RecoverMethods recoverMethods) {
        this.cache = cache;
        this.recoverMethods = recoverMethods;

        model = cache.getPassRecoveryModel();
    }

    @Override
    public void recoveryStart(RequestCallbackListener listener) {
        Disposable disposable = recoverMethods.recoveryStart(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void recoveryConfirm(String otp, RequestCallbackListener listener) {
        Disposable disposable = recoverMethods.recoveryConfirm(otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void changePassword(RequestCallbackListener listener) {
        Disposable disposable = recoverMethods.changePassword(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof RecoverStartResponse) {
                model.setFlowToken(((RecoverStartResponse) response).getFlowToken());

            } else if (response instanceof RecoverConfirmResponse) {
                model.setAccessToken(((RecoverConfirmResponse) response).getAccessToken());
                model.setClientId(((RecoverConfirmResponse) response).getClientId());

            } else if (response instanceof ChangePasswordResponse) {

            }

            listener.onSuccess();

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    public void clearModel() {
        cache.clearPassRecoveryModel();
    }
}
