package com.rusoft.chequepay.data.repository.operations;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class FilterDataModel {
    @Getter @Setter private Account account;
    @Getter @Setter private Long dateStart;
    @Getter @Setter private Long dateEnd;
    @Getter @Setter private Long contractorId;
    @Getter @Setter private List<BusinessConstants.OperationGroupType> operationTypes = new ArrayList<>();
    @Getter @Setter private List<CurrencyUtils.Currency> currency = new ArrayList<>();
    @Getter @Setter private boolean updated;

    public void clear() {
        account = null;
        contractorId = null;
        operationTypes.clear();
        currency.clear();
        updated = false;
    }
}
