package com.rusoft.chequepay.data.entities.application.invoice_3.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.application.invoice_3.InvoiceDetail;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class InvoiceCreateRequest extends BodyRequest {

    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;

    @SerializedName("agreement")
    @Expose @Setter
    private String agreement;

    @SerializedName("agreementDate")
    @Expose @Setter
    private Long agreementDate;

    @SerializedName("amount")
    @Expose @Setter
    private Double amount;

    @SerializedName("applicationType")
    @Expose @Setter
    private String applicationType;

    @SerializedName("comment")
    @Expose @Setter
    private String comment;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("invoiceDetails")
    @Expose @Setter
    private List<InvoiceDetail> invoiceDetails;

    @SerializedName("lifetime")
    @Expose @Setter
    private Integer lifetime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("paymentType")
    @Expose @Setter
    private int paymentType;

    @SerializedName("receiverId")
    @Expose @Setter
    private String receiverId;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("sumIncludesNDS")
    @Expose @Setter
    private Boolean sumIncludesNDS;

}
