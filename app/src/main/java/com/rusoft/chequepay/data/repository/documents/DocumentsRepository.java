package com.rusoft.chequepay.data.repository.documents;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.document.response.Document;
import com.rusoft.chequepay.data.entities.document.response.DocumentListResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.DocumentsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class DocumentsRepository extends BaseRepository implements IDocumentsRepository {

    private CacheManager cache;
    private Preferences preferences;
    private DocumentsMethods documentsMethods;

    @Getter private DocsFilterModel model;

    @Inject
    public DocumentsRepository(CacheManager cache, Preferences preferences, DocumentsMethods documentsMethods) {
        this.cache = cache;
        this.preferences = preferences;
        this.documentsMethods = documentsMethods;

        model = cache.getDocumentsModel();
    }

    public void getPdf(Long documentId, RequestCallbackListener listener) {
        Disposable disposable = documentsMethods.getPdf(preferences.getAuthToken(), preferences.getClientId(), documentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(listener::onSuccess,
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void updateDocumentsList(RequestCallbackListener listener) {
        if (cache.getUser().getDocuments().isEmpty()) {
            flagsManager.turnOn(Flags.DOCUMENTS);
        }

        if (flagsManager.isFlagOff(Flags.DOCUMENTS)) {
            listener.onSuccess();
            return;
        }

        if (model.isFilterDataChanged()) model.clearCounters();

        if (model.isNextPageAvailable()) {
            Disposable disposable = documentsMethods.getDocumentList(preferences.getAuthToken(), preferences.getClientId(), model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(listener::onPreExecute)
                    .doFinally(listener::onPostExecute)
                    .subscribe(response -> handleResponse(listener, response),
                            listener::onError);
            compositeDisposable.add(disposable);
        }
    }

    public List<Document> getDocuments() {
        return cache.getUser().getDocuments();
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {
            if (response instanceof DocumentListResponse) {
                List<Document> filteredDocList = new ArrayList<>();
                DocumentListResponse documentListResponse = (DocumentListResponse) response;
                for (Document document : documentListResponse.getDocuments()) {
                    if (BusinessConstants.DocType.PERMITTED_DOC_LIST.contains(document.getDocumentType())) {
                        filteredDocList.add(document);
                    }
                }

                if (model.getCurrentPageNumber() == BusinessConstants.PAGE_NUMBER_START){
                    cache.getUser().getDocuments().clear();
                }

                cache.getUser().getDocuments().addAll(filteredDocList);

                model.setCurrentCount(documentListResponse.getDocuments().size());
                model.setTotalCount(documentListResponse.getTotalCount());
            }

            listener.onSuccess();

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    public void clearModel() {
        cache.clearDocumentsModel();
    }
}
