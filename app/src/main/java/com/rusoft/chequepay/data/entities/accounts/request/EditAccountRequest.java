package com.rusoft.chequepay.data.entities.accounts.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class EditAccountRequest extends BodyRequest {

    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;
    @SerializedName("brokerId")
    @Expose @Setter
    private Integer brokerId;
    @SerializedName("contractDate")
    @Expose @Setter
    private Integer contractDate;
    @SerializedName("contractNumber")
    @Expose @Setter
    private String contractNumber;
    @SerializedName("currentTime")
    @Expose @Setter
    private Integer currentTime;
    @SerializedName("name")
    @Expose @Setter
    private String name;
    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;
    @SerializedName("senderId")
    @Expose @Setter
    private Integer senderId;
}
