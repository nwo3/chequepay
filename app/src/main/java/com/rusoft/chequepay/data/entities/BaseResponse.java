package com.rusoft.chequepay.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true)
public class BaseResponse {

    @SerializedName("error")
    @Expose @Getter @Setter
    private Integer error;

    @SerializedName("state")
    @Expose @Getter @Setter
    private Integer state;
}
