package com.rusoft.chequepay.data.entities.commission.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;

public class CommissionTransferResponse extends BaseResponse{

    @SerializedName("comission")
    @Expose @Getter @Setter
    private Double comission;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("receiverName")
    @Expose @Getter @Setter
    private String receiverName;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}
