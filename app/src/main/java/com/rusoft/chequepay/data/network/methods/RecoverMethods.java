package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.recover.request.ChangePasswordRequest;
import com.rusoft.chequepay.data.entities.recover.request.RecoverConfirmRequest;
import com.rusoft.chequepay.data.entities.recover.request.RecoverStartRequest;
import com.rusoft.chequepay.data.entities.recover.response.ChangePasswordResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverConfirmResponse;
import com.rusoft.chequepay.data.entities.recover.response.RecoverStartResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.passrecovery.PassRecoveryModel;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Recover: восстановление пароля
 */

public class RecoverMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public RecoverMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<RecoverStartResponse> recoveryStart(PassRecoveryModel model) {
        return chequepayApi.recoveryStart(RecoverStartRequest.builder()
                .confirmType(model.getType())
                .source(model.getSource())
                .build());
    }

    public Single<RecoverConfirmResponse> recoveryConfirm(String otp, PassRecoveryModel model) {
        return chequepayApi.recoveryConfirm(RecoverConfirmRequest.builder()
                .code(otp)
                .flowToken(model.getFlowToken())
                .build());
    }

    public Single<ChangePasswordResponse> changePassword(PassRecoveryModel model) {
        return chequepayApi.changePassword(model.getAccessToken(), ChangePasswordRequest.builder()
                .recoverFlowToken(model.getFlowToken())
                .senderId(model.getClientId())
                .newPassword(model.getPass())
                .newPasswordConfirm(model.getPassConfirm())
                .currentTime(System.currentTimeMillis())
                .build());
    }
}
