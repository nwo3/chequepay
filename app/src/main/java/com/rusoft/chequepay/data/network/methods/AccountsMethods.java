package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.accounts.request.CheckAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.CheckShortAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.CloseAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.request.OpenAccountRequest;
import com.rusoft.chequepay.data.entities.accounts.response.CheckAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CheckShortAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.CloseAccountResponse;
import com.rusoft.chequepay.data.entities.accounts.response.OpenAccountResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;
import com.rusoft.chequepay.data.repository.account.NewAccountModel;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Accounts : Операции со счетами
 */
public class AccountsMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public AccountsMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<CheckAccountResponse> checkAccount(String token, String operatorLogin, long senderId) {
        return chequepayApi.checkAccount(token, new CheckAccountRequest(System.currentTimeMillis(), operatorLogin, senderId));
    }

    public Single<CheckShortAccountResponse> checkShortAccount(String token, String operatorLogin, long senderId) {
        //TODO узнать про includeDeleted, есть ли случаи, где true?
        return chequepayApi.checkShortAccount(token, new CheckShortAccountRequest(System.currentTimeMillis(), false, operatorLogin, senderId));
    }

    public Single<OpenAccountResponse> createAccount(String token, long senderId, NewAccountModel model) {
        return chequepayApi.openAccount(token, OpenAccountRequest.builder()
                .senderId(senderId)
                .accountName(model.getName())
                .currency(model.getCurrency())
                .bankId(model.getBank().getBankWalletId())
                .issuerId(model.getIssuer().getIssuerWalletId())
                .activeAccount(false)
                .specialProperty("0")
                .currentTime(System.currentTimeMillis())
                .build());
    }

    public Single<CloseAccountResponse> closeAccount(String token, long senderId, String accountNumber) {
        return chequepayApi.closeAccount(token, CloseAccountRequest.builder()
                .senderId(senderId)
                .accountNumber(accountNumber)
                .withdrawApplication(false)
                .currentTime(System.currentTimeMillis())
                .build());
    }
}