package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetSenderListRequest extends BodyRequest {
    @SerializedName("contractorId")
    @Expose @Setter
    private long contractorId;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("dateFinish")
    @Expose @Setter
    private long dateFinish;

    @SerializedName("dateStart")
    @Expose @Setter
    private long dateStart;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("operationSenderType")
    @Expose @Setter
    private long operationSenderType;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("pageNumber")
    @Expose @Setter
    private long pageNumber;

    @SerializedName("paymentType")
    @Expose @Setter
    private String paymentType;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("sortField")
    @Expose @Setter
    private List<SortField> sortField;

    @SerializedName("uidSubstring")
    @Expose @Setter
    private String uidSubstring;
}
