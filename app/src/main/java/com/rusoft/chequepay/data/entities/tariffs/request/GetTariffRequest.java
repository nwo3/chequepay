package com.rusoft.chequepay.data.entities.tariffs.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetTariffRequest extends BodyRequest {

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("providerCode")
    @Expose @Setter
    private String providerCode;

    @SerializedName("receiverAccount")
    @Expose @Setter
    private String receiverAccount;

    @SerializedName("receiverId")
    @Expose @Setter
    private String receiverId;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("tariffOwner")
    @Expose @Setter
    private String tariffOwner;

    @SerializedName("tradeOperation")
    @Expose @Setter
    private Boolean tradeOperation;

}
