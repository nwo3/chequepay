package com.rusoft.chequepay.data.repository.registration.model;

import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.utils.CurrencyUtils;

import lombok.Getter;
import lombok.Setter;

public class AccountSubModel {
    @Getter @Setter private CurrencyUtils.Currency currency;
    @Getter @Setter private Issuer issuer;
    @Getter @Setter private com.rusoft.chequepay.data.entities.info.Bank bank;

    private RegistrationModel registrationModel;

    public AccountSubModel(RegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public boolean isRequiredDataReceived(){
        return currency != null && issuer != null && bank != null;
    }

    @Override
    public String toString() {
        return "AccountSubModel{" +
                "currency=" + currency +
                ", issuer=" + issuer +
                ", bank=" + bank +
                '}';
    }
}
