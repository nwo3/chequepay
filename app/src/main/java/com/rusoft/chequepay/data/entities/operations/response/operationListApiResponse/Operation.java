package com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Operation {
    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("contractor")
    @Expose @Getter @Setter
    private String contractor;

    @SerializedName("contractorAccount")
    @Expose @Getter @Setter
    private String contractorAccount;

    @SerializedName("contractorId")
    @Expose @Getter @Setter
    private Long contractorId;

    @SerializedName("date")
    @Expose @Getter @Setter
    private Long date;

    @SerializedName("depositAmount")
    @Expose @Getter @Setter
    private String depositAmount;

    @SerializedName("finalContractor")
    @Expose @Getter @Setter
    private String finalContractor;

    @SerializedName("finalContractorId")
    @Expose @Getter @Setter
    private Long finalContractorId;

    @SerializedName("newOperation")
    @Expose @Getter @Setter
    private Boolean newOperation;

    @SerializedName("operationId")
    @Expose @Getter @Setter
    private Long operationId;

    @SerializedName("operationTypeId")
    @Expose @Getter @Setter
    private Integer operationTypeId;

    @SerializedName("operationTypeName")
    @Expose @Getter @Setter
    private String operationTypeName;

    @SerializedName("operationUID")
    @Expose @Getter @Setter
    private String operationUID;

    @SerializedName("paymentTypeId")
    @Expose @Getter @Setter
    private Long paymentTypeId;

    @SerializedName("paymentTypeName")
    @Expose @Getter @Setter
    private String paymentTypeName;

    @SerializedName("saldo")
    @Expose @Getter @Setter
    private String saldo;

    @SerializedName("saldoPrevious")
    @Expose @Getter @Setter
    private String saldoPrevious;

    @SerializedName("withdrawAmount")
    @Expose @Getter @Setter
    private String withdrawAmount;

    @Getter @Setter
    private Boolean isSelectedAccount = false;

    @Getter @Setter
    private String accountName = "";
}
