package com.rusoft.chequepay.data.repository.transactions.deposit;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IDepositRepository {

    void getTariff(RequestCallbackListener listener);
    void createDeposit(String otp, RequestCallbackListener listener);

    void clearModel();

}
