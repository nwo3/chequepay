package com.rusoft.chequepay.data.entities.messages.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetMessageRequest extends BodyRequest {
    @SerializedName("id")
    @Expose @Setter
    private long id;

    @SerializedName("walletId")
    @Expose @Setter
    private long walletId;
}
