package com.rusoft.chequepay.data.repository.documents;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.operations.request.SortField;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

public class DocsFilterModel {

    @Getter private Long dateStart, dateFinish;
    @Getter private BusinessConstants.DocCategory docCategory;
    @Getter private BusinessConstants.DocStatus docStatus;
    @Getter @Setter private String language;
    @Getter @Setter private List<SortField> sortFields;

    @Getter private Integer currentPageNumber;
    @Setter private int currentCount, totalCount;
    @Getter private boolean isFilterDataChanged = false;

    public DocsFilterModel() {
        //default data initialization
        language = Locale.getDefault().getLanguage();

        docCategory = BusinessConstants.DocCategory.ALL;
        docStatus = BusinessConstants.DocStatus.ALL;

        sortFields = new ArrayList<>();
        sortFields.add(new SortField("DOC_TIME", "DESC"));

        Calendar calendar = Calendar.getInstance();
        dateFinish = calendar.getTimeInMillis();
        calendar.add(Calendar.DATE, -AppConstants.WEEK);
        dateStart = calendar.getTimeInMillis();

        clearCounters();
    }

    public boolean isSearchEnabled() {
        return dateStart != null && dateFinish != null &&
                docCategory != null && docStatus != null;
    }

    public void setDateStart(Long dateStart) {
        this.dateStart = dateStart;
        isFilterDataChanged = true;
    }

    public void setDateFinish(Long dateFinish) {
        this.dateFinish = dateFinish;
        isFilterDataChanged = true;
    }

    public void setDocCategory(BusinessConstants.DocCategory docCategory) {
        isFilterDataChanged = this.docCategory != docCategory;
        this.docCategory = docCategory;
    }

    public void setDocStatus(BusinessConstants.DocStatus docStatus) {
        isFilterDataChanged = this.docStatus != docStatus;
        this.docStatus = docStatus;
    }

    public void incrementPageNumber() {
        if (isNextPageAvailable()) currentPageNumber++;
    }

    boolean isNextPageAvailable() {
        return (currentCount % BusinessConstants.PAGE_ITEMS_COUNT == 0)
                && (currentCount <= totalCount);
    }

    void clearCounters() {
        currentCount = 0;
        totalCount = 0;
        currentPageNumber = BusinessConstants.PAGE_NUMBER_START;
        isFilterDataChanged = false;
    }

}
