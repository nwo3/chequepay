package com.rusoft.chequepay.data.entities.application;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Setter;

@Builder
public class ResidentBankDetails implements Serializable {

    @SerializedName("bankAccount")
    @Expose @Setter
    private String bankAccount;

    @SerializedName("bicRus")
    @Expose @Setter
    private String bicRus;

    @SerializedName("inn")
    @Expose @Setter
    private String inn;

    @SerializedName("kpp")
    @Expose @Setter
    private String kpp;

}
