package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class TransferTemplate extends BaseTemplate {

    @SerializedName("chequeValue")
    @Expose @Getter @Setter
    private double chequeValue;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("commentEn")
    @Expose @Getter @Setter
    private String commentEn;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private String currency;

    @SerializedName("providerCode")
    @Expose @Getter @Setter
    private String providerCode;

    @SerializedName("receiverAccount")
    @Expose @Getter @Setter
    private String receiverAccount;

    @SerializedName("senderAccount")
    @Expose @Getter @Setter
    private String senderAccount;

}
