package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Company {

    @SerializedName("confirmationDocument")
    @Expose @Getter @Setter
    private ConfirmationDocument confirmationDocument;

    @SerializedName("fullName")
    @Expose @Getter @Setter
    private String fullName;

    @SerializedName("fullNameEn")
    @Expose @Getter @Setter
    private String fullNameEn;

    @SerializedName("kpp")
    @Expose @Getter @Setter
    private String kpp;

    @SerializedName("ogrn")
    @Expose @Getter @Setter
    private String ogrn;

    @SerializedName("okvedCode")
    @Expose @Getter @Setter
    private String okvedCode;

    @SerializedName("persons")
    @Expose @Getter @Setter
    private List<PersonEmployee> persons = null;

    @SerializedName("registrationCertificate")
    @Expose @Getter @Setter
    private RegistrationCertificate registrationCertificate;

    @SerializedName("shortName")
    @Expose @Getter @Setter
    private String shortName;

    @SerializedName("shortNameEn")
    @Expose @Getter @Setter
    private String shortNameEn;

    @SerializedName("taxNumber")
    @Expose @Getter @Setter
    private String taxNumber;

    @SerializedName("website")
    @Expose @Getter @Setter
    private String website;

}
