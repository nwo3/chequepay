package com.rusoft.chequepay.data.entities.application.invoice_3.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.entities.application.invoice_3.Statistics;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class InvoicesListResponse extends BaseResponse {

    @SerializedName("applications")
    @Expose @Getter @Setter
    public List<Application> applications = null;

    @SerializedName("applicationsCount")
    @Expose @Getter @Setter
    public Integer applicationsCount;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    public Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    public String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    public String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    public String signature;

    @SerializedName("statistics")
    @Expose @Getter @Setter
    public Statistics statistics;

    @SerializedName("type")
    @Expose @Getter @Setter
    public Integer type;

}
