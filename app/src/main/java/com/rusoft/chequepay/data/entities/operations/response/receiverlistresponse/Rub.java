package com.rusoft.chequepay.data.entities.operations.response.receiverlistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Rub {
    @SerializedName("totalTransferAmount")
    @Expose @Getter @Setter
    private String totalTransferAmount;
}
