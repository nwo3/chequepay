package com.rusoft.chequepay.data.repository.transactions.deposit;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;
import com.rusoft.chequepay.utils.CurrencyUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class DepositModel extends BaseTransactionModel {
    @Getter Account account;

    public void setAccount(Account account) {
        this.account = account;
        this.currency = CurrencyUtils.getStrCode(account.getCurrency());
    }

    @Getter @Setter Issuer issuer;
    @Getter @Setter Bank bank;
    @Getter @Setter BusinessConstants.RefundMode mode;
    @Getter @Setter String currency;

    @Getter @Setter Double amount;
    @Getter @Setter Double tariff;
    @Getter Double commissionAmount;
    @Getter Double total;

    @Getter @Setter String applicationCoreId;
    @Getter @Setter boolean isTransactionDone;

    public boolean updateNumericData() {
        boolean isSuccess;

        if (amount != null && this.tariff != null){
            total = amount * (1 + this.tariff / 100);
            commissionAmount = total - amount;
            isSuccess = true;
        } else {
            commissionAmount = null;
            total = null;
            isSuccess = false;
        }

        return isSuccess;
    }

    public boolean isAllDataReceived(){
        return account != null && issuer != null && bank != null &&
                mode != null && amount != null &&
                tariff != null && total != null
                && currency != null;
    }
}
