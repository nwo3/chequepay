package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@AllArgsConstructor
public class BankAccount {

    @SerializedName("bankForeign")
    @Expose @Getter @Setter
    private BankForeign bankForeign;

    @SerializedName("bankInn")
    @Expose @Getter @Setter
    private String bankInn;

    @SerializedName("bankKpp")
    @Expose @Getter @Setter
    private String bankKpp;

    @SerializedName("bankRus")
    @Expose @Getter @Setter
    private BankRus bankRus;

    @SerializedName("number")
    @Expose @Getter @Setter
    private String number;

    @SerializedName("owner")
    @Expose @Getter @Setter
    private String owner;

    @SerializedName("ownerEn")
    @Expose @Getter @Setter
    private String ownerEn;

    public BankAccount getCopy() {
        return new BankAccount(
                this.getBankForeign(),
                this.getBankInn(),
                this.getBankKpp(),
                this.getBankRus(),
                this.getNumber(),
                this.getOwner(),
                this.getOwnerEn());
    }

    public String getCurrentBic() {
        if (bankRus != null) return bankRus.getBic();
        else if (bankForeign != null) return bankForeign.getBic();
        else return null;
    }

}
