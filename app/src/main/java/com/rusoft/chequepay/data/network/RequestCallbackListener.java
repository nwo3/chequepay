package com.rusoft.chequepay.data.network;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;

import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

public interface RequestCallbackListener {
    void onPreExecute(Disposable disposable);
    void onPostExecute();
    void onError(Throwable throwable);
    default void onSuccess(){}
    default void onSuccess(String result){}
    default void onSuccess(Double result){}
    default void onSuccess(Integer result){}
    default void onSuccess(Long result){}
    default void onSuccess(BaseResponse baseResponse){}
    default void onSuccess(ResponseBody response){}
    default void onSuccess(ItemOperationResponse itemOperationResponse, Operation operation){}
}
