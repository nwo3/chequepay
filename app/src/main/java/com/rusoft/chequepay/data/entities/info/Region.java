package com.rusoft.chequepay.data.entities.info;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Region {

    @SerializedName("code")
    @Expose @Getter @Setter
    private Integer code;

    @SerializedName("nameEn")
    @Expose @Getter @Setter
    private String nameEn;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;


    public Region(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
