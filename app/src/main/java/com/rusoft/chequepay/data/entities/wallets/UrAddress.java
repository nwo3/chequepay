package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class UrAddress {
    @SerializedName("countryId")
    @Expose @Getter @Setter
    private Integer countryId;

    @SerializedName("countryName")
    @Expose @Getter @Setter
    private String countryName;

    @SerializedName("countryNameEn")
    @Expose @Getter @Setter
    private String countryNameEn;

    @SerializedName("postcode")
    @Expose @Getter @Setter
    private String postcode;

    @SerializedName("regionId")
    @Expose @Getter @Setter
    private Integer regionId;

    @SerializedName("regionName")
    @Expose @Getter @Setter
    private String regionName;

    @SerializedName("regionNameEn")
    @Expose @Getter @Setter
    private String regionNameEn;

    @SerializedName("settlement")
    @Expose @Getter @Setter
    private String settlement;

    @SerializedName("settlementEn")
    @Expose @Getter @Setter
    private String settlementEn;

    @SerializedName("state")
    @Expose @Getter @Setter
    private String state;

    @SerializedName("stateEn")
    @Expose @Getter @Setter
    private String stateEn;

    @SerializedName("street")
    @Expose @Getter @Setter
    private String street;

    @SerializedName("streetEn")
    @Expose @Getter @Setter
    private String streetEn;


}
