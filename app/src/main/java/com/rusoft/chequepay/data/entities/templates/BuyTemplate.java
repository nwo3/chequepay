package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class BuyTemplate extends BaseTemplate{

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("amount")
    @Expose @Getter @Setter
    private double amount;

    @SerializedName("applicationType")
    @Expose @Getter @Setter
    private String applicationType;

    @SerializedName("bankId")
    @Expose @Getter @Setter
    private String bankId;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private String currency;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    private String receiverId;

    @SerializedName("refundMode")
    @Expose @Getter @Setter
    private int refundMode;
}
