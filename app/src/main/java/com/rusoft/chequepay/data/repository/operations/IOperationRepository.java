package com.rusoft.chequepay.data.repository.operations;

import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.network.RequestCallbackListener;

import java.util.List;

public interface IOperationRepository {
    List<Operation> getUniqueOperationList(long contractorId);
    void getOperationShortList(RequestCallbackListener listener);
    void getOperationList(RequestCallbackListener listener);
    void getOperation(long operationId, RequestCallbackListener listener);
}
