package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.application.NotResidentBankDetails;
import com.rusoft.chequepay.data.entities.application.ResidentBankDetails;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class SaleTemplate extends BaseTemplate {

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("amount")
    @Expose @Getter @Setter
    private Double amount;

    @SerializedName("applicationType")
    @Expose @Getter @Setter
    private String applicationType;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private String currency;

    @SerializedName("notResidentBankDetails")
    @Expose @Getter @Setter
    private NotResidentBankDetails notResidentBankDetails;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    private String receiverId;

    @SerializedName("refundMode")
    @Expose @Getter @Setter
    private Integer refundMode;

    @SerializedName("residentBankDetails")
    @Expose @Getter @Setter
    private ResidentBankDetails residentBankDetails;

}
