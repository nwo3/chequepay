package com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.application.NotResidentBankDetails;
import com.rusoft.chequepay.data.entities.application.ResidentBankDetails;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class PurchaseFromIssuerCreateRequest extends BodyRequest {

    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;

    @SerializedName("amount")
    @Expose @Setter
    private Double amount;

    @SerializedName("applicationType")
    @Expose @Setter
    private String applicationType;

    @SerializedName("bankId")
    @Expose @Setter
    private String bankId;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("notResidentBankDetails")
    @Expose @Setter
    private NotResidentBankDetails notResidentBankDetails;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("paymentType")
    @Expose @Setter
    private int paymentType;

    @SerializedName("price")
    @Expose @Setter
    private double price;

    @SerializedName("receiverId")
    @Expose @Setter
    private String receiverId;

    @SerializedName("refundMode")
    @Expose @Setter
    private int refundMode;

    @SerializedName("relatedApplicationsId")
    @Expose @Setter
    private List<String> relatedApplicationsId;

    @SerializedName("repeatableApplication")
    @Expose @Setter
    private Boolean repeatableApplication;

    @SerializedName("residentBankDetails")
    @Expose @Setter
    private ResidentBankDetails residentBankDetails;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

}
