package com.rusoft.chequepay.data.service.userdata;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.data.event.UserDataServiceEvent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.user.UserRepository;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class UserDataService extends Service {

    @Inject
    UserRepository userRepository;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDaggerComponent();
    }

    private void initializeDaggerComponent() {
        DaggerUserDataServiceComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .build().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        updateUserData();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void updateUserData() {
        userRepository.updateUserData(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                EventBus.getDefault().post(new UserDataServiceEvent(AppConstants.EVENT_USER_DATA_RECEIVING_START));
            }

            @Override
            public void onPostExecute() {
                EventBus.getDefault().post(new UserDataServiceEvent(AppConstants.EVENT_USER_DATA_RECEIVING_END));
                stopSelf();
            }

            @Override
            public void onError(Throwable throwable) {
                EventBus.getDefault().post(new UserDataServiceEvent(throwable));
            }

            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new UserDataServiceEvent(AppConstants.EVENT_USER_DATA_RECEIVING_SUCCESS));
            }
        });
    }
}
