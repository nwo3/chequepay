package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@AllArgsConstructor
public class Passport {

    @SerializedName("departmentCode")
    @Expose @Getter @Setter
    private String departmentCode;

    @SerializedName("issueDate")
    @Expose @Getter @Setter
    private Long issueDate;

    @SerializedName("issuer")
    @Expose @Getter @Setter
    private String issuer;

    @SerializedName("issuerEn")
    @Expose @Getter @Setter
    private String issuerEn;

    @SerializedName("number")
    @Expose @Getter @Setter
    private String number;

    @SerializedName("type")
    @Expose @Getter @Setter
    private String type;

}
