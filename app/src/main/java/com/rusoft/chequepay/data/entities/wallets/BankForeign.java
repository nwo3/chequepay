package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@AllArgsConstructor
public class BankForeign {

    @SerializedName("additionalInfo")
    @Expose @Getter @Setter
    private String additionalInfo;

    @SerializedName("address")
    @Expose @Getter @Setter
    private String address;

    @SerializedName("bic")
    @Expose @Getter @Setter
    private String bic;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("swift")
    @Expose @Getter @Setter
    private String swift;

    public BankForeign getCopy() {
        return new BankForeign(
                this.getAdditionalInfo(),
                this.getAddress(),
                this.getBic(),
                this.getName(),
                this.getSwift());
    }
}
