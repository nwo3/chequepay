package com.rusoft.chequepay.data.entities.info;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Issuer {

    @SerializedName("issuerDeleted")
    @Expose @Getter @Setter
    private Boolean issuerDeleted;

    @SerializedName("issuerEnabled")
    @Expose @Getter @Setter
    private Boolean issuerEnabled;

    @SerializedName("issuerShortName")
    @Expose @Getter @Setter
    private String issuerShortName;

    @SerializedName("issuerShortNameEn")
    @Expose @Getter @Setter
    private String issuerShortNameEn;

    @SerializedName("issuerWalletId")
    @Expose @Getter @Setter
    private String issuerWalletId;


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Issuer other = (Issuer) obj;
        return issuerWalletId.equals(other.issuerWalletId);
    }

    @Override
    public String toString() {
        return issuerShortName;
    }
}
