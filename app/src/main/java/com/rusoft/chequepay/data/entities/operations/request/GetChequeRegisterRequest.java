package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetChequeRegisterRequest extends BodyRequest {
    @SerializedName("accountNumber")
    @Expose @Setter
    private String accountNumber;

    @SerializedName("chequeId")
    @Expose @Setter
    private String chequeId;

    @SerializedName("contractorAccountNumber")
    @Expose @Setter
    private String contractorAccountNumber;

    @SerializedName("contractorId")
    @Expose @Setter
    private long contractorId;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("dateFinish")
    @Expose @Setter
    private long dateFinish;

    @SerializedName("dateStart")
    @Expose @Setter
    private long dateStart;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("pageNumber")
    @Expose @Setter
    private long pageNumber;

    @SerializedName("paymentType")
    @Expose @Setter
    private String paymentType;

    @SerializedName("sender")
    @Expose @Setter
    private Boolean sender;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("sortField")
    @Expose @Setter
    private List<SortField> sortField;
}
