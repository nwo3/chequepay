package com.rusoft.chequepay.data.entities.clientdata.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class ListOfBrokerRequest extends BodyRequest {
    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;
}
