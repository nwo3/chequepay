package com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class NotResidentBankDetails {
    @SerializedName("bankAddress")
    @Expose @Getter @Setter
    private String bankAddress;

    @SerializedName("bankName")
    @Expose @Getter @Setter
    private String bankName;

    @SerializedName("bicInternational")
    @Expose @Getter @Setter
    private String bicInternational;

    @SerializedName("iban")
    @Expose @Getter @Setter
    private String iban;

    @SerializedName("inn")
    @Expose @Getter @Setter
    private String inn;

    @SerializedName("kpp")
    @Expose @Getter @Setter
    private String kpp;

    @SerializedName("otherBankDetails")
    @Expose @Getter @Setter
    private String otherBankDetails;

    @SerializedName("receiver")
    @Expose @Getter @Setter
    private String receiver;

    @SerializedName("receiverEn")
    @Expose @Getter @Setter
    private String receiverEn;

    @SerializedName("registrationCode")
    @Expose @Getter @Setter
    private String registrationCode;

    @SerializedName("swift")
    @Expose @Getter @Setter
    private String swift;
}
