package com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Cheque {
    @SerializedName("chequeStateName")
    @Expose @Getter @Setter
    private String chequeStateName;

    @SerializedName("payOrderDate")
    @Expose @Getter @Setter
    private long payOrderDate;

    @SerializedName("payOrderNum")
    @Expose @Getter @Setter
    private long payOrderNum;
}
