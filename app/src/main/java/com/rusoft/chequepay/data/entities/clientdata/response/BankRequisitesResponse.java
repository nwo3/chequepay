package com.rusoft.chequepay.data.entities.clientdata.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;

public class BankRequisitesResponse extends BaseResponse {
    @SerializedName("bankAccount")
    @Expose @Setter @Getter
    private BankAccount bankAccount;

    @SerializedName("currentTime")
    @Expose @Setter @Getter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Setter @Getter
    private String flowToken;

    @SerializedName("kpp")
    @Expose @Setter @Getter
    private String kpp;

    @SerializedName("phone")
    @Expose @Setter @Getter
    private String phone;

    @SerializedName("signature")
    @Expose @Setter @Getter
    private String signature;

    @SerializedName("taxNumber")
    @Expose @Setter @Getter
    private String taxNumber;
}
