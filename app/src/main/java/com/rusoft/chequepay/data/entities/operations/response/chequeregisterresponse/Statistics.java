package com.rusoft.chequepay.data.entities.operations.response.chequeregisterresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Statistics {
    @SerializedName("balanceForBeginning")
    @Expose @Getter @Setter
    private String balanceForBeginning;

    @SerializedName("balanceForEnd")
    @Expose @Getter @Setter
    private String balanceForEnd;
}
