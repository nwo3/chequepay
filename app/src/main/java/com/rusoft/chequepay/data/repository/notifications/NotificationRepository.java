package com.rusoft.chequepay.data.repository.notifications;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.notifications.response.GetNotificationsResponse;
import com.rusoft.chequepay.data.entities.notifications.response.RemoveNotificationResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.NotificationMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.Preferences;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationRepository extends BaseRepository implements INotificationRepository {
    private Preferences preferences;
    private NotificationMethods notificationMethods;

    @Inject
    public NotificationRepository(Preferences preferences, NotificationMethods notificationMethods) {
        this.preferences = preferences;
        this.notificationMethods = notificationMethods;
    }

    public void getCountNotification(RequestCallbackListener listener) {
        Disposable disposable = notificationMethods.getNotification(preferences.getAuthToken(), preferences.getClientId(), preferences.getTimestamp())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);

    }

    public void removeNotification(RequestCallbackListener listener) {
        Disposable disposable = notificationMethods.removeNotification(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0){
            if (response instanceof GetNotificationsResponse) {
                GetNotificationsResponse getNotificationsResponse = (GetNotificationsResponse) response;
                Integer invoiceReceiver = getNotificationsResponse.getHighPriorityUpdatedEntities().getInvoceReceiver();
                Integer operationReceiver = getNotificationsResponse.getHighPriorityUpdatedEntities().getOperationReceiver();
                listener.onSuccess(invoiceReceiver + operationReceiver);

            } else if (response instanceof RemoveNotificationResponse) {
                listener.onSuccess();
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

}
