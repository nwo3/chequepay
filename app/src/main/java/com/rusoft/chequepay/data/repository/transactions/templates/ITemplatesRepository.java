package com.rusoft.chequepay.data.repository.transactions.templates;


import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface ITemplatesRepository {

    void getAllTemplates(boolean forced, RequestCallbackListener listener);

    void saveTemplate(BusinessConstants.TemplateType type, String title, RequestCallbackListener listener);
    void getTemplate(ShortTemplate template, RequestCallbackListener listener);
    void deleteTemplate(ShortTemplate template, RequestCallbackListener listener);

}
