package com.rusoft.chequepay.data.entities.notifications.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetNotificationRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("processId")
    @Expose @Setter
    private long processId;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    @SerializedName("timeZone")
    @Expose @Setter
    private String timeZone;
}
