package com.rusoft.chequepay.data.entities.recover.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class RecoverConfirmRequest extends BodyRequest {

    @SerializedName("code")
    @Expose @Setter
    public String code;

    @SerializedName("flowToken")
    @Expose @Setter
    public String flowToken;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
