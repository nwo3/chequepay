package com.rusoft.chequepay.data.entities.wallets.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;

public class WalletInfoAccountResponse extends BaseResponse{

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private Long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("residencyCountryId")
    @Expose @Getter @Setter
    private Long residencyCountryId;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("taxNumber")
    @Expose @Getter @Setter
    private String taxNumber;

    @SerializedName("walletFullNameEn")
    @Expose @Getter @Setter
    private String walletFullNameEn;

    @SerializedName("walletFullNameRu")
    @Expose @Getter @Setter
    private String walletFullNameRu;

    @SerializedName("walletId")
    @Expose @Getter @Setter
    private String walletId;

    @SerializedName("walletShortNameEn")
    @Expose @Getter @Setter
    private String walletShortNameEn;

    @SerializedName("walletShortNameRu")
    @Expose @Getter @Setter
    private String walletShortNameRu;
}
