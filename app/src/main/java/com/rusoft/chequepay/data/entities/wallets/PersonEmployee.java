package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class PersonEmployee extends PersonBase{

    @SerializedName("birthDate")
    @Expose @Getter @Setter
    private Long birthDate;

    @SerializedName("email")
    @Expose @Getter @Setter
    private String email;

    @SerializedName("firstName")
    @Expose @Getter @Setter
    private String firstName;

    @SerializedName("firstNameEn")
    @Expose @Getter @Setter
    private String firstNameEn;

    @SerializedName("lastName")
    @Expose @Getter @Setter
    private String lastName;

    @SerializedName("lastNameEn")
    @Expose @Getter @Setter
    private String lastNameEn;

    @SerializedName("mobilePhone")
    @Expose @Getter @Setter
    private String mobilePhone;

    @SerializedName("patronymic")
    @Expose @Getter @Setter
    private String patronymic;

    @SerializedName("patronymicEn")
    @Expose @Getter @Setter
    private String patronymicEn;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("post")
    @Expose @Getter @Setter
    private String post;

    @SerializedName("postEn")
    @Expose @Getter @Setter
    private String postEn;

    @SerializedName("type")
    @Expose @Getter @Setter
    private String type;

}
