package com.rusoft.chequepay.data.entities.templates;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.application.invoice_3.InvoiceDetail;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class InvoiceTemplate extends BaseTemplate {
    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    private String accountNumber;

    @SerializedName("agreement")
    @Expose @Getter @Setter
    private String agreement;

    @SerializedName("agreementDate")
    @Expose @Getter @Setter
    private Long agreementDate;

    @SerializedName("amount")
    @Expose @Getter @Setter
    private double amount;

    @SerializedName("applicationType")
    @Expose @Getter @Setter
    private String applicationType;

    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private String currency;

    @SerializedName("invoiceDetails")
    @Expose @Getter @Setter
    private List<InvoiceDetail> invoiceDetails;

    @SerializedName("lifetime")
    @Expose @Getter @Setter
    private int lifetime;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    private String receiverId;

    @SerializedName("sumIncludesNDS")
    @Expose @Getter @Setter
    private Boolean sumIncludesNDS;

}
