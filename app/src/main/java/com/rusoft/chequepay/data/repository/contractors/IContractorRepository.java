package com.rusoft.chequepay.data.repository.contractors;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IContractorRepository {
    void getWalletInfoByAccount(String accountNumber, RequestCallbackListener listener);

    void addCorrespondent(RequestCallbackListener listener);
    void getContractorList(RequestCallbackListener listener);
    void getCorrespondentList(RequestCallbackListener listener);
    void getInfoContractor(long correspondentId, RequestCallbackListener listener);
    void deleteContractor(long correspondentId, RequestCallbackListener listener);
}
