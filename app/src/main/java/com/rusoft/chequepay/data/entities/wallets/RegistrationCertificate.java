package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class RegistrationCertificate {

    @SerializedName("issueDate")
    @Expose @Getter @Setter
    private Long issueDate;

    @SerializedName("issuer")
    @Expose @Getter @Setter
    private String issuer;

    @SerializedName("issuerEn")
    @Expose @Getter @Setter
    private String issuerEn;

    @SerializedName("number")
    @Expose @Getter @Setter
    private String number;

}
