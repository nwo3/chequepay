package com.rusoft.chequepay.data.entities.operations.response.unsignedactresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Operation {
    @SerializedName("amount")
    @Expose @Getter @Setter
    private String amount;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private long currency;

    @SerializedName("docNumber")
    @Expose @Getter @Setter
    private String docNumber;

    @SerializedName("operationType")
    @Expose @Getter @Setter
    private long operationType;

    @SerializedName("operationUID")
    @Expose @Getter @Setter
    private String operationUID;

    @SerializedName("senderId")
    @Expose @Getter @Setter
    private long senderId;

    @SerializedName("shortName")
    @Expose @Getter @Setter
    private String shortName;
}
