package com.rusoft.chequepay.data.entities.wallets;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class ConfirmationDocument {

    @SerializedName("expirationDate")
    @Expose @Getter @Setter
    private Long expirationDate;

    @SerializedName("issueDate")
    @Expose @Getter @Setter
    private Long issueDate;

    @SerializedName("issuer")
    @Expose @Getter @Setter
    private String issuer;

    @SerializedName("issuerEn")
    @Expose @Getter @Setter
    private String issuerEn;

    @SerializedName("number")
    @Expose @Getter @Setter
    private String number;

    @SerializedName("type")
    @Expose @Getter @Setter
    private String type;

}
