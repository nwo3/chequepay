package com.rusoft.chequepay.data.repository.contractors;

import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.clientdata.response.Contractor;
import com.rusoft.chequepay.data.entities.clientdata.response.ContractorListResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.AddCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.correspondents.response.DeleteCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.EditCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.InfoCorrespondentResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.ListCorrespondentsResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoByAccountResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.CorrespondentsMethods;
import com.rusoft.chequepay.data.network.methods.DataMethods;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class ContractorRepository extends BaseRepository implements IContractorRepository {

    private CacheManager cache;
    private Preferences preferences;

    private DataMethods dataMethods;
    private CorrespondentsMethods correspondentsMethods;
    private WalletsMethods walletsMethods;

    @Getter private CorrespondentModel correspondentModel;

    @Inject
    public ContractorRepository(CacheManager cacheManager, Preferences preferences, DataMethods dataMethods, CorrespondentsMethods correspondentsMethods, WalletsMethods walletsMethods) {
        this.cache = cacheManager;
        this.preferences = preferences;
        this.dataMethods = dataMethods;
        this.walletsMethods = walletsMethods;
        this.correspondentsMethods = correspondentsMethods;

        this.correspondentModel = cache.getCorrespondentModel();
    }

    public Correspondent getCorrespondentFromList(Long contractorId) {
        Correspondent correspondent = null;
        for (Correspondent item : cache.getUser().getCorrespondents()) {
            if (item.getWalletId() == contractorId) {
                correspondent = item;
                break;
            }
        }
        return correspondent;
    }

    public Contractor getContractorFromList(Long contractorId) {
        if (contractorId == null) return null;
        Contractor contractor = null;
        for (Contractor item : cache.getUser().getContractors()) {
            if (item.getContractorId() == contractorId) {
                contractor = item;
                break;
            }
        }
        return contractor;
    }

    public List<Long> getCorrespondentIdList() {
        List<Long> correspondentIdList = new ArrayList<>();
        for (Correspondent item : cache.getUser().getCorrespondents()) {
            correspondentIdList.add(item.getWalletId());
        }
        return correspondentIdList;
    }

    public void getWalletInfoByAccount(String accountNumber, RequestCallbackListener listener) {
        Disposable disposable = walletsMethods.getWalletInfoByAccount(preferences.getAuthToken(), preferences.getClientId(), accountNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void getContractorList(RequestCallbackListener listener) {
        Disposable disposable = dataMethods.getContractorList(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void addCorrespondent(RequestCallbackListener listener) {
        Disposable disposable = correspondentsMethods.addCorrespondents(preferences.getAuthToken(), preferences.getClientId(),
                Long.valueOf(correspondentModel.getAccountNumber()), correspondentModel.getCorrespondentName(), correspondentModel.getComment(), correspondentModel.getAccounts())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void editCorrespondent(RequestCallbackListener listener) {
        Disposable disposable = correspondentsMethods.editCorrespondents(preferences.getAuthToken(), preferences.getClientId(),
                Long.valueOf(correspondentModel.getAccountNumber()), correspondentModel.getCorrespondentName(), correspondentModel.getComment(), correspondentModel.getAccounts())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void getCorrespondentList(RequestCallbackListener listener) {
        if (cache.getUser().getCorrespondents().size() > 0
                && flagsManager.isFlagOff(Flags.CONTACTS)){
            listener.onPostExecute();
            listener.onSuccess();
            return;
        }

        Disposable disposable = correspondentsMethods.getListCorrespondents(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void getInfoContractor(long correspondentId, RequestCallbackListener listener) {
        Disposable disposable = correspondentsMethods.getInfoCorrespondent(preferences.getAuthToken(), preferences.getClientId(), correspondentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    public void deleteContractor(long correspondentId, RequestCallbackListener listener) {
        Disposable disposable = correspondentsMethods.deleteCorrespondent(preferences.getAuthToken(), preferences.getClientId(), correspondentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0){

            if (response instanceof ContractorListResponse) {
                cache.getUser().setContractors(((ContractorListResponse) response).getContractors());
                listener.onSuccess();

            } else if (response instanceof AddCorrespondentResponse) {
                flagsManager.turnOn(Flags.CONTACTS);
                listener.onSuccess();

            } else if (response instanceof EditCorrespondentResponse) {
                flagsManager.turnOn(Flags.CONTACTS);
                listener.onSuccess();

            } else if (response instanceof DeleteCorrespondentResponse) {
                flagsManager.turnOn(Flags.CONTACTS);
                listener.onSuccess();

            } else if (response instanceof ListCorrespondentsResponse) {
                flagsManager.turnOff(Flags.CONTACTS);
                cache.getUser().setCorrespondents(((ListCorrespondentsResponse) response).getCorrespondents());
                listener.onSuccess();

            } else if (response instanceof InfoCorrespondentResponse) {
                listener.onSuccess(response);

            } else if (response instanceof WalletInfoByAccountResponse) {
                listener.onSuccess(response);
            }
        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }
}
