package com.rusoft.chequepay.data.repository.registration.model;

import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.BankForeign;
import com.rusoft.chequepay.data.entities.wallets.BankRus;

import lombok.Getter;
import lombok.Setter;

public class BankSubModel {
    private RegistrationModel registrationModel;
    @Getter @Setter private String bik;
    @Getter @Setter private boolean isBikFound;
    @Getter @Setter private String name;
    @Getter @Setter private String corrAcc;
    @Getter @Setter private String swift;
    @Getter @Setter private String address;
    @Getter @Setter private String additionalInfo;
    @Getter @Setter private String accNum;
    @Getter @Setter private String inn;
    @Getter @Setter private String kpp;
    @Getter @Setter private String receiver;

    public BankSubModel(RegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public boolean isRequiredDataReceived() {
        if (!registrationModel.isInProfileEditingMode()) {
            if (registrationModel.isResident()){
                return bik != null && name != null && corrAcc != null && accNum != null &&
                        inn != null && kpp != null && receiver != null;
            } else {
                return name != null && address != null && accNum != null && receiver != null;
            }
        } else {
            if (registrationModel.isResident()){
                return isBikChanged(bik) || isAccNumChanged(accNum) ||
                        isInnChanged(inn) || isKppChanged(kpp) || isReceiverChanged(receiver);
            } else {
                return isNameChanged(name) || isSwiftChanged(swift) ||
                        isAddressChanged(address) || isAccNumChanged(accNum) ||
                        isReceiverChanged(receiver) || isAdditionalInfoChanged(additionalInfo);
            }
        }
    }

    //region Profile edit mode
    void updateByProfile() {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();

        BankAccount bankAccount = registrationModel.getComparedProfile().getBankAccount().getCopy();
        if (registrationModel.isResident()) {
            BankRus bankRus = bankAccount.getBankRus().getCopy();
            setName(bankRus.getName());
            setBik(bankRus.getBic());
            setCorrAcc(bankRus.getCorAccount());
            setInn(bankAccount.getBankInn());
            setKpp(bankAccount.getBankKpp());

        } else {
            BankForeign bankForeign = bankAccount.getBankForeign().getCopy();
            setName(bankForeign.getName());
            setBik(bankForeign.getBic());
            setSwift(bankForeign.getSwift());
            setAddress(bankForeign.getAddress());
            setAccNum(bankAccount.getNumber());
            setAdditionalInfo(bankForeign.getAdditionalInfo());
        }

        setAccNum(bankAccount.getNumber());
        setReceiver(bankAccount.getOwner());
    }

    private boolean isBikChanged(String newBik) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        BankAccount bankAccount = registrationModel.getComparedProfile().getBankAccount();
        String oldBik = registrationModel.isResident() ? bankAccount.getBankRus().getBic() : bankAccount.getBankForeign().getBic();
        if (oldBik == null) {
            return newBik != null;
        } else {
            return newBik != null && !oldBik.equals(newBik);
        }
    }

    private boolean isAccNumChanged(String newAccNum) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldAccNum = registrationModel.getComparedProfile().getBankAccount().getNumber();
        if (oldAccNum == null) {
            return newAccNum != null;
        } else {
            return newAccNum != null && !oldAccNum.equals(newAccNum);
        }
    }

    private boolean isInnChanged(String newInn) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldInn = registrationModel.getComparedProfile().getBankAccount().getBankInn();
        if (oldInn == null) {
            return newInn != null;
        } else {
            return newInn != null && !oldInn.equals(newInn);
        }
    }

    private boolean isKppChanged(String newKpp) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldKpp = registrationModel.getComparedProfile().getBankAccount().getBankKpp();
        if (oldKpp == null) {
            return newKpp != null;
        } else {
            return newKpp != null && !oldKpp.equals(newKpp);
        }
    }

    private boolean isReceiverChanged(String newReceiver) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldReceiver = registrationModel.getComparedProfile().getBankAccount().getOwner();
        if (oldReceiver == null) {
            return newReceiver != null;
        } else {
            return newReceiver != null && !oldReceiver.equals(newReceiver);
        }
    }

    private boolean isNameChanged(String newName) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldName = registrationModel.getComparedProfile().getBankAccount().getBankForeign().getName();
        if (oldName == null) {
            return newName != null;
        } else {
            return newName != null && !oldName.equals(newName);
        }
    }

    private boolean isSwiftChanged(String newSwift) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldSwift = registrationModel.getComparedProfile().getBankAccount().getBankForeign().getSwift();
        if (oldSwift == null) {
            return newSwift != null;
        } else {
            return newSwift != null && !oldSwift.equals(newSwift);
        }
    }

    private boolean isAddressChanged(String newAddress) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldAddress = registrationModel.getComparedProfile().getBankAccount().getBankForeign().getAddress();
        if (oldAddress == null) {
            return newAddress != null;
        } else {
            return newAddress != null && !oldAddress.equals(newAddress);
        }
    }

    private boolean isAdditionalInfoChanged(String newAdditionalInfo) {
        if (registrationModel.getComparedProfile() == null) throw new InternalError();
        String oldAdditionalInfo = registrationModel.getComparedProfile().getBankAccount().getBankForeign().getAdditionalInfo();
        if (oldAdditionalInfo == null) {
            return newAdditionalInfo != null;
        } else {
            return newAdditionalInfo != null && !oldAdditionalInfo.equals(newAdditionalInfo);
        }
    }

    //endregion


    @Override
    public String toString() {
        return "BankSubModel{" +
                "bik='" + bik + '\'' +
                ", isBikFound=" + isBikFound +
                ", name='" + name + '\'' +
                ", corrAcc='" + corrAcc + '\'' +
                ", swift='" + swift + '\'' +
                ", address='" + address + '\'' +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", accNum='" + accNum + '\'' +
                ", inn='" + inn + '\'' +
                ", kpp='" + kpp + '\'' +
                ", receiver='" + receiver + '\'' +
                '}';
    }
}
