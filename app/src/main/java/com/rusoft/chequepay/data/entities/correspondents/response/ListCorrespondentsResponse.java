package com.rusoft.chequepay.data.entities.correspondents.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ListCorrespondentsResponse extends BaseResponse {
    @SerializedName("correspondents")
    @Expose @Getter @Setter
    private List<Correspondent> correspondents = null;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private long currentTime;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;
}
