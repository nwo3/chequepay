package com.rusoft.chequepay.data.repository.transactions.deposit;


import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.application.purchasefromissuer_7.response.PurchaseFromIssuerCreateResponse;
import com.rusoft.chequepay.data.entities.tariffs.response.GetTariffResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.ApplicationMethods;
import com.rusoft.chequepay.data.network.methods.InfoMethods;
import com.rusoft.chequepay.data.network.methods.TariffsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class DepositRepository extends BaseRepository implements IDepositRepository {

    private CacheManager cache;
    private Preferences preferences;
    private InfoMethods infoMethods;
    private TariffsMethods tariffMethods;
    private ApplicationMethods applicationMethods;
    private UserRepository userRepository;

    @Getter private DepositModel model;

    @Inject
    public DepositRepository(CacheManager cache, Preferences preferences, InfoMethods infoMethods, TariffsMethods tariffsMethods, ApplicationMethods applicationMethods, UserRepository userRepository) {
        this.cache = cache;
        this.preferences = preferences;

        this.infoMethods = infoMethods;
        this.tariffMethods = tariffsMethods;
        this.applicationMethods = applicationMethods;
        this.userRepository = userRepository;

        this.compositeDisposable = new CompositeDisposable();
        this.model = cache.getDepositModel();
    }

    @Override
    public void getTariff(RequestCallbackListener listener) {
        if (model.getAccount() == null || model.getIssuer() == null){
            return;
        }

        Disposable disposable =  tariffMethods.getTariff(preferences.getAuthToken(), preferences.getClientId(), model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void createDeposit(String otp, RequestCallbackListener listener) {
        Disposable disposable =  applicationMethods.createDeposit(preferences.getAuthToken(), preferences.getClientId(), otp, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof GetTariffResponse){
                double tariff = ((GetTariffResponse) response).getTariff();
                listener.onSuccess(tariff);

            } else if (response instanceof PurchaseFromIssuerCreateResponse){
                PurchaseFromIssuerCreateResponse res = (PurchaseFromIssuerCreateResponse) response;
                model.setApplicationCoreId(res.getApplicationCoreId());
                model.setFlowToken(res.getFlowToken());
                flagsManager.turnOn(Flags.ACCOUNTS);
                listener.onSuccess();
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    @Override
    public void clearModel(){
        cache.clearDepositModel();
    }
}
