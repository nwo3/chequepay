package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.cheques.request.GetChequeRequest;
import com.rusoft.chequepay.data.entities.cheques.response.GetChequeResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Cheques : Операции с электронными чеками
 */
public class ChequesMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public ChequesMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<GetChequeResponse> getCheque(String token, long senderId, String accountNumber) {
        return chequepayApi.getCheque(token, GetChequeRequest.builder()
                .senderId(senderId)
                .accountNumber(accountNumber)
                .language(Locale.getDefault().getLanguage())
                .currentTime(System.currentTimeMillis())
                .build());
    }
}
