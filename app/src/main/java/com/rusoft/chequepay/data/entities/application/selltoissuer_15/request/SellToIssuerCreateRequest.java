package com.rusoft.chequepay.data.entities.application.selltoissuer_15.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.application.NotResidentBankDetails;
import com.rusoft.chequepay.data.entities.application.ResidentBankDetails;

import lombok.Builder;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
public class SellToIssuerCreateRequest extends BodyRequest {

    @SerializedName("accountNumber")
    @Expose @Setter
    public String accountNumber;

    @SerializedName("amount")
    @Expose @Setter
    public Double amount;

    @SerializedName("applicationType")
    @Expose @Setter
    public String applicationType;

    @SerializedName("commission")
    @Expose @Setter
    public Double commission;

    @SerializedName("currency")
    @Expose @Setter
    public String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("notResidentBankDetails")
    @Expose @Setter
    public NotResidentBankDetails notResidentBankDetails;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("paymentType")
    @Expose @Setter
    public int paymentType;

    @SerializedName("price")
    @Expose @Setter
    public Double price;

    @SerializedName("receiverId")
    @Expose @Setter
    public String receiverId;

    @SerializedName("refundMode")
    @Expose @Setter
    public int refundMode;

    @SerializedName("residentBankDetails")
    @Expose @Setter
    public ResidentBankDetails residentBankDetails;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

}
