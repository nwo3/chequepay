package com.rusoft.chequepay.data.repository.registration;


import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.check.response.CheckLoginParamsResponse;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.entities.info.response.GetBankInfoResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationConfirmResponse;
import com.rusoft.chequepay.data.entities.wallets.response.RegistrationResponse;
import com.rusoft.chequepay.data.entities.wallets.response.WalletEditResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.InfoMethods;
import com.rusoft.chequepay.data.network.methods.WalletsMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

public class RegistrationRepository extends BaseRepository implements IRegistrationRepository {

    private CacheManager cache;
    private Preferences preferences;
    private InfoMethods infoMethods;
    private WalletsMethods walletsMethods;

    @Getter private RegistrationModel model;

    @Inject
    public RegistrationRepository(CacheManager cache, Preferences preferences, InfoMethods infoMethods, WalletsMethods walletsMethods) {
        this.cache = cache;
        this.preferences = preferences;
        this.infoMethods = infoMethods;
        this.walletsMethods = walletsMethods;

        model = cache.getRegistrationModel();
    }

    public List<Country> getCountries() {
        return cache.getCountries();
    }

    public List<Region> getRegions() {
        return cache.getRegions();
    }

    @Override
    public void getCountriesAndRegions(RequestCallbackListener listener){
        if (getCountries() != null && getRegions() != null) {
            listener.onSuccess();
            return;
        }

        Disposable disposable = Single.zip(
                infoMethods.getCountries(),
                infoMethods.getRegions(),
                (countryListResponse, regionListResponse) -> {
                    Integer error = ErrorUtils.getError(countryListResponse, regionListResponse);
                    if (error == 0){
                        cache.setCountries(countryListResponse.getCountries());
                        cache.setRegions(regionListResponse.getRegionList());
                        return true;

                    } else {
                        listener.onError(new ResponseError(error));
                        return false;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(result -> listener.onSuccess(), listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void checkLoginParams(String phone, String email, RequestCallbackListener listener){
        Disposable disposable = infoMethods.checkLoginParams(phone, email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void getBankInfo(String bic, RequestCallbackListener listener){
        Disposable disposable = infoMethods.getBankInfo(bic)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void registration(RequestCallbackListener listener){
        Disposable disposable = walletsMethods.registration(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void registrationConfirm (String flowToken, String otp, RequestCallbackListener listener){
        Disposable disposable = walletsMethods.registrationConfirm(flowToken, otp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void editProfile (String otp, RequestCallbackListener listener){
        Disposable disposable = walletsMethods.editProfile(otp, preferences.getAuthToken(), preferences.getClientId(), model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0){

            if (response instanceof GetBankInfoResponse) {
                listener.onSuccess(response);

            } else if (response instanceof RegistrationResponse) {
                model.setFlowToken(((RegistrationResponse) response).getFlowToken());
                listener.onSuccess();

            } else if (response instanceof RegistrationConfirmResponse) {
                listener.onSuccess();

            } else if (response instanceof WalletEditResponse) {
                model.setFlowToken(((WalletEditResponse) response).getFlowToken());
                listener.onSuccess();
                if (model.isActionDone()) flagsManager.turnOn(Flags.PROFILE);

            } else if (response instanceof CheckLoginParamsResponse) {
                listener.onSuccess();
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }

    /**
     * primary mode Registration (true)
     * secondary mode Profile edit (false)
     */
    public void setRegistrationMode(boolean enabled) {
        model.setInProfileEditingMode(!enabled);
        if (!enabled) model.updateModelByProfile(cache.getUser().getFullProfile());
    }

    @Override
    public void clearModel(){
        cache.clearRegistrationModel();
    }
}
