package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.wallets.Account;
import com.rusoft.chequepay.data.entities.wallets.Address;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.Company;
import com.rusoft.chequepay.data.entities.wallets.Person;
import com.rusoft.chequepay.data.entities.wallets.UrAddress;

import lombok.Builder;
import lombok.Setter;

@Builder
public class WalletEditRequest extends BodyRequest {

    @SerializedName("account")
    @Expose @Setter
    public Account account;

    @SerializedName("bankAccount")
    @Expose @Setter
    public BankAccount bankAccount;

    @SerializedName("cashAvailable")
    @Expose @Setter
    public Boolean cashAvailable;

    @SerializedName("clientId")
    @Expose @Setter
    public Long clientId;

    @SerializedName("company")
    @Expose @Setter
    public Company company;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("factAddress")
    @Expose @Setter
    public Address factAddress;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("paymentType")
    @Expose @Setter
    public Integer paymentType;

    @SerializedName("person")
    @Expose @Setter
    public Person person;

    @SerializedName("regType")
    @Expose @Setter
    public Integer regType;

    @SerializedName("residencyCountryId")
    @Expose @Setter
    public Integer residencyCountryId;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

    @SerializedName("urAddress")
    @Expose @Setter
    public UrAddress urAddress;

}
