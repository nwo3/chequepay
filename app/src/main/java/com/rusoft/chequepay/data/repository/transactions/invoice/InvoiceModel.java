package com.rusoft.chequepay.data.repository.transactions.invoice;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class InvoiceModel extends BaseTransactionModel {
    @Getter @Setter Account account;
    @Getter @Setter String currency;
    @Getter @Setter boolean inputModeManual;
    @Getter @Setter Correspondent chosenContact;
    String contractorWalletIdManual, contractorWalletIdChosen;

    @Getter @Setter Integer lifetime; //срок действия
    @Getter @Setter Double amount;
    @Getter @Setter String comment;

    //region Данные для расширенной версии выставления счёта. На данный момент не используются.
    @Getter @Setter String contractNum;
    @Getter @Setter Long date;
    //endregion

    @Getter @Setter String applicationCoreId;
    @Getter @Setter boolean isTransactionDone;

    public String getContractorWalletId() {
        return inputModeManual ? contractorWalletIdManual : contractorWalletIdChosen;
    }

    public void setContractorWalletId(String walletId) {
        if (inputModeManual) {
            contractorWalletIdManual = walletId;
        } else {
            contractorWalletIdChosen = walletId;
        }
    }

    public boolean isAllDataReceived() {
        return account != null && getContractorWalletId() != null
                && lifetime != null && amount != null
                && comment != null && !comment.isEmpty();
        //&& contractNum != null && date != null
    }
}
