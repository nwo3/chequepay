package com.rusoft.chequepay.data.service.update;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ChequepayApiModule.class})
public interface UpdateDataServiceComponent {
    void inject(UpdateDataService updateDataService);
}
