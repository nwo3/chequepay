package com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ItemOperationResponse extends BaseResponse {
    @SerializedName("actHash")
    @Expose @Getter @Setter
    private String actHash;
    
    @SerializedName("actId")
    @Expose @Getter @Setter
    private long actId;
    
    @SerializedName("amount")
    @Expose @Getter @Setter
    private String amount;
    
    @SerializedName("cheque")
    @Expose @Getter @Setter
    private Cheque cheque; 
    
    @SerializedName("chequeNumber")
    @Expose @Getter @Setter
    private String chequeNumber;
    
    @SerializedName("comment")
    @Expose @Getter @Setter
    private String comment;
    
    @SerializedName("contractorAccount")
    @Expose @Getter @Setter
    private String contractorAccount;
    
    @SerializedName("contractorId")
    @Expose @Getter @Setter
    private long contractorId;

    @SerializedName("contractorName")
    @Expose @Getter @Setter
    private String contractorName;

    @SerializedName("currency")
    @Expose @Getter @Setter
    private long currency;

    @SerializedName("currentTime")
    @Expose @Getter @Setter
    private long currentTime;

    @SerializedName("date")
    @Expose @Getter @Setter
    private long date;

    @SerializedName("documents")
    @Expose @Getter @Setter
    private List<Document> documents = null;

    @SerializedName("firstViewed")
    @Expose @Getter @Setter
    private Boolean firstViewed;

    @SerializedName("flowToken")
    @Expose @Getter @Setter
    private String flowToken;

    @SerializedName("issuerCommission")
    @Expose @Getter @Setter
    private String issuerCommission;

    @SerializedName("mediatorCommission")
    @Expose @Getter @Setter
    private String mediatorCommission;

    @SerializedName("notResidentBankDetails")
    @Expose @Getter @Setter
    private NotResidentBankDetails notResidentBankDetails;

    @SerializedName("operationTypeName")
    @Expose @Getter @Setter
    private String operationTypeName;

    @SerializedName("operationUid")
    @Expose @Getter @Setter
    private String operationUid;

    @SerializedName("payerBankId")
    @Expose @Getter @Setter
    private long payerBankId;

    @SerializedName("payerBankName")
    @Expose @Getter @Setter
    private String payerBankName;

    @SerializedName("phone")
    @Expose @Getter @Setter
    private String phone;

    @SerializedName("price")
    @Expose @Getter @Setter
    private String price;

    @SerializedName("providerCode")
    @Expose @Getter @Setter
    private long providerCode;

    @SerializedName("receiverAccount")
    @Expose @Getter @Setter
    private String receiverAccount;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    private long receiverId;

    @SerializedName("receiverName")
    @Expose @Getter @Setter
    private String receiverName;

    @SerializedName("refundMode")
    @Expose @Getter @Setter
    private String refundMode;

    @SerializedName("residentBankDetails")
    @Expose @Getter @Setter
    private ResidentBankDetails residentBankDetails;

    @SerializedName("senderAccount")
    @Expose @Getter @Setter
    private String senderAccount;

    @SerializedName("senderId")
    @Expose @Getter @Setter
    private long senderId;

    @SerializedName("senderName")
    @Expose @Getter @Setter
    private String senderName;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;

    @SerializedName("stateId")
    @Expose @Getter @Setter
    private long stateId;

    @SerializedName("stateName")
    @Expose @Getter @Setter
    private String stateName;

    @SerializedName("transferAmount")
    @Expose @Getter @Setter
    private String transferAmount;

    @Getter @Setter
    private Document document;
}
