package com.rusoft.chequepay.data.entities.clientdata.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class BankAccount {
    @SerializedName("bankForeign")
    @Expose @Setter @Getter
    private BankForeign bankForeign;

    @SerializedName("bankInn")
    @Expose @Setter @Getter
    private String bankInn;

    @SerializedName("bankKpp")
    @Expose @Setter @Getter
    private String bankKpp;

    @SerializedName("bankRus")
    @Expose @Setter @Getter
    private BankRus bankRus;

    @SerializedName("number")
    @Expose @Setter @Getter
    private String number;

    @SerializedName("owner")
    @Expose @Setter @Getter
    private String owner;

    @SerializedName("ownerEn")
    @Expose @Setter @Getter
    private String ownerEn;
}
