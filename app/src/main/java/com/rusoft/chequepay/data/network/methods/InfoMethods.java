package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.check.request.CheckLoginParamsRequest;
import com.rusoft.chequepay.data.entities.check.response.CheckLoginParamsResponse;
import com.rusoft.chequepay.data.entities.info.request.BankIssuersListRequest;
import com.rusoft.chequepay.data.entities.info.request.GetBankInfoRequest;
import com.rusoft.chequepay.data.entities.info.response.BankIssuersListResponse;
import com.rusoft.chequepay.data.entities.info.response.CountryListResponse;
import com.rusoft.chequepay.data.entities.info.response.GetBankInfoResponse;
import com.rusoft.chequepay.data.entities.info.response.RegionListResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Info : Info Open Api Controller
 */
public class InfoMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public InfoMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<BankIssuersListResponse> getBankIssuersList(long senderId, String currency) {
        return chequepayApi.getBankIssuersList(new BankIssuersListRequest(currency, senderId));
    }

    public Single<GetBankInfoResponse> getBankInfo(String bic) {
        return chequepayApi.getBankInfo(new GetBankInfoRequest(bic));
    }

    public Single<CountryListResponse> getCountries() {
        return chequepayApi.getCountries(new BodyRequest());
    }

    public Single<RegionListResponse> getRegions() { //returns only russian regions!
        return chequepayApi.getRegions(new BodyRequest());
    }

    public Single<CheckLoginParamsResponse> checkLoginParams(String phone, String email) {
        return chequepayApi.checkLoginParams(CheckLoginParamsRequest.builder()
                .phone(phone)
                .email(email)
                .build());
    }
}