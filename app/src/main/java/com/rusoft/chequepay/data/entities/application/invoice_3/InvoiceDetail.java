package com.rusoft.chequepay.data.entities.application.invoice_3;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Setter;

/** Используется только для расширенной версии запроса */
public class InvoiceDetail {

    @SerializedName("goodsName")
    @Expose @Setter
    private String goodsName;

    @SerializedName("goodsNameEn")
    @Expose @Setter
    private String goodsNameEn;

    @SerializedName("goodsPrice")
    @Expose @Setter
    private String goodsPrice;

    @SerializedName("measurementId")
    @Expose @Setter
    private Long measurementId;

    @SerializedName("ndsAmount")
    @Expose @Setter
    private String ndsAmount;

    @SerializedName("ndsId")
    @Expose @Setter
    private Long ndsId;

    @SerializedName("quant")
    @Expose @Setter
    private Long quant;

    @SerializedName("rowNumber")
    @Expose @Setter
    private Long rowNumber;

    @SerializedName("summ")
    @Expose @Setter
    private String summ;

    @SerializedName("totalValue")
    @Expose @Setter
    private String totalValue;

}
