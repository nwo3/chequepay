package com.rusoft.chequepay.data.repository.registration.model;

import lombok.Getter;
import lombok.Setter;

public class PassportSubModel {
    private RegistrationModel registrationModel;
    @Getter @Setter private String series;
    @Getter @Setter private String number;
    @Getter @Setter private String fullNumber;
    @Getter @Setter private String issuer;
    @Getter @Setter private Long issueDate;
    @Getter @Setter private String departmentCode;

    public PassportSubModel(RegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public boolean isRequiredDataReceived() {
        return number != null && (!registrationModel.isResident() || series != null) && issuer != null && issueDate != null;
    }

    void updateByProfile() {
        if (registrationModel.comparedProfile == null) throw new InternalError();
        if (registrationModel.comparedProfile.getPerson() == null) return; //Organization not implemented

        com.rusoft.chequepay.data.entities.wallets.Passport passport = registrationModel.comparedProfile.getPerson().getPassport();
        setFullNumber(passport.getNumber());
        setIssuer(passport.getIssuer());
        setIssueDate(passport.getIssueDate());
        setDepartmentCode(passport.getDepartmentCode());
    }

    @Override
    public String toString() {
        return "PassportSubModel{" +
                "series='" + series + '\'' +
                ", number='" + number + '\'' +
                ", fullNumber='" + fullNumber + '\'' +
                ", issuer='" + issuer + '\'' +
                ", issueDate=" + issueDate +
                ", departmentCode='" + departmentCode + '\'' +
                '}';
    }
}
