package com.rusoft.chequepay.data.repository.transactions.withdrawal;

import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IWithdrawalRepository {

    void updateEmitterData(RequestCallbackListener listener);
    void getTariff(RequestCallbackListener listener);
    void createWithdraw(String otp, RequestCallbackListener listener);

    void clearModel();

}
