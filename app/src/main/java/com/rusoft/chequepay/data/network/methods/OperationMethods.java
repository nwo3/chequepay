package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.operations.request.GetAccountStatementRequest;
import com.rusoft.chequepay.data.entities.operations.request.GetItemOperationRequest;
import com.rusoft.chequepay.data.entities.operations.request.SortField;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.OperationListResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Operations : Реестр + Сведения об операциях
 */
public class OperationMethods {

    private ChequepayApi chequepayApi;

    @Inject
    public OperationMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<OperationListResponse> getAccountStatementShort(String token, long senderId) {
        List<SortField> sortFields = new ArrayList<>();
        SortField sortFieldDate = new SortField("DATE", "DESC");
        sortFields.add(sortFieldDate);

        Calendar calendar = Calendar.getInstance();
        long dateEnd = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH, -1);
        long dateStart = calendar.getTimeInMillis();

        GetAccountStatementRequest getAccountStatementRequest = GetAccountStatementRequest.builder()
                .sortField(sortFields)
                .currentTime(System.currentTimeMillis())
                .dateStart(dateStart)
                .dateFinish(dateEnd)
                .senderId(senderId)
                .language(Locale.getDefault().getLanguage())
                .pageNumber(BusinessConstants.PAGE_NUMBER_START) // TODO PAGINATION
                .currency(new ArrayList<>())
                .build();

        return chequepayApi.getAccountStatement(token, getAccountStatementRequest);
    }

    public Single<OperationListResponse> getAccountStatement(String accountName, Long dateStart, Long dateEnd, Long contractorId, List<String> currency, List<Integer> operationTypeList, String token, long senderId) {
        List<SortField> sortFields = new ArrayList<>();
        SortField sortFieldDate = new SortField("DATE", "DESC");
        sortFields.add(sortFieldDate);

        GetAccountStatementRequest getAccountStatementRequest = GetAccountStatementRequest.builder()
                .accountNumber(accountName)
                .sortField(sortFields)
                .currentTime(System.currentTimeMillis())
                .dateStart(dateStart)
                .dateFinish(dateEnd)
                .senderId(senderId)
                .language(Locale.getDefault().getLanguage())
                .pageNumber(BusinessConstants.PAGE_NUMBER_START) // TODO PAGINATION
                .currency(currency)
                .operationTypeList(operationTypeList)
                .contractorId(contractorId)
                .build();

        return chequepayApi.getAccountStatement(token, getAccountStatementRequest);
    }

    public Single<ItemOperationResponse> getItemOperation(String token, long senderId, String operationUid, long operationType) {

        GetItemOperationRequest getItemOperationRequest = GetItemOperationRequest.builder()
                .operationUid(operationUid)
                .operationType(operationType)
                .currentTime(System.currentTimeMillis())
                .language(Locale.getDefault().getLanguage())
                .senderId(senderId)
                .build();

        return chequepayApi.getItemOperation(token, getItemOperationRequest);
    }
}
