package com.rusoft.chequepay.data.repository.transactions.invoice;


import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.templates.InvoiceTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;

public interface IInvoiceRepository {

    void getWalletInfo(ValidationPatterns dataType, String data, RequestCallbackListener listener);
    void getWalletInfoByAccount(String accountId, RequestCallbackListener listener);
    void createInvoice(String otp, RequestCallbackListener listener);

    void updateInvoicesList(boolean forced, RequestCallbackListener listener);
    void getInvoiceDetail(String applicationCoreId, String applicationType, RequestCallbackListener listener);
    void refuseInvoice(String applicationCoreId, String applicationType, RequestCallbackListener listener);

    void applyTemplate(InvoiceTemplate template);
    void clearModel();

}
