package com.rusoft.chequepay.data.entities.wallets.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class SecurityEditRequest extends BodyRequest {

    @SerializedName("allowedIpAddresses")
    @Expose @Setter
    public List<String> allowedIpAddresses;

    @SerializedName("authByOtp")
    @Expose @Setter
    public Boolean authByOtp;

    @SerializedName("clientId")
    @Expose @Setter
    public Long clientId;
    
    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

    @SerializedName("smsNotification")
    @Expose @Setter
    public Boolean smsNotification;

}
