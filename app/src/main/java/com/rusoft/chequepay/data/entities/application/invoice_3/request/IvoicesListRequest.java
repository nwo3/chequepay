package com.rusoft.chequepay.data.entities.application.invoice_3.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;
import com.rusoft.chequepay.data.entities.operations.request.SortField;

import java.util.List;

import lombok.Builder;
import lombok.Setter;

@Builder
public class IvoicesListRequest extends BodyRequest {

    @SerializedName("applicationType")
    @Expose @Setter
    public String applicationType;

    @SerializedName("contractorId")
    @Expose @Setter
    public Long contractorId;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("dateFinish")
    @Expose @Setter
    public Long dateFinish;

    @SerializedName("dateStart")
    @Expose @Setter
    public Long dateStart;

    @SerializedName("language")
    @Expose @Setter
    public String language;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("pageNumber")
    @Expose @Setter
    public Integer pageNumber;

    @SerializedName("paymentType")
    @Expose @Setter
    public String paymentType;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

    @SerializedName("senderTab")
    @Expose @Setter
    public Boolean senderTab;

    @SerializedName("sortField")
    @Expose @Setter
    public List<SortField> sortField;

    @SerializedName("state")
    @Expose @Setter
    public String state;

}
