package com.rusoft.chequepay.data.entities.application.invoice_3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class Application implements Serializable{

    @SerializedName("accountNumber")
    @Expose @Getter @Setter
    public String accountNumber;

    @SerializedName("amount")
    @Expose @Getter @Setter
    public Double amount;

    @SerializedName("applicationCoreId")
    @Expose @Getter @Setter
    public String applicationCoreId;

    @SerializedName("applicationType")
    @Expose @Getter @Setter
    public String applicationType;

    @SerializedName("comment")
    @Expose @Getter @Setter
    public String comment;

    @SerializedName("commentEn")
    @Expose @Getter @Setter
    public String commentEn;

    @SerializedName("currency")
    @Expose @Getter @Setter
    public Integer currency;

    @SerializedName("date")
    @Expose @Getter @Setter
    public Long date;

    @SerializedName("invoiceDocId")
    @Expose @Getter @Setter
    public Integer invoiceDocId;

    @SerializedName("newApplication")
    @Expose @Getter @Setter
    public Boolean newApplication;

    @SerializedName("number")
    @Expose @Getter @Setter
    public String number;

    @SerializedName("payOrderDate")
    @Expose @Getter @Setter
    public Long payOrderDate;

    @SerializedName("payOrderNum")
    @Expose @Getter @Setter
    public Integer payOrderNum;

    @SerializedName("paymentType")
    @Expose @Getter @Setter
    public Integer paymentType;

    @SerializedName("paymentTypeName")
    @Expose @Getter @Setter
    public String paymentTypeName;

    @SerializedName("receiverId")
    @Expose @Getter @Setter
    public Long receiverId;

    @SerializedName("receiverName")
    @Expose @Getter @Setter
    public String receiverName;

    @SerializedName("senderId")
    @Expose @Getter @Setter
    public Long senderId;

    @SerializedName("senderName")
    @Expose @Getter @Setter
    public String senderName;

    @SerializedName("stateId")
    @Expose @Getter @Setter
    public Long stateId;

    @SerializedName("stateName")
    @Expose @Getter @Setter
    public String stateName;

    public boolean isNew() {
        return getStateId() == 0;
    }
}
