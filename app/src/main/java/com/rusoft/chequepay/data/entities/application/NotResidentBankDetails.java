package com.rusoft.chequepay.data.entities.application;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Setter;

@Builder
public class NotResidentBankDetails implements Serializable {

    @SerializedName("bankAddress")
    @Expose @Setter
    private String bankAddress;

    @SerializedName("bankName")
    @Expose @Setter
    private String bankName;

    @SerializedName("bicInternational")
    @Expose @Setter
    private String bicInternational;

    @SerializedName("iban")
    @Expose @Setter
    private String iban;

    @SerializedName("otherBankDetails")
    @Expose @Setter
    private String otherBankDetails;

    @SerializedName("registrationCode")
    @Expose @Setter
    private String registrationCode;

    @SerializedName("swift")
    @Expose @Setter
    private String swift;

}
