package com.rusoft.chequepay.data.entities.signin.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Setter;
import lombok.ToString;

@ToString(doNotUseGetters = true)
public class ConfirmAuthRequest extends BodyRequest {
    @SerializedName("flowToken")
    @Expose @Setter
    private String flowToken;

    @SerializedName("login")
    @Expose @Setter
    private String login;

    @SerializedName("otp")
    @Expose @Setter
    private String otp;

    @SerializedName("password")
    @Expose @Setter
    private String password;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

    public ConfirmAuthRequest(String flowToken, String login, String otp, String password, long senderId) {
        this.flowToken = flowToken;
        this.login = login;
        this.otp = otp;
        this.password = password;
        this.senderId = senderId;
    }


}
