package com.rusoft.chequepay.data.entities.notifications.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class HighPriorityUpdatedEntities {
    /*
        case unsupported = "-1"
        case appBuyChequeSender = "1"
        case appBuyChequeReceiver = "2"
        case appSellChequeSender = "3"
        case appSellChequeReceiver = "4"
        case appPayChequeSender = "5"
        case appPayChequeReceiver = "6"
        case appIssuerCheque = "7"
        case appVerifyCheque = "8"
        case invoceReceiver = "9"
        case invoceSender = "10"
        case operationSender = "11"
        case operationReceiver = "12"
        case account = "13"
        case issuerSubAccount = "14"
        case inkassoReceiver = "15"
        case inkassoSender = "16"
        case accountOperation = "17"
     */

    @SerializedName("9")
    @Expose @Setter @Getter
    private Integer invoceReceiver = 0;

    @SerializedName("12")
    @Expose @Setter @Getter
    private Integer operationReceiver = 0;

}
