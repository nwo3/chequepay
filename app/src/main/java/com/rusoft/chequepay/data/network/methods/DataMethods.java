package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.clientdata.request.ContractorListRequest;
import com.rusoft.chequepay.data.entities.clientdata.response.ContractorListResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * API:
 * Data : Данные клиентов
 */
public class DataMethods {
    private ChequepayApi chequepayApi;

    @Inject
    public DataMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<ContractorListResponse> getContractorList(String token, long senderId) {
        return chequepayApi.getContractorList(token, ContractorListRequest.builder()
                .currentTime(System.currentTimeMillis())
                .language(Locale.getDefault().getLanguage())
                .senderId(senderId).build());
    }
}
