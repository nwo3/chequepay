package com.rusoft.chequepay.data.network.methods;

import com.rusoft.chequepay.data.entities.notifications.request.GetNotificationRequest;
import com.rusoft.chequepay.data.entities.notifications.request.RemoveNotificationRequest;
import com.rusoft.chequepay.data.entities.notifications.response.GetNotificationsResponse;
import com.rusoft.chequepay.data.entities.notifications.response.RemoveNotificationResponse;
import com.rusoft.chequepay.data.network.ChequepayApi;

import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;

public class NotificationMethods {
    private ChequepayApi chequepayApi;

    @Inject
    public NotificationMethods(ChequepayApi chequepayApi) {
        this.chequepayApi = chequepayApi;
    }

    public Single<GetNotificationsResponse> getNotification(String token, long senderId, long processId) {
        return chequepayApi.getNotification(token, GetNotificationRequest.builder()
                .senderId(senderId)
                .language(Locale.getDefault().getLanguage())
                .currentTime(System.currentTimeMillis())
                .processId(processId)
                .timeZone("Europe/Moscow")
                .build());
    }

    public Single<RemoveNotificationResponse> removeNotification(String token, long senderId) {
        return chequepayApi.removeNotification(token, RemoveNotificationRequest.builder()
                .senderId(senderId)
                .currentTime(System.currentTimeMillis())
                .build());
    }
}
