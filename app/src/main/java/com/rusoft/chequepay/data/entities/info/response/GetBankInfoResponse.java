package com.rusoft.chequepay.data.entities.info.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BaseResponse;

import lombok.Getter;
import lombok.Setter;


public class GetBankInfoResponse extends BaseResponse {

    @SerializedName("korAccount")
    @Expose @Getter @Setter
    private String korAccount;

    @SerializedName("name")
    @Expose @Getter @Setter
    private String name;

    @SerializedName("signature")
    @Expose @Getter @Setter
    private String signature;


}
