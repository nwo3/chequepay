package com.rusoft.chequepay.data.entities.accounts.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class OpenAccountRequest extends BodyRequest {
    @SerializedName("accountName")
    @Expose @Setter
    private String accountName;

    @SerializedName("activeAccount")
    @Expose @Setter
    private Boolean activeAccount;

    @SerializedName("bankId")
    @Expose @Setter
    private String bankId;

    @SerializedName("brokerId")
    @Expose @Setter
    private Integer brokerId;

    @SerializedName("clientId")
    @Expose @Setter
    private Integer clientId;

    @SerializedName("contractDate")
    @Expose @Setter
    private Integer contractDate;

    @SerializedName("contractNumber")
    @Expose @Setter
    private String contractNumber;

    @SerializedName("currency")
    @Expose @Setter
    private String currency;

    @SerializedName("currentTime")
    @Expose @Setter
    private Long currentTime;

    @SerializedName("issuerId")
    @Expose @Setter
    private String issuerId;

    @SerializedName("operatorLogin")
    @Expose @Setter
    private String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    private Long senderId;

    @SerializedName("specialProperty")
    @Expose @Setter
    private String specialProperty;
}
