package com.rusoft.chequepay.data.entities.application.invoice_3.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class InvoiceUpdateRequest extends BodyRequest {

    @SerializedName("applicationCoreId")
    @Expose @Setter
    public String applicationCoreId;

    @SerializedName("applicationType")
    @Expose @Setter
    public String applicationType;

    @SerializedName("comment")
    @Expose @Setter
    public String comment;

    @SerializedName("comment2")
    @Expose @Setter
    public String comment2;

    @SerializedName("currentTime")
    @Expose @Setter
    public Long currentTime;

    @SerializedName("operatorLogin")
    @Expose @Setter
    public String operatorLogin;

    @SerializedName("senderId")
    @Expose @Setter
    public Long senderId;

    @SerializedName("state")
    @Expose @Setter
    public String state;

}
