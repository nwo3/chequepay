package com.rusoft.chequepay.data.entities.info;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class BankIssuer {

    @SerializedName("bankDeleted")
    @Expose @Getter @Setter
    private Boolean bankDeleted;

    @SerializedName("bankEnabled")
    @Expose @Getter @Setter
    private Boolean bankEnabled;

    @SerializedName("bankShortName")
    @Expose @Getter @Setter
    private String bankShortName;

    @SerializedName("bankShortNameEn")
    @Expose @Getter @Setter
    private String bankShortNameEn;

    @SerializedName("bankWalletId")
    @Expose @Getter @Setter
    private String bankWalletId;

    @SerializedName("issuerDeleted")
    @Expose @Getter @Setter
    private Boolean issuerDeleted;

    @SerializedName("issuerEnabled")
    @Expose @Getter @Setter
    private Boolean issuerEnabled;

    @SerializedName("issuerShortName")
    @Expose @Getter @Setter
    private String issuerShortName;

    @SerializedName("issuerShortNameEn")
    @Expose @Getter @Setter
    private String issuerShortNameEn;

    @SerializedName("issuerWalletId")
    @Expose @Getter @Setter
    private String issuerWalletId;

    @Override
    public String toString() {
        return issuerShortName;
    }

    public Bank getBank(){
        return new Bank(bankDeleted, bankEnabled, bankShortName, bankShortNameEn, bankWalletId);
    }

    public Issuer getIssuer(){
        return new Issuer(issuerDeleted, issuerEnabled, issuerShortName, issuerShortNameEn, issuerWalletId);
    }
}
