package com.rusoft.chequepay.data.entities.application.invoice_3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Document {

    @SerializedName("id")
    @Expose @Getter @Setter
    public String id;

    @SerializedName("idDownload")
    @Expose @Getter @Setter
    public Long idDownload;

    @SerializedName("link")
    @Expose @Getter @Setter
    public Boolean link;

    @SerializedName("name")
    @Expose @Getter @Setter
    public String name;

    @SerializedName("type")
    @Expose @Getter @Setter
    public Integer type;

}
