package com.rusoft.chequepay.data.repository.transactions.templates;


import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.data.entities.templates.response.AllTemplatesResponse;
import com.rusoft.chequepay.data.entities.templates.response.GetTemplateResponse;
import com.rusoft.chequepay.data.entities.templates.response.SaveOrDeleteTemplateResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.network.ResponseError;
import com.rusoft.chequepay.data.network.methods.TemplatesMethods;
import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.data.repository.transactions.BaseTransactionModel;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.Preferences;
import com.rusoft.chequepay.utils.flagsmanager.Flags;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TemplatesRepository extends BaseRepository implements ITemplatesRepository {

    private CacheManager cache;
    private Preferences preferences;

    private TemplatesMethods templatesMethods;

    private DepositModel depositModel;
    private WithdrawalModel withdrawalModel;
    private TransferModel transferModel;
    private InvoiceModel invoiceModel;

    @Inject
    public TemplatesRepository(CacheManager cache, Preferences preferences, TemplatesMethods templatesMethods) {
        this.cache = cache;
        this.preferences = preferences;

        this.templatesMethods = templatesMethods;

        this.depositModel = cache.getDepositModel();
        this.withdrawalModel = cache.getWithdrawalModel();
        this.transferModel = cache.getTransferModel();
        this.invoiceModel = cache.getInvoiceModel();
    }

    @Override
    public void getAllTemplates(boolean forced, RequestCallbackListener listener) {
        if (!forced && cache.getUser().getTemplatesList().size() > 0 && flagsManager.isFlagOff(Flags.TEMPLATE)){
            listener.onSuccess();
            return;
        }

        Disposable disposable = templatesMethods.getAllTemplates(preferences.getAuthToken(), preferences.getClientId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    @Override
    public void saveTemplate(BusinessConstants.TemplateType type, String title, RequestCallbackListener listener) {
        BaseTransactionModel model = getModel(type);
        model.setName(title);
        Disposable disposable = templatesMethods.saveTemplate(preferences.getAuthToken(), preferences.getClientId(), type, model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(listener::onPreExecute)
                .doFinally(listener::onPostExecute)
                .subscribe(response -> handleResponse(listener, response),
                        listener::onError);
        compositeDisposable.add(disposable);
    }

    private BaseTransactionModel getModel(BusinessConstants.TemplateType type) {
        switch (type) {
            case BUY:
                return depositModel;
            case SALE:
                return withdrawalModel;
            case TRANSFER:
                return transferModel;
            case INVOICE:
                return invoiceModel;
            default:
                throw new IllegalArgumentException(ErrorUtils.WRONG_TEMPLATE_TYPE);
        }
    }

    @Override
    public void getTemplate(ShortTemplate template,
                            RequestCallbackListener listener) {

        Long templateId = template.getTemplateId();
        if (cache.getUser().getTemplate(templateId) != null){
            listener.onSuccess();
            return;
        }

        switch (template.getTemplateType()) {
            case BUY:
                compositeDisposable.add(
                        templatesMethods.getBuyTemplate(preferences.getAuthToken(), preferences.getClientId(), templateId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case SALE:
                compositeDisposable.add(
                        templatesMethods.getSaleTemplate(preferences.getAuthToken(), preferences.getClientId(), templateId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case TRANSFER:
                compositeDisposable.add(
                        templatesMethods.getTransferTemplate(preferences.getAuthToken(), preferences.getClientId(), templateId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case INVOICE:
                compositeDisposable.add(
                        templatesMethods.getInvoiceTemplate(preferences.getAuthToken(), preferences.getClientId(), templateId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            default:
                throw new IllegalArgumentException(ErrorUtils.WRONG_TEMPLATE_TYPE);
        }
    }


    @Override
    public void deleteTemplate(ShortTemplate template,
                               RequestCallbackListener listener) {

        switch (template.getTemplateType()) {
            case BUY:
                compositeDisposable.add(
                        templatesMethods.deleteBuyTemplate(preferences.getAuthToken(), preferences.getClientId(), template.getTemplateId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case SALE:
                compositeDisposable.add(
                        templatesMethods.deleteSaleTemplate(preferences.getAuthToken(), preferences.getClientId(), template.getTemplateId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case TRANSFER:
                compositeDisposable.add(
                        templatesMethods.deleteTransferTemplate(preferences.getAuthToken(), preferences.getClientId(), template.getTemplateId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            case INVOICE:
                compositeDisposable.add(
                        templatesMethods.deleteInvoiceTemplate(preferences.getAuthToken(), preferences.getClientId(), template.getTemplateId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(listener::onPreExecute)
                                .doFinally(listener::onPostExecute)
                                .subscribe(response -> handleResponse(listener, response),
                                        listener::onError));
                break;

            default:
                throw new IllegalArgumentException(ErrorUtils.WRONG_TEMPLATE_TYPE);
        }
    }

    @Override
    protected void handleResponse(RequestCallbackListener listener, BaseResponse response) {
        super.handleResponse(listener, response);

        if (response.getError() == 0) {

            if (response instanceof AllTemplatesResponse) {
                flagsManager.turnOff(Flags.TEMPLATE);
                cache.getUser().setTemplatesList(((AllTemplatesResponse) response).getTemplates());
                listener.onSuccess();

            } else if (response instanceof GetTemplateResponse) {
                cache.getUser().addTemplate(((GetTemplateResponse) response).getTemplate());
                listener.onSuccess();

            } else if (response instanceof SaveOrDeleteTemplateResponse) {
                flagsManager.turnOn(Flags.TEMPLATE);
                listener.onSuccess(((SaveOrDeleteTemplateResponse) response).getTemplateId());
            }

        } else {
            listener.onError(new ResponseError(response.getError()));
        }
    }
}
