package com.rusoft.chequepay.data.entities.operations.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rusoft.chequepay.data.entities.BodyRequest;

import lombok.Builder;
import lombok.Setter;

@Builder
public class GetItemOperationRequest extends BodyRequest {
    @SerializedName("currentTime")
    @Expose @Setter
    private long currentTime;

    @SerializedName("language")
    @Expose @Setter
    private String language;

    @SerializedName("operationType")
    @Expose @Setter
    private long operationType;

    @SerializedName("operationUid")
    @Expose @Setter
    private String operationUid;

    @SerializedName("senderId")
    @Expose @Setter
    private long senderId;

}
