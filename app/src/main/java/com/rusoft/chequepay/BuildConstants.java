package com.rusoft.chequepay;

import android.support.annotation.NonNull;

public class BuildConstants {

    @NonNull
    public static String getHostUrl() {
        return isDev() ? "https://lk23.chequepay.ru/fpweb/cp/" // dev
                : "https://lk.chequepay.ru/fpweb/cp/"; // stage
    }

    public static boolean isDev() {
        return BuildConfig.FLAVOR.equals("dev");
    }

    //Residents
    public static final String TEST_LOGIN = "test@test.com";
    //public static final String TEST_LOGIN = "epifanov.ka@spb.rusoft-company.ru";
    //public static final String TEST_LOGIN = "zabalueva.iv@softest.pro";

    //Non-Residents
    //public static final String TEST_LOGIN = "efimova.em@softest.pro";
    //public static final String TEST_LOGIN = "test.testov.001@mail.ru"; //no accounts

    public static final String TEST_PASSWORD = "cpUser123";

}
