package com.rusoft.chequepay;

import android.app.Application;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.AppLongPollingComponent;
import com.rusoft.chequepay.di.DaggerAppComponent;
import com.rusoft.chequepay.di.DaggerAppLongPollingComponent;
import com.rusoft.chequepay.di.module.AppModule;
import com.rusoft.chequepay.di.module.CacheManagerModule;
import com.rusoft.chequepay.di.module.LocalNavigationModule;
import com.rusoft.chequepay.di.module.NavigationModule;
import com.rusoft.chequepay.di.module.NetworkLongPollingModule;
import com.rusoft.chequepay.di.module.NetworkModule;
import com.rusoft.chequepay.di.module.PreferencesModule;

public class App extends Application {
    public static App INSTANCE;
    private AppComponent appComponent;
    private AppLongPollingComponent appLongPollingComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .networkModule(new NetworkModule())
                    .navigationModule(new NavigationModule())
                    .localNavigationModule(new LocalNavigationModule())
                    .preferencesModule(new PreferencesModule(this))
                    .cacheManagerModule(new CacheManagerModule())
                    .build();
        }
        return appComponent;
    }

    public AppLongPollingComponent getAppLongPollingComponent() {
        if (appLongPollingComponent == null) {
            appLongPollingComponent = DaggerAppLongPollingComponent.builder()
                    .appModule(new AppModule(this))
                    .networkLongPollingModule(new NetworkLongPollingModule())
                    .navigationModule(new NavigationModule())
                    .localNavigationModule(new LocalNavigationModule())
                    .preferencesModule(new PreferencesModule(this))
                    .cacheManagerModule(new CacheManagerModule())
                    .build();
        }
        return appLongPollingComponent;
    }
}
