package com.rusoft.chequepay.components;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.utils.CurrencyUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TotalBalanceSectionView extends LinearLayout{

    private final int HIGHEST_VALUE_PROGRESS_STATE = 80;

    @BindView(R.id.R_sum) TextView labelRub;
    @BindView(R.id.S_sum) TextView labelUsd;
    @BindView(R.id.E_sum) TextView labelEur;
    @BindView(R.id.R_progress) ProgressBar progressRub;
    @BindView(R.id.S_progress) ProgressBar progressUsd;
    @BindView(R.id.E_progress) ProgressBar progressEur;

    public TotalBalanceSectionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_total_balance_section, this, true);
        ButterKnife.bind(this);
    }

    public void update(double rubAmount, double usdAmount, double eurAmount){
        updateLabels(rubAmount, usdAmount, eurAmount);

        if (rubAmount == 0 && usdAmount == 0 && eurAmount == 0){
            return;
        }

        double usdR = usdAmount * CurrencyUtils.DEF_COST_USDRUB;
        double eurR = eurAmount * CurrencyUtils.DEF_COST_EURRUB;
        double highestValue = Math.max(Math.max(rubAmount, usdR), Math.max(usdR, eurR));

        if (rubAmount == highestValue){
            progressRub.setProgress(HIGHEST_VALUE_PROGRESS_STATE);
            progressUsd.setProgress(getProgressState(usdR, highestValue));
            progressEur.setProgress(getProgressState(eurR, highestValue));

        } else if (usdR == highestValue){
            progressUsd.setProgress(HIGHEST_VALUE_PROGRESS_STATE);
            progressRub.setProgress(getProgressState(rubAmount, highestValue));
            progressEur.setProgress(getProgressState(eurR, highestValue));

        } else if (eurR == highestValue){
            progressEur.setProgress(HIGHEST_VALUE_PROGRESS_STATE);
            progressRub.setProgress(getProgressState(rubAmount, highestValue));
            progressUsd.setProgress(getProgressState(usdR, highestValue));
        }
    }

    private void updateLabels(double rubAmount, double usdAmount, double eurAmount) {
        labelRub.setText(CurrencyUtils.format(rubAmount, CurrencyUtils.Currency.RUB.getCode()));
        labelUsd.setText(CurrencyUtils.format(usdAmount, CurrencyUtils.Currency.USD.getCode()));
        labelEur.setText(CurrencyUtils.format(eurAmount, CurrencyUtils.Currency.EUR.getCode()));
    }

    private int getProgressState(double amount, double highestValue) {
        int result = (int) (amount / highestValue * HIGHEST_VALUE_PROGRESS_STATE);
        return result;
    }
}
