package com.rusoft.chequepay.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;

import lombok.Getter;
import lombok.Setter;


public class SuccessIndicatorView extends View {
    private static final int START_ANGLE_POINT = 0;
    private static final int END_ANGLE_POINT = 360;
    private static final int LINE_DURATION = AppConstants.DURATION_FAST;
    private static final int CIRCLE_DURATION = AppConstants.DURATION_LONG;

    @Getter @Setter private float angle;
    private final Paint paint;
    private final RectF rect;

    @Getter @Setter private Path path;
    private Point p1, p2, p3;

    public SuccessIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);

        final int color = context.getResources().getColor(R.color.deep_turquoise);
        final int viewSize = context.getResources().getDimensionPixelSize(R.dimen.success_view_size);
        final int circleSize = context.getResources().getDimensionPixelSize(R.dimen.success_view_circle_size);
        final int sizeDelta = (viewSize - circleSize) / 2;
        final int strokeWidth = context.getResources().getDimensionPixelSize(R.dimen.success_view_stroke_width);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(color);
        rect = new RectF(sizeDelta + strokeWidth, sizeDelta + strokeWidth, circleSize + strokeWidth + sizeDelta, circleSize + strokeWidth + sizeDelta);
        angle = START_ANGLE_POINT;

        path = new Path();
        p1 = new Point(((int) (sizeDelta + circleSize * 0.32)), ((int) (sizeDelta + circleSize * 0.53)));
        p2 = new Point(((int) (sizeDelta + circleSize * 0.48)), ((int) (sizeDelta + circleSize * 0.7)));
        p3 = new Point(((int) (sizeDelta + circleSize * 0.74)), (int) (sizeDelta + circleSize * 0.4));
        path.moveTo(p1.x, p1.y);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, paint);
        canvas.drawPath(path, paint);
    }

    public void startAnimation() {
        CircleAnimation circleAnimation = new CircleAnimation(this, END_ANGLE_POINT, CIRCLE_DURATION);
        LineAnimation lineAnimation1 = new LineAnimation(this, p1, p2, LINE_DURATION);
        LineAnimation lineAnimation2 = new LineAnimation(this, p2, p3, LINE_DURATION);
        circleAnimation.setAnimationListener(getAnimationListenerWithAnimationOnEnd(this, lineAnimation1));
        lineAnimation1.setAnimationListener(getAnimationListenerWithAnimationOnEnd(this, lineAnimation2));
        this.startAnimation(circleAnimation);
    }

    private Animation.AnimationListener getAnimationListenerWithAnimationOnEnd(View v, Animation nextAnimation) {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //ignore
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.startAnimation(nextAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //ignore
            }
        };
    }

    private class CircleAnimation extends Animation {
        private SuccessIndicatorView successIndicatorView;
        private float oldAngle, newAngle;
        private int duration;

        private CircleAnimation(SuccessIndicatorView successIndicatorView, int newAngle, int duration) {
            this.successIndicatorView = successIndicatorView;
            this.oldAngle = successIndicatorView.getAngle();
            this.newAngle = newAngle;
            setDuration(duration);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation transformation) {
            float nextAngle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);
            successIndicatorView.setAngle(nextAngle);
            successIndicatorView.requestLayout();
        }
    }

    private class LineAnimation extends Animation {
        private SuccessIndicatorView successIndicatorView;
        private Point A, B;
        private int duration;

        private LineAnimation(SuccessIndicatorView successIndicatorView, Point A, Point B, int duration) {
            this.successIndicatorView = successIndicatorView;
            this.A = A;
            this.B = B;
            setDuration(duration);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            PointF nextPoint = getNextPoint(interpolatedTime);
            successIndicatorView.getPath().lineTo(nextPoint.x, nextPoint.y);
            successIndicatorView.requestLayout();
        }

        private PointF getNextPoint(float interpolatedTime) {
            float x = A.x - (A.x - B.x) * interpolatedTime;
            float y = A.y - (A.y - B.y) * interpolatedTime;
            return new PointF(x, y);
        }
    }
}
