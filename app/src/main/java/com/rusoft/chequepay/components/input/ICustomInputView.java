package com.rusoft.chequepay.components.input;


import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public interface ICustomInputView {

    EditText getTextView();

    void setInputType(int type);

    String getText();

    String getTitle();

    void setText(String text);

    void setTextColor(int color);

    boolean isEmpty();

    void addTextWatcher(TextWatcher textWatcher);

    void setOnFocusChangedListener(View.OnFocusChangeListener listener);

    void setSelection(int position);

    void setError(String text);

    void setEnabled(boolean enabled);

    boolean isOptional();

    boolean validate(boolean requestFocusOnFail);

}
