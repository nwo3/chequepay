package com.rusoft.chequepay.components.input;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentInputView extends RelativeLayout implements ICustomInputView {
    
    @BindView(R.id.title) TextView title;
    @BindView(R.id.comment_box) EditText comment;

    private String titleStr, hintStr, textStr;
    private Boolean editable;

    public CommentInputView(Context context, AttributeSet attrs) {
        super(context, attrs);


        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CommentInputView, 0, 0);
        titleStr = a.getString(R.styleable.CommentInputView_comment_title);
        hintStr = a.getString(R.styleable.CommentInputView_comment_hint);
        textStr = a.getString(R.styleable.CommentInputView_comment_text);
        editable = a.getBoolean(R.styleable.CommentInputView_comment_editable, true);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_comment_input, this, true);
        ButterKnife.bind(this);

        int white50ResId = getContext().getResources().getColor(R.color.white_50);
        int whiteResId = getContext().getResources().getColor(R.color.white);

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //ignore
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                title.setTextColor(charSequence.length() > 0 ? white50ResId : whiteResId);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //ignore
            }
        });

        comment.setImeOptions(EditorInfo.IME_ACTION_DONE);
        comment.setRawInputType(InputType.TYPE_CLASS_TEXT);

        title.setText(titleStr);
        if (!CommonUtils.isStringNullOrEmpty(hintStr)) comment.setHint(hintStr);
        comment.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        comment.setSingleLine(false);
        comment.setEnabled(editable);
    }

    @Override
    public void setInputType(int type) {
        comment.setInputType(type);
    }

    @Override
    public void addTextWatcher(TextWatcher textWatcher){
        comment.addTextChangedListener(textWatcher);
    }

    @Override
    public void setOnFocusChangedListener(OnFocusChangeListener listener) {
        comment.setOnFocusChangeListener(listener);
    }

    @Override
    public void setSelection(int position) {
        comment.setSelection(position);
    }

    @Override
    public String getText(){
        return comment.getText().toString();
    }

    @Override
    public String getTitle() {
        return titleStr;
    }

    @Override
    public void setText(String text){
        textStr = text;
        comment.setText(text);
    }

    @Override
    public void setTextColor(int color) {
        comment.setTextColor(ContextCompat.getColor(getContext(), color));
    }

    @Override
    public boolean isEmpty() {
        return comment.getText().length() == 0;
    }

    @Override
    public EditText getTextView() {
        return comment;
    }

    @Override
    public void setError(String text) {
        //ignore
    }

    @Override
    public boolean isOptional() {
        return false;
    }

    @Override
    public boolean validate(boolean requestFocusOnFail) {
        return true;
    }
}
