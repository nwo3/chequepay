package com.rusoft.chequepay.components;


import android.content.Context;
import android.content.res.TypedArray;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rusoft.chequepay.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpandableTextView extends RelativeLayout {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.selector) ImageView selector;
    @BindView(R.id.content) TextView content;

    private Integer titleInt, contentInt;
    private boolean isExpanded;

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode())
            return;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView, 0, 0);
        titleInt = a.getResourceId(R.styleable.ExpandableTextView_etv_title, 0);
        contentInt = a.getResourceId(R.styleable.ExpandableTextView_etv_text, 0);
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_expandable_text, this, true);
        ButterKnife.bind(this);

        title.setText(titleInt);
        content.setText(contentInt);
        content.setMovementMethod(LinkMovementMethod.getInstance());
        expand(false);

        setOnClickListener((view -> expand(!isExpanded)));
    }

    public void setTextTitle(Integer resourceId) {
        title.setText(resourceId);
    }

    public void setTextContent(Integer resourceId) {
        content.setText(resourceId);
    }

    private void expand(boolean enable) {
        isExpanded = enable;

        selector.setBackgroundResource(enable ? R.drawable.ic_arrow_up : R.drawable.ic_arrow_down_enabled);

        LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, enable ? LayoutParams.WRAP_CONTENT : 0);
        params.addRule(RelativeLayout.BELOW, R.id.title);
        content.setLayoutParams(params);
    }
}
