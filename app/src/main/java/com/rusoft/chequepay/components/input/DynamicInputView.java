package com.rusoft.chequepay.components.input;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


public class DynamicInputView extends LinearLayout implements ICustomInputView {

    @AllArgsConstructor
    private enum Type {
        TEXT(0),
        NUMBER(1),
        PASSWORD(2),
        SELECTOR(3),
        MULTILINE(4),
        PHONE(5),
        EMAIL(6);

        int val;

        public static Type fromInt(int value) {
            for (Type type : Type.values()) {
                if (value == type.val) {
                    return type;
                }
            }

            return TEXT;
        }
    }


    @AllArgsConstructor
    private enum Style {
        WHITE(0, R.layout.view_input_dynamic_white),
        BLACK(1, R.layout.view_input_dynamic_black);

        int val;
        @Getter int layoutResId;

        public static Style fromInt(int value) {
            for (Style style : Style.values()) {
                if (value == style.val) {
                    return style;
                }
            }

            return WHITE;
        }
    }

    @BindView(R.id.input_container) TextInputLayout container;
    @BindView(R.id.text) TextInputEditText text;

    private Type type;
    private Style style;
    private String hintStr;
    private Integer maxLength;
    @Setter private boolean isOptional;

    // needed for skip first validation, because of specific of
    // TextWatcher.onTextChanged, which invokes when view created
    private boolean isValidatedOnce;

    @Getter private ValidationPatterns validationPattern;
    @Getter @Setter private OnFocusLostListener onFocusLostListener;

    public DynamicInputView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DynamicInputView, 0, 0);
        type = Type.fromInt(a.getInt(R.styleable.DynamicInputView_dynamic_input_type, 0));
        style = Style.fromInt(a.getInt(R.styleable.DynamicInputView_dynamic_input_style, /*default*/ Style.BLACK.val));
        hintStr = a.getString(R.styleable.DynamicInputView_dynamic_input_hint);
        maxLength = a.getInteger(R.styleable.DynamicInputView_dynamic_input_length, 0);
        isOptional = a.getBoolean(R.styleable.StaticInputView_input_is_optional, false);
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(style.getLayoutResId(), this, true);
        ButterKnife.bind(this);

        setTitle(hintStr);
        setInputLength(maxLength);

        switch (type) {
            case TEXT:
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                break;

            case NUMBER:
                text.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;

            case PASSWORD:
                text.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                if (style == Style.BLACK)
                    container.setPasswordVisibilityToggleEnabled(true); //no need to show toggle on AuthScreen
                break;

            case SELECTOR:
                text.setFocusable(false);
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                switch (style) {
                    case WHITE:
                        text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.background_input_selector, 0);
                        break;

                    case BLACK:
                        text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.background_input_selector_b, 0);
                        break;
                }
                break;

            case MULTILINE:
                text.setSingleLine(false);
                text.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                break;

            case PHONE:
                text.setInputType(InputType.TYPE_CLASS_PHONE);
                break;

            case EMAIL:
                text.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
        }

        text.setOnFocusChangeListener((view, focused) -> {
            if (focused) {

                if (type == Type.SELECTOR && isEnabled()) {
                    text.performClick();
                }

            } else {

                validate(false);

                if (onFocusLostListener != null) {
                    onFocusLostListener.onFocusLost();
                }
            }
        });

        text.setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_DONE) {
                performFocusLost();
                UiUtils.hideKeyboard(getContext());
                return true;
            }
            return false;
        });
    }

    private void setInputLength(int maxLength) {
        if (maxLength != 0) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            text.setFilters(FilterArray);
        }
    }

    @Override
    public void setInputType(int type) {
        text.setInputType(type);
    }

    public void performFocusLost() {
        if (onFocusLostListener != null) {
            onFocusLostListener.onFocusLost();
        }
    }

    @Override
    public String getText() {
        return text.getText().toString();
    }

    @Override
    public void setText(String text) {
        this.text.setText(text);
        setError(null);
    }

    @Override
    public void setTextColor(int color) {
        this.text.setTextColor(ContextCompat.getColor(getContext(), color));
    }

    @Override
    public String getTitle() {
        return hintStr;
    }

    /**
     * Помечает ' * ' title поля, если обязательно для запонения.
     * Style.WHITE используется на экране AuthScreen, ' * ' не нужна.
     */
    public void setTitle(String text) {
        if (style != Style.WHITE && !isOptional) {
            text += " *";
        }

        container.setHint(text);
    }

    @Override
    public boolean isEmpty() {
        return text.getText().toString().isEmpty();
    }

    @Override
    public void addTextWatcher(TextWatcher watcher) {
        text.addTextChangedListener(watcher);
    }

    @Override
    public void setOnFocusChangedListener(OnFocusChangeListener listener) {
        text.setOnFocusChangeListener(listener);
    }

    @Override
    public void setSelection(int position) {
        text.setSelection(position);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        text.setOnClickListener(l);
    }

    @Override
    public void setError(String text) {
        container.setErrorEnabled(text != null);
        new android.os.Handler().post(() -> container.setError(text));
    }

    @Override
    public EditText getTextView() {
        return text;
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        text.setEnabled(enabled);

        if (enabled) {
            text.setCompoundDrawablesWithIntrinsicBounds(0, 0, type == Type.SELECTOR ? R.drawable.background_input_selector : 0, 0);
        } else {
            text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock_gray, 0);
        }
    }

    public void setValidationPattern(ValidationPatterns validationPattern) {
        this.validationPattern = validationPattern;
        setInputLength(validationPattern.getMaxLength());
    }

    @Override
    public boolean validate(boolean requestFocusOnFail) {
        boolean result = false;
        if (!isShown()) {
            result = true;
        } else {
            if (isEmpty()) {
                if (isOptional()) {
                    setError(null);
                    result = isValidatedOnce;

                } else {
                    setError(getContext().getString(R.string.error_empty_field, hintStr));
                    onValidationFailed(requestFocusOnFail);
                    result = false;
                }
            } else if (validationPattern != null) {
                TextView textViewError = container.findViewById(R.id.textinput_error);
                if (textViewError != null) {
                    textViewError.setPadding(0, getResources().getDimensionPixelSize(R.dimen.error_text_padding_top), 0, 0);
                }

                if (!validationPattern.validate(getText())) {
                    if (validationPattern.equals(ValidationPatterns.PASSWORD) && getText().length() < BusinessConstants.PASSWORD_MIN_LENGTH) {
                        setError(getContext().getString(R.string.error_wrong_pass_2));

                    } else if (validationPattern.equals(ValidationPatterns.AMC_PASSWORD) && getText().length() < BusinessConstants.AMC_PASSWORD_MIN_LENGTH) {
                        setError(getContext().getString(R.string.error_wrong_pass_4));

                    } else {
                        setError(getContext().getString(validationPattern.getErrorTextResId()));
                    }

                    onValidationFailed(requestFocusOnFail);
                    result = false;

                } else {
                    result = true;
                    setError(null);
                }
            }
        }

        isValidatedOnce = true;
        return result;
    }

    public boolean validate() {
        return validate(false);
    }

    private void onValidationFailed(boolean requestFocusOnFail) {
        if (!requestFocusOnFail) return;

        if (type == Type.SELECTOR) {
            text.performClick();
        } else {
            UiUtils.requestFocusAndShowKeyboard(getContext(), text);
        }
    }
}
