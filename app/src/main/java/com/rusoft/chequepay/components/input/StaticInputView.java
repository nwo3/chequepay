package com.rusoft.chequepay.components.input;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


public class StaticInputView extends RelativeLayout implements ICustomInputView{

    @AllArgsConstructor
    private enum Type {
        INPUT(0),
        INPUT_NUMERIC(1),
        INPUT_DOUBLE(2),
        SELECTOR(3),
        INPUT_AND_SELECTOR(4),
        STATIC_LABEL(5),
        PASSWORD(6),
        EMAIL(7),
        PHONE(8);

        int val;
        public static Type fromInt(int value) {
            for (Type type : Type.values()) {
                if (value == type.val) {
                    return type;
                }
            }
            return INPUT;
        }
    }

    @AllArgsConstructor
    private enum Style {
        WHITE(0, R.layout.view_input_static_white),
        BLACK(1, R.layout.view_input_static_black),
        PASS_RECOVERY(2, R.layout.view_input_static_pass_recovery);

        int val;
        @Getter int layoutResId;

        public static Style fromInt(int value) {
            for (Style style : Style.values()) {
                if (value == style.val) {
                    return style;
                }
            }

            return WHITE;
        }
    }

    @BindView(R.id.title) TextView title;
    @BindView(R.id.text) EditText text;
    @BindView(R.id.selector) ImageView selector;
    @BindView(R.id.line) View line;
    @BindView(R.id.error) TextView error;

    private String titleStr, hintStr, textStr;
    private Type type;
    private Style style;
    private boolean isEmpty, isOptional;
    private Integer maxLength;

    @Getter private ValidationPatterns validationPattern;
    @Getter @Setter private OnFocusLostListener onFocusLostListener;

    public StaticInputView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StaticInputView, 0, 0);
        type = StaticInputView.Type.fromInt(a.getInt(R.styleable.StaticInputView_input_type, -1));
        style = StaticInputView.Style.fromInt(a.getInt(R.styleable.StaticInputView_input_style, /*default*/ Style.WHITE.val));
        titleStr = a.getString(R.styleable.StaticInputView_input_title);
        hintStr = a.getString(R.styleable.StaticInputView_input_hint);
        textStr = a.getString(R.styleable.StaticInputView_input_text);
        isOptional = a.getBoolean(R.styleable.StaticInputView_input_is_optional, false);
        maxLength = a.getInteger(R.styleable.StaticInputView_input_length, 0);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(style.getLayoutResId(), this, true);
        ButterKnife.bind(this);

        setTitle(titleStr);
        setStateEmptyText(true);
        setHint(hintStr);
        setText(textStr);
        setInputLength(maxLength);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //ignore
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setStateEmptyText(charSequence.length() == 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //ignore
            }
        };

        switch (type) {
            case INPUT:
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                selector.setVisibility(INVISIBLE);
                break;

            case INPUT_NUMERIC:
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_CLASS_NUMBER);
                selector.setVisibility(INVISIBLE);
                break;

            case INPUT_DOUBLE:
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                selector.setVisibility(INVISIBLE);
                break;

            case SELECTOR:
                int paddingRight = getResources().getDimensionPixelOffset(R.dimen.space_large_l);
                text.setPadding(0, 0, paddingRight, 0);
                text.setFocusable(false);
                text.setClickable(false);
                text.addTextChangedListener(textWatcher);
                text.setOnClickListener(view -> StaticInputView.this.performClick());
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                selector.setOnClickListener(view -> StaticInputView.this.performClick());
                selector.setVisibility(VISIBLE);
                break;

            case INPUT_AND_SELECTOR:
                text.setFocusable(true);
                text.setClickable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                selector.setVisibility(VISIBLE);
                selector.setOnClickListener(view -> StaticInputView.this.performClick());
                break;

            case STATIC_LABEL:
                text.setFocusable(false);
                text.setClickable(false);
                text.addTextChangedListener(textWatcher);
                selector.setVisibility(INVISIBLE);
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                break;

            case PASSWORD:
                text.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                text.setTransformationMethod(PasswordTransformationMethod.getInstance());
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                selector.setVisibility(INVISIBLE);
                break;

            case EMAIL:
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                selector.setVisibility(INVISIBLE);
                break;

            case PHONE:
                text.setFocusable(true);
                text.addTextChangedListener(textWatcher);
                text.setInputType(InputType.TYPE_CLASS_PHONE);
                selector.setVisibility(INVISIBLE);
                break;
        }


        text.setOnFocusChangeListener((view, focused) -> {
            if (focused) {

                if (type == Type.SELECTOR && isEnabled()) {
                    text.performClick();
                }

            } else {

                validate(false);

                if (onFocusLostListener != null) {
                    onFocusLostListener.onFocusLost();
                }
            }
        });

        text.setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_DONE) {
                performFocusLost();
                UiUtils.hideKeyboard(getContext());
                return true;
            }
            return false;
        });
    }

    @Override
    public void setInputType(int type) {
        text.setInputType(type);
    }

    public void setHint(String hint) {
        text.setHint(hint);
    }

    public void setTitle(String titleStr){
        this.title.setText(titleStr);
    }

    @Override
    public void setText(String text){
        this.text.setText(text);
        setStateEmptyText(text == null || text.isEmpty());
        setError(null);
    }

    @Override
    public void setTextColor(int color) {
        this.text.setTextColor(ContextCompat.getColor(getContext(), color));
    }

    @Override
    public String getText(){
        return text.getText().toString();
    }

    @Override
    public String getTitle() {
        return titleStr;
    }

    private void setStateEmptyText(boolean enabled){
        isEmpty = enabled;
        Resources resources = getContext().getResources();
        int color1 = resources.getColor(style == Style.WHITE || style == Style.PASS_RECOVERY ? R.color.white : R.color.black);
        int color2 = resources.getColor(style == Style.WHITE || style == Style.PASS_RECOVERY ? R.color.white_50 : R.color.gray);

        if (!(style == Style.BLACK && type == Type.SELECTOR)) {
            title.setTextColor(isEmpty ? color1 : color2);
        }

        text.setTextColor(isEmpty ? color2 : color1);
    }

    public void setError(String errorText){
        error.setVisibility(errorText != null ? VISIBLE : INVISIBLE);
        error.setText(errorText != null ? errorText : "");
        line.setBackgroundColor(getContext().getResources().getColor(errorText != null ?
                R.color.error_indication : R.color.turquoise_50));
    }

    @Override
    public void setOnFocusChangedListener(OnFocusChangeListener listener){
        text.setOnFocusChangeListener(listener);
    }

    @Override
    public void setSelection(int position) {
        text.setSelection(position);
    }

    @Override
    public void addTextWatcher(TextWatcher textWatcher){
        text.addTextChangedListener(textWatcher);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        text.setEnabled(enabled);
        selector.setEnabled(enabled);

        if (style == Style.WHITE || style == Style.PASS_RECOVERY){
            title.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
            line.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
        }

        if (type == Type.SELECTOR) {
            selector.setImageDrawable(getContext().getDrawable(enabled ?
                    (style == Style.BLACK ? R.drawable.background_input_selector_b : R.drawable.background_input_selector) :
                    (style == Style.BLACK ? R.drawable.ic_lock_gray : R.drawable.ic_lock_white)));
        }
    }

    @Override
    public boolean isEmpty() {
        return getText().length() == 0;
    }

    @Override
    public EditText getTextView() {
        return text;
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    public void performFocusLost(){
        if (onFocusLostListener != null) {
            onFocusLostListener.onFocusLost();
        }
    }

    private void setInputLength(int maxLength) {
        if (maxLength != 0) {
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            text.setFilters(FilterArray);
        }
    }

    public void setValidationPattern(ValidationPatterns validationPattern) {
        this.validationPattern = validationPattern;
        setInputLength(validationPattern.getMaxLength());
    }

    @Override
    public boolean validate(boolean requestFocusOnFail) {
        if (isShown()){
            if (isEmpty()){
                if (isOptional()){
                    setError(null);
                    return true;

                } else {
                    setError(getContext().getString(R.string.error_empty_field, titleStr));
                    onValidationFailed(requestFocusOnFail);
                    return false;
                }
            }

            if (validationPattern != null) {
                if (!validationPattern.validate(getText())) {

                    if (validationPattern.equals(ValidationPatterns.PASSWORD) && getText().length() < BusinessConstants.PASSWORD_MIN_LENGTH){
                        setError(getContext().getString(R.string.error_wrong_pass_2));

                    } else if (validationPattern.equals(ValidationPatterns.AMC_PASSWORD) && getText().length() < BusinessConstants.AMC_PASSWORD_MIN_LENGTH) {
                        setError(getContext().getString(R.string.error_wrong_pass_4));

                    } else {
                        setError(getContext().getString(validationPattern.getErrorTextResId()));
                    }

                    onValidationFailed(requestFocusOnFail);
                    return false;
                }
            }
        }

        setError(null);
        return true;
    }

    public boolean validate() {
        return validate(false);
    }

    private void onValidationFailed(boolean requestFocusOnFail) {
        if (!requestFocusOnFail) return;

        if (type == Type.SELECTOR){
            text.performClick();
        } else {
            UiUtils.requestFocusAndShowKeyboard(getContext(), text);
        }
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener listener){
        text.setOnEditorActionListener(listener);
    }
}
