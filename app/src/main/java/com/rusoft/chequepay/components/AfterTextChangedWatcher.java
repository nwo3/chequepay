package com.rusoft.chequepay.components;

import android.text.TextWatcher;


public interface AfterTextChangedWatcher extends TextWatcher {

    default void onTextChanged(CharSequence charSequence, int i, int i1, int i2){}

    default void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){}

}
