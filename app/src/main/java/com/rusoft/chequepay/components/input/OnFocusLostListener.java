package com.rusoft.chequepay.components.input;


public interface OnFocusLostListener {
    void onFocusLost();
}
