package com.rusoft.chequepay.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Setter;


public class PinView extends RelativeLayout {

    private static final int FINGERPRINT_BUTTON = 9;
    private static final int DELETE_BUTTON = 11;

    public interface CallbacksListener {
        void onPinEntered(String code);
        void onFingerprintClicked();
    }

    private static final int CODE_LENGTH = 5;

    @BindView(R.id.dots_container) ViewGroup dotsContainer;
    @BindView(R.id.keys_container) ViewGroup keysContainer;

    private StringBuilder code = new StringBuilder("");
    private Drawable dotFilledDrawable, dotEmptyDrawable;
    private Vibrator vibrator;
    @Setter private CallbacksListener listener;

    public PinView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_pin, this, true);
        ButterKnife.bind(this);

        dotFilledDrawable = getContext().getDrawable(R.drawable.background_pin_dot_filled);
        dotEmptyDrawable = getContext().getDrawable(R.drawable.background_pin_dot_empty);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);

        for (int i = 0; i < keysContainer.getChildCount(); i++) {
            View v = keysContainer.getChildAt(i);

            if (i < 9 || i == 10) {
                int keyNum = i == 10 ? 0 : i + 1;
                v.setOnClickListener(view -> appendNum(keyNum));

            } else if (i == DELETE_BUTTON) {
                v.setOnClickListener(view -> removeLastNum());

            } else if (i == FINGERPRINT_BUTTON) {
                v.setOnClickListener(view -> listener.onFingerprintClicked());
            }
        }
    }

    public void setFingerprintEnabled(boolean enabled) {
        for (int i = 0; i < keysContainer.getChildCount(); i++) {
            if (i == FINGERPRINT_BUTTON) {
                View fingerprintBrn = keysContainer.getChildAt(i);
                fingerprintBrn.setEnabled(enabled);
                fingerprintBrn.setVisibility(enabled ? VISIBLE : INVISIBLE);
            }
        }
    }

    private void appendNum(int keyNum) {
        if (code.length() < CODE_LENGTH) {
            code.append(keyNum);
            vibrate(AppConstants.DURATION_INSTANT);
            updateDotsState();
        }

        if (code.length() == CODE_LENGTH && listener != null) {
            listener.onPinEntered(code.toString());
        }
    }

    private void removeLastNum() {
        code.setLength(Math.max(code.length() - 1, 0));
        updateDotsState();
        vibrate(AppConstants.DURATION_INSTANT);
    }

    private void updateDotsState() {
        for (int i = 0; i < dotsContainer.getChildCount(); i++) {
            View v = dotsContainer.getChildAt(i);
            v.setBackground(i < code.length() ? dotFilledDrawable : dotEmptyDrawable);
        }
    }

    private void vibrate(int duration) {
        if (vibrator != null) {
            vibrator.vibrate(duration);
        }
    }

    public void clear() {
        code.setLength(0);
        updateDotsState();
    }

    public void onWrongPin() {
        vibrate(AppConstants.DURATION_FAST);
        Animation shake;
        shake = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.horizontal_shake);
        dotsContainer.startAnimation(shake);
        clear();
    }
}
