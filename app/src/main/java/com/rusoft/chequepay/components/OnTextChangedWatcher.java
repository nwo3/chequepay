package com.rusoft.chequepay.components;

import android.text.Editable;
import android.text.TextWatcher;


public interface OnTextChangedWatcher extends TextWatcher {

    default void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){}

    default void afterTextChanged(Editable var1){}

}
