package com.rusoft.chequepay.di;

import android.content.Context;

import com.rusoft.chequepay.di.module.AppModule;
import com.rusoft.chequepay.di.module.CacheManagerModule;
import com.rusoft.chequepay.di.module.LocalNavigationModule;
import com.rusoft.chequepay.di.module.NavigationModule;
import com.rusoft.chequepay.di.module.NetworkModule;
import com.rusoft.chequepay.di.module.PreferencesModule;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.utils.CacheManager;
import com.rusoft.chequepay.utils.Preferences;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, NavigationModule.class, LocalNavigationModule.class,
        PreferencesModule.class, CacheManagerModule.class})
public interface AppComponent {
    Retrofit retrofit();
    Router router();
    NavigatorHolder navigatorHolder();
    LocalCiceroneHolder localCiceroneHolder();
    Preferences preferences();
    CacheManager cacheManager();
    Context context();
}
