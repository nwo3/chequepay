package com.rusoft.chequepay.di.module;

import com.rusoft.chequepay.data.network.ChequepayApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ChequepayApiModule {

    @Provides
    ChequepayApi provideChequepayApi(Retrofit retrofit) {
        return retrofit.create(ChequepayApi.class);
    }
}
