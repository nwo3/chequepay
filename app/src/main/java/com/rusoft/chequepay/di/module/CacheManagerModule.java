package com.rusoft.chequepay.di.module;

import com.rusoft.chequepay.utils.CacheManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CacheManagerModule {
    CacheManager cacheManager;

    public CacheManagerModule() {
        cacheManager = new CacheManager();
    }

    @Provides
    @Singleton
    CacheManager provideCacheManager() {
        return cacheManager;
    }
}
