package com.rusoft.chequepay.di.module;

import android.content.Context;

import com.rusoft.chequepay.utils.Preferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {
    Preferences preferences;

    public PreferencesModule(Context context) {
        preferences = new Preferences(context);
    }

    @Provides
    @Singleton
    Preferences providePreferences() {
        return preferences;
    }
}
