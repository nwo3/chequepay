package com.rusoft.chequepay;

import android.content.Context;

import com.rusoft.chequepay.data.entities.info.Country;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class BusinessConstants {

    public static final int MIN_AGE = 18;

    public static final int COUNTRY_CODE_RUSSIA = 643;
    public static final Country COUNTRY_RUSSIA = new Country(COUNTRY_CODE_RUSSIA, "Russia", "Россия");

    public static final int OTP_LENGTH = 5;
    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int AMC_PASSWORD_MIN_LENGTH = 8;
    public static final int RECOVERY_PHONE_MAX_LENGTH = 14;

    public static final int PIN_ATTEMPTS_COUNT = 3;

    public static final int INVOICE_MAX_LIFETIME = 120;
    public static final String APPLICATION_TYPE_INVOICE = "3";
    public static final String APPLICATION_TYPE_PURCHASE = "7";
    public static final String APPLICATION_TYPE_SELL = "15";
    public static final String HEADER_CONFIRMATION_TYPE = "2";

    public static final int PAYMENT_TYPE = 4;

    public static final int NOTIFY_ITEM_POSITION = 2;

    public static final int PAGE_NUMBER_START = 1;
    public static final int PAGE_ITEMS_COUNT = 50;

    public static final String PROVIDER_CODE_INTERNAL_TRANSFER = "-1003"; //Внутренний перевод
    public static final String PROVIDER_CODE_CONSOLIDATION_OF_MEANS_ON_MASTER_ACCOUNT = "-1004"; //Консолидация средств на мастер-счете
    public static final String PROVIDER_CODE_BV_DEALER_CLIENT_TRANSFER = "-1026"; //Перевод Брокера Клиенту получателю в Брокерской версии
    public static final String PROVIDER_CODE_TERMINAL_DEALER_CLIENT_TRANSFER = "-1028"; //Перевод от дилера клиенту терминальной сети
    public static final String PROVIDER_CODE_TERMINAL_CLIENT_DEALER_TRANSFER = "-1029"; //Перевод от клиента терминальной сети дилеру
    public static final String PROVIDER_CODE_CHARGE_OFF_OF_THE_OWN_AMOUNTS = "-1033"; //Списание собственных средств Эмитента
    public static final String PROVIDER_CODE_TRANSFER_BETWEEN_BROKER = "-1034"; //Перевод между Брокерами
    public static final String PROVIDER_CODE_RETURN_CHEQUE_TO_ISSUER = "-1018"; //Возврат чека эмитенту
    public static final String PROVIDER_CODE_BV_CLIENT_DEALER_TRANSFER = "-1027"; //Перевод Клиента отправителя Брокеру в Брокерской версии
    public static final String PROVIDER_CODE_SALE_CHEQUE_TO_DEALER = "-1030"; //Продажа чеков Брокеру/Дилеру
    public static final String PROVIDER_CODE_SALE_CHEQUE_TO_EMITTER = "-1017"; //Продажа чеков Эмитенту
    public static final String PROVIDER_CODE_CREDITING_AMOUNTS_THROUGH_DEALER = "-1031"; //Зачисление средств через Брокера
    public static final String PROVIDER_CODE_PURCHASE_CHEQUE_EMITTER = "-1032"; //Покупка чека у Эмитента
    public static final String PROVIDER_CODE_TRANSFER = "-1002"; //Простой перевод
    public static final String PROVIDER_CODE_WITHDRAWAL = "-1016"; //Вывод средств (через Банк)

    public interface IBusinessEnumInterface {
        String toString(Context context);
    }

    @AllArgsConstructor
    public enum RefundMode implements IBusinessEnumInterface {
        TRANSFER(1, R.string.bank_transfer),
        CASH(2, R.string.cash_transfer);

        @Getter int val;
        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<RefundMode> asList(){
            return new ArrayList<>(EnumSet.allOf(RefundMode.class));
        }

        @Nullable
        public static RefundMode getMode(int val){
            for (RefundMode mode: RefundMode.values()){
                if (mode.getVal() == val){
                    return mode;
                }
            }
            return null;
        }
    }

    @AllArgsConstructor
    public enum OperationGroupType implements IBusinessEnumInterface {
        GROUP_TRANSFER(1, R.string.operation_type_group_transfer),
        GROUP_REFILL(2, R.string.operation_type_group_refill),
        GROUP_WITHDRAWAL(3, R.string.operation_type_group_withdrawal);

        @Getter int code;
        @Getter int titleResId;

        public static List<OperationGroupType> asList() {
            return new ArrayList<>(EnumSet.allOf(OperationGroupType.class));
        }

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }
    }

    @AllArgsConstructor
    public enum OperationType implements IBusinessEnumInterface {
        EMISSION_CHEQUE(1, R.string.operation_type_emission_cheque_1),
        RECEIVING_TRANSFER(2, R.string.operation_type_receiving_transfer_2_1),
        RECEIVING_BY_REJECTED_OPERATION(3, R.string.operation_type_receiving_by_rejected_operation_3),
        RECEIVING_PROTECTION_TRANSFER(4, R.string.operation_type_receiving_protection_transfer_4),
        RECEIVING_SYSTEM_COMISSION(5, R.string.operation_type_receiving_system_comission_5),
        RECEIVING_INKASSO_CHEQUE(6, R.string.operation_type_receiving_inkasso_cheque_6),
        RECEIVING_INKASSO_CHEQUE_FOR_PAYMENT(7, R.string.operation_type_receiving_inkasso_cheque_for_payment_7),
        EMISSION_COMMISSION_CHEQUE(8, R.string.operation_type_emission_comission_cheque_8),
        RECEIVING_EMITTER_COMISSION(9, R.string.operation_type_receiving_emitter_comission_9),
        RECEIVING_FOR_CURRENCY_EXCHANGE(11, R.string.operation_type_receiving_for_currency_exchange_11),
        RECEIVING_AFTER_CURRENCY_EXCHANGE(12, R.string.operation_type_receiving_after_currency_exchange_12),
        TRANSFER_CHEQUE_FOR_EXCHANGE(14, R.string.operation_type_transfer_cheque_for_exchange_14),
        CHECK_PRESENTATION_FOR_PAYMENT(21, R.string.operation_type_check_presentation_for_payment_21),
        RECEIVING_SELL_CHEQUE(22, R.string.operation_type_receiving_sell_cheque_22),
        CHECK_RETURN(23, R.string.operation_type_check_return_23),
        ENROLLING_CHEQUE_ENROLL(24, R.string.operation_type_enrolling_cheque_enroll_24),
        PURCHASE_CHEQUE_ENROLL(25, R.string.operation_type_purchase_cheque_enroll_25),
        RECEIVING_CHARGE_OFF_OWN_E_CHECK(26, R.string.operation_type_receiving_charge_off_own_e_check_26),
        TRANSFER_BETWEEN_BROKER_ENROLL(27, R.string.operation_type_transfer_between_broker_enroll_27),
        RETURN_CHEQUE_TO_EMITTER_ENROLL(28, R.string.operation_type_return_cheque_to_emitter_enroll_28),
        RETURN_CHEQUE_TO_EMITTER_WITHDRAW(-28, R.string.operation_type_return_cheque_to_emitter_withdraw_m28),
        TRANSFER_BETWEEN_BROKER_WITHDRAW(-27, R.string.operation_type_transfer_between_broker_withdraw_m27),
        SENDING_CHARGE_OFF_OWN_E_CHECK(-26, R.string.operation_type_sending_charge_off_own_e_check_m26),
        PURCHASE_CHEQUE_WITHDRAW(-25, R.string.operation_type_purchase_cheque_withdraw_m25),
        ENROLLING_CHEQUE_WITHDRAW(-24, R.string.operation_type_enrolling_cheque_withdraw_m24),
        RETURNING_CHEQUE_WITHDRAW(-23, R.string.operation_type_returning_cheque_withdraw_m23),
        SENDING_SELL_CHEQUE(-22, R.string.operation_type_sending_sell_cheque_m22),
        PRESENT_CHEQUE_FOR_PAYMENT_WITHDRAW(-21, R.string.operation_type_present_cheque_for_payment_withdraw_m21),
        SENDING_AFTER_CURRENCY_EXCHANGE(-12, R.string.operation_type_sending_after_currency_exchange_m12),
        SENDING_FOR_CURRENCY_EXCHANGE(-11, R.string.operation_type_sending_for_currency_exchange_m11),
        WRITE_OFF_EMITTER_COMMISSION_CHEQUE(-8, R.string.operation_type_write_off_emitter_commission_cheque_m8),
        WRITE_OFF_PAID_INKASSO_CHEQUE(-7, R.string.operation_type_write_off_paid_inkasso_cheque_m7),
        SENDING_CHEQUE_THRU_INKASSO(-6, R.string.operation_type_sending_cheque_thru_inkasso_m6),
        WRITE_OFF_SYSTEM_COMMISSION(-5, R.string.operation_type_write_off_system_commission_m5),
        SENDING_INKASSO_CHEQUE(-4, R.string.operation_type_sending_inkasso_cheque_m4),
        SENDING_REJECT_TRANSFER(-3, R.string.operation_type_sending_reject_transfer_m3),
        SENDING_TRANSFER(-2, R.string.operation_type_sending_transfer_m2_1),
        WRITE_OFF_PAID_CHEQUE(-1, R.string.operation_type_write_off_paid_cheque);

        @Getter int code;
        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<OperationType> asList() {
            return new ArrayList<>(EnumSet.allOf(OperationType.class));
        }

        public static List<Integer> getOperationTypeCodeList(List<BusinessConstants.OperationType> operationTypes) {
            List<Integer> operationTypeCodeList = new ArrayList<>();
            for (BusinessConstants.OperationType item : operationTypes) {
                operationTypeCodeList.add(item.getCode());
            }
            return operationTypeCodeList;
        }

        public static List<Integer> getOperationTypeTransfer() {
            return new ArrayList<>(Arrays.asList(OperationType.RECEIVING_TRANSFER.code,
                    OperationType.SENDING_TRANSFER.code,
                    OperationType.WRITE_OFF_SYSTEM_COMMISSION.code,
                    OperationType.RECEIVING_SYSTEM_COMISSION.code));
        }

        public static List<Integer> getOperationTypeWithdrawal() {
            return new ArrayList<>(Arrays.asList(OperationType.RECEIVING_SELL_CHEQUE.code,
                    OperationType.SENDING_SELL_CHEQUE.code,
                    OperationType.CHECK_RETURN.code,
                    OperationType.RETURNING_CHEQUE_WITHDRAW.code));
        }

        public static List<Integer> getOperationTypeReplenishment() {
            return new ArrayList<>(Arrays.asList(OperationType.ENROLLING_CHEQUE_ENROLL.code,
                    OperationType.ENROLLING_CHEQUE_WITHDRAW.code,
                    OperationType.PURCHASE_CHEQUE_ENROLL.code,
                    OperationType.PURCHASE_CHEQUE_WITHDRAW.code));
        }

        @Nullable
        public static OperationType getOperationType(int code){
            for (OperationType operationType: OperationType.values()){
                if (operationType.getCode() == code){
                    return operationType;
                }
            }
            return null;
        }
    }

    @AllArgsConstructor
    public enum DocCategory implements IBusinessEnumInterface {
        ALL(R.string.fragment_docs_all_category),
        NOTICES(R.string.fragment_docs_category_notices),
        APPLICATIONS(R.string.fragment_docs_category_applications),
        ACT_OF_RENDERED_SERVICES(R.string.fragment_docs_category_act_of_rendered_services),
        INVOICE_FOR_PAYMENTS(R.string.fragment_docs_category_invoice_for_payments),
        ACT_OF_TRANSFERS(R.string.fragment_docs_category_act_of_transfers),
        INVOICES(R.string.fragment_docs_category_invoices);

        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<DocCategory> asList(){
            return new ArrayList<>(EnumSet.allOf(DocCategory.class));
        }
    }

    @AllArgsConstructor
    public enum DocStatus implements IBusinessEnumInterface {
        ALL(R.string.all),
        SENT(R.string.fragment_docs_status_sent),
        RECEIVED(R.string.fragment_docs_status_received);

        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<DocStatus> asList(){
            return new ArrayList<>(EnumSet.allOf(DocStatus.class));
        }
    }

    @AllArgsConstructor
    public enum DocType implements IBusinessEnumInterface {
        UNDEFINED(-1000,R.string.document_type_undefined),
        CHEQUE(-1,R.string.document_type_cheque),
        PDF(0, R.string.document_type_pdf),
        APPLICATION_PURCHASE_CHEQUE(1, R.string.document_type_application_purchase_cheque),
        CHEQUE_ACCEPTANCE_PROTOCOL(2, R.string.document_type_cheque_acceptance_protocol),
        APPLICATION_SELL_CHEQUE(3, R.string.document_type_application_sell_cheque),
        ACCOUNT_FOR_ENROLLMENT(4, R.string.document_type_account_for_enrollment),
        APPLICATION_PAY_CHEQUE(5, R.string.document_type_application_pay_cheque),
        APPLICATION_TRANSFER_CHEQUE(6, R.string.document_type_application_transfer_cheque),
        ACT_OF_RENDERED_SERVICES(7, R.string.document_type_act_of_rendered_services),
        INVOICE(8, R.string.document_type_invoice),
        ISSUE_CHEQUE(9, R.string.document_type_issue_cheque),
        VERIFY_CHEQUE(10, R.string.document_type_verify_cheque),
        NOTICE_OF_VERIFY_CHEQUE(11, R.string.document_type_notice_of_verify_cheque),
        REJECT_PAYMENT(12, R.string.document_type_reject_payment),
        INVOICE_FOR_PAYMENT(13, R.string.document_type_invoice_for_payment),
        CLAIM_CHECK(14, R.string.document_type_claim_check),
        NOTICE_OF_FUNDS_CREDITING(15, R.string.document_type_notice_of_funds_crediting),
        KEY_TRANSFER_DOC(16, R.string.document_type_key_transfer_doc),
        BROKER_TRANSACTION_NOTICE(17, R.string.document_type_broker_transaction_notice),
        CLIENT_TRANSACTION_NOTICE(18, R.string.document_type_client_transaction_notice),
        PAYMENT_REFUSAL_NOTICE(19, R.string.document_type_payment_refusal_notice),
        PURCHASE_REFUSAL_NOTICE(20, R.string.document_type_purchase_refusal_notice),
        SALE_REFUSAL_NOTICE(21, R.string.document_type_sale_refusal_notice),
        REPAYMENT_REFUSAL_NOTICE(22, R.string.document_type_repayment_refusal_notice),
        ORDER_OF_MONEY_TRANSFER_TO_CLIENT(23, R.string.document_type_order_of_money_transfer_to_client),
        BROKER_COMPLETE_NOTIFICATION(24, R.string.document_type_broker_complete_notification),
        ORDER_OF_MONEY_TRANSFER(25, R.string.document_type_order_of_money_transfer),
        INVOICE_FOR_PAYMENT_NON_RESIDENT(32, R.string.document_type_invoice_for_payment_non_resident),
        NOTICE_OF_ENROLLMENT_CREDITING(33, R.string.document_type_notice_of_enrollment_crediting);

        @Getter int val;
        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<DocType> asList() {
            return new ArrayList<>(EnumSet.allOf(DocType.class));
        }

        public static String getTextByVal(Context context, int val) {
            for (DocType type : asList()) {
                if (type.val == val) {
                    return context.getString(type.titleResId);
                }
            }

            return null;
        }

        public static final List<Integer> PERMITTED_DOC_LIST = new ArrayList<Integer>() {{
            add(DocType.CHEQUE_ACCEPTANCE_PROTOCOL.val);
            add(DocType.APPLICATION_TRANSFER_CHEQUE.val);
            add(DocType.APPLICATION_PURCHASE_CHEQUE.val);
            add(DocType.APPLICATION_SELL_CHEQUE.val);
            add(DocType.INVOICE_FOR_PAYMENT.val);
            add(DocType.CLIENT_TRANSACTION_NOTICE.val);
            add(DocType.REJECT_PAYMENT.val);
        }};
    }

    @Nullable
    @AllArgsConstructor
    public enum TemplateType implements IBusinessEnumInterface {
        BUY(R.string.deposit_2, R.drawable.ic_deposit),
        SALE(R.string.withdrawal_2, R.drawable.ic_withdraw),
        TRANSFER(R.string.transfer_2, R.drawable.ic_transfer),
        INVOICE(R.string.invoice_2, R.drawable.ic_cheque_2);

        int titleResId;
        @Getter int iconResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static TemplateType getTemplateFromActionType(int actionType) {
            switch (actionType) {
                case AppConstants.ACTION_TYPE_TRANSFER:
                case AppConstants.ACTION_TYPE_INVOICE_TRANSFER:
                    return TRANSFER;

                case AppConstants.ACTION_TYPE_DEPOSIT:
                    return BUY;

                case AppConstants.ACTION_TYPE_WITHDRAWAL:
                    return SALE;

                case AppConstants.ACTION_TYPE_INVOICE:
                    return INVOICE;

                default:
                    return null;
            }
        }
    }

    @AllArgsConstructor
    public enum RegType implements IBusinessEnumInterface {
        START(1, R.string.regtype_start),
        STANDART(2, R.string.regtype_standart),
        ISSUER(3, R.string.regtype_issuer),
        DEALER(4, R.string.regtype_dealer),
        BANK(5, R.string.regtype_bank);

        @Getter int val;
        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<RegType> asList(){
            return new ArrayList<>(EnumSet.allOf(RegType.class));
        }
        
        public static int getTitleResId(int val){
            for (RegType rt : RegType.values()){
                if (rt.val == val){
                    return rt.getTitleResId();
                }
            }
            
            return R.string.unknown;
        }

        public static RegType get(int val) {
            for (RegType rt : RegType.values()){
                if (rt.val == val){
                    return rt;
                }
            }

            return STANDART;
        }
    }

    @AllArgsConstructor
    public enum ClientCategory implements IBusinessEnumInterface {
        INDIVIDUAL(R.string.fl),
        ORGANIZATION(R.string.ul);

        @Getter int titleResId;

        @Override
        public String toString(Context context) {
            return context.getString(titleResId);
        }

        public static List<ClientCategory> asList(){
            return new ArrayList<>(EnumSet.allOf(ClientCategory.class));
        }
    }

    public enum PassRecoveryType {EMAIL, PHONE}
}
