package com.rusoft.chequepay.utils;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.info.Country;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class CurrencyUtils {
    public static final String ISO_RUB = "643";
    public static final String ISO_USD = "840";
    public static final String ISO_EUR = "978";

    public static final int DEF_COST_USDRUB = 60;
    public static final int DEF_COST_EURRUB = 70;

    public static final int DEF_RUB_MIN = 10;
    public static final int DEF_RUB_MAX = 10000000;
    public static final int DEF_USD_MIN = 1;
    public static final int DEF_USD_MAX = 1695000;
    public static final int DEF_EUR_MIN = 1;
    public static final int DEF_EUR_MAX = 1555000;

    @AllArgsConstructor
    public enum Currency {
        RUB(1, ISO_RUB, "\u20BD", true, "Рубль"),
        USD(2, ISO_USD, "$", false, "Доллар"),
        EUR(3, ISO_EUR, "€", false, "Евро");

        @Getter int code;
        @Getter String strCode;
        @Getter String symbol;
        @Getter boolean isEnabledForResident;
        @Getter String name;

        @Override
        public String toString() {
            return name();
        }

        public static List<CurrencyUtils.Currency> asList(){
            return new ArrayList<>(EnumSet.allOf(CurrencyUtils.Currency.class));
        }
    }

    public static String getSymbol(int code) {
        for (Currency currency : Currency.values()) {
            if (currency.code == code) {
                return currency.symbol;
            }
        }
        return null;
    }

    public static String getSymbol(String strCode) {
        for (Currency currency : Currency.values()) {
            if (currency.strCode.equals(strCode)) {
                return currency.symbol;
            }
        }
        return null;
    }

    public static String getStrCode(int code) {
        for (Currency currency : Currency.values()) {
            if (currency.code == code) {
                return currency.strCode;
            }
        }
        return null;
    }

    public static String getName(int code) {
        for (Currency currency : Currency.values()) {
            if (currency.code == code) {
                return currency.getName() + ", " + currency.name();
            }
        }
        return null;
    }

    public static String getName(String strCode) {
        for (Currency currency : Currency.values()) {
            if (currency.strCode.equals(strCode)) {
                return currency.getName() + ", " + currency.name();
            }
        }
        return null;
    }

    public static Currency getCurrency(String currencyISO) {
        for (Currency currency : Currency.values()) {
            if (currency.strCode.equals(currencyISO)) {
                return currency;
            }
        }
        return null;
    }

    public static Currency getCurrency(int code) {
        for (Currency currency : Currency.values()) {
            if (currency.code == code) {
                return currency;
            }
        }
        return null;
    }

    public static List<String> getCurrencyStrCodeList(List<CurrencyUtils.Currency> currencyList) {
        List<String> currencyStrList = new ArrayList<>();
        for (CurrencyUtils.Currency item : currencyList) {
            currencyStrList.add(item.getStrCode());
        }
        return currencyStrList;
    }

    public static Currency getCurrencyByCountry(Country country){
        if (country.getCountryId() == BusinessConstants.COUNTRY_CODE_RUSSIA){
            return Currency.RUB;
        }

        int[] euroIds = {
                40, 56, 276, 300, //Австрия, Бельгия, Германия, Греция
                372, 724, 380, 196, //Ирландия, Испания, Италия, Кипр
                428, 440, 442, 470, // Латвия, Литва, Люксембург, Мальта
                528, 620, 703, 705, // Нидерланды, Португалия, Словакия, Словения
                246, 250, 233 //Финляндия, Франция, Эстония
        };

        for (int id : euroIds){
            if (id == country.getCountryId()){
                return Currency.EUR;
            }
        }

        //others
        return Currency.USD;
    }

    /**
     * Converts double value to locale independent currency string. Always uses dots as decimal
     * separators and spaces as grouping separators.
     *
     * @param amount       value to be converted
     * @param groupingUsed true if grouping separation should be used for large number
     *                     (e.g. 1 000 000 instead of 1000000)
     * @param decimals     number of digits after decimal separator
     * @return locale specific currency string
     */
    public static String format(double amount, boolean groupingUsed, int decimals) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator(',');
        formatSymbols.setGroupingSeparator(' ');
        DecimalFormat formatter = new DecimalFormat();
        formatter.setDecimalFormatSymbols(formatSymbols);

        //if (amount % 1 == 0) decimals = 0; // uncomment if want to remove zeroes

        formatter.setMinimumFractionDigits(decimals);
        formatter.setMaximumFractionDigits(decimals);
        formatter.setGroupingUsed(groupingUsed);
        return formatter.format(Math.abs(amount));
    }

    public static String format(double amount, boolean groupingUsed) {
        return format(amount, groupingUsed, 2); //TODO Уточнить количество знаков после запятой
    }

    public static String format(double amount) {
        return format(amount, true);
    }

    public static String format(double amount, String currencyStrCode) {
        return format(amount, true) + " " + getSymbol(currencyStrCode);
    }

    public static String format(double amount, int currencyCode) {
        return format(amount, true) + " " + getSymbol(currencyCode);
    }

    public static double getFormatDouble(String amount) {
        Double formatAmount = Double.parseDouble(amount.replace(",", "."));
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        decimalFormat.applyPattern("###.##");
        return Double.valueOf(decimalFormat.format(formatAmount));
    }

    public static String getCurrencySymbolFromAccountNumber(String accountNumber) {
        return getSymbol(getCurrencyFromAccountNumber(accountNumber).getCode());
    }

    public static Currency getCurrencyFromAccountNumber(String accountNumber) {
        String currency = accountNumber.substring(0, 2);
        switch (currency) {
            case "RU":
                return Currency.RUB;

            case "EU":
                return Currency.EUR;

            case "US":
                return Currency.USD;

            default:
                throw new InternalError(ErrorUtils.INTERNAL_ERROR + " undefined currency: " + accountNumber);
        }
    }

    public static int getCurrencyDrawableFromAccNumber(String accNumber) {
        String currency = accNumber.substring(0, 2);
        switch (currency) {
            case "RU":
                return R.drawable.ic_currency_ru;

            case "US":
                return R.drawable.ic_currency_us;

            case "EU":
                return R.drawable.ic_currency_eu;

            default:
                return R.drawable.ic_accounts_item_circle_burger;
        }
    }

    public static boolean isAmountInRange(String currencyStrCode, Double amount) {
        boolean result;

        switch (currencyStrCode) {
            case ISO_RUB:
                result = amount >= DEF_RUB_MIN && amount <= DEF_RUB_MAX;
                break;

            case ISO_USD:
                result = amount >= DEF_USD_MIN && amount <= DEF_USD_MAX;
                break;

            case ISO_EUR:
                result = amount >= DEF_EUR_MIN && amount <= DEF_EUR_MAX;
                break;

            default:
                LogUtil.e(ErrorUtils.INTERNAL_ERROR);
                result = false;
        }

        return result;
    }
}
