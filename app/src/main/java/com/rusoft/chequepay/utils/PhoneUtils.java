package com.rusoft.chequepay.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.ICustomInputView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.rusoft.chequepay.AppConstants.FILE_PHONE_MASKS;
import static com.rusoft.chequepay.BusinessConstants.RECOVERY_PHONE_MAX_LENGTH;
import static com.rusoft.chequepay.utils.PhoneUtils.formatPhone;
import static com.rusoft.chequepay.utils.PhoneUtils.getPhoneDigits;
import static com.rusoft.chequepay.utils.PhoneUtils.getPhoneMasksArray;

public class PhoneUtils {

    public static String getPhoneDigits(String sequence) {
        return sequence.replaceAll("[^0-9+]", "");
    }

    public static List<String> getPhoneMasksArray(Context context) {
        List<String> phoneMasksList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        AssetManager assetManager = context.getAssets();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assetManager.open(FILE_PHONE_MASKS)));
            String piece = "";
            while ((piece = bufferedReader.readLine()) != null) {
                stringBuilder.append(piece);
            }
            bufferedReader.close();
            JSONArray jsonMasksArray = new JSONArray(stringBuilder.toString());
            for (int i = 0; i < jsonMasksArray.length(); i++) {
                phoneMasksList.add(jsonMasksArray.getJSONObject(i).getString("mask"));
            }

            return phoneMasksList;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return phoneMasksList;
    }

    public static String formatPhone(String mask, String phoneNumber) {
        char[] charMask = mask.toCharArray();
        char[] formattedCharMask = new char[mask.length()];
        for (int i = 0; i < mask.length(); i++) {
            if (Character.isDigit(charMask[i])) {
                formattedCharMask[i] = '_';
            } else {
                formattedCharMask[i] = charMask[i];
            }
        }

        char[] chars = new char[formattedCharMask.length];
        char[] charsPhone = phoneNumber.toCharArray();
        ArrayList<String> aList = new ArrayList<>();
        for (char itemPhone : charsPhone) {
            aList.add(String.valueOf(itemPhone));
        }

        ListIterator<String> listIterator = aList.listIterator();
        for (int i = 0; i < formattedCharMask.length; i++) {
            char item = formattedCharMask[i];
            if (item != "_".charAt(0)) {
                chars[i] = item;
            } else {
                try {
                    chars[i] = aList.get(listIterator.nextIndex()).charAt(0);
                    listIterator.next();
                } catch (Exception e) {
                    chars[i] = '_';
                }
            }
        }
        return String.valueOf(chars);
    }

    public interface PhoneInputListener {
        void onPhoneChanged(boolean isValid, String number);
    }

    public static void initializePhoneInputMasksSetting(@NonNull Context context, @NonNull ICustomInputView phoneInput, @Nullable PhoneInputListener listener) {
        PhoneMasksHelper helper = new PhoneMasksHelper(context, phoneInput, listener);
        helper.initializePhoneInputWatcher();
    }
}

class PhoneMasksHelper {

    private Context context;
    private ICustomInputView phoneInput;
    private PhoneUtils.PhoneInputListener listener;

    private String mask = "";
    private boolean isEditPhoneNumber = false;

    public PhoneMasksHelper(@NonNull Context context, @NonNull ICustomInputView phoneInput, @Nullable PhoneUtils.PhoneInputListener listener) {
        this.context = context;
        this.phoneInput = phoneInput;
        this.listener = listener;
    }

    protected void initializePhoneInputWatcher() {
        List<String> phoneMasksList = getPhoneMasksArray(context);

        if (phoneMasksList.size() > 0) {
            phoneInput.addTextWatcher((AfterTextChangedWatcher) (Editable s) -> {
                if (!isEditPhoneNumber) {
                    isEditPhoneNumber = true;

                    String number = s.toString().replaceAll("\\D+", "");
                    int maskNum = 0;
                    for (String tempMask : phoneMasksList) {
                        String emptyMaskFormated = tempMask.replaceAll("\\D+", "");
                        if (emptyMaskFormated.startsWith(number)) {
                            mask = tempMask;
                            maskNum++;
                        }
                    }

                    if (maskNum == 0) {
                        phoneInput.setText(formatPhone(mask, number));
                    } else {
                        String text = phoneInput.getText();
                        if (text.length() > 0) phoneInput.setText("+" + number);
                        else phoneInput.setText("");
                    }

                    char[] afterStringArr = phoneInput.getText().toCharArray();
                    int lastNumberPosition = 0;
                    for (int i = 0; i < afterStringArr.length; i++) {
                        if (Character.isDigit(afterStringArr[i])) {
                            lastNumberPosition = i + 1;
                        }
                    }
                    phoneInput.setSelection(lastNumberPosition);

                    if (lastNumberPosition == 0 && !phoneInput.getText().isEmpty())
                        phoneInput.setText("");

                    boolean isNumberValid = (phoneInput.getText().length() == mask.length()) && (!phoneInput.getText().contains("_"));
                    if (listener != null)
                        listener.onPhoneChanged(isNumberValid, PhoneUtils.getPhoneDigits(phoneInput.getText()));

                } else {
                    isEditPhoneNumber = false;
                }
            });

        } else {
            phoneInput.addTextWatcher(new MaskedTextChangedListener(AppConstants.PHONE_RECOVERY_FORMAT, phoneInput.getTextView(),
                    (boolean maskFilled, String extractedValue) -> {
                        boolean isNumberValid = !(extractedValue.length() < RECOVERY_PHONE_MAX_LENGTH);
                        if (listener != null)
                            listener.onPhoneChanged(isNumberValid, getPhoneDigits(phoneInput.getText()));
                    }
            ));
        }
    }

}
