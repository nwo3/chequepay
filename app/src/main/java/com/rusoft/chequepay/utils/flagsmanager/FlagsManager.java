package com.rusoft.chequepay.utils.flagsmanager;

import java.util.HashMap;
import java.util.Map;

public class FlagsManager {
    private static FlagsManager INSTANCE;
    private Map<Flags, Boolean> map;

    public static FlagsManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FlagsManager();
        }
        return INSTANCE;
    }

    private FlagsManager() {
        map = new HashMap<>();
        for (Flags f : Flags.values()) {
            map.put(f, false);
        }
    }

    public Boolean isFlagOn(Flags flag){
        return map.get(flag);
    }

    public Boolean isFlagOff(Flags flag){
        return !map.get(flag);
    }

    public void turnOn(Flags flag){
        map.put(flag, true);
    }

    public void turnOff(Flags flag){
        map.put(flag, false);
    }

    public void updateAll(){
        for(Flags f : Flags.values()){
            if (f.isUpdateFlag) turnOn(f);
        }
    }
}
