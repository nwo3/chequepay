package com.rusoft.chequepay.utils;


import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.repository.account.NewAccountModel;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.data.repository.documents.DocsFilterModel;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.FilterDataModel;
import com.rusoft.chequepay.data.repository.passrecovery.PassRecoveryModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.security.SecurityEditModel;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;
import com.rusoft.chequepay.data.repository.user.UserModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CacheManager {
    private UserModel user;
    private SecurityEditModel securityEditModel;
    private RegistrationModel registrationModel;
    private NewAccountModel newAccountModel;
    private DocsFilterModel documentsModel;
    private PassRecoveryModel passRecoveryModel;

    private CorrespondentModel correspondentModel;
    private FilterDataModel filterDataModel;

    private TransferModel transferModel;
    private DepositModel depositModel;
    private WithdrawalModel withdrawalModel;
    private InvoiceModel invoiceModel;
    private DetailOperationModel detailOperationModel;

    @Getter @Setter List<Country> countries;
    @Getter @Setter List<Region> regions;

    public UserModel getUser() {
        if (user == null) {
            user = new UserModel();
        }

        return user;
    }

    public void clearUserData() {
        user = null;
        securityEditModel = null;
        newAccountModel = null;
        correspondentModel = null;
        filterDataModel = null;
        transferModel = null;
        depositModel = null;
        withdrawalModel = null;
        invoiceModel = null;
    }

    public SecurityEditModel getSecurityEditModel() {
        if (securityEditModel == null) {
            securityEditModel = new SecurityEditModel();
        }

        return securityEditModel;
    }

    public void clearSecurityEditModel() {
        securityEditModel = null;
    }

    public TransferModel getTransferModel() {
        if (transferModel == null) {
            transferModel = new TransferModel();
        }

        return transferModel;
    }

    public void clearTransferModel() {
        transferModel = null;
    }

    public DepositModel getDepositModel() {
        if (depositModel == null) {
            depositModel = new DepositModel();
        }

        return depositModel;
    }

    public CorrespondentModel getCorrespondentModel() {
        if (correspondentModel == null) {
            correspondentModel = new CorrespondentModel();
        }
        return correspondentModel;
    }

    public DetailOperationModel getDetailOperationModel() {
        if (detailOperationModel == null) {
            detailOperationModel = new DetailOperationModel();
        }
        return detailOperationModel;
    }

    public FilterDataModel getFilterDataModel() {
        if (filterDataModel == null) {
            filterDataModel = new FilterDataModel();
        }
        return filterDataModel;
    }

    public void clearDepositModel() {
        depositModel = null;
    }

    public WithdrawalModel getWithdrawalModel() {
        if (withdrawalModel == null) {
            withdrawalModel = new WithdrawalModel();
        }

        return withdrawalModel;
    }

    public void clearWithdrawalModel() {
        withdrawalModel = null;
    }

    public InvoiceModel getInvoiceModel() {
        if (invoiceModel == null) {
            invoiceModel = new InvoiceModel();
        }

        return invoiceModel;
    }

    public void clearInvoiceModel() {
        invoiceModel = null;
    }

    public NewAccountModel getNewAccountModel() {
        if (newAccountModel == null) {
            newAccountModel = new NewAccountModel();
        }

        return newAccountModel;
    }

    public void clearNewAccountModel() {
        newAccountModel = null;
    }

    public DocsFilterModel getDocumentsModel() {
        if (documentsModel == null) {
            documentsModel = new DocsFilterModel();
        }

        return documentsModel;
    }

    public void clearDocumentsModel() {
        documentsModel = null;
    }

    public PassRecoveryModel getPassRecoveryModel() {
        if (passRecoveryModel == null) {
            passRecoveryModel = new PassRecoveryModel();
        }

        return passRecoveryModel;
    }

    public void clearPassRecoveryModel() {
        passRecoveryModel = null;
    }

    public RegistrationModel getRegistrationModel() {
        if (registrationModel == null) {
            registrationModel = new RegistrationModel();
            //registrationModel = RegistrationModel.MOCK(); //model for tests
        }

        return registrationModel;
    }

    public void clearRegistrationModel() {
        registrationModel = null;
    }
}
