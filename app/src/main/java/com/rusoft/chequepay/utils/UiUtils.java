package com.rusoft.chequepay.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.ICustomInputView;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.dialogs.CheckBoxPickerDialog;
import com.rusoft.chequepay.ui.dialogs.RadioButtonPickerDialog;

import java.util.List;

public class UiUtils {

    public static View.OnTouchListener getTouchListener() {
        return (view, motionEvent) -> {
            int action = motionEvent.getAction();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    animateViewAlpha(view, AppConstants.ENABLED_VIEW_ALPHA, AppConstants.DISABLED_VIEW_ALPHA);
                    return true;

                case MotionEvent.ACTION_UP:
                    animateViewAlpha(view, AppConstants.DISABLED_VIEW_ALPHA, AppConstants.ENABLED_VIEW_ALPHA);
                    view.performClick();
                    return true;

                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_CANCEL:
                    animateViewAlpha(view, AppConstants.DISABLED_VIEW_ALPHA, AppConstants.ENABLED_VIEW_ALPHA);
                    return false;
            }
            return false;
        };
    }

    public static void animateViewAlpha(View view, float fromAlpha, float toAlpha) {
        Animation animation = new AlphaAnimation(fromAlpha, toAlpha);
        animation.setDuration(AppConstants.DURATION_FAST);
        animation.setFillAfter(true);
        view.startAnimation(animation);
    }

    public static void requestFocusAndShowKeyboard(Context context, View view) {
        if (view != null) {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Service.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        }
    }

    public static void hideKeyboard(Context context) {
        if (context instanceof Activity) {
            View view = ((Activity) context).getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    public static void setKeyboardDoneKeyListener(Context context, View view) {
        view.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                view.clearFocus();
                hideKeyboard(context);
                return true;
            }
            return false;
        });
    }

    public interface KeyboardListener{
        void onKeyboardOpened();
        void onKeyboardClosed();
    }

    public static void setKeyboardListener(ViewGroup container, KeyboardListener listener){
        container.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            container.getWindowVisibleDisplayFrame(r);
            int screenHeight = container.getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                listener.onKeyboardOpened();

                if (container instanceof ScrollView) {
                    new Handler().post(() -> ((ScrollView) container).smoothScrollTo(0, keypadHeight));
                }

            } else {
                new Handler().post(listener::onKeyboardClosed);
            }
        });
    }

    public static void expand(final ViewGroup v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ?
                        ViewGroup.LayoutParams.WRAP_CONTENT : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density)); // 1 dp/ms
        v.startAnimation(a);
    }

    public static void collapse(final ViewGroup v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density)); // 1 dp/ms
        v.startAnimation(a);
    }

    //region Screen sizes getters
    public static int getScreenWidthPx(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeightPx(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static float getScreenWidthDP(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return getScreenWidthPx(context) / density;
    }

    public static float getScreenHeightDP(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return getScreenHeightPx(context) / density;
    }
    //endregion

    //region Dialogs
    public static void showInfoDialog(Context context, String message, int buttonTextResId,
                                      DialogInterface.OnClickListener buttonClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(context.getString(buttonTextResId), buttonClickListener)
                .show();
    }

    public static void showInfoDialog(Context context, int messageResId, int buttonTextResId) {
        showInfoDialog(context, context.getString(messageResId), buttonTextResId, (d, i) -> d.dismiss());
    }

    public static void showQuestionDialog(Context context, int messageResId, String messageAddition,
                                          int negativeButtonTextResId, int positiveButtonTextResId,
                                          boolean isCancellable, DialogInterface.OnClickListener buttonClickListener) {

        showQuestionDialog(context, context.getString(messageResId, messageAddition),
                negativeButtonTextResId, positiveButtonTextResId, isCancellable, buttonClickListener);
    }

    public static void showQuestionDialog(Context context, String message,
                                          int negativeButtonTextResId, int positiveButtonTextResId,
                                          boolean isCancellable, DialogInterface.OnClickListener buttonClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setNegativeButton(context.getString(negativeButtonTextResId), buttonClickListener)
                .setPositiveButton(context.getString(positiveButtonTextResId), buttonClickListener)
                .setCancelable(isCancellable)
                .show();
    }

    public static void showOnExitDialog(Context context) {
        showQuestionDialog(context, R.string.dialog_on_exit, null,
                R.string.cancel, R.string.exit,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            if (context instanceof BaseActivity) {
                                ((BaseActivity) context).finishAffinity();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    public static <T> void showRadioButtonPickerDialog(Context context, List<T> list, int defItemNum, RadioButtonPickerDialog.PickerListener pickerListener, Boolean isResident, Integer currencyFilter) {
        RadioButtonPickerDialog dialog = new RadioButtonPickerDialog<T>();
        dialog.setItems(list);
        dialog.setDefItemNum(defItemNum != -1 ? defItemNum : 0);
        dialog.setListener(pickerListener);
        dialog.setIsResident(isResident);
        dialog.setCurrencyFilter(currencyFilter);
        dialog.show(((Activity) context).getFragmentManager(), dialog.getClass().getSimpleName());
    }

    public static <T> void showRadioButtonPickerDialog(Context context, List<T> list, int defItemNum, RadioButtonPickerDialog.PickerListener pickerListener) {
        showRadioButtonPickerDialog(context, list, defItemNum, pickerListener, null, null);
    }

    public static <T> void showRadioButtonPickerDialog(Context context, List<T> list, int defItemNum, RadioButtonPickerDialog.PickerListener pickerListener, Integer currencyFilter) {
        showRadioButtonPickerDialog(context, list, defItemNum, pickerListener, null, currencyFilter);
    }

    public static <T> void showRadioButtonPickerDialog(Context context, List<T> list, int defItemNum, RadioButtonPickerDialog.PickerListener pickerListener, boolean isResident) {
        showRadioButtonPickerDialog(context, list, defItemNum, pickerListener, isResident, null);
    }

    public static <T> void showCheckBoxPickerDialog(Context context, int checkAllTitleResId, List<T> list, List<T> alreadySelected, CheckBoxPickerDialog.PickerListener pickerListener) {
        CheckBoxPickerDialog dialog = new CheckBoxPickerDialog<T>();
        dialog.setCheckAllTitle(context.getString(checkAllTitleResId));
        dialog.setItems(list);
        dialog.setSelectedItems(alreadySelected);
        dialog.setListener(pickerListener);
        dialog.show(((Activity) context).getFragmentManager(), dialog.getClass().getSimpleName());
    }
    //endregion

    public static boolean isInputEmpty(ICustomInputView v) {
        return v == null || v.getText() == null || v.getText().trim().length() == 0;
    }

    public static boolean validateInputViews(ICustomInputView... inputs){
        for (ICustomInputView input : inputs){
            if (!input.validate(true)) {
                return false;
            }
        }
        return true;
    }

}
