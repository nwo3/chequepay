package com.rusoft.chequepay.utils;

import android.util.Log;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * This class wraps {@link android.util.Log} and automatically adds to log message class name,
 * method name and line number at which log method was invoked. As a tag parameter it uses
 * {@link #TAG} field to allow filter logcat messages by its value.
 * Log output can be completely disabled by setting {@link #loggingEnabled} to false.
 *
 * Created by shirshov on 12.04.2017.
 */

public class LogUtil {

    private static final String TAG = "LogUtil";
    private static final int MAX_LOG_ENTRY_SIZE = 3000;
    private static final String CONTINUE_TAG = "[CONTINUE]";

    @Getter
    @Setter
    private static boolean loggingEnabled = true;

    /**
     * List of class names that could be between LogUtil entries in stackTrace. Only start of the
     * class names is needed.
     */
    @Getter
    @Setter
    private static String[] systemClasses = {"java", "com.android"};

    /**
     * Wraps {@link android.util.Log#v(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void v(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.v(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#d(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void d(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.d(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#i(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void i(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.i(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#w(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void w(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.w(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#e(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void e(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.e(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#wtf(String, String)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void wtf(String msg) {
        if (loggingEnabled) {
            for (String message : splitLongLogMessages(getLocation() + msg)) {
                Log.wtf(TAG, message);
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#v(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void v(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.v(TAG, message);
                } else {
                    Log.v(TAG, message, e);
                }
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#d(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void d(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.d(TAG, message);
                } else {
                    Log.d(TAG, message, e);
                }
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#i(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void i(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.i(TAG, message);
                } else {
                    Log.i(TAG, message, e);
                }
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#w(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void w(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.w(TAG, message);
                } else {
                    Log.w(TAG, message, e);
                }
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#e(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void e(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.e(TAG, message);
                } else {
                    Log.e(TAG, message, e);
                }
            }
        }
    }

    /**
     * Wraps {@link android.util.Log#wtf(String, String, Throwable)} as stated in class description
     *
     * @param msg Message to be printed in log
     */
    public static void wtf(String msg, Throwable e) {
        if (loggingEnabled) {
            List<String> messageParts = splitLongLogMessages(getLocation() + msg);
            Iterator<String> iterator = messageParts.iterator();
            while (iterator.hasNext()){
                String message = iterator.next();
                if (iterator.hasNext()){
                    Log.wtf(TAG, message);
                } else {
                    Log.wtf(TAG, message, e);
                }
            }
        }
    }

    private static String getLocation() {
            boolean found = false;
            for (StackTraceElement traceElement : Thread.currentThread().getStackTrace()) {
                found = found || isTraceElementPointsToLogUtil(traceElement);
                if (found && !isTraceElementPointsToLogUtil(traceElement) && !isTraceElementPointsToSystemClass(traceElement)) {
                    return MessageFormat.format("[{0}:{1}:{2}]: ",
                            parseClassName(traceElement),
                            traceElement.getMethodName(),
                            traceElement.getLineNumber());
                }
            }
        return "[]: ";
    }

    private static String parseClassName(StackTraceElement traceElement) {
        String className = traceElement.getClassName();
        int endIndex = className.indexOf('$');
        if (endIndex > 0) {
            className = className.substring(0, endIndex);
        }
        return className.substring(className.lastIndexOf('.') + 1, className.length());
    }

    private static boolean isTraceElementPointsToLogUtil (StackTraceElement trace){
        return trace.getClassName().startsWith(LogUtil.class.getName());
    }

    private static boolean isTraceElementPointsToSystemClass(StackTraceElement trace){
        boolean result = false;
        String traceClassName = trace.getClassName();
        for (String systemClassName : systemClasses) {
            result = traceClassName.startsWith(systemClassName);
            if (result) {
                break;
            }
        }
        return result;
    }

    private static List<String> splitLongLogMessages(String logMessage) {
        List<String> splittedMessages = new ArrayList<>();
        while (true) {
            if (logMessage.length() > MAX_LOG_ENTRY_SIZE) {
                splittedMessages.add(logMessage.substring(0, MAX_LOG_ENTRY_SIZE));
                logMessage = CONTINUE_TAG + logMessage.substring(MAX_LOG_ENTRY_SIZE, logMessage.length());
            } else {
                splittedMessages.add(logMessage);
                return splittedMessages;
            }
        }
    }

    private LogUtil() {
    }
}
