package com.rusoft.chequepay.utils.flagsmanager;


import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Flags {
    EVENT(true),
    INVOICES(true),
    CONTACTS(true),
    ACCOUNTS(true),
    TEMPLATE(true),
    PROFILE(true),
    DOCUMENTS(true),

    TOKEN_EXPIRED(false),
    IP_RESTRICTION(false);

    boolean isUpdateFlag;
}
