package com.rusoft.chequepay.utils;


import android.content.Context;

import com.rusoft.chequepay.AppConstants;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.NoSuchPaddingException;

import devliving.online.securedpreferencestore.DefaultRecoveryHandler;
import devliving.online.securedpreferencestore.SecuredPreferenceStore;

public class Preferences {

    private static final String STORAGE_NAME = "storage";

    private static final String KEY_AUTH_TOKEN = "key_auth_token";
    private static final String KEY_CLIENT_ID = "key_client_id";
    private static final String KEY_PIN = "key_pin";
    private static final String KEY_TIMESTAMP = "timestamp";

    private static final String IS_FINGERPRINT_AUTHENTICATION_DIALOG_SHOWN = "is_fingerprint_authentication_dialog_shown";
    private static final String FINGERPRINT_AUTHENTICATION_ENABLED = "fingerprint_authentication_enabled";
    private static final String AUTHENTICATED_BY_FINGERPRINT = "authenticated_by_fingerprint";

    private SecuredPreferenceStore securedPreferences;

    public Preferences(Context context) {
        try {
            SecuredPreferenceStore.init(context, new DefaultRecoveryHandler());
            securedPreferences = SecuredPreferenceStore.getSharedInstance();

        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException |
                UnrecoverableEntryException | NoSuchPaddingException | InvalidAlgorithmParameterException |
                InvalidKeyException | NoSuchProviderException e) {

            LogUtil.e("SecuredPreferenceStore creation failed with exception:");
            e.printStackTrace();
        }
    }

    private SecuredPreferenceStore getSecuredPreferences() {
        return securedPreferences;
    }

    public void clear() {
        storeToken("");
        storeClientId(AppConstants.NO_DATA);
        storePinCode("");
        setAuthenticatedByFingerprint(false);
        setFingerprintAuthenticationEnabled(false);
        setIsFingerprintAuthenticationDialogShown(false);
    }

    public void remove(String key) {
        getSecuredPreferences().edit().remove(key).apply();
    }

    private boolean contains(String key) {
        return getSecuredPreferences().contains(key);
    }

    //region get/put methods
    private String getString(String key) {
        return getSecuredPreferences().getString(key, "");
    }

    private void putString(String key, String value) {
        getSecuredPreferences().edit().putString(key, value).apply();
    }

    private int getInt(String key) {
        return getSecuredPreferences().getInt(key, AppConstants.NO_DATA);
    }

    private void putInt(String key, int value) {
        getSecuredPreferences().edit().putInt(key, value).apply();
    }

    private long getLong(String key) {
        return getSecuredPreferences().getLong(key, AppConstants.NO_DATA);
    }

    private void putLong(String key, long value) {
        getSecuredPreferences().edit().putLong(key, value).apply();
    }

    private boolean getBoolean(String key) {
        return getSecuredPreferences().getBoolean(key, false);
    }

    private void putBoolean(String key, boolean value) {
        getSecuredPreferences().edit().putBoolean(key, value).apply();
    }
    //endregion

    public void storeToken(String token) {
        putString(KEY_AUTH_TOKEN, token);
    }

    public void storeClientId(long clientId) {
        putLong(KEY_CLIENT_ID, clientId);
    }

    public void storePinCode(String pin) {
        putString(KEY_PIN, pin);
    }

    public void setIsFingerprintAuthenticationDialogShown(boolean IsFingerprintAuthenticationDialogShown) {
        putBoolean(IS_FINGERPRINT_AUTHENTICATION_DIALOG_SHOWN, IsFingerprintAuthenticationDialogShown);
    }

    public void setFingerprintAuthenticationEnabled(boolean isFingerprintAuthenticationEnabled) {
        putBoolean(FINGERPRINT_AUTHENTICATION_ENABLED, isFingerprintAuthenticationEnabled);
    }

    public void setAuthenticatedByFingerprint(boolean isAuthenticatedByFingerprint) {
        putBoolean(AUTHENTICATED_BY_FINGERPRINT, isAuthenticatedByFingerprint);
    }

    public void storeTimestamp(long timestamp) {
        putLong(KEY_TIMESTAMP, timestamp);
    }

    public String getAuthToken() {
        return getString(KEY_AUTH_TOKEN);
    }

    public long getClientId() {
        return getLong(KEY_CLIENT_ID);
    }

    public String getPinCode() {
        return getString(KEY_PIN);
    }

    public boolean IsFingerprintAuthenticationDialogShown() {
        return getBoolean(IS_FINGERPRINT_AUTHENTICATION_DIALOG_SHOWN);
    }
    public boolean isFingerprintAuthenticationEnabled() {
        return getBoolean(FINGERPRINT_AUTHENTICATION_ENABLED);
    }

    public boolean isAuthenticatedByFingerprint() {
        return getBoolean(AUTHENTICATED_BY_FINGERPRINT);
    }

    public long getTimestamp() {
        return getLong(KEY_TIMESTAMP);
    }

    public boolean isAuthorized() {
        return !getAuthToken().isEmpty() && getClientId() != AppConstants.NO_DATA
                && (!getPinCode().isEmpty() || isAuthenticatedByFingerprint());
    }
}
