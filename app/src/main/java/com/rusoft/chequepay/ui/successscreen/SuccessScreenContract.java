package com.rusoft.chequepay.ui.successscreen;

import com.rusoft.chequepay.ui.base.IBaseActivityView;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface SuccessScreenContract {

    interface IView extends IBaseActivityView{
        void setText(String text);
        void done();
        void showAddTemplateDialog();
        void showTemplateAddedSuccessfullyDialog();
        void hideAddTemplate(boolean state);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(int actionType);
        void addTemplate(int actionType, String title);
    }

}
