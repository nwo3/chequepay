package com.rusoft.chequepay.ui.successscreen;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.SuccessIndicatorView;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class SuccessActivity extends BaseActivity implements SuccessScreenContract.IView {

    @Inject
    SuccessScreenPresenter presenter;

    @BindView(R.id.logo) View logo;
    @BindView(R.id.text) TextView text;
    @BindView(R.id.success) SuccessIndicatorView successIndicator;
    @BindView(R.id.add_template) View addTemplate;
    @BindView(R.id.done) View done;

    private int actionType;

    @Override
    protected int layoutResId() {
        return R.layout.activity_success;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerSuccessScreenComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .successScreenModule(new SuccessScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        actionType = getIntent().getIntExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.NO_DATA);
        presenter.onViewCreated(actionType);
        successIndicator.startAnimation();
        addTemplate.setOnClickListener(view -> showAddTemplateDialog());
        addTemplate.setOnTouchListener(UiUtils.getTouchListener());
        done.setOnClickListener(view -> done());
        done.setOnTouchListener(UiUtils.getTouchListener());

    }

    @Override
    public void setText(String text) {
        this.text.setText(text);
    }

    @Override
    public void showAddTemplateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(this.getString(R.string.dialog_on_creating_template_title));
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_with_input, null, false);
        final EditText input = viewInflated.findViewById(R.id.input);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            String title = input.getText().toString();
            if (!title.isEmpty()){
                presenter.addTemplate(actionType, title);
                dialog.dismiss();
            } else {
                input.setError(getString(R.string.dialog_on_creating_template_hint));
                input.requestFocus();
            }

        });

        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    @Override
    public void hideAddTemplate(boolean state) {
        addTemplate.setVisibility(state ? INVISIBLE : VISIBLE);
    }

    @Override
    public void showTemplateAddedSuccessfullyDialog() {
        UiUtils.showInfoDialog(this, getString(R.string.success_screen_create_template_success), R.string.ok, (dialogInterface, i) -> done());
    }

    @Override
    public void done() {
        switch (actionType) {
            case AppConstants.ACTION_TYPE_REGISTRATION:
            case AppConstants.ACTION_TYPE_PASS_RECOVERY:
                goToAuth();
                break;

            default:
                finish();
        }
    }

    private void goToAuth() {
        Intent intent = new Intent(this, AuthActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, actionType);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        done();
    }
}
