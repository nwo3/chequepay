package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.EventsAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;

public class ActivitiesFragment extends BaseRootFragment implements ActivitiesContract.IView, ActivitiesAdapter.ItemClickListener {

    @Inject
    ActivitiesPresenter presenter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.segmented2) SegmentedGroup segmentedGroup;
    @BindView(R.id.label) TextView emptyExcerptLabel;
    @BindView(R.id.rv_operations) RecyclerView recyclerViewActivities;
    @BindView(R.id.btn_segmented_control_operation) RadioButton rbOperations;
    @BindView(R.id.btn_segmented_control_action) RadioButton rbActivities;

    private List<Application> activitiesList = new ArrayList<>();
    private ActivitiesAdapter eventsAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_activities;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_activities);
    }

    public static ActivitiesFragment getNewInstance(String name) {
        ActivitiesFragment activitiesFragment = new ActivitiesFragment();

        Bundle arguments = new Bundle();
        activitiesFragment.setArguments(arguments);

        return activitiesFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerActivitiesComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .activitiesModule(new ActivitiesModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        eventsAdapter = new ActivitiesAdapter(getContext(), activitiesList, this);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewActivities.setLayoutManager(llm);
        recyclerViewActivities.setAdapter(eventsAdapter);
        recyclerViewActivities.addItemDecoration(new EventsAdapter.DividerCustomItemDecoration(getContext()));

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());

        rbOperations.setTextColor(getResources().getColor(R.color.white));
        rbActivities.setTextColor(getResources().getColor(R.color.white));
        segmentedGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.btn_segmented_control_operation:
                    presenter.onClickOperations();
                    break;
                case R.id.btn_segmented_control_action:

                    break;
            }
        });

        presenter.onViewCreated();
    }

    @Override
    public void showActivitiesList(List<Application> applicationsList) {
        activitiesList.clear();
        activitiesList.addAll(applicationsList);
        eventsAdapter.notifyDataSetChanged();

        recyclerViewActivities.setVisibility(applicationsList.isEmpty() ? View.INVISIBLE : View.VISIBLE);
        emptyExcerptLabel.setVisibility(applicationsList.isEmpty() ? View.VISIBLE : View.INVISIBLE );
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        super.showLoadingIndicator(enabled);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(enabled);
        }
    }

    @Override
    public void onActivitySelected(Application invoiceInfoData) {
        presenter.onClickItem(invoiceInfoData);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }
}
