package com.rusoft.chequepay.ui.mainscreen.fragments.more.faq;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.ExpandableTextView;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;

public class FaqFragment extends BaseBranchFragment implements FaqContract.IView {

    @BindView(R.id.container) LinearLayout container;

    @Inject
    FaqPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_faq;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_faq);
    }

    public static FaqFragment getNewInstance(String containerName) {
        FaqFragment fragment = new FaqFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerFaqComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .faqModule(new FaqModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        container.removeAllViews();
        Map<Integer, Integer> mapFaq = new LinkedHashMap<>();
        mapFaq.put(R.string.faq_title_1, R.string.faq_text_1);
        mapFaq.put(R.string.faq_title_2, R.string.faq_text_2);
        mapFaq.put(R.string.faq_title_3, R.string.faq_text_3);
        mapFaq.put(R.string.faq_title_4, R.string.faq_text_4);
        mapFaq.put(R.string.faq_title_5, R.string.faq_text_5);
        mapFaq.put(R.string.faq_title_6, R.string.faq_text_6);
        mapFaq.put(R.string.faq_title_7, R.string.faq_text_7);
        mapFaq.put(R.string.faq_title_8, R.string.faq_text_8);
        mapFaq.put(R.string.faq_title_9, R.string.faq_text_9);
        mapFaq.put(R.string.faq_title_10, R.string.faq_text_10);
        mapFaq.put(R.string.faq_title_11, R.string.faq_text_11);
        mapFaq.put(R.string.faq_title_12, R.string.faq_text_12);
        mapFaq.put(R.string.faq_title_13, R.string.faq_text_13);
        mapFaq.put(R.string.faq_title_14, R.string.faq_text_14);
        mapFaq.put(R.string.faq_title_15, R.string.faq_text_15);
        mapFaq.put(R.string.faq_title_16, R.string.faq_text_16);
        mapFaq.put(R.string.faq_title_17, R.string.faq_text_17);
        mapFaq.put(R.string.faq_title_18, R.string.faq_text_18);
        mapFaq.put(R.string.faq_title_19, R.string.faq_text_19);
        mapFaq.put(R.string.faq_title_20, R.string.faq_text_20);
        mapFaq.put(R.string.faq_title_21, R.string.faq_text_21);
        mapFaq.put(R.string.faq_title_22, R.string.faq_text_22);
        mapFaq.put(R.string.faq_title_23, R.string.faq_text_23);
        mapFaq.put(R.string.faq_title_24, R.string.faq_text_24);
        mapFaq.put(R.string.faq_title_25, R.string.faq_text_25);
        mapFaq.put(R.string.faq_title_26, R.string.faq_text_26);
        mapFaq.put(R.string.faq_title_27, R.string.faq_text_27);
        mapFaq.put(R.string.faq_title_28, R.string.faq_text_28);
        mapFaq.put(R.string.faq_title_29, R.string.faq_text_29);
        mapFaq.put(R.string.faq_title_30, R.string.faq_text_30);
        mapFaq.put(R.string.faq_title_31, R.string.faq_text_31);
        mapFaq.put(R.string.faq_title_32, R.string.faq_text_32);
        mapFaq.put(R.string.faq_title_33, R.string.faq_text_33);
        mapFaq.put(R.string.faq_title_34, R.string.faq_text_34);
        mapFaq.put(R.string.faq_title_35, R.string.faq_text_35);
        mapFaq.put(R.string.faq_title_36, R.string.faq_text_36);
        mapFaq.put(R.string.faq_title_37, R.string.faq_text_37);
        mapFaq.put(R.string.faq_title_38, R.string.faq_text_38);
        mapFaq.put(R.string.faq_title_39, R.string.faq_text_39);

        for (Map.Entry<Integer, Integer> entry : mapFaq.entrySet()) {
            Integer keyLabel = entry.getKey();
            Integer value = entry.getValue();

            LinearLayout itemView = (LinearLayout) getLayoutInflater().inflate(R.layout.list_faq_item, null, false);
            ExpandableTextView expandableTextView = (ExpandableTextView) itemView.getChildAt(0);

            expandableTextView.setTextTitle(keyLabel);
            expandableTextView.setTextContent(value);
            container.addView(itemView);
        }
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }
}
