package com.rusoft.chequepay.ui.registration.fragment.individual.personal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.OnTextChangedWatcher;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.PersonSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.ui.registration.fragment.base.BaseRegFragment;
import com.rusoft.chequepay.utils.PhoneUtils;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;

public class RegPersonalFragment extends BaseRegFragment implements RegPersonalContract.IView {

    @Inject
    RegPersonalPresenter presenter;

    @BindView(R.id.last_name) DynamicInputView last_name;
    @BindView(R.id.first_name) DynamicInputView first_name;
    @BindView(R.id.patronymic) DynamicInputView patronymic;
    @BindView(R.id.birth_date) DynamicInputView birth_date;
    @BindView(R.id.inn) DynamicInputView inn;
    @BindView(R.id.mob_num) DynamicInputView phone;
    @BindView(R.id.email_2) DynamicInputView email;
    @BindView(R.id.web_site) DynamicInputView web;
    @BindView(R.id.focus_handler) View focus_handler;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_personal;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.reg_title_personal);
    }

    public static RegPersonalFragment getNewInstance() {
        return new RegPersonalFragment();
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegPersonalComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regPersonalModule(new RegPersonalModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ((RegistrationActivity) getActivity()).scrollToStart();

        last_name.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (last_name != null && last_name.validate())
                presenter.onLastNameFocusLost(last_name.getText());
        });

        first_name.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (first_name != null && first_name.validate())
                presenter.onFirstNameFocusLost(first_name.getText());
        });

        patronymic.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (patronymic != null && patronymic.validate())
                presenter.onPatronymicFocusLost(patronymic.getText());
        });

        patronymic.getTextView().setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_NEXT) {
                if (birth_date.getText().isEmpty()) {
                    showDatePicker();
                } else {
                    inn.requestFocus();
                }
                return true;
            }
            return false;
        });

        birth_date.setOnClickListener(v -> {
            if (birth_date.isEnabled()) showDatePicker();
        });

        inn.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (inn != null && inn.validate())
                presenter.onInnFocusLost(inn.getText());
        });

        email.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (email != null && email.validate()) {
                presenter.onEmailFocusLost(email.validate() ? email.getText() : null);
            }
        });

        web.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (web != null && web.validate())
                presenter.onWebSiteFocusLost(web.validate() ? web.getText() : null);
        });

        last_name.setValidationPattern(ValidationPatterns.CREDENTIALS);
        first_name.setValidationPattern(ValidationPatterns.CREDENTIALS);
        patronymic.setValidationPattern(ValidationPatterns.CREDENTIALS);
        birth_date.setValidationPattern(ValidationPatterns.EMPTY_PATTERN);
        inn.setValidationPattern(ValidationPatterns.PERSONAL_INN);
        email.setValidationPattern(ValidationPatterns.EMAIL);
        web.setValidationPattern(ValidationPatterns.WEB_SITE);
    }

    @Override
    public void updateViewByResidency(boolean isResident) {
        inn.setVisibility(isResident ? View.VISIBLE : View.GONE);

        if (isResident) {
            phone.setValidationPattern(ValidationPatterns.PHONE_RUS);
            phone.addTextWatcher(new MaskedTextChangedListener(AppConstants.PHONE_FORMAT, phone.getTextView(),
                    (maskFilled, extractedValue) -> {
                        if (maskFilled) {
                            presenter.onPhoneFocusLost(phone.validate() ? AppConstants.PHONE_CODE_RUS + extractedValue : null);
                        }
                    }
            ));
        } else {
            PhoneUtils.initializePhoneInputMasksSetting(getContext(), phone,
                    (isValid, number) -> presenter.onPhoneFocusLost(isValid ? number : null));
        }
    }


    @Override
    public void updateScreenIfInProfileMode(boolean isInProfileEditMode) {
        if (isInProfileEditMode) {
            last_name.setEnabled(false);
            first_name.setEnabled(false);
            patronymic.setEnabled(false);
            birth_date.setEnabled(false);
            inn.setEnabled(false);
        }
    }


    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(getContext(),

                // OnDateSetListener
                (view, year, month, day) -> {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, day);

                    birth_date.setError(null);
                    birth_date.setText(AppConstants.SDF_ddMMyyyy.format(calendar.getTime()));

                    if (inn.isShown()) {
                        inn.requestFocus();
                    } else {
                        phone.requestFocus();
                    }

                    presenter.onBirthDateSelected(calendar);
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    @Override
    public void setErrorOnBirthDateSelector(int textResId) {
        birth_date.setError(getString(textResId));
    }

    @Override
    public void setErrorOnPhoneInput(int textResId) {
        phone.setError(getString(textResId));
    }

    @Override
    public void setErrorOnEmailInput(int textResId) {
        email.setError(getString(textResId));
    }

    @Override
    public void restoreState(PersonSubModel model) {
        last_name.setText(model.getLastName());
        first_name.setText(model.getFirstName());
        patronymic.setText(model.getPatronymic());
        birth_date.setText(model.getBirthDateTs() != null ? AppConstants.SDF_ddMMyyyy.format(model.getBirthDateTs()) : null);
        inn.setText(model.getInn());
        phone.setText(model.getMobPhone());
        email.setText(model.getEmail());
        web.setText(model.getWebSite());
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        focus_handler.requestFocus();

        if (UiUtils.validateInputViews(last_name, first_name, patronymic, birth_date, inn, phone, email, web)) {
            presenter.onClickNext(PhoneUtils.getPhoneDigits(phone.getText()), email.getText());
        }
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_PROFILE_EDIT);
        startActivity(intent);
    }

    @Override
    public void exit() {
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public String getContainerName() {
        return Screens.REGISTRATION_CONTAINER;
    }
}
