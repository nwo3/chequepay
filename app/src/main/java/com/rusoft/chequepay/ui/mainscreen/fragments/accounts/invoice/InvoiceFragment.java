package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class InvoiceFragment extends BaseBranchFragment implements InvoiceContract.IView {

    @Inject
    InvoicePresenter presenter;

    @BindView(R.id.account) StaticInputView account;
    @BindView(R.id.switch_) SwitchCompat modeSwitch;
    @BindView(R.id.target_manual) StaticInputView target_manual;
    @BindView(R.id.target_selector) StaticInputView target_selector;
    @BindView(R.id.lifetime) StaticInputView lifetime;
    @BindView(R.id.amount) StaticInputView amount;
    @BindView(R.id.comment) StaticInputView comment;

    @BindView(R.id.proceed) TextView proceed;

    //disabled
    @BindView(R.id.contract_num) StaticInputView contractNum;
    @BindView(R.id.date) StaticInputView date;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_invoice;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_invoice);
    }

    public static InvoiceFragment getNewInstance(String containerName, Serializable data) {
        InvoiceFragment fragment = new InvoiceFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerInvoiceComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .invoiceModule(new InvoiceModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        account.setOnClickListener(view -> presenter.onClickAccount());
        target_selector.setOnClickListener(view -> presenter.onClickContactsSelector());

        modeSwitch.setOnCheckedChangeListener((v, state) -> presenter.onModeSwitchStateChanged(state));

        target_manual.setOnFocusChangedListener((view, focused) -> {
            if (!focused && !UiUtils.isInputEmpty(target_manual)) {
                presenter.onContractorFocusLost(target_manual.getText());
            }
        });

        lifetime.setOnFocusChangedListener((view, focused) -> {
            if (!focused) {
                if (!UiUtils.isInputEmpty(lifetime)) presenter.onLifetimeFocusLost(lifetime.getText());
                lifetime.validate();
            }
        });

        amount.setOnFocusChangedListener((view, focused) -> {
            if (amount != null){
                if (focused) {
                    amount.setText("");
                } else {
                    amount.validate();
                    presenter.onAmountFocusLost(amount.getText());
                }
            }
        });

        amount.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                UiUtils.hideKeyboard(getContext());
                v.clearFocus();
                return true;
            }
            return false;
        });

        UiUtils.setKeyboardDoneKeyListener(getContext(), comment);
        comment.setOnFocusChangedListener((view, focused) -> {
            if (comment != null && !focused) {
                String trim = comment.getText().trim();
                presenter.onCommentFocusLost(trim);
                comment.setText(trim);
                comment.validate();
            }
        });

        comment.addTextWatcher((AfterTextChangedWatcher) editable -> {
            if (comment != null && !UiUtils.isInputEmpty(comment)) presenter.onCommentFocusLost(comment.getText());
            else disableProceedButton(true);
        });

        contractNum.setOnFocusChangedListener((view, focused) -> {
            if (!focused && !UiUtils.isInputEmpty(contractNum))
                presenter.onContractNumFocusLost(contractNum.getText());
        });

        date.setOnClickListener((view -> presenter.onClickDate()));

        proceed.setOnClickListener(view -> presenter.onClickProceed());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void changeTargetInputMode(boolean state){
        modeSwitch.setChecked(state);
        target_manual.setVisibility(state ? View.GONE : View.VISIBLE);
        target_selector.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void applyTemplateMode() {
        modeSwitch.setVisibility(View.GONE);
        target_manual.setVisibility(View.GONE);
        target_selector.setVisibility(View.VISIBLE);
        target_selector.setEnabled(false);
    }

    @Override
    public void setAccountText(String text) {
        if (account != null) account.setText(text);
    }

    @Override
    public void setTargetManualText(String text) {
        if (target_manual != null) target_manual.setText(text);
    }

    @Override
    public void setTargetSelectorText(String text) {
        if (target_selector != null) target_selector.setText(text);
    }

    @Override
    public void setLifetimeText(String text) {
        if (lifetime != null) lifetime.setText(text);
    }

    @Override
    public void setCommentText(String text) {
        if (comment != null) comment.setText(text);
    }

    @Override
    public void setDateText(String text) {
        if (date != null) date.setText(text);
    }

    @Override
    public void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onAccountSelected(accounts.get(selected)));
    }

    @Override
    public void showContactsSelectorDialog(List<Correspondent> correspondents, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), correspondents, selectedIndex,
                selected -> presenter.onContactSelected(correspondents.get(selected)));
    }

    @Override
    public void showTargetInputError(Integer textResId) {
        target_manual.setError(textResId != null ? getString(textResId) : null);
    }

    @Override
    public void setAmountText(String text) {
        amount.setText(text);
    }

    @Override
    public void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(getContext(),
                (picker, y, m, d) -> {
                    calendar.set(y, m, d);
                    presenter.onDateSelected(calendar.getTimeInMillis());
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    @Override
    public void showAddNewAccountDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_accounts, null,
                R.string.cancel, R.string.dialog_on_empty_accounts_accept,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToNewAccount();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_INVOICE);
        startActivity(intent);
    }

    @Override
    public void disableAccountInput(boolean disabled) {
        account.setEnabled(!disabled);
    }

    @Override
    public void disableModeSwitch(boolean disabled) {
        modeSwitch.setEnabled(!disabled);
        modeSwitch.setAlpha(!disabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
    }

    @Override
    public void disableContractorInput(boolean disabled) {
        target_manual.setEnabled(!disabled);
    }

    @Override
    public void disableOtherInputs(boolean disabled) {
        lifetime.setEnabled(!disabled);
        amount.setEnabled(!disabled);
        comment.setEnabled(!disabled);
        contractNum.setEnabled(!disabled);
        date.setEnabled(!disabled);
    }

    @Override
    public void disableProceedButton(boolean disabled) {
        proceed.setEnabled(!disabled);
    }

    @Override
    public void disableTargetSelectorAndSwitch() {
        target_selector.setEnabled(false);
        modeSwitch.setVisibility(View.GONE);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
