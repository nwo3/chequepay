package com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer;

import android.app.DownloadManager;
import android.content.Context;
import android.os.Environment;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.documents.DocumentsRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

import static android.content.Context.DOWNLOAD_SERVICE;

public class PdfViewerPresenter extends BasePresenter implements PdfViewerContract.IPresenter {

    private WeakReference<PdfViewerContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context context;
    private byte[] pdf;

    @Inject
    DocumentsRepository documentsRepository;

    @Inject
    public PdfViewerPresenter(PdfViewerContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context context) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.context = context;
    }

    @Override
    public void onViewCreated() {
        addRepositories(documentsRepository);
    }

    @Override
    public void showPdf(Long idDownload) {
        documentsRepository.getPdf(idDownload, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError();
            }

            @Override
            public void onSuccess(ResponseBody response) {
                try {
                    pdf = response.bytes();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (pdf != null) {
                    view.get().showPdf(pdf);
                }
            }
        });
    }

    @Override
    public void downloadPdf(Long idDownload) {
        File file = null;
        try {
            file = new File(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()), String.valueOf(idDownload) + ".pdf");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(pdf);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (file != null) {
            DownloadManager downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
            downloadManager.addCompletedDownload(file.getName(), file.getName(), true, "application/pdf",file.getAbsolutePath(),file.length(),true);
            view.get().showSnackbar(context.getString(R.string.fragment_pdf_downloaded));
        } else {
            view.get().showSnackbar(context.getString(R.string.error_save_pdf));
        }
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }
}
