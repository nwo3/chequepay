package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

class IPsAdapter extends RecyclerView.Adapter<IPsAdapter.AccountsViewHolder> {
    private List<String> ips;

    interface ItemClickListener {
        void onIpSelected(int pos, String ip);
        void onIpDelete(int pos);
    }

    private ItemClickListener listener;

    public IPsAdapter(List<String> ips, ItemClickListener listener) {
        this.ips = ips;
        this.listener = listener;
    }

    @Override
    public AccountsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ip_item, parent, false);
        return new AccountsViewHolder(view);
    }

    class AccountsViewHolder extends RecyclerView.ViewHolder {
        View del;
        TextView ip;

        AccountsViewHolder(View itemView) {
            super(itemView);
            del = itemView.findViewById(R.id.delete);
            ip = itemView.findViewById(R.id.ip);
        }
    }

    @Override
    public void onBindViewHolder(AccountsViewHolder holder, int position) {
        String ip = ips.get(position);
        holder.ip.setText(ip);
        holder.ip.setOnClickListener(v -> listener.onIpSelected(position, ip));
        holder.ip.setOnTouchListener(UiUtils.getTouchListener());

        holder.del.setOnClickListener(view -> listener.onIpDelete(position));
        holder.del.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public int getItemCount() {
        return ips.size();
    }
}
