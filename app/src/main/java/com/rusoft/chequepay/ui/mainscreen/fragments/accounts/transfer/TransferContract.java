package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface TransferContract {

    interface IView extends IBaseView {
        void setSourceText(String text);
        void setTargetManualText(String text);
        void setContactSelectorText(String text);
        void setContactAccountSelectorText(String text);
        void setContactAccountSelectorHint(String text);
        void setAmountText(String text);
        void setCommentText(String text);
        void setCurrencyText(String text);
        void setCommissionText(String text);
        void setTotalText(String text);

        void changeTargetInputMode(boolean state);
        void applyTemplateMode();

        void showSourceAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident);
        void showContactsSelectorDialog(List<Correspondent> correspondents, int selectedIndex);
        void showContactAccountsSelectorDialog(List<com.rusoft.chequepay.data.entities.correspondents.response.Account> accounts, int selectedIndex);

        void showTargetInputError(Integer textResId);

        void disableSourceAccountInput(boolean disabled);
        void disableTargetManualInput(boolean disabled);
        void disableContactAccountSelector(boolean disabled);
        void disableAmountInput(boolean disabled);
        void disableTargetSelectorAndSwitch();

        void disableModeSwitch(boolean disabled);
        void disableProceedButton(boolean disabled);

        void showAddNewAccountDialog();
        void showDepositDialog(Account account, boolean isInfoDialogType);

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(Serializable data);

        void onClickSource();
        void onSourceAccountSelected(Account account);

        void onModeSwitchStateChanged(boolean state); //false: manual, true: from list

        void onTargetManualFocusLost(String inputData);

        void onClickContactSelector();
        void onClickContactAccountSelector();

        void onContactSelected(Correspondent correspondents);
        void onContactAccountSelected(com.rusoft.chequepay.data.entities.correspondents.response.Account account);

        void onAmountFocusLost(String inputData);
        void onCommentFocusLost(String inputData);

        void goToDeposit(Account account);
        void goToNewAccount();

        void onClickProceed();
        void clearModel();
    }
}
