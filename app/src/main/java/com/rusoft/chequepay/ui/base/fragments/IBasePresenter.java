package com.rusoft.chequepay.ui.base.fragments;

public interface IBasePresenter {

    void onClickBack();
    void onResume();
    void onDestroy();
    void unsubscribe();

}
