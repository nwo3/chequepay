package com.rusoft.chequepay.ui.mainscreen.fragments.more.faq;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {FaqModule.class, ChequepayApiModule.class})
public interface FaqComponent {
    void inject(FaqFragment fragment);
}
