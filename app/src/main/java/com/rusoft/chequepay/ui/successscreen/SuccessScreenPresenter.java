package com.rusoft.chequepay.ui.successscreen;

import android.content.Context;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositRepository;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceRepository;
import com.rusoft.chequepay.data.repository.transactions.templates.TemplatesRepository;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransfersRepository;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class SuccessScreenPresenter extends BasePresenter implements SuccessScreenContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    RegistrationRepository registrationRepository;

    @Inject
    DepositRepository depositRepository;

    @Inject
    TransfersRepository transfersRepository;

    @Inject
    WithdrawalRepository withdrawalRepository;

    @Inject
    InvoiceRepository invoiceRepository;

    @Inject
    TemplatesRepository templatesRepository;

    private WeakReference<SuccessScreenContract.IView> view;

    @Inject
    public SuccessScreenPresenter(SuccessScreenContract.IView view, RegistrationRepository registrationRepository, DepositRepository depositRepository, WithdrawalRepository withdrawalRepository, InvoiceRepository invoiceRepository) {
        this.view = new WeakReference<>(view);
        this.registrationRepository = registrationRepository;
        this.depositRepository = depositRepository;
        this.withdrawalRepository = withdrawalRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public void onViewCreated(int actionType) {
        updateView(actionType);
    }

    private void updateView(int actionType) {
        Context context = (Context) view.get();
        switch (actionType){

            case AppConstants.ACTION_TYPE_AUTH:
                view.get().hideAddTemplate(true);
                break;

            case AppConstants.ACTION_TYPE_REGISTRATION:
                view.get().setText(context.getString(R.string.success_screen_registration_text));
                view.get().hideAddTemplate(true);
                break;

            case AppConstants.ACTION_TYPE_TRANSFER:
                TransferModel transferModel = transfersRepository.getModel();
                view.get().setText(context.getString(R.string.success_screen_text_transfer, transferModel.getOperationUID()));
                view.get().hideAddTemplate(transferModel.isTemplateUsed());
                break;

            case AppConstants.ACTION_TYPE_DEPOSIT:
                DepositModel depositModel = depositRepository.getModel();
                view.get().setText(context.getString(R.string.success_screen_text_deposit, depositModel.getApplicationCoreId()));
                view.get().hideAddTemplate(depositModel.isTemplateUsed());
                break;

            case AppConstants.ACTION_TYPE_WITHDRAWAL:
                WithdrawalModel withdrawalModel = withdrawalRepository.getModel();
                view.get().setText(context.getString(R.string.success_screen_text_withdrawal, withdrawalModel.getApplicationCoreId(),
                        CurrencyUtils.format(withdrawalModel.getAmount(), withdrawalModel.getCurrency())));
                view.get().hideAddTemplate(withdrawalModel.isTemplateUsed());
                break;

            case AppConstants.ACTION_TYPE_INVOICE:
                InvoiceModel invoiceModel = invoiceRepository.getModel();
                view.get().setText(context.getString(R.string.success_screen_text_invoice, invoiceModel.getApplicationCoreId()));
                view.get().hideAddTemplate(invoiceModel.isTemplateUsed());
                break;

            case AppConstants.ACTION_TYPE_SECURITY_SETTINGS_CHANGE:
                view.get().setText(context.getString(R.string.success_screen_text_security_settings_changed));
                view.get().hideAddTemplate(true);
                break;

            case AppConstants.ACTION_TYPE_PASS_RECOVERY:
                view.get().setText(context.getString(R.string.success_screen_text_pass_recovery));
                view.get().hideAddTemplate(true);
                break;

            case AppConstants.ACTION_TYPE_PROFILE_EDIT:
                view.get().setText(context.getString(R.string.success_screen_profile_edit_text));
                view.get().hideAddTemplate(true);
                break;
        }
    }

    @Override
    public void addTemplate(int actionType, String title) {
        BusinessConstants.TemplateType type = BusinessConstants.TemplateType.getTemplateFromActionType(actionType);
        if (type == null) throw new IllegalArgumentException(ErrorUtils.WRONG_TEMPLATE_TYPE);

        templatesRepository.saveTemplate(type, title, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(Long result) {
                view.get().showTemplateAddedSuccessfullyDialog();
            }
        });
    }
}