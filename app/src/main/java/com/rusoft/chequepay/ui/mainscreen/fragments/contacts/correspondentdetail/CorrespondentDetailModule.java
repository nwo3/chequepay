package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.correspondentdetail;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class CorrespondentDetailModule {
    private final CorrespondentDetailContract.IView view;

    public CorrespondentDetailModule(CorrespondentDetailContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    CorrespondentDetailContract.IView provideCorrespondentDetailView() {
        return view;
    }
}
