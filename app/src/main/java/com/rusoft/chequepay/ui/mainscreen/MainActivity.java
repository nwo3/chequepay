package com.rusoft.chequepay.ui.mainscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.BackPressedEvent;
import com.rusoft.chequepay.data.event.IpRestrictionEvent;
import com.rusoft.chequepay.data.event.TokenExpiredEvent;
import com.rusoft.chequepay.data.event.ToolbarBtnOnClickEvent;
import com.rusoft.chequepay.data.event.UpdateDataEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.containers.AccountsContainer;
import com.rusoft.chequepay.ui.mainscreen.containers.ContactsContainer;
import com.rusoft.chequepay.ui.mainscreen.containers.EventsContainer;
import com.rusoft.chequepay.ui.mainscreen.containers.MoreContainer;
import com.rusoft.chequepay.ui.mainscreen.containers.PaymentsContainer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;

import static com.rusoft.chequepay.BusinessConstants.NOTIFY_ITEM_POSITION;

public class MainActivity extends BaseActivity implements MainScreenContract.IView {
    @Inject
    MainScreenPresenter presenter;

    @Inject
    NavigatorHolder navigatorHolder;

    @BindView(R.id.toolbar) @Getter Toolbar toolbar;
    @BindView(R.id.nav_bar) AHBottomNavigation navBar;
    @BindView(R.id.ib_filter_btn) ImageButton ibFilterToggle;

    private AccountsContainer accountsContainerFragment;
    private PaymentsContainer paymentsContainerFragment;
    private EventsContainer eventsContainerFragment;
    private ContactsContainer contactsContainerFragment;
    private MoreContainer moreContainerFragment;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
    private AHBottomNavigationAdapter navigationAdapter;
    private AHNotification notification;

    @Getter @Setter private boolean fragmentKeyEventsOnClick;
    @Getter @Setter private String fragmentKeyEvents;

    private String from;

    @Override
    protected int layoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerMainScreenComponent.builder()
                .appLongPollingComponent(App.INSTANCE.getAppLongPollingComponent())
                .mainScreenModule(new MainScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    private Navigator navigator = new Navigator() {
        @Override
        public void applyCommands(Command[] commands) {
            for (Command command : commands) applyCommand(command);
        }

        private void applyCommand(Command command) {
            if (command instanceof Back) {
                finish();
            } else if (command instanceof SystemMessage) {

            } else if (command instanceof Replace) {
                FragmentManager fm = getSupportFragmentManager();
                switch (((Replace) command).getScreenKey()) {
                    case Screens.ACCOUNTS_CONTAINER:
                        fm.beginTransaction()
                                .detach(paymentsContainerFragment)
                                .detach(eventsContainerFragment)
                                .detach(contactsContainerFragment)
                                .detach(moreContainerFragment)
                                .attach(accountsContainerFragment)
                                .commitNow();
                        break;
                    case Screens.PAYMENTS_CONTAINER:
                        fm.beginTransaction()
                                .detach(accountsContainerFragment)
                                .detach(eventsContainerFragment)
                                .detach(contactsContainerFragment)
                                .detach(moreContainerFragment)
                                .attach(paymentsContainerFragment)
                                .commitNow();
                        break;
                    case Screens.EVENTS_CONTAINER:
                        fm.beginTransaction()
                                .detach(accountsContainerFragment)
                                .detach(paymentsContainerFragment)
                                .detach(contactsContainerFragment)
                                .detach(moreContainerFragment)
                                .attach(eventsContainerFragment)
                                .commitNow();
                        break;
                    case Screens.CONTACTS_CONTAINER:
                        fm.beginTransaction()
                                .detach(accountsContainerFragment)
                                .detach(paymentsContainerFragment)
                                .detach(eventsContainerFragment)
                                .detach(moreContainerFragment)
                                .attach(contactsContainerFragment)
                                .commitNow();
                        break;
                    case Screens.MORE_CONTAINER:
                        fm.beginTransaction()
                                .detach(accountsContainerFragment)
                                .detach(paymentsContainerFragment)
                                .detach(eventsContainerFragment)
                                .detach(contactsContainerFragment)
                                .attach(moreContainerFragment)
                                .commitNow();
                        break;
                }

            }
        }
    };

    private void initContainers() {
        FragmentManager fm = getSupportFragmentManager();
        accountsContainerFragment = (AccountsContainer) fm.findFragmentByTag(Screens.ACCOUNTS_CONTAINER);
        if (accountsContainerFragment == null) {
            accountsContainerFragment = AccountsContainer.getNewInstance(Screens.ACCOUNTS_CONTAINER);
            fm.beginTransaction()
                    .add(R.id.fragment_container, accountsContainerFragment, Screens.ACCOUNTS_CONTAINER)
                    .detach(accountsContainerFragment).commitNow();
        }

        paymentsContainerFragment = (PaymentsContainer) fm.findFragmentByTag(Screens.PAYMENTS_CONTAINER);
        if (paymentsContainerFragment == null) {
            paymentsContainerFragment = PaymentsContainer.getNewInstance(Screens.PAYMENTS_CONTAINER);
            fm.beginTransaction()
                    .add(R.id.fragment_container, paymentsContainerFragment, Screens.PAYMENTS_CONTAINER)
                    .detach(paymentsContainerFragment).commitNow();
        }

        eventsContainerFragment = (EventsContainer) fm.findFragmentByTag(Screens.EVENTS_CONTAINER);
        if (eventsContainerFragment == null) {
            eventsContainerFragment = EventsContainer.getNewInstance(Screens.EVENTS_CONTAINER);
            fm.beginTransaction()
                    .add(R.id.fragment_container, eventsContainerFragment, Screens.EVENTS_CONTAINER)
                    .detach(eventsContainerFragment).commitNow();
        }

        contactsContainerFragment = (ContactsContainer) fm.findFragmentByTag(Screens.CONTACTS_CONTAINER);
        if (contactsContainerFragment == null) {
            contactsContainerFragment = ContactsContainer.getNewInstance(Screens.CONTACTS_CONTAINER);
            fm.beginTransaction()
                    .add(R.id.fragment_container, contactsContainerFragment, Screens.CONTACTS_CONTAINER)
                    .detach(contactsContainerFragment).commitNow();
        }

        moreContainerFragment = (MoreContainer) fm.findFragmentByTag(Screens.MORE_CONTAINER);
        if (moreContainerFragment == null) {
            moreContainerFragment = MoreContainer.getNewInstance(Screens.MORE_CONTAINER, from);
            fm.beginTransaction()
                    .add(R.id.fragment_container, moreContainerFragment, Screens.MORE_CONTAINER)
                    .detach(moreContainerFragment).commitNow();
        }
    }

    @Override
    protected void initViews() {
        from = Objects.requireNonNull(getIntent().getExtras()).getString(Screens.KEY_FROM);
        initContainers();

        if (from == null) {
            throw new RuntimeException();
        } else if (from.equals(Screens.AUTH_ACTIVITY) || from.equals(AppConstants.FROM_PIN_TO_HELP)) {
            //auth screen has direct access only to help fragment in 'more' container
            //pin screen has direct access to main and to help screens
            navBar.setVisibility(View.GONE);
        } else {
            navBar.setVisibility(View.VISIBLE);
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_nav_bar_menu);
        navigationAdapter.setupWithBottomNavigation(navBar);

        navBar.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        navBar.addItems(bottomNavigationItems);
        navBar.setDefaultBackgroundColor(getResources().getColor(R.color.dark_slate_blue));
        navBar.setAccentColor(getResources().getColor(R.color.deep_turquoise_second));
        navBar.setInactiveColor(getResources().getColor(R.color.white));

        navBar.setOnTabSelectedListener((position, wasSelected) -> {
            setFragmentKeyEventsOnClick(true);
            switch (position) {
                case 0:
                    presenter.onClickNavAccounts();
                    return true;
                case 1:
                    presenter.onClickNavPayments();
                    return true;
                case 2:
                    presenter.onClickNavEvents();
                    return true;
                case 3:
                    presenter.onClickNavContacts();
                    return true;
                case 4:
                    presenter.onClickNavMore();
                    return true;
                default:
                    return false;
            }
        });

        ibFilterToggle.setOnClickListener(v -> EventBus.getDefault().post(new ToolbarBtnOnClickEvent()));

        presenter.onViewCreated(from);
    }

    @Override
    public void showProgress(boolean enabled) {
        if (progress != null) {
            progress.bringToFront();
            if (enabled) {
                progress.setAlpha(AppConstants.ENABLED_VIEW_ALPHA);
                progress.setVisibility(View.VISIBLE);
            } else {
                progress.animate()
                        .alpha(AppConstants.INVISIBLE_VIEW_ALPHA)
                        .setDuration(AppConstants.DURATION_VERY_FAST)
                        .withEndAction(() -> progress.setVisibility(View.INVISIBLE))
                        .start();
            }
        }
    }

    @Override
    public void showUnreadIndicator(int count) {
        notification = new AHNotification.Builder()
                .setTextColor(getResources().getColor(R.color.white))
                .setBackgroundColor(getResources().getColor(R.color.bright_turquoise_text))
                .setText(" " + String.valueOf(count) + " ")
                .build();

        navBar.setNotification(notification, NOTIFY_ITEM_POSITION);
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new BackPressedEvent());
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(UpdateDataEvent event) {
        presenter.getUnreadMessages();
    }

    @Subscribe
    public void onEvent(TokenExpiredEvent event) {
        presenter.onCatchTokenExpiredEvent();
    }

    @Subscribe
    public void onEvent(IpRestrictionEvent event) {
        presenter.onCatchIpRestrictionEvent();
    }

    @Override
    public void goToAuth() {
        finishAffinity();
        startActivity(new Intent(this, AuthActivity.class));
    }

    public void hideToolbarBtn() {
        (getToolbar().findViewById(R.id.ib_filter_btn)).setVisibility(View.GONE);
    }

    public void showToolbarBtn(int image) {
        ((ImageButton) getToolbar().findViewById(R.id.ib_filter_btn)).setImageDrawable(getResources().getDrawable(image));
        (getToolbar().findViewById(R.id.ib_filter_btn)).setVisibility(View.VISIBLE);
    }

    public int getCountUnreadMessages() {
        int count = 0;
        if (notification != null) {
            count = Integer.valueOf(notification.getText().trim());
        }
        return count;
    }
}