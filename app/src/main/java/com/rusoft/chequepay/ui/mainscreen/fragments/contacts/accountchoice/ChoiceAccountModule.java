package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ChoiceAccountModule {
    private final ChoiceAccountContract.IView view;

    public ChoiceAccountModule(ChoiceAccountContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ChoiceAccountContract.IView provideView() {
        return view;
    }
}
