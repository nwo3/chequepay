package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.TotalBalanceSectionView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.event.DepositScreenEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


public class AccountsFragment extends BaseRootFragment implements AccountsContract.IView, AccountsAdapter.ItemClickListener {

    @Inject AccountsPresenter presenter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.totalBalanceSection) TotalBalanceSectionView totalBalanceSection;
    @BindView(R.id.deposit) View deposit;
    @BindView(R.id.withdraw) View withdraw;
    @BindView(R.id.transfer) View transfer;
    @BindView(R.id.invoice) View invoice;
    @BindView(R.id.add_account) View addNewAccount;
    @BindView(R.id.section_3_title) View section3Title;
    @BindView(R.id.recycler) RecyclerView recycler;

    public static AccountsFragment getNewInstance(String containerName) {
        AccountsFragment fragment = new AccountsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_accounts;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_accounts);
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerAccountsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .accountsModule(new AccountsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());

        deposit.setOnClickListener(view -> presenter.onClickDeposit());
        withdraw.setOnClickListener(view -> presenter.onClickWithdraw());
        transfer.setOnClickListener(view -> presenter.onClickTransfer());
        invoice.setOnClickListener(view -> presenter.onClickInvoice());

        addNewAccount.setOnClickListener(view -> presenter.onClickNewAccount());

        deposit.setOnTouchListener(UiUtils.getTouchListener());
        withdraw.setOnTouchListener(UiUtils.getTouchListener());
        transfer.setOnTouchListener(UiUtils.getTouchListener());
        invoice.setOnTouchListener(UiUtils.getTouchListener());
        addNewAccount.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void updateTotalBalanceSection(double rubAmount, double usdAmount, double eurAmount) {
        totalBalanceSection.update(rubAmount, usdAmount, eurAmount);
    }

    @Override
    public void updateAccountsRecyclerView(List<Account> accounts) {
        if (accounts.size() > 0){
            section3Title.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.VISIBLE);
            recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            recycler.setAdapter(new AccountsAdapter(this, accounts));
            recycler.setHasFixedSize(true);
            recycler.setNestedScrollingEnabled(false);

        } else {
            section3Title.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRefillAccountsDialog() {
        super.showRefillAccountsDialog();
    }

    @Override
    public void onAccountSelected(Account account) {
        presenter.onAccountSelected(account);
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        super.showLoadingIndicator(enabled);
        if (!enabled) swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Subscribe
    public void onEvent(DepositScreenEvent depositScreenEvent) {
        presenter.onClickDeposit();
    }
}
