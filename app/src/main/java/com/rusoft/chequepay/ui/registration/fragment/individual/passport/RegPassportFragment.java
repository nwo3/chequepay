package com.rusoft.chequepay.ui.registration.fragment.individual.passport;

import android.app.DatePickerDialog;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.OnTextChangedWatcher;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.PassportSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.ui.registration.fragment.base.BaseRegFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;

public class RegPassportFragment extends BaseRegFragment implements RegPassportContract.IView {

    @Inject
    RegPassportPresenter presenter;

    @BindView(R.id.serial) DynamicInputView serial;
    @BindView(R.id.num) DynamicInputView num;
    @BindView(R.id.passport_issuer) DynamicInputView passport_issuer;
    @BindView(R.id.date) DynamicInputView date;
    @BindView(R.id.code) DynamicInputView code;
    @BindView(R.id.focus_handler) View focus_handler;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_passport;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.reg_title_passport);
    }

    public static RegPassportFragment getNewInstance() {
        return new RegPassportFragment();
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegPassportComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regPassportModule(new RegPassportModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ((RegistrationActivity) getActivity()).scrollToStart();

        passport_issuer.setValidationPattern(ValidationPatterns.PASSPORT_ISSUER);
        date.setValidationPattern(ValidationPatterns.EMPTY_PATTERN);
    }

    @Override
    public void initializeInputListeners() {
        serial.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (serial != null && serial.validate()) presenter.onSeriesFocusLost(serial.getText());
        });

        serial.getTextView().setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_NEXT) {
                num.requestFocus();
                return true;
            }
            return false;
        });

        num.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (num != null && num.validate()) presenter.onNumFocusLost(num.getText());
        });

        passport_issuer.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (passport_issuer != null && passport_issuer.validate()) presenter.onIssuerFocusLost(passport_issuer.getText());
        });

        passport_issuer.getTextView().setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_NEXT) {
                if (date.getText().isEmpty()) {
                    showDatePicker();
                } else {
                    code.requestFocus();
                }
                return true;
            }
            return false;
        });

        date.setOnClickListener(v -> showDatePicker());
    }

    @Override
    public void updateViewByResidency(boolean isResident) {
        code.setValidationPattern(isResident ? ValidationPatterns.PASSPORT_DEPARTMENT_CODE_RUS :
                ValidationPatterns.PASSPORT_DEPARTMENT_CODE_OTHER);

        if (isResident) {
            code.getTextView().setKeyListener(DigitsKeyListener.getInstance("0123456789-"));
            code.addTextWatcher(new MaskedTextChangedListener(AppConstants.PASSPORT_CODE_FORMAT, code.getTextView(),
                    (maskFilled, extractedValue) -> {
                        if (code != null && code.validate()) presenter.onCodeFocusLost(code.getText());
                    }
            ));
        } else {
            code.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
                if (code != null && code.validate()) presenter.onCodeFocusLost(code.getText());
            });
        }

        num.setValidationPattern(isResident ? ValidationPatterns.PASSPORT_NUMBER_RUS
                : ValidationPatterns.PASSPORT_NUMBER_AND_SERIAL_NUMBER);

        serial.setOptional(!isResident);
        serial.setVisibility(isResident ? View.VISIBLE : View.GONE);
        serial.setValidationPattern(ValidationPatterns.PASSPORT_SERIAL_RUS);

        num.setInputType(isResident ? InputType.TYPE_CLASS_NUMBER : InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        serial.setInputType(isResident ? InputType.TYPE_CLASS_NUMBER : InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        code.setInputType(isResident ? InputType.TYPE_CLASS_PHONE : InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(getContext(),

                // OnDateSetListener
                (view, year, month, day) -> {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, day);

                    date.setError(null);
                    date.setText(AppConstants.SDF_ddMMyyyy.format(calendar.getTime()));
                    code.requestFocus();

                    presenter.onDateSelected(calendar.getTimeInMillis());
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    @Override
    public void setErrorOnIssueDateInput(int textResId) {
        date.setError(getString(textResId));
    }

    @Override
    public void restoreState(PassportSubModel model) {
        num.setText(model.getNumber());
        serial.setText(model.getSeries());
        passport_issuer.setText(model.getIssuer());
        date.setText(model.getIssueDate() != null ? AppConstants.SDF_ddMMyyyy.format(model.getIssueDate()) : null);
        code.setText(model.getDepartmentCode());
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        focus_handler.requestFocus();

        if (UiUtils.validateInputViews(serial, num, passport_issuer, date, code)){
            presenter.onClickNext();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public String getContainerName() {
        return Screens.REGISTRATION_CONTAINER;
    }
}
