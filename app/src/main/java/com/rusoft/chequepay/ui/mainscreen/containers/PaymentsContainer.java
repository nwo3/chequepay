package com.rusoft.chequepay.ui.mainscreen.containers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseContainerFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit.DepositFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice.InvoiceFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer.TransferFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal.WithdrawalFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.payments.PaymentsFragment;

import java.io.Serializable;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class PaymentsContainer extends BaseContainerFragment {

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    private Navigator navigator;

    public static PaymentsContainer getNewInstance(String name) {
        PaymentsContainer paymentsContainer = new PaymentsContainer();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.EXTRA_NAME, name);
        paymentsContainer.setArguments(arguments);

        return paymentsContainer;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerContainersComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_container;
    }

    @Override
    protected String toolbarTitle() {
        return null;
    }

    @Override
    protected void initViews() {
        //ignore
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.container) {
                @Override
                protected Intent createActivityIntent(Context context, String screenKey, Object data) {
                    return null;
                }

                @Override
                protected Fragment createFragment(String screenKey, Object data) {
                    switch (screenKey) {
                        case Screens.PAYMENTS_FRAGMENT:
                            return PaymentsFragment.getNewInstance(Screens.PAYMENTS_CONTAINER);

                        case Screens.MONEY_DEPOSIT_FRAGMENT:
                            return DepositFragment.getNewInstance(Screens.PAYMENTS_CONTAINER, (Serializable) data);

                        case Screens.MONEY_WITHDRAWAL_FRAGMENT:
                            return WithdrawalFragment.getNewInstance(Screens.PAYMENTS_CONTAINER, (Serializable) data);

                        case Screens.MONEY_TRANSFER_FRAGMENT:
                            return TransferFragment.getNewInstance(Screens.PAYMENTS_CONTAINER, (Serializable) data);

                        case Screens.INVOICE_FRAGMENT:
                            return InvoiceFragment.getNewInstance(Screens.PAYMENTS_CONTAINER, (Serializable) data);

                        default:
                            throw new RuntimeException();
                    }
                }

                @Override
                protected void showSystemMessage(String message) {
                    showSnackbar(message);
                }

                @Override
                protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
                    if (!(command instanceof Replace)) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right);
                    }
                }

                @Override
                protected void exit() {
                    //ignore
                }
            };
        }
        return navigator;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.container) == null) {
            ciceroneHolder.getCicerone(Screens.PAYMENTS_CONTAINER).getRouter().replaceScreen(Screens.PAYMENTS_FRAGMENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ciceroneHolder.getCicerone(Screens.PAYMENTS_CONTAINER).getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        ciceroneHolder.getCicerone(Screens.PAYMENTS_CONTAINER).getNavigatorHolder().removeNavigator();
        super.onPause();
    }
}
