package com.rusoft.chequepay.ui.registration.fragment.base;

import android.os.Handler;

import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;

public abstract class BaseRegFragment extends BaseBranchFragment implements IBaseRegView {

    @Override
    public void updateNextButtonState(boolean enabled) {
        new Handler().post(() -> ((RegistrationActivity) getActivity()).updateNextButtonState(enabled));
    }
}
