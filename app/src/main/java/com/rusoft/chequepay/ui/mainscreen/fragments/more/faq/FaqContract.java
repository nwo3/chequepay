package com.rusoft.chequepay.ui.mainscreen.fragments.more.faq;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface FaqContract {

    interface IView extends IBaseView {}

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
    }
}
