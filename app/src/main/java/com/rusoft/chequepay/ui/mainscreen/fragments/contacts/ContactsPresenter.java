package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ContactsPresenter extends BasePresenter implements ContactsContract.IPresenter {

    @Inject
    ContractorRepository contractorRepository;

    @Inject
    UserRepository userRepository;

    private WeakReference<ContactsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;


    @Inject
    public ContactsPresenter(ContactsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(contractorRepository, userRepository);
    }

    @Override
    public void onClickContact(long contractorId) {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().newScreenChain(Screens.CONTACT_DETAIL_FRAGMENT, contractorId);
    }

    @Override
    public void onClickAddContact() {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().newScreenChain(Screens.ADD_CONTACT_FRAGMENT, null);
    }

    @Override
    public void onRefresh() {
        getContractorsList();
    }

    private void showContractor() {
        if (userRepository.getUser().getCorrespondents().size() > 0) {
            view.get().showContractorsList(userRepository.getUser().getCorrespondents());
        } else {
            view.get().showEmptyContractorsList();
        }
    }

    @Override
    public void getContractorsList() {
            contractorRepository.getCorrespondentList(new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showLoadingIndicator(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showLoadingIndicator(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    showContractor();
                }
            });
    }

}
