package com.rusoft.chequepay.ui.base;

import com.rusoft.chequepay.data.repository.BaseRepository;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BasePresenter implements IBasePresenter {

    private List<BaseRepository> repositories = new ArrayList<>();

    protected void addRepositories(BaseRepository... repositories) {
        this.repositories.addAll(Arrays.asList(repositories));
    }

    @Override
    public void unsubscribe() {
        for (BaseRepository repository : repositories) {
            repository.clearDisposable();
        }
    }

    @Override
    public void onClickBack() {
        //ignore
    }

    @Override
    public void onResume() {
        //ignore
    }

    @Override
    public void onDestroy() {
        //ignore
    }
}
