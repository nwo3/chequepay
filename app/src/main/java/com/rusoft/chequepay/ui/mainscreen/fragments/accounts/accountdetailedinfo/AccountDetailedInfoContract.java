package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetailedinfo;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;

public interface AccountDetailedInfoContract {

    interface IView extends IBaseView {

        void setFieldsText(String amount, String number, String name, String regdate, String issuer, String payer);

    }

    interface IPresenter extends IBasePresenter {

        void onViewCreated(Serializable data);

    }
}
