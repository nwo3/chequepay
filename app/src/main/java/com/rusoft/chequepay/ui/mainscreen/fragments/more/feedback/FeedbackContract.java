package com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface FeedbackContract {

    interface IView extends IBaseView {

    }

    interface IPresenter extends IBasePresenter {

    }
}
