package com.rusoft.chequepay.ui.registration.fragment.individual.personal;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegPersonalModule.class, ChequepayApiModule.class})
public interface RegPersonalComponent {
    void inject(RegPersonalFragment fragment);
}
