package com.rusoft.chequepay.ui.otpscreen;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.passrecovery.PassRecoveryRepository;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.security.SecurityRepository;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositRepository;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceRepository;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransfersRepository;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.LogUtil;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;


public class OtpScreenPresenter extends BasePresenter implements OtpScreenContract.IPresenter {

    private WeakReference<OtpScreenContract.IView> view;

    @Inject
    UserRepository userRepository;

    @Inject
    TransfersRepository transfersRepository;

    @Inject
    DepositRepository depositRepository;

    @Inject
    WithdrawalRepository withdrawalRepository;

    @Inject
    InvoiceRepository invoiceRepository;

    @Inject
    RegistrationRepository registrationRepository;

    @Inject
    SecurityRepository securityRepository;

    @Inject
    PassRecoveryRepository passRecoveryRepository;

    @Inject
    public OtpScreenPresenter(OtpScreenContract.IView view, UserRepository userRepository,
                              TransfersRepository transfersRepository, DepositRepository depositRepository,
                              WithdrawalRepository withdrawalRepository, InvoiceRepository invoiceRepository,
                              RegistrationRepository registrationRepository, SecurityRepository securityRepository,
                              PassRecoveryRepository passRecoveryRepository) {
        this.view = new WeakReference<>(view);
        this.userRepository = userRepository;
        this.transfersRepository = transfersRepository;
        this.depositRepository = depositRepository;
        this.withdrawalRepository = withdrawalRepository;
        this.invoiceRepository = invoiceRepository;
        this.registrationRepository = registrationRepository;
        this.securityRepository = securityRepository;
        this.passRecoveryRepository = passRecoveryRepository;
    }

    @Override
    public void enteredOtpLengthChanged(int length) {
        view.get().setProceedEnabled(length == BusinessConstants.OTP_LENGTH);
    }

    @Override
    public void onClickProceed(int otpType, String otp) {
        switch (otpType) {
            case AppConstants.ACTION_TYPE_AUTH:
                authConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_REGISTRATION:
                registrationConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_TRANSFER:
                transferConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_DEPOSIT:
                depositConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_WITHDRAWAL:
                withdrawalConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_INVOICE:
                invoiceConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_SECURITY_SETTINGS_CHANGE:
                securitySettingsChangeConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_PASS_RECOVERY:
                passRecoveryConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_PROFILE_EDIT:
                profileEditConfirm(otp);
                break;

            case AppConstants.ACTION_TYPE_INVOICE_TRANSFER:
                transferInvoiceConfirm(otp);
                break;

            case AppConstants.NO_DATA:
            LogUtil.e(ErrorUtils.INTERNAL_ERROR);
            break;
        }
    }

    private void authConfirm(String otp) {
        String login = userRepository.getUser().getLogin();
        String password = userRepository.getUser().getPassword();
        userRepository.authConfirm(login, password, otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onSuccess() {
                view.get().goToPin();
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    private void registrationConfirm(String otp) {
        RegistrationModel model = registrationRepository.getModel();
        registrationRepository.registrationConfirm(model.getFlowToken(), otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                model.setActionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void profileEditConfirm(String otp) {
        RegistrationModel model = registrationRepository.getModel();
        registrationRepository.editProfile(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                model.setActionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void transferConfirm(String otp) {
        transfersRepository.sendTransfer(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                transfersRepository.getModel().setTransactionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void depositConfirm(String otp) {
        depositRepository.createDeposit(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                depositRepository.getModel().setTransactionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void withdrawalConfirm(String otp) {
        withdrawalRepository.createWithdraw(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                withdrawalRepository.getModel().setTransactionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void invoiceConfirm(String otp) {
        invoiceRepository.createInvoice(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                invoiceRepository.getModel().setTransactionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void securitySettingsChangeConfirm(String otp) {
        securityRepository.changeSettings(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                securityRepository.getModel().setEditingDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }

    private void passRecoveryConfirm(String otp) {
        passRecoveryRepository.recoveryConfirm(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().exit();
            }
        });
    }

    private void transferInvoiceConfirm(String otp) {
        transfersRepository.sendInvoiceTransfer(otp, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                transfersRepository.getModel().setTransactionDone(true);
                view.get().goToSuccessScreen();
            }
        });
    }
}
