package com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class FeedbackModule {
    private final FeedbackContract.IView view;

    public FeedbackModule(FeedbackContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    FeedbackContract.IView provideView() {
        return view;
    }
}
