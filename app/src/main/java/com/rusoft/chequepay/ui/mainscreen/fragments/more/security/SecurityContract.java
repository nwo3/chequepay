package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface SecurityContract {

    interface IView extends IBaseView {

        void showPassTab();

        void showIpTab();
        void showSmsTab();
        void showErrorOnRepeatPassView(int textResId);

        void initializeIpTab(List<String> IPs);

        void changeFingerprintAuthenticationSwitchState(boolean state);
        void changeIpRestrictionSwitchState(boolean state);
        void updateIpRecycler();
        void changeSmsNotificationSwitchState(boolean state);

        void changeOtpSwitchState(boolean state);
        void enableProceed(boolean enabled);

        void showFingerprintAuthenticationStateChangingDialog(boolean state);

        void goToOtp();

    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickPassTab();
        void onClickIpTab();
        void onClickSmsTab();

        void onFingerprintSwitchStateChangedConfirm(boolean state);

        void onNewPassFocusLost(String text);
        void onRepeatPassFocusLost(String text);
        void onOldPassFocusLost(String text);

        void onIPSwitchStateChanged(boolean state);
        void onAddIP(String ip);
        void onEditIp(int index, String newIp);
        void onDeleteIp(int pos);

        void onSmsSwitchStateChanged(boolean state);
        void onOtpSwitchStateChanged(boolean state);

        void onClickProceed();

    }
}
