package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface ActivitiesContract {

    interface IView extends IBaseView {
        void showActivitiesList(List<Application> applicationsList);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickOperations();
        void onClickItem(Application invoiceInfoData);
        void onRefresh();

    }
}
