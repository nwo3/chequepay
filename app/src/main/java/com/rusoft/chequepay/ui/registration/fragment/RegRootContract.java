package com.rusoft.chequepay.ui.registration.fragment;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.repository.registration.model.RootSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;

import java.util.List;

public interface RegRootContract {

    interface IView extends IBaseRegView {
        void showStatusPicker(List<BusinessConstants.RegType> types, int selectedIndex);
        void showResidencyPicker(List<Country> countries, int selectedIndex);
        void showCategoryPicker(List<BusinessConstants.ClientCategory> categories, int selectedIndex);

        void setStatusText(int textResId);
        void setResidencyText(String text);
        void setCategoryText(int textResId);

        void lockStatusAndCategory(); //CP-382

        void disableResidencySelector(boolean disable);

        void restoreState(RootSubModel root);

        void showPassConfirmError(Integer textResId);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickStatus();
        void onStatusSelected(BusinessConstants.RegType type);

        void onClickResidency();
        void onResidentSelected(Country country);

        void onClickCategory();
        void onCategorySelected(BusinessConstants.ClientCategory category);

        void onPassFocusLost(String text);
        void onPassConfirmFocusLost(String text);

        void onClickNext();

    }
}
