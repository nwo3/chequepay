package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {InvoiceModule.class, ChequepayApiModule.class})
public interface InvoiceComponent {
    void inject(InvoiceFragment fragment);
}
