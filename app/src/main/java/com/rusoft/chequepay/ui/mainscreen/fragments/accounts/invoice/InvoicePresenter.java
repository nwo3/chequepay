package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice;

import android.os.Handler;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.templates.InvoiceTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class InvoicePresenter extends BasePresenter implements InvoiceContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    InvoiceRepository invoiceRepository;

    private WeakReference<InvoiceContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    private InvoiceModel model;

    @Inject
    public InvoicePresenter(InvoiceContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, invoiceRepository);
        this.model = invoiceRepository.getModel();

        onModeSwitchStateChanged(false);

        UserModel user = userRepository.getUser();

        if (data == null) {
            onAccountSelected(user.getOldestAccount());

        } else if (data instanceof Account) {
            onAccountSelected((Account) data);

        } else if (data instanceof Correspondent) {
            Correspondent correspondentFromContacts = (Correspondent) data;
            onAccountSelected(user.getOldestAccount());
            onModeSwitchStateChanged(true);
            onContactSelected(correspondentFromContacts);
            if (correspondentFromContacts.isFromContact()) {
                view.get().disableTargetSelectorAndSwitch();
            }
        } else if (data instanceof InvoiceTemplate) {
            invoiceRepository.applyTemplate((InvoiceTemplate) data);
            view.get().applyTemplateMode();
            recoverViewData();
        }

        updateViewControlsState();
    }

    private void updateUserAccounts() {
        userRepository.updateAccounts( new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onModeSwitchStateChanged(boolean state) {
        view.get().changeTargetInputMode(state);
        model.setInputModeManual(!state);
        updateViewControlsState();
    }

    @Override
    public void onClickAccount() {
        List<Account> accounts = userRepository.getUser().getAccounts();
        if (accounts != null && accounts.size() > 0) {
            int selectedIndex = accounts.indexOf(model.getAccount());
            boolean isResident = userRepository.getUser().isResident();
            view.get().showAccountSelectorDialog(accounts, selectedIndex, isResident);
        } else {
            view.get().showAddNewAccountDialog();
        }
    }

    @Override
    public void onAccountSelected(Account account) {
        if (account == null) {
            return;
        }

        model.setAccount(account);
        model.setCurrency(CurrencyUtils.getStrCode(account.getCurrency()));
        view.get().setAccountText(account.toString());
        updateViewControlsState();
    }

    @Override
    public void onClickContactsSelector() {
        List<Correspondent> correspondents = userRepository.getUser().getCorrespondents();
        if (correspondents != null && correspondents.size() > 0) {
            int selectedIndex = correspondents.indexOf(model.getChosenContact());
            view.get().showContactsSelectorDialog(correspondents, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_empty_contacts);
        }
    }

    @Override
    public void onContactSelected(Correspondent correspondent) {
        model.setChosenContact(correspondent);
        view.get().setTargetSelectorText(correspondent.getName());
        onContractorFocusLost(String.valueOf(correspondent.getWalletId()));
    }

    @Override
    public void onContractorFocusLost(String inputData) {
        ValidationPatterns type = ValidationPatterns.getType(inputData);
        if (type == null) {
            model.setContractorWalletId(null);
            view.get().showTargetInputError(R.string.error_incorrect_requisites);
            return;
        }

        switch (type) {
            case WALLET_ID:
                model.setContractorWalletId(inputData);
                updateViewControlsState();
                break;

            case ACCOUNT_ID:

                invoiceRepository.getWalletInfoByAccount(inputData, new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showLoadingIndicator(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showLoadingIndicator(false);
                        updateViewControlsState();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        model.setContractorWalletId(null);
                        view.get().showTargetInputError(R.string.error_requisites_not_found);
                        view.get().showError(throwable);
                    }

                    @Override
                    public void onSuccess() {
                        view.get().showTargetInputError(null);
                        //todo handle receiverAccountId belongs to user
                        //todo handle разные currency, ошибка 2316
                    }
                });
                break;

            case EMAIL:
            case PHONE_RUS:
            case PHONE_OTHER:
                invoiceRepository.getWalletInfo(type, inputData, new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showLoadingIndicator(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showLoadingIndicator(false);
                        updateViewControlsState();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        model.setContractorWalletId(null);
                        view.get().showTargetInputError(R.string.error_requisites_not_found);
                        view.get().showError(throwable);
                    }

                    @Override
                    public void onSuccess() {
                        view.get().showTargetInputError(null);
                        //todo handle receiverAccountId belongs to user
                        //todo handle разные currency, ошибка 2316
                    }
                });
                break;

            default:
                model.setContractorWalletId(null);
                view.get().showTargetInputError(R.string.error_incorrect_requisites);
                break;
        }
    }

    @Override
    public void onLifetimeFocusLost(String inputData) {
        if (ValidationPatterns.validate(inputData, ValidationPatterns.DOUBLE)) {
            Integer lifetime = Integer.valueOf(inputData);

            if (lifetime > BusinessConstants.INVOICE_MAX_LIFETIME) {
                lifetime = BusinessConstants.INVOICE_MAX_LIFETIME;
                view.get().setLifetimeText(String.valueOf(lifetime));
            }
            model.setLifetime(lifetime);
        }
        updateViewControlsState();
    }


    @Override
    public void onAmountFocusLost(String inputData) {
        if (ValidationPatterns.validate(inputData, ValidationPatterns.DOUBLE)) {
            Double amount = Double.valueOf(inputData);

            if (CurrencyUtils.isAmountInRange(model.getCurrency(), amount)) {
                view.get().setAmountText(CurrencyUtils.format(amount, model.getCurrency()));
                model.setAmount(amount);
            } else {
                view.get().showSnackbar(R.string.error_on_unacceptable_amount);
                view.get().setAmountText("");
                model.setAmount(null);
            }
        } else {
            model.setAmount(null);
        }

        updateViewControlsState();
    }

    @Override
    public void onCommentFocusLost(String inputData) {
        model.setComment(inputData);
        updateViewControlsState();
    }

    @Override
    public void onContractNumFocusLost(String inputData) {
        //need to validate inputData
        model.setContractNum(inputData);
        updateViewControlsState();
    }

    @Override
    public void onClickDate() {
        view.get().showDatePicker();
    }

    @Override
    public void onDateSelected(long date) {
        model.setDate(date);
        view.get().setDateText(AppConstants.SDF_ddMMyyyy.format(date));
        updateViewControlsState();
    }

    @Override
    public void onClickProceed() {
        invoiceRepository.createInvoice(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    private void updateViewControlsState() {
        if (view == null) return;

        view.get().disableModeSwitch(model.getAccount() == null);
        view.get().disableContractorInput(model.getAccount() == null);
        view.get().disableOtherInputs(model.getAccount() == null);
        view.get().disableProceedButton(!model.isAllDataReceived());
    }

    private void recoverViewData() {
        if (view == null) return;

        view.get().setAccountText(model.getAccount() != null ? model.getAccount().toString() : null);

        if (!model.isTemplateUsed()) {
            if (model.isInputModeManual()) {
                view.get().setTargetManualText(model.getContractorWalletId());
            } else {
                view.get().setTargetSelectorText(model.getChosenContact() != null ? model.getChosenContact().toString() : null);
            }
        } else {
            Correspondent correspondent = userRepository.getUser().getCorrespondentByWalletId(model.getContractorWalletId());
            view.get().setTargetSelectorText(correspondent != null ? correspondent.toString() : model.getContractorWalletId());
        }

        view.get().setLifetimeText(model.getLifetime() != null ? String.valueOf(model.getLifetime()) : null);
        view.get().setAmountText(model.getAmount() != null ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommentText(model.getComment());
    }


    @Override
    public void goToNewAccount() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model.isTransactionDone()) {
            new Handler().post(this::goBack);
        } else {
            updateUserAccounts();
            model.clearFlowToken();
            recoverViewData();
        }
    }

    private void goBack() {
        invoiceRepository.clearModel();
        invoiceRepository.clearDisposable();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
