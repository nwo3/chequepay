package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.listofcontactoperations;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ListOfContactOperationsModule.class, ChequepayApiModule.class})
public interface ListOfContactOperationsComponent {
    void inject(ListOfContactOperationsFragment fragment);
}
