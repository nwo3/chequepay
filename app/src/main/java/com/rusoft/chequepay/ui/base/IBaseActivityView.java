package com.rusoft.chequepay.ui.base;


public interface IBaseActivityView {
    void showError(Throwable throwable);
    void showProgressDialog(boolean enabled);
    void showSnackbar(int textResId);
}
