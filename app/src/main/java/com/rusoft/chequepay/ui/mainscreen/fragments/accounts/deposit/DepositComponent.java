package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {DepositModule.class, ChequepayApiModule.class})
public interface DepositComponent {
    void inject(DepositFragment fragment);
}
