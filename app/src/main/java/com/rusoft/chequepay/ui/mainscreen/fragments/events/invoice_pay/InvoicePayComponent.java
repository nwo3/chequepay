package com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {InvoicePayModule.class, ChequepayApiModule.class})
public interface InvoicePayComponent {
    void inject(InvoicePayFragment fragment);
}
