package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail;

import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface OperationDetailContract {

    interface IView extends IBaseView {
        void showOperationInfo(ItemOperationResponse itemOperationResponse, Operation operation, boolean hideAddBtn, boolean hidePdfLoadBtn);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void getOperationInfo();
        void addCorrespondent();
        void downloadPdf();
    }
}
