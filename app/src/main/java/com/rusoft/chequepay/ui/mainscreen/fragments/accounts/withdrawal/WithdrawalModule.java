package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class WithdrawalModule {
    private final WithdrawalContract.IView view;

    public WithdrawalModule(WithdrawalContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    WithdrawalContract.IView provideWithdrawalDepositView() {
        return view;
    }
}
