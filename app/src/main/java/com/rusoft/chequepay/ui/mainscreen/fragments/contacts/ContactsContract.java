package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface ContactsContract {

    interface IView extends IBaseView {
        void showContractorsList(List<Correspondent> contractors);
        void showEmptyContractorsList();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void onClickContact(long contractorId);
        void onClickAddContact();
        void getContractorsList();
        void onRefresh();
    }
}
