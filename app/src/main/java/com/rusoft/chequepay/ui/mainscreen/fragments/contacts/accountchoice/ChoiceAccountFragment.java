package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class ChoiceAccountFragment extends BaseBranchFragment implements ChoiceAccountContract.IView, ChoiceAccountAdapter.IAccountsAdapter {

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.btn_add_account) TextView btnAddAccount;
    @BindView(R.id.tv_empty_list) TextView tvEmpty;
    @BindView(R.id.switch_account) SwitchCompat switchCompat;
    @BindView(R.id.tv_accounts) TextView tvAccountTitles;
    @BindView(R.id.iv_account_name) StaticInputView ivAccountName;
    @BindView(R.id.iv_account_number) StaticInputView ivAccountNumber;
    @BindView(R.id.sv_nestedsv) NestedScrollView scrollView;

    @Inject
    ChoiceAccountPresenter presenter;

    private List<Operation> operationList = new ArrayList<>();
    private ChoiceAccountAdapter choiceListAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_account_search;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.fragment_account_list_title);
    }

    public static ChoiceAccountFragment getNewInstance(String containerKey, String numberAccount) {
        ChoiceAccountFragment fragment = new ChoiceAccountFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerKey);
        arguments.putString(Screens.KEY_DATA, numberAccount);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerChoiceAccountComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .choiceAccountModule(new ChoiceAccountModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        enableAddButton(false);

        presenter.onViewCreated();

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        choiceListAdapter = new ChoiceAccountAdapter(this, operationList);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(choiceListAdapter);

        presenter.getFilteredOperations(getArguments().getString(Screens.KEY_DATA));

        btnAddAccount.setOnClickListener(v -> presenter.onAddAccount(ivAccountName.getText(), ivAccountNumber.getText()));

        switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                chooseAccountFromListMode();
            } else {
                accountManualInputMode();
            }
        });

        ivAccountName.addTextWatcher((AfterTextChangedWatcher) editable -> presenter.onTextChanged(ivAccountName.getText(), ivAccountNumber.getText()));
        ivAccountNumber.addTextWatcher((AfterTextChangedWatcher) editable -> presenter.onTextChanged(ivAccountName.getText(), ivAccountNumber.getText()));
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void showAccountList(List<Operation> operations) {
        operationList.clear();
        operationList.addAll(operations);
        choiceListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyList() {
        tvEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickItem(Operation operation, Integer position) {
        for (Operation item : operationList) {
            item.setIsSelectedAccount(false);
        }
        operationList.get(position).setIsSelectedAccount(true);
        choiceListAdapter.notifyDataSetChanged();
        ivAccountNumber.setText(operation.getContractorAccount());
    }

    @Override
    public void enableAddButton(boolean enabled) {
        btnAddAccount.setEnabled(enabled);
    }

    private void chooseAccountFromListMode() {
        UiUtils.hideKeyboard(getContext());
        tvAccountTitles.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        ivAccountNumber.setVisibility(View.GONE);
        ivAccountNumber.setText("");
        tvEmpty.setText(R.string.error_empty_list);
        for (Operation item : operationList) {
            if (item.getIsSelectedAccount()) {
                ivAccountNumber.setText(item.getContractorAccount());
            }
        }
    }

    private void accountManualInputMode() {
        tvAccountTitles.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        ivAccountNumber.setVisibility(View.VISIBLE);
        ivAccountNumber.setValidationPattern(ValidationPatterns.ACCOUNT_ID);
        ivAccountNumber.setText("");
        tvEmpty.setText("");
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
