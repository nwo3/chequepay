package com.rusoft.chequepay.ui.mainscreen.fragments.more;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {MoreModule.class, ChequepayApiModule.class})
public interface MoreComponent {
    void inject(MoreFragment fragment);
}
