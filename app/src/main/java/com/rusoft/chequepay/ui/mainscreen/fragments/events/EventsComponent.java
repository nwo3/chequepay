package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {EventsModule.class, ChequepayApiModule.class})
public interface EventsComponent {
    void inject(EventsFragment fragment);
}
