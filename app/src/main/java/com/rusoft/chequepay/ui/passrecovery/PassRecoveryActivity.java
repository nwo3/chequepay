package com.rusoft.chequepay.ui.passrecovery;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.successscreen.SuccessActivity;
import com.rusoft.chequepay.utils.PhoneUtils;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class PassRecoveryActivity extends BaseActivity implements PassRecoveryScreenContract.IView {

    @Inject
    PassRecoveryScreenPresenter presenter;

    @BindView(R.id.phone) StaticInputView phone;
    @BindView(R.id.pass) StaticInputView pass;
    @BindView(R.id.pass_confirm) StaticInputView pass_confirm;
    @BindView(R.id.proceed) TextView proceed;
    @BindView(R.id.help) View help;

    @Override
    protected int layoutResId() {
        return R.layout.activity_pass_recovery;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerPassRecoveryScreenComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .passRecoveryScreenModule(new PassRecoveryScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        pass.setValidationPattern(ValidationPatterns.AMC_PASSWORD);
        pass_confirm.setValidationPattern(ValidationPatterns.AMC_PASSWORD);

        PhoneUtils.initializePhoneInputMasksSetting(this, phone,
                (isValid, number) -> {
                    presenter.onPhoneTextChanged(number, isValid);
                    phone.setError(isValid ? null : getString(R.string.error_wrong_phone));
                });

        pass.setOnFocusLostListener(() -> {
            if (pass != null) presenter.onPassFocusLost(pass.validate() ? pass.getText() : null);
        });

        pass_confirm.setOnFocusLostListener(() -> {
            if (pass_confirm != null) presenter.onPassConfirmFocusLost(pass_confirm.validate() ? pass_confirm.getText() : null);
        });

        proceed.setOnClickListener(v -> presenter.onClickProceed());
        help.setOnClickListener(v -> presenter.onClickHelp());

        proceed.setOnTouchListener(UiUtils.getTouchListener());
        help.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void updateScreenState(PassRecoveryScreenPresenter.ScreenState state) {
        switch (state){
            case PHONE:
                phone.setVisibility(View.VISIBLE);
                pass.setVisibility(View.GONE);
                pass_confirm.setVisibility(View.GONE);
                proceed.setText(getText(R.string.pass_recovery));
                break;

            case PASS:
                phone.setVisibility(View.GONE);
                pass.setVisibility(View.VISIBLE);
                pass_confirm.setVisibility(View.VISIBLE);
                proceed.setText(getText(R.string.save));
                break;
        }
    }

    @Override
    public void setProceedEnabled(boolean enabled) {
        proceed.setEnabled(enabled);
        proceed.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
    }

    @Override
    public void showPassConfirmError(int textResId) {
        pass_confirm.setError(getString(textResId));
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(this, OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_PASS_RECOVERY);
        startActivity(intent);
    }

    @Override
    public void goToSuccessScreen() {
        Intent intent = new Intent(this, SuccessActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_PASS_RECOVERY);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToAuth() {
        finish();
    }


    @Override
    public void goToHelp() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Screens.KEY_FROM, Screens.AUTH_ACTIVITY);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToAuth();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }
}
