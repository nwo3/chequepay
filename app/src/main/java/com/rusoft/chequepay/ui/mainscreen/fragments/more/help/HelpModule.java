package com.rusoft.chequepay.ui.mainscreen.fragments.more.help;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class HelpModule {
    private final HelpContract.IView view;

    public HelpModule(HelpContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    HelpContract.IView provideView() {
        return view;
    }
}