package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DepositModule {
    private final DepositContract.IView view;

    public DepositModule(DepositContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    DepositContract.IView provideMoneyDepositView() {
        return view;
    }
}
