package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.Getter;

public class ProfileFragment extends BaseBranchFragment implements ProfileContract.IView {

    @Inject
    ProfilePresenter presenter;

    @BindView(R.id.container_menu) View menuContainer;
    @BindView(R.id.menu_personal) View menuPersonal;
    @BindView(R.id.menu_passport) View menuPassport;
    @BindView(R.id.menu_reg_address) View menuRegAddress;
    @BindView(R.id.menu_home_address) View menuHomeAddress;
    @BindView(R.id.menu_requisites) View menuRequisites;

    @BindView(R.id.container_info) View infoContainer;
    @BindView(R.id.info_1) StaticInputView info1;
    @BindView(R.id.info_2) StaticInputView info2;
    @BindView(R.id.info_3) StaticInputView info3;
    @BindView(R.id.info_4) StaticInputView info4;
    @BindView(R.id.info_5) StaticInputView info5;
    @BindView(R.id.info_6) StaticInputView info6;
    @BindView(R.id.info_7) StaticInputView info7;
    @BindView(R.id.info_8) StaticInputView info8;

    @BindView(R.id.edit) TextView edit;

    @Getter private ProfileScreenState state;
    private StaticInputView[] labels;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_profile_menu;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_profile);
    }

    public static ProfileFragment getNewInstance(String containerName) {
        ProfileFragment fragment = new ProfileFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerProfileComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .profileModule(new ProfileModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        labels = new StaticInputView[]{info1, info2, info3, info4, info5, info6, info7, info8};

        presenter.onViewCreated();

        menuPersonal.setOnClickListener(v -> presenter.onClickMenuPersonal());
        menuPassport.setOnClickListener(v -> presenter.onClickMenuPassport());
        menuRegAddress.setOnClickListener(v -> presenter.onClickMenuRegAddress());
        menuHomeAddress.setOnClickListener(v -> presenter.onClickMenuHomeAddress());
        menuRequisites.setOnClickListener(v -> presenter.onClickMenuOrganization());

        edit.setOnClickListener(v -> {
            disableEdit(true);
            presenter.onClickEdit();
        });
        edit.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void updateScreenState(ProfileScreenState state) {
        this.state = state;

        //Labels
        int[] titles = state.getTitlesResIds();
        for (int i = 0; i < labels.length; i++) {
            if (i < titles.length){
                labels[i].setTitle(getContext().getString(titles[i]));
                labels[i].setText("-");
                labels[i].setVisibility(View.VISIBLE);
            } else {
                labels[i].setVisibility(View.GONE);
            }
        }

        //Layouts
        menuContainer.setVisibility(state.equals(ProfileScreenState.MENU) ? View.VISIBLE : View.INVISIBLE);
        infoContainer.setVisibility(state.equals(ProfileScreenState.MENU) ? View.INVISIBLE : View.VISIBLE);

        edit.setVisibility(state.hasWritableFields ? View.VISIBLE : View.GONE);

        updateToolbarTitle(getString(state.getToolbarTitleResId()));
    }

    @Override
    public void updateData(String... data) {
        for (int i = 0; i < labels.length; i++) {
            if (i < data.length) {
                labels[i].setText(data[i]);
            }
        }
    }

    @Override
    public void disableEdit(boolean disabled) {
        edit.setEnabled(!disabled);
        edit.setTextColor(disabled ? getResources().getColor(R.color.bright_blue_50)
                : getResources().getColor(R.color.bright_blue));
    }

    @Override
    public void setReqFieldText(int textResId) {
        ((TextView) menuRequisites).setText(textResId);
    }


    @Override
    public void goToProfileEdit(int screenTypeKey) {
        Intent intent = new Intent(getContext(), RegistrationActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, screenTypeKey);
        startActivity(intent);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        disableEdit(false);
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
