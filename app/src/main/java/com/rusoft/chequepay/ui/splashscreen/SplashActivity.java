package com.rusoft.chequepay.ui.splashscreen;


import android.content.Intent;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.pinscreen.PinActivity;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity implements SplashContract.IView {

    @Inject
    SplashPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerSplashComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .splashModule(new SplashModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
    }

    @Override
    public void goToAuth() {
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }

    @Override
    public void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void goToPin() {
        startActivity(new Intent(this, PinActivity.class));
        finish();
    }
}
