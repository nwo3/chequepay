package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AddContactModule {
    private final AddContactContract.IView view;

    public AddContactModule(AddContactContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    AddContactContract.IView provideAddContactView() {
        return view;
    }
}
