package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.correspondentdetail;

import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface CorrespondentDetailContract {

    interface IView extends IBaseView {
        void showContractorInfo(String name, String numberClient, String inn, String comment, List<Account> accountList);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void getTitle(long contractorId);
        void getContractorInfo(long contractorId);
    }
}
