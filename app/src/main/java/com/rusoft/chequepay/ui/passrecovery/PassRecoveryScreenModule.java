package com.rusoft.chequepay.ui.passrecovery;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PassRecoveryScreenModule {
    private final PassRecoveryScreenContract.IView view;

    public PassRecoveryScreenModule(PassRecoveryScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    PassRecoveryScreenContract.IView provideView() {
        return view;
    }
}
