package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;

import java.util.List;

import static com.rusoft.chequepay.utils.UiUtils.animateViewAlpha;

public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.AccountsViewHolder> {
    private List<ShortTemplate> templates;
    private Context context;

    interface ItemClickListener {
        void onTemplateClicked(ShortTemplate template);
        void onTemplateLongClicked(ShortTemplate template);
    }

    private ItemClickListener listener;

    public TemplatesAdapter(ItemClickListener listener, List<ShortTemplate> templates) {
        this.listener = listener;
        this.templates = templates;
    }

    @Override
    public AccountsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.list_templates_item, parent, false);
        return new AccountsViewHolder(view);
    }

    class AccountsViewHolder extends RecyclerView.ViewHolder {
        View container;
        ImageView icon;
        TextView title, type;

        AccountsViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            icon = itemView.findViewById(R.id.icon);
            title = itemView.findViewById(R.id.title);
            type = itemView.findViewById(R.id.type);
        }
    }

    @Override
    public void onBindViewHolder(AccountsViewHolder holder, int position) {
        ShortTemplate template = templates.get(position);
        holder.icon.setBackgroundResource(template.getTemplateType().getIconResId());
        holder.title.setText(template.getName());
        holder.type.setText(template.getTemplateType().toString(context));

        holder.container.setOnClickListener(view -> listener.onTemplateClicked(template));
        holder.container.setOnLongClickListener(view -> {
            listener.onTemplateLongClicked(template);
            return true;
        });

        holder.container.setOnTouchListener((view, motionEvent) -> {
            int action = motionEvent.getAction();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    animateViewAlpha(view, AppConstants.ENABLED_VIEW_ALPHA, AppConstants.DISABLED_VIEW_ALPHA);
                    return false;

                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    animateViewAlpha(view, AppConstants.DISABLED_VIEW_ALPHA, AppConstants.ENABLED_VIEW_ALPHA);
                    return false;
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }
}
