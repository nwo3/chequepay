package com.rusoft.chequepay.ui.mainscreen.fragments.more.faq;

import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class FaqPresenter extends BasePresenter implements FaqContract.IPresenter {

    private WeakReference<FaqContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public FaqPresenter(FaqContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        //ignore
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    private void goBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
