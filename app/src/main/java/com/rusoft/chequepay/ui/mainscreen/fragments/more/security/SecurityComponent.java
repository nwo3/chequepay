package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {SecurityModule.class, ChequepayApiModule.class})
public interface SecurityComponent {
    void inject(SecurityFragment fragment);
}
