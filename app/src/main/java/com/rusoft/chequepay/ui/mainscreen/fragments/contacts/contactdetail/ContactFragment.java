package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.contactdetail;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class ContactFragment extends BaseBranchFragment implements ContactContract.IView {

    @Inject
    ContactPresenter presenter;

    @BindView(R.id.tv_info) View tvInfoCorrespondent;
    @BindView(R.id.tv_get_statement) View tvStatement;
    @BindView(R.id.transfer) View transfer;
    @BindView(R.id.invoice) View invoice;
    @BindView(R.id.transfer_underline) View transferUnderline;
    @BindView(R.id.invoice_underline) View invoiceUnderline;
    @BindView(R.id.tv_delete) View tvDeleteCorrespondent;
    @BindView(R.id.tv_edit) View tvEdit;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_contact;
    }

    @Override
    protected String toolbarTitle() {
        return null;
    }

    public static ContactFragment getNewInstance(String name, long contractorId) {
        ContactFragment contactFragment = new ContactFragment();

        Bundle arguments = new Bundle();
        arguments.putLong(Screens.KEY_DATA, contractorId);
        contactFragment.setArguments(arguments);

        return contactFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerContactComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .contactModule(new ContactModule(this))
                .build().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewCreated(getArguments().getLong(Screens.KEY_DATA));
    }

    @Override
    protected void initViews() {
        tvInfoCorrespondent.setOnClickListener(v -> presenter.onClickInfo(getArguments().getLong(Screens.KEY_DATA)));
        tvStatement.setOnClickListener(v -> presenter.onClickShowStatement(getArguments().getLong(Screens.KEY_DATA)));

        transfer.setOnClickListener(v -> presenter.onClickTransfer());
        invoice.setOnClickListener(v -> presenter.onClickInvoice());
        tvEdit.setOnClickListener(v -> presenter.onClickEdit());

        tvDeleteCorrespondent.setOnClickListener(view -> UiUtils.showQuestionDialog(getContext(), R.string.dialog_on_contact_delete, null,
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            presenter.onDeleteCorrespondent();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }));

        tvInfoCorrespondent.setOnTouchListener(UiUtils.getTouchListener());
        tvStatement.setOnTouchListener(UiUtils.getTouchListener());
        transfer.setOnTouchListener(UiUtils.getTouchListener());
        invoice.setOnTouchListener(UiUtils.getTouchListener());
        tvDeleteCorrespondent.setOnTouchListener(UiUtils.getTouchListener());
        tvEdit.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void hideTransferAndInvoice() {
        transfer.setVisibility(View.GONE);
        transferUnderline.setVisibility(View.GONE);

        invoice.setVisibility(View.GONE);
        invoiceUnderline.setVisibility(View.GONE);
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

}
