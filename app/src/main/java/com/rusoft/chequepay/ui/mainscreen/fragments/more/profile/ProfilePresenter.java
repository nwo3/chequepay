package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import android.content.Context;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.wallets.Address;
import com.rusoft.chequepay.data.entities.wallets.BankAccount;
import com.rusoft.chequepay.data.entities.wallets.BankForeign;
import com.rusoft.chequepay.data.entities.wallets.BankRus;
import com.rusoft.chequepay.data.entities.wallets.Passport;
import com.rusoft.chequepay.data.entities.wallets.Person;
import com.rusoft.chequepay.data.entities.wallets.response.FullProfileResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.ErrorUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ProfilePresenter extends BasePresenter implements ProfileContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    RegistrationRepository registrationRepository;

    private WeakReference<ProfileContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;

    private FullProfileResponse profile;

    @Inject
    public ProfilePresenter(ProfileContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated() {
        addRepositories(userRepository);
        view.get().updateScreenState(ProfileScreenState.MENU);
        updateProfile();
    }

    private void updateProfile() {
        userRepository.updateFullProfile(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                profile = userRepository.getUser().getFullProfile();
                view.get().setReqFieldText(profile.isResident() ? R.string.bank_req : R.string.mainscreen_title_organization_requisites);

                switch (view.get().getState()) {
                    case PERSONAL:
                        onClickMenuPersonal();
                        break;

                    case PASSPORT:
                        onClickMenuPassport();
                        break;

                    case REG_ADDRESS:
                        onClickMenuRegAddress();
                        break;

                    case HOME_ADDRESS:
                        onClickMenuHomeAddress();
                        break;

                    case REQUISITES_RESIDENT:
                    case REQUISITES_NON_RESIDENT:
                        onClickMenuOrganization();
                        break;
                }
            }
        });
    }

    @Override
    public void onClickMenuPersonal() {
        view.get().updateScreenState(ProfileScreenState.PERSONAL);

        Person person = profile.getPerson();
        if (person != null) {
            view.get().updateData(
                    getStr(profile.getWalletId()),
                    getStr(person.getFirstName(), person.getLastName()),
                    getStr(person.getEmail()),
                    getStr(person.getPhone()),
                    appContext.getString(BusinessConstants.RegType.getTitleResId(profile.getRegType())),
                    getStr(person.getTaxNumber()),
                    getStr(person.getWebsite())
            );
        }
    }

    @Override
    public void onClickMenuPassport() {
        view.get().updateScreenState(ProfileScreenState.PASSPORT);

        Passport passport = profile.getPerson().getPassport();
        view.get().updateData(
                getStr(passport.getNumber()),
                getStr(passport.getIssuer()),
                getStr(passport.getDepartmentCode()),
                passport.getIssueDate() != null ? AppConstants.SDF_ddMMyyyy.format(passport.getIssueDate()) : "-"
        );
    }

    @Override
    public void onClickMenuRegAddress() {
        view.get().updateScreenState(ProfileScreenState.REG_ADDRESS);

        Address address = profile.getUrAddress();
        view.get().updateData(
                getStr(address.getCountryName()),
                getStr(address.getRegionName()),
                getStr(address.getSettlement()),
                getStr(address.getPostcode()),
                getStr(address.getStreet())
        );
    }

    @Override
    public void onClickMenuHomeAddress() {
        view.get().updateScreenState(ProfileScreenState.HOME_ADDRESS);

        Address address = profile.getFactAddress();
        view.get().updateData(
                getStr(address.getCountryName()),
                getStr(address.getRegionName()),
                getStr(address.getSettlement()),
                getStr(address.getPostcode()),
                getStr(address.getStreet())
        );
    }

    @Override
    public void onClickMenuOrganization() {
        BankAccount bankAccount = profile.getBankAccount();
        if (profile.isResident()){
            view.get().updateScreenState(ProfileScreenState.REQUISITES_RESIDENT);
            BankRus bankRus = bankAccount.getBankRus();
            view.get().updateData(
                getStr(bankRus.getName()),
                getStr(bankRus.getBic()),
                getStr(bankRus.getCorAccount()),
                getStr(bankAccount.getNumber()),
                getStr(bankAccount.getBankInn()),
                getStr(bankAccount.getBankKpp()),
                getStr(bankAccount.getOwner())
            );
        } else {
            view.get().updateScreenState(ProfileScreenState.REQUISITES_NON_RESIDENT);
            BankForeign bankForeign = bankAccount.getBankForeign();
            view.get().updateData(
                    getStr(bankForeign.getName()),
                    getStr(bankForeign.getBic()),
                    getStr(bankForeign.getSwift()),
                    getStr(bankForeign.getAddress()),
                    getStr(bankAccount.getNumber()),
                    getStr(bankForeign.getAdditionalInfo())
            );
        }
    }

    private String getStr(String... strings){
        if (strings.length == 0) {
            return "-";
        } else if (strings[0] == null || strings[0].isEmpty()){
            return "-";
        }

        StringBuilder result = new StringBuilder();
        for (String s : strings) {
            result.append(" ").append(s);
        }

        return result.toString().trim();
    }

    @Override
    public void onClickEdit() {
        switch (view.get().getState()) {

            case PERSONAL:
                view.get().goToProfileEdit(AppConstants.REG_ACTIVITY_PROFILE_EDIT_PERSONAL);
                break;

            case HOME_ADDRESS:
                registrationRepository.getCountriesAndRegions(new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showProgressDialog(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showProgressDialog(false);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        view.get().showError(throwable);
                        view.get().disableEdit(false);
                    }

                    @Override
                    public void onSuccess() {
                        view.get().goToProfileEdit(AppConstants.REG_ACTIVITY_PROFILE_EDIT_HOME_ADDRESS);
                    }
                });

                break;

            case REQUISITES_RESIDENT:
            case REQUISITES_NON_RESIDENT:
                view.get().goToProfileEdit(AppConstants.REG_ACTIVITY_PROFILE_EDIT_BANK);
                break;

            case MENU:
            case PASSPORT:
            case REG_ADDRESS:
            default:
                throw new InternalError(ErrorUtils.INTERNAL_ERROR);
        }
    }

    @Override
    public void onClickBack() {
        if (view.get().getState().equals(ProfileScreenState.MENU)){
            localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
        } else {
            view.get().updateScreenState(ProfileScreenState.MENU);
        }
    }

    @Override
    public void onResume() {
        updateProfile();
    }

}
