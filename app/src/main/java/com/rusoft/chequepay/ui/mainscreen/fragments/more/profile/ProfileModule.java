package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileModule {
    private final ProfileContract.IView view;

    public ProfileModule(ProfileContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ProfileContract.IView provideView() {
        return view;
    }
}
