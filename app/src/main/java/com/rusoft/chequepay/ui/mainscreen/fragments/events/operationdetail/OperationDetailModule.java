package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class OperationDetailModule {
    private final OperationDetailContract.IView view;

    public OperationDetailModule(OperationDetailContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    OperationDetailContract.IView provideOperationDetailView() {
        return view;
    }
}
