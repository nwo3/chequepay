package com.rusoft.chequepay.ui.base.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.BackPressedEvent;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

public abstract class BaseRootFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
            toolbar.getNavigationIcon().setAlpha(AppConstants.INVISIBLE_ALPHA);

            updateToolbarTitle(toolbarTitle());

        } else if (getActivity() instanceof RegistrationActivity) {
            Toolbar toolbar = ((RegistrationActivity) getActivity()).getToolbar();
            toolbar.setNavigationIcon(R.drawable.ic_close);
            toolbar.getNavigationIcon().setAlpha(AppConstants.VISIBLE_ALPHA);

            updateToolbarTitle(toolbarTitle());
            toolbar.setNavigationOnClickListener(v -> onNavigationClicked());
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Subscribe
    public void onEvent(BackPressedEvent event) {
        onBackPressed();
    }

    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        exit();
    }

    protected void onBackPressed() {
        UiUtils.hideKeyboard(getContext());
        exit();
    }

    protected void exit() {
        UiUtils.showOnExitDialog(getContext());
    }
}
