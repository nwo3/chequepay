package com.rusoft.chequepay.ui.registration;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.BackPressedEvent;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.pinscreen.PinActivity;
import com.rusoft.chequepay.ui.registration.fragment.RegRootFragment;
import com.rusoft.chequepay.ui.registration.fragment.individual.account.RegAccountFragment;
import com.rusoft.chequepay.ui.registration.fragment.individual.address.RegAddressFragment;
import com.rusoft.chequepay.ui.registration.fragment.individual.bank.RegBankFragment;
import com.rusoft.chequepay.ui.registration.fragment.individual.passport.RegPassportFragment;
import com.rusoft.chequepay.ui.registration.fragment.individual.personal.RegPersonalFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.Getter;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;

import static com.rusoft.chequepay.ui.Screens.REG_ACCOUNT_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_ADDRESS_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_BANK_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_HOME_ADDRESS_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_PASSPORT_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_PERSON_FRAGMENT;
import static com.rusoft.chequepay.ui.Screens.REG_ROOT_FRAGMENT;

public class RegistrationActivity extends BaseActivity implements RegistrationContract.IView {

    @Inject
    RegistrationPresenter presenter;

    @Inject
    NavigatorHolder navigatorHolder;

    @BindView(R.id.toolbar) @Getter Toolbar toolbar;
    @BindView(R.id.next) TextView next;

    @BindView(R.id.scroll) ScrollView scroll;

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(),
            R.id.fragment_container) {

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch(screenKey) {

                case REG_ROOT_FRAGMENT:
                    return RegRootFragment.getNewInstance();

                case REG_PERSON_FRAGMENT:
                    return RegPersonalFragment.getNewInstance();

                case REG_PASSPORT_FRAGMENT:
                    return RegPassportFragment.getNewInstance();

                case REG_ADDRESS_FRAGMENT:
                    return RegAddressFragment.getNewInstance(RegAddressFragment.ScreenType.REGISTRATION_ADDRESS);

                case REG_HOME_ADDRESS_FRAGMENT:
                    return RegAddressFragment.getNewInstance(RegAddressFragment.ScreenType.HOME_ADDRESS);

                case REG_BANK_FRAGMENT:
                    return RegBankFragment.getNewInstance();

                case REG_ACCOUNT_FRAGMENT:
                    return RegAccountFragment.getNewInstance();

                default:
                    throw new RuntimeException("Unknown screen key");
            }
        }

        @Override
        protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right);
        }

        @Override
        protected void showSystemMessage(String message) {
            RegistrationActivity.this.showSnackbar(message);
        }

        @Override
        protected void exit() {
            finish();
        }
    };

    @Override
    protected int layoutResId() {
        return R.layout.activity_registration;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegistrationComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .registrationModule(new RegistrationModule(this))
                .build().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initViews() {
        int key = Objects.requireNonNull(getIntent().getExtras()).getInt(AppConstants.KEY_ACTION_TYPE);
        presenter.onViewCreated(key);
        next.setOnClickListener(v -> presenter.onClickNext());
        next.setOnTouchListener(UiUtils.getTouchListener());
    }

    public void scrollToStart() {
        scroll.scrollTo(0, 0);
    }

    public void updateNextButtonState(boolean enabled){
        next.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA_WEAK);
    }

    @Override
    public void disableNext(boolean disabled) {
        updateNextButtonState(!disabled);
        next.setEnabled(!disabled);
    }

    @Override
    public void setNextButtonTitle(int titleResId) {
        next.setText(titleResId);
    }

    @Override
    public void goToAuth() {
        finish();
    }

    @Override
    public void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void goToPin() {
        startActivity(new Intent(this, PinActivity.class));
        finish();
    }

    @Override
    public void goToOtp() {
        startActivity(new Intent(this, OtpActivity.class));
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new BackPressedEvent());
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        presenter.onDestroy();
        super.onDestroy();
    }
}
