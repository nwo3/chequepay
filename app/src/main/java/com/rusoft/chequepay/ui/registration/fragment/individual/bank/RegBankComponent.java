package com.rusoft.chequepay.ui.registration.fragment.individual.bank;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegBankModule.class, ChequepayApiModule.class})
public interface RegBankComponent {
    void inject(RegBankFragment fragment);
}
