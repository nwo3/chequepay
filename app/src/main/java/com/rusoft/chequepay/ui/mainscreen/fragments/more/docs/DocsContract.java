package com.rusoft.chequepay.ui.mainscreen.fragments.more.docs;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.document.response.Document;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface DocsContract {

    interface IView extends IBaseView {
        void initializeDocumentsRecycler(List<Document> documents);
        void updateDocumentsRecycler();

        void showCategoryPickerDialog(List<BusinessConstants.DocCategory> categories, int selectedIndex);
        void showStatusPickerDialog(List<BusinessConstants.DocStatus> statuses, int selectedIndex);
        void showPeriodPickerDialog();

        void setCategoryText(int textResId);
        void setStatusText(int textResId);
        void setPeriodText(String text);
        void setPeriodError(Integer textResId);

        void enableSearchButton(boolean enable);

        void showFilter(boolean show);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onRecyclerBottomReached();

        void onDocSelected(long docId);

        void onClickCategory();
        void onCategorySelected(BusinessConstants.DocCategory category);

        void onClickStatus();
        void onStatusSelected(BusinessConstants.DocStatus status);

        void onClickPeriod();
        void onPeriodSelected(long dateStart, long dateFinish);

        void onClickSearch();

    }
}
