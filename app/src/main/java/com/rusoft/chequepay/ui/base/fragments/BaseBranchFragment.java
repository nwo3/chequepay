package com.rusoft.chequepay.ui.base.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.BackPressedEvent;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

public abstract class BaseBranchFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            Toolbar toolbar = mainActivity.getToolbar();
            toolbar.getNavigationIcon().setAlpha(AppConstants.VISIBLE_ALPHA);
            toolbar.setNavigationOnClickListener(v -> onNavigationClicked());

            updateToolbarTitle(toolbarTitle());

        } else if (getActivity() instanceof RegistrationActivity) {
            Toolbar toolbar = ((RegistrationActivity) getActivity()).getToolbar();
            toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
            toolbar.getNavigationIcon().setAlpha(AppConstants.VISIBLE_ALPHA);

            updateToolbarTitle(toolbarTitle());

            toolbar.setNavigationOnClickListener(v -> {
                UiUtils.hideKeyboard(getContext());
                onNavigationClicked();
            });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Subscribe
    public void onEvent(BackPressedEvent backPressedEvent) {
        onBackPressed();
    }

    protected abstract void onBackPressed();

    protected abstract void onNavigationClicked();
}
