package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface WithdrawalContract {

    interface IView extends IBaseView {
        void setAccountText(String text);
        void setModeText(Integer textResId);
        void setCurrencyText(String text);
        void setIssuerLabelText(String text);
        void setAmountText(String text);
        void setCommissionText(String text);
        void setTotalText(String text);

        void showCommission(boolean enabled);

        void setTpAccountText(String text);
        void setTpBankText(String text);
        void setTpKorText(String text);
        void setTpInnText(String text);
        void setTpKppText(String text);
        void setTpBikText(String text);

        void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident);
        void showModeSelectorDialog(List<BusinessConstants.RefundMode> modes, int selectedIndex);

        void disableAccountInput(boolean disabled);
        void disableModeInput(boolean disabled);
        void disableAmountInput(boolean disabled);
        void disableProceedButton(boolean disabled);

        void showAddNewAccountDialog();
        void showDepositDialog(Account account, boolean isInfoDialogType);

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(Serializable data);

        void onClickAccount();
        void onAccountSelected(Account account);

        void onClickMode();
        void onModeSelected(BusinessConstants.RefundMode mode);

        void onAmountFocusLost(String inputData);

        void onThirdPartySwitchStateChanged(boolean state);

        void onClickProceed();

        void goToDeposit(Account account);
        void goToNewAccount();

    }
}