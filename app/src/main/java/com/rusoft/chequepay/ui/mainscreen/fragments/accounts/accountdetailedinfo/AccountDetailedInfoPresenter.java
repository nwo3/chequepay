package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetailedinfo;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class AccountDetailedInfoPresenter extends BasePresenter implements AccountDetailedInfoContract.IPresenter {

    @Inject
    UserRepository userRepository;

    private WeakReference<AccountDetailedInfoContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public AccountDetailedInfoPresenter(AccountDetailedInfoContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository);

        if (data instanceof Account) {
            Account account = (Account) data;
            view.get().setFieldsText(
                    CurrencyUtils.format(account.getAmount(), account.getCurrency()),
                    account.getAccountNumber(),
                    account.getName(),
                    AppConstants.SDF_ddMMyyyy.format(account.getRegDate()),
                    account.getIssuerShortName(),
                    account.getBankShortName());
        } else {
            throw new IllegalArgumentException(ErrorUtils.WRONG_DATA_TYPE);
        }
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        //ignore
    }

    private void goBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
