package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface ProfileContract {

    interface IView extends IBaseView {
        void updateScreenState(ProfileScreenState state);
        void updateData(String... data);
        void disableEdit(boolean disabled);
        void setReqFieldText(int textResId);

        void goToProfileEdit(int screenTypeKey);

        ProfileScreenState getState();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickMenuPersonal();
        void onClickMenuPassport();
        void onClickMenuRegAddress();
        void onClickMenuHomeAddress();
        void onClickMenuOrganization();

        void onClickEdit();

    }
}
