package com.rusoft.chequepay.ui.base.fragments;


public abstract class BaseContainerFragment extends BaseRootFragment {

    @Override
    protected void onNavigationClicked() {
        //ignore
    }

    @Override
    protected void onBackPressed() {
        //ignore
    }

}
