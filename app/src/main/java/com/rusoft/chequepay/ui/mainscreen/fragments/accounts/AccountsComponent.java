package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {AccountsModule.class, ChequepayApiModule.class})
public interface AccountsComponent {
    void inject(AccountsFragment fragment);
}
