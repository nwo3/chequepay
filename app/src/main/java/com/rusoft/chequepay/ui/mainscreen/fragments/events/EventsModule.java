package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class EventsModule {
    private final EventsContract.IView view;

    public EventsModule(EventsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    EventsContract.IView provideEventsView() {
        return view;
    }
}
