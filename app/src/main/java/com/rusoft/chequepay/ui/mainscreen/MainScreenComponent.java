package com.rusoft.chequepay.ui.mainscreen;

import com.rusoft.chequepay.di.AppLongPollingComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppLongPollingComponent.class,
        modules = {MainScreenModule.class, ChequepayApiModule.class})
public interface MainScreenComponent {
    void inject(MainActivity activity);
}
