package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.AccountsViewHolder> {
    private List<Account> accounts;

    interface ItemClickListener {
        void onAccountSelected(Account account);
    }

    private ItemClickListener listener;

    public AccountsAdapter(ItemClickListener listener, List<Account> accounts) {
        this.listener = listener;
        this.accounts = accounts;
    }

    @Override
    public AccountsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_accounts_item, parent, false);
        return new AccountsViewHolder(view);
    }

    class AccountsViewHolder extends RecyclerView.ViewHolder {
        View container;
        TextView title, account, amount;

        AccountsViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            title = itemView.findViewById(R.id.title);
            account = itemView.findViewById(R.id.account);
            amount = itemView.findViewById(R.id.amount);
        }
    }

    @Override
    public void onBindViewHolder(AccountsViewHolder holder, int position) {
        Account account = accounts.get(position);
        holder.container.setOnClickListener(view -> listener.onAccountSelected(account));
        holder.container.setOnTouchListener(UiUtils.getTouchListener());
        holder.title.setText(account.getName());
        holder.account.setText(account.getAccountNumber());
        holder.amount.setText(CurrencyUtils.format(account.getAmount(), account.getCurrency()));
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
