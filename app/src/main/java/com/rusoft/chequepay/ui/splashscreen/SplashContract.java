package com.rusoft.chequepay.ui.splashscreen;


import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface SplashContract {

    interface IView {
        void goToAuth();
        void goToMain();
        void goToPin();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
    }
}
