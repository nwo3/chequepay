package com.rusoft.chequepay.ui.mainscreen.fragments.more;

import android.os.Handler;

import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.messages.MessageRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class MorePresenter extends BasePresenter implements MoreContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    MessageRepository messageRepository;

    private WeakReference<MoreContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public MorePresenter(MoreContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(String keyFrom) {
        if (!keyFrom.equals(Screens.MAIN_ACTIVITY) && !keyFrom.equals(Screens.PIN_ACTIVITY)) {
            new Handler().post(() -> {
                localCiceroneHolder.getCicerone(view.get().getContainerName())
                        .getRouter().replaceScreen(Screens.HELP_FRAGMENT, keyFrom);
            });
        }
    }

    @Override
    public void onClickProfile() {
        localCiceroneHolder.getCicerone(view.get().getContainerName())
                .getRouter().newScreenChain(Screens.PROFILE_FRAGMENT);
    }

    @Override
    public void onClickSecurity() {
        localCiceroneHolder.getCicerone(view.get().getContainerName())
                .getRouter().newScreenChain(Screens.SECURITY_FRAGMENT);
    }

    @Override
    public void onClickDocs() {
        localCiceroneHolder.getCicerone(view.get().getContainerName())
                .getRouter().newScreenChain(Screens.DOCS_FRAGMENT);
    }

    @Override
    public void onClickFaq() {
        localCiceroneHolder.getCicerone(view.get().getContainerName())
                .getRouter().newScreenChain(Screens.FAQ_FRAGMENT);
    }

    @Override
    public void onClickFeedback() {
        localCiceroneHolder.getCicerone(view.get().getContainerName())
                .getRouter().newScreenChain(Screens.FEEDBACK_FRAGMENT);
    }

    @Override
    public void onClickLogout() {
        messageRepository.disconnectMessage(new RequestCallbackListener() {

            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onSuccess() {
                userRepository.logout();
                view.get().goToAuth();
            }
        });
    }
}
