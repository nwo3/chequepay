package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface ChoiceAccountContract {

    interface IView extends IBaseView {
        void showAccountList(List<Operation> operations);
        void showEmptyList();
        void enableAddButton(boolean enabled);
    }

    interface IPresenter extends IBasePresenter {
        void onAddAccount(String accountName, String accountNumber);
        void getFilteredOperations(String numberAccount);
        void onViewCreated();
        void onTextChanged(String accountName, String accountNumber);
    }
}
