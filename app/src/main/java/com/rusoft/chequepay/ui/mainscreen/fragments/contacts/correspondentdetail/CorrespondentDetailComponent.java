package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.correspondentdetail;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {CorrespondentDetailModule.class, ChequepayApiModule.class})
public interface CorrespondentDetailComponent {
    void inject(CorrespondentDetailFragment fragment);
}
