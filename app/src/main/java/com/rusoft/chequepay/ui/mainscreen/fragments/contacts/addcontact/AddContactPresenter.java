package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact;

import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class AddContactPresenter extends BasePresenter implements AddContactContract.IPresenter {

    @Inject
    ContractorRepository contractorRepository;

    private WeakReference<AddContactContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private CorrespondentModel correspondentModel;


    @Inject
    public AddContactPresenter(AddContactContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(contractorRepository);

        this.correspondentModel = contractorRepository.getCorrespondentModel();
        if (correspondentModel.isEdit()) {
            view.get().updateToolbarTitle(correspondentModel.getCorrespondentName());
        }
    }

    @Override
    public void saveContractor() {
        if (correspondentModel.isEdit()) {
            contractorRepository.editCorrespondent(new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    updateCorrespondentList();
                }
            });
        } else {
            contractorRepository.addCorrespondent(new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    updateCorrespondentList();
                }
            });
        }
    }

    @Override
    public void saveFieldData(String nameContractor, String numberClient, String inn, String comment, List<Account> accounts) {
        correspondentModel.setAccounts(accounts);
        correspondentModel.setComment(comment);
        correspondentModel.setAccountNumber(numberClient);
        correspondentModel.setInn(inn);
        correspondentModel.setCorrespondentName(nameContractor);
    }

    @Override
    public void clearFieldData() {
        if (!correspondentModel.isEdit()) {
            correspondentModel.clear();
            view.get().updateLockedFields("", "", "");
            view.get().updateFields("", new ArrayList<>());
        }
    }


    @Override
    public void startAccountList(String numberAccount) {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().navigateTo(Screens.ACCOUNT_LIST_FRAGMENT, numberAccount);
    }

    @Override
    public void onUpdateFields() {
        if (correspondentModel != null) {
            view.get().updateFields(correspondentModel.getComment(),
                    correspondentModel.getAccounts());
            view.get().updateLockedFields(correspondentModel.getCorrespondentName(),
                    correspondentModel.getAccountNumber(),
                    correspondentModel.getInn());
        }
    }

    private void updateCorrespondentList() {
        contractorRepository.getCorrespondentList(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                correspondentModel.clear();
                exit();
            }
        });
    }

    private void exit() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

    @Override
    public void onClickBack() {
        correspondentModel.clear();
        exit();
    }
}
