package com.rusoft.chequepay.ui.registration.fragment.individual.address;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.AddressSubModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class RegAddressPresenter extends BasePresenter implements RegAddressContract.IPresenter {

    @Inject
    RegistrationRepository repository;

    private WeakReference<RegAddressContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private AddressSubModel subModel;
    private RegAddressFragment.ScreenType type;

    @Inject
    public RegAddressPresenter(RegAddressContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated(RegAddressFragment.ScreenType type) {
        model = repository.getModel();
        this.type = type;
        switch (type){
            case REGISTRATION_ADDRESS:
                subModel = model.regAddress;
                break;

            case HOME_ADDRESS:
                subModel = model.factAddress;
                break;
        }

        view.get().updateNextButtonState(subModel.isRequiredDataReceived());

        view.get().showSwitcher(type == RegAddressFragment.ScreenType.HOME_ADDRESS);

        //set default country only on fragment first initialization
        onCountrySelected(subModel.getCountry() != null ? subModel.getCountry() : BusinessConstants.COUNTRY_RUSSIA);

        view.get().updateViewByResidency(model.isResident());
    }

    @Override
    public void onSwitchStateChanged(boolean enabled) {
        if (enabled){
            subModel.copy(model.regAddress);
            onCountrySelected(subModel.getCountry());
            onRegionSelected(subModel.getRegion());
        } else {
            subModel.clear();
        }

        view.get().restoreState(subModel);
        view.get().lockFields(enabled);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickCountry() {
        List<Country> countries = repository.getCountries();
        int selectedIndex = countries.indexOf(subModel.getCountry());
        view.get().showCountriesPicker(countries, selectedIndex);
    }

    @Override
    public void onCountrySelected(Country country) {
        subModel.setCountry(country);
        view.get().updateCountrySubjectState(subModel.isFromRussia());
        view.get().setCountryText(country.getName());
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickRegion() {
        List<Region> regions = repository.getRegions();
        int selectedIndex = regions.indexOf(subModel.getRegion());
        view.get().showRegionsPicker(regions, selectedIndex);
    }

    @Override
    public void onRegionSelected(Region region) {
        subModel.setRegion(region);
        view.get().setRegionText(region.getName());
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onDistrictFocusLost(String text) {
        subModel.setRegion(new Region(text));
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onPostCodeFocusLost(String text) {
        subModel.setPostCode(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onSettlementFocusLost(String text) {
        subModel.setSettlement(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onStreetFocusLost(String text) {
        subModel.setStreet(!text.isEmpty() ? text : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickNext() {
        if (!model.isInProfileEditingMode()) {
            if (subModel.isRequiredDataReceived()){
                switch (type){
                    case REGISTRATION_ADDRESS:
                        router.navigateTo(Screens.REG_HOME_ADDRESS_FRAGMENT);
                        break;

                    case HOME_ADDRESS:
                        router.navigateTo(Screens.REG_BANK_FRAGMENT);
                        break;
                }

            } else {
                view.get().showSnackbar(R.string.error_on_check_required_data_fail);
            }

        } else {
            if (!subModel.isRequiredDataReceived()) {
                return;
            }

            repository.editProfile(null, new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    view.get().goToOtp();
                }
            });
        }

    }

    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        if (model.isInProfileEditingMode() && model.isActionDone()){
            repository.clearModel();
            view.get().exit();
            return;
        }

        view.get().restoreState(subModel);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onDestroy() {
        if (model.isInProfileEditingMode()) {
            repository.clearModel();
        }
    }
}
