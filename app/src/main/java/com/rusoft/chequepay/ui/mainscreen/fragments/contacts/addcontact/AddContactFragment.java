package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact;

import android.animation.Animator;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.CommentInputView;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class AddContactFragment extends BaseBranchFragment implements AddContactContract.IView {

    @BindView(R.id.ll_container_accounts) LinearLayout llContainerAccounts;
    @BindView(R.id.btn_save_contact) TextView btnSaveAccount;
    @BindView(R.id.iv_name) StaticInputView ivName;
    @BindView(R.id.iv_number) StaticInputView ivNumber;
    @BindView(R.id.iv_inn) StaticInputView ivInn;
    @BindView(R.id.iv_comment) CommentInputView ivComment;

    @Inject
    AddContactPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_add_contact;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.fragment_add_contact_title);
    }

    public static AddContactFragment getNewInstance(String containerKey, Account account) {
        AddContactFragment contactFragment = new AddContactFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerKey);
        arguments.putSerializable(Screens.KEY_DATA, account);
        contactFragment.setArguments(arguments);

        return contactFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerAddContactComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .addContactModule(new AddContactModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ivNumber.setEnabled(false);
        ivInn.setEnabled(false);
        createAddButton();
        updateProceedButtonState();

        btnSaveAccount.setOnTouchListener(UiUtils.getTouchListener());
        btnSaveAccount.setOnClickListener(v -> {
            presenter.saveFieldData(ivName.getText(), ivNumber.getText(), ivInn.getText(), ivComment.getText(), getAccountList());
            presenter.saveContractor();
        });

        ivName.addTextWatcher((AfterTextChangedWatcher) editable -> updateProceedButtonState());
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void addAccount(String accountName, String accountNumber) {
        View nextView = getLayoutInflater().inflate(R.layout.view_account, null, false);
        (nextView.findViewById(R.id.iv_delete)).setVisibility(View.VISIBLE);
        nextView.setOnTouchListener(UiUtils.getTouchListener());
        ((TextView) nextView.findViewById(R.id.title)).setText(accountName);
        ((TextView) nextView.findViewById(R.id.account)).setText(accountNumber);
        ((ImageView) nextView.findViewById(R.id.iv_image)).setImageResource(CurrencyUtils.getCurrencyDrawableFromAccNumber(accountNumber));

        nextView.setOnClickListener(v -> {
            int index = ((ViewGroup) v.getParent()).indexOfChild(v);
            v.animate().alpha(AppConstants.INVISIBLE_VIEW_ALPHA).setDuration(AppConstants.DURATION_FAST).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    //ignore
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    llContainerAccounts.removeViewAt(index);
                    if (getAccountList().size() == 0) {
                        presenter.clearFieldData();
                        updateProceedButtonState();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    //ignore
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    //ignore
                }
            });
        });
        llContainerAccounts.addView(nextView, llContainerAccounts.getChildCount() - 1);
    }

    public void updateProceedButtonState() {
        btnSaveAccount.setEnabled(!ivName.isEmpty() && getAccountList().size() != 0);
    }

    @Override
    public void updateFields(String comment, List<Account> accounts) {
        ivComment.setText(comment);
        llContainerAccounts.removeAllViews();
        createAddButton();
        for (Account item : accounts) {
            addAccount(item.getName(), item.getNumber());
        }
    }

    @Override
    public void updateLockedFields(String name, String walletId, String inn) {
        ivNumber.setText(walletId);
        ivName.setText(name);
        ivInn.setText(inn);
    }

    private List<Account> getAccountList() {
        List<Account> accounts = new ArrayList<>();
        int countItem = llContainerAccounts.getChildCount() - 1;
        for(int i = 0; i < countItem; i++) {
            accounts.add(new Account(
                    ((TextView) llContainerAccounts.getChildAt(i).findViewById(R.id.title)).getText().toString(),
                    ((TextView) llContainerAccounts.getChildAt(i).findViewById(R.id.account)).getText().toString()));
        }
        return accounts;
    }

    private void createAddButton() {
        View addView = getLayoutInflater().inflate(R.layout.view_account, null, false);
        addView.setOnTouchListener(UiUtils.getTouchListener());
        ((TextView) addView.findViewById(R.id.title)).setText(getString(R.string.fragment_add_contact_add_account));
        (addView.findViewById(R.id.account)).setVisibility(View.GONE);
        ((ImageView) addView.findViewById(R.id.iv_image)).setImageResource(R.drawable.ic_add_2);
        addView.setOnClickListener((View v) -> {
            presenter.saveFieldData(ivName.getText(), ivNumber.getText(), ivInn.getText(), ivComment.getText(), getAccountList());

            List<Account> pickedAccounts = getAccountList();
            if (pickedAccounts.size() > 0) {
                presenter.startAccountList(pickedAccounts.get(0).getNumber());
            } else {
                presenter.startAccountList("");
            }
        });
        llContainerAccounts.addView(addView);
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onUpdateFields();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
