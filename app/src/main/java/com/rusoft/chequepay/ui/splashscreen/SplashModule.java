package com.rusoft.chequepay.ui.splashscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {
    private final SplashContract.IView view;

    public SplashModule(SplashContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    SplashContract.IView provideView() {
        return view;
    }
}
