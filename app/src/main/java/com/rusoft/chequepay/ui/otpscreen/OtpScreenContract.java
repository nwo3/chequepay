package com.rusoft.chequepay.ui.otpscreen;

import com.rusoft.chequepay.ui.base.IBaseActivityView;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface OtpScreenContract {

    interface IView extends IBaseActivityView{
        void setProceedEnabled(boolean enabled);
        void goToMain();
        void goToAuth();
        void goToPin();
        void goToSuccessScreen();
        void exit();
    }

    interface IPresenter extends IBasePresenter {

        void onClickProceed(int otpType, String otp);
        void enteredOtpLengthChanged(int length);
    }

}
