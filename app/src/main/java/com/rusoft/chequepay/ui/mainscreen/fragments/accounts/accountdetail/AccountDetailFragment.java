package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetail;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.event.DepositScreenEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.EventsAdapter;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;

import static com.rusoft.chequepay.AppConstants.DURATION_INSTANT;
import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class AccountDetailFragment extends BaseBranchFragment implements AccountDetailContract.IView, EventsAdapter.ItemClickListener {

    @Inject
    AccountDetailPresenter presenter;

    @BindView(R.id.title) TextView accTitle;
    @BindView(R.id.account) TextView accNumber;
    @BindView(R.id.amount) TextView accAmount;

    @BindView(R.id.tabs) SegmentedGroup tabs;
    @BindView(R.id.tab_operation) RadioButton rbOperation;
    @BindView(R.id.tab_excerpt) RadioButton rbExcerpt;

    @BindView(R.id.menu_container) View menuContainer;
    @BindView(R.id.detail) View detail;
    @BindView(R.id.deposit) View deposit;
    @BindView(R.id.withdraw) View withdraw;
    @BindView(R.id.transfer) View transfer;
    @BindView(R.id.invoice) View invoice;
    @BindView(R.id.cheque) View cheque;
    @BindView(R.id.delete) View delete;

    @BindView(R.id.recycler_view) RecyclerView excerptRecycler;
    @BindView(R.id.label) View emptyExcerptLabel;
    @BindView(R.id.scroll_view) ScrollView scrollView;

    private Handler handler = new Handler();

    @Override
    protected int layoutResId() {
        return R.layout.fragment_account_detail;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_account_detail);
    }

    public static AccountDetailFragment getNewInstance(String containerName, Serializable data) {
        AccountDetailFragment fragment = new AccountDetailFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerAccountDetailComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .accountDetailModule(new AccountDetailModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        showOperationsTab();

        rbOperation.setTextColor(getResources().getColor(R.color.white));
        rbExcerpt.setTextColor(getResources().getColor(R.color.white));
        tabs.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id){
                case R.id.tab_operation:
                    presenter.onClickOperationsTab();
                    break;

                case R.id.tab_excerpt:
                    presenter.onClickExcerptTab();
                    break;
            }
        });

        detail.setOnClickListener(view -> presenter.onClickDetails());
        deposit.setOnClickListener(view -> presenter.onClickDeposit());
        withdraw.setOnClickListener(view -> presenter.onClickWithdraw());
        transfer.setOnClickListener(view -> presenter.onClickTransfer());
        invoice.setOnClickListener(view -> presenter.onClickInvoice());
        cheque.setOnClickListener(view -> presenter.onClickCheque());
        delete.setOnClickListener(view -> showAreYouSureDialog());

        detail.setOnTouchListener(UiUtils.getTouchListener());
        deposit.setOnTouchListener(UiUtils.getTouchListener());
        withdraw.setOnTouchListener(UiUtils.getTouchListener());
        transfer.setOnTouchListener(UiUtils.getTouchListener());
        invoice.setOnTouchListener(UiUtils.getTouchListener());
        cheque.setOnTouchListener(UiUtils.getTouchListener());
        delete.setOnTouchListener(UiUtils.getTouchListener());
    }

    private void showAreYouSureDialog() {
        UiUtils.showQuestionDialog(getContext(), R.string.dialog_on_account_closing, null,
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            presenter.onAccountCloseConfirmed();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void setDetailsData(String title, String number, String amount) {
        accTitle.setText(title);
        accNumber.setText(number);
        accAmount.setText(amount);
    }

    @Override
    public void showOperationsTab() {
        updateToolbarTitle(getString(R.string.mainscreen_title_account_detail));
        menuContainer.setVisibility(View.VISIBLE);
        excerptRecycler.setVisibility(View.GONE);
        emptyExcerptLabel.setVisibility(View.GONE);
    }

    @Override
    public void showExcerptTab(List<Operation> operations) {
        updateToolbarTitle(getString(R.string.mainscreen_title_statement));
        menuContainer.setVisibility(View.GONE);

        if (operations.size() > 0){
            EventsAdapter adapter = new EventsAdapter(getContext(), operations, this, false);
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            excerptRecycler.setLayoutManager(llm);
            excerptRecycler.addItemDecoration(new EventsAdapter.DividerCustomItemDecoration(getContext()));
            excerptRecycler.setNestedScrollingEnabled(false);
            excerptRecycler.setAdapter(adapter);

            handler.postDelayed(() -> {
                scrollView.scrollTo(0, 0);
                emptyExcerptLabel.setVisibility(View.GONE);
                excerptRecycler.setVisibility(View.VISIBLE);
            }, DURATION_INSTANT);

        } else {
            emptyExcerptLabel.setVisibility(View.VISIBLE);
            excerptRecycler.setVisibility(View.GONE);
        }
    }

    @Override
    public void enableDeposit(boolean enabled) {
        deposit.setEnabled(enabled);
        deposit.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
    }

    @Override
    public void eventSelected(long operationId, boolean isNewOperation) {
        if (isNewOperation) {
            int countUnreadMessages = ((MainActivity) getActivity()).getCountUnreadMessages();
            ((MainActivity) getActivity()).showUnreadIndicator(--countUnreadMessages);
        }
        presenter.onClickOperation(operationId);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void showRefillAccountsDialog() {
        super.showRefillAccountsDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Subscribe
    public void onEvent(DepositScreenEvent depositScreenEvent) {
        presenter.onClickDeposit();
    }
}
