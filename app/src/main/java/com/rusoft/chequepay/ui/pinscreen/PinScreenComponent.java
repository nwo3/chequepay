package com.rusoft.chequepay.ui.pinscreen;


import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {PinScreenModule.class, ChequepayApiModule.class})
public interface PinScreenComponent {
    void inject(PinActivity activity);
}
