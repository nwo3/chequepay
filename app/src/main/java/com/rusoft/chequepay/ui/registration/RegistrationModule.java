package com.rusoft.chequepay.ui.registration;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegistrationModule {
    private final RegistrationContract.IView view;

    public RegistrationModule(RegistrationContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegistrationContract.IView provideView() {
        return view;
    }
}
