package com.rusoft.chequepay.ui.registration.fragment;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.registration.model.RootSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class RegRootPresenter extends BasePresenter implements RegRootContract.IPresenter {

    @Inject
    RegistrationRepository repository;

    private WeakReference<RegRootContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private RootSubModel subModel;

    @Inject
    public RegRootPresenter(RegRootContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated() {
        model = repository.getModel();
        subModel = model.root;

        onStatusSelected(BusinessConstants.RegType.STANDART);
        onCategorySelected(BusinessConstants.ClientCategory.INDIVIDUAL);
        view.get().lockStatusAndCategory();
        getResidencyData();

        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    private void getResidencyData(){
        repository.getCountriesAndRegions(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().disableResidencySelector(true);
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().disableResidencySelector(false);
                onResidentSelected(BusinessConstants.COUNTRY_RUSSIA);
            }
        });
    }

    @Override
    public void onClickStatus() {
        List<BusinessConstants.RegType> types = new ArrayList<>();
        types.add(BusinessConstants.RegType.STANDART);
        types.add(BusinessConstants.RegType.BANK);
        types.add(BusinessConstants.RegType.ISSUER);
        int selectedIndex = types.indexOf(subModel.getRegType());
        view.get().showStatusPicker(types, selectedIndex);
    }

    @Override
    public void onClickResidency() {
        List<Country> countries = repository.getCountries();
        int selectedIndex = countries.indexOf(subModel.getResidencyCountry());
        view.get().showResidencyPicker(countries, selectedIndex);
    }

    @Override
    public void onClickCategory() {
        List<BusinessConstants.ClientCategory> categories = BusinessConstants.ClientCategory.asList();
        int selectedIndex = categories.indexOf(subModel.getCategory());
        view.get().showCategoryPicker(categories, selectedIndex);
    }

    @Override
    public void onStatusSelected(BusinessConstants.RegType type) {
        subModel.setRegType(type);
        view.get().setStatusText(type.getTitleResId());
        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    @Override
    public void onResidentSelected(Country country) {
        if (subModel.getResidencyCountry() != null && !subModel.getResidencyCountry().equals(country)) {
            model.clearBankSubModel();
        }

        subModel.setResidencyCountry(country);
        view.get().setResidencyText(country.getName());
        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    @Override
    public void onCategorySelected(BusinessConstants.ClientCategory category) {
        subModel.setCategory(category);
        view.get().setCategoryText(category.getTitleResId());
        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    @Override
    public void onPassFocusLost(String text) {
        subModel.setPass(text);
        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    @Override
    public void onPassConfirmFocusLost(String text) {
        subModel.setPassConfirm(text);

        if (!subModel.isPassMatches()) {
            view.get().showPassConfirmError(R.string.error_passwords_do_not_match);
        } else {
            view.get().showPassConfirmError(null);
        }

        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }

    @Override
    public void onClickNext() {
        if (!subModel.isPassMatches()) {
            view.get().showPassConfirmError(R.string.error_passwords_do_not_match);

        } else if (!subModel.isAllDataReceived()){
            view.get().showSnackbar(R.string.error_on_check_required_data_fail);

        } else {
            router.newScreenChain(Screens.REG_PERSON_FRAGMENT);
        }
    }

    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        view.get().restoreState(subModel);
        view.get().updateNextButtonState(subModel.isAllDataReceived());
    }
    
}
