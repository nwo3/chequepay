package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.listofcontactoperations;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ListOfContactOperationsModule {
    private final ListOfContactOperationsContract.IView view;

    public ListOfContactOperationsModule(ListOfContactOperationsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ListOfContactOperationsContract.IView provideView() {
        return view;
    }
}
