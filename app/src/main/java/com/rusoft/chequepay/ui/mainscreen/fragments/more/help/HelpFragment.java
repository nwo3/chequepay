package com.rusoft.chequepay.ui.mainscreen.fragments.more.help;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.pinscreen.PinActivity;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class HelpFragment extends BaseBranchFragment implements HelpContract.IView {

    @Inject
    HelpPresenter presenter;

    @BindView(R.id.faq) View faq;
    @BindView(R.id.feedback) View feedback;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_help;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_help);
    }

    public static HelpFragment getNewInstance(String containerName, String from) {
        HelpFragment fragment = new HelpFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putString(Screens.KEY_DATA, from);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerHelpComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .helpModule(new HelpModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getString(Screens.KEY_DATA));

        faq.setOnClickListener(v -> presenter.onClickFaq());
        feedback.setOnClickListener(v -> presenter.onClickFeedback());

        faq.setOnTouchListener(UiUtils.getTouchListener());
        feedback.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void goToAuth() {
        startActivity(new Intent(getContext(), AuthActivity.class));
        getActivity().finish();
    }

    @Override
    public void goToPin() {
        startActivity(new Intent(getContext(), PinActivity.class));
        getActivity().finish();
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

}
