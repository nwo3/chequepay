package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.correspondentdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.CommentInputView;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class CorrespondentDetailFragment extends BaseBranchFragment implements CorrespondentDetailContract.IView {

    @BindView(R.id.iv_name) StaticInputView ivName;
    @BindView(R.id.iv_number_client) StaticInputView ivNumberClient;
    @BindView(R.id.iv_inn) StaticInputView ivInn;
    @BindView(R.id.iv_comment) CommentInputView ivComment;
    @BindView(R.id.ll_container_accounts) LinearLayout llContainer;

    @Inject
    CorrespondentDetailPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_correspondent_detail;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.detailed_info);
    }

    public static CorrespondentDetailFragment getNewInstance(String name, long position) {
        CorrespondentDetailFragment contactFragment = new CorrespondentDetailFragment();

        Bundle arguments = new Bundle();
        arguments.putLong(Screens.KEY_DATA, position);
        contactFragment.setArguments(arguments);

        return contactFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerCorrespondentDetailComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .correspondentDetailModule(new CorrespondentDetailModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        //presenter.getTitle(getArguments().getLong(Screens.KEY_DATA)); //По просьбе Цуканова
        presenter.getContractorInfo(getArguments().getLong(Screens.KEY_DATA));
    }

    @Override
    public void showContractorInfo(String name, String numberClient, String inn, String comment, List<Account> accountList) {
        ivName.setText(name);
        ivNumberClient.setText(numberClient);
        ivInn.setText(inn);
        ivComment.setText(comment);
        for (Account item : accountList) {
            View accountView = getLayoutInflater().inflate(R.layout.view_account, null, false);
            ((TextView) accountView.findViewById(R.id.title)).setText(item.getName());
            ((TextView) accountView.findViewById(R.id.account)).setText(item.getNumber());
            ImageView iv = accountView.findViewById(R.id.iv_image);
            iv.setImageResource(CurrencyUtils.getCurrencyDrawableFromAccNumber(item.getNumber()));
            llContainer.addView(accountView);
        }
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
