package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount;

import android.annotation.SuppressLint;
import android.content.Context;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.account.AccountRepository;
import com.rusoft.chequepay.data.repository.account.NewAccountModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class NewAccountPresenter extends BasePresenter implements NewAccountContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    AccountRepository newAccountRepository;

    private WeakReference<NewAccountContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;

    private NewAccountModel model;

    @Inject
    public NewAccountPresenter(NewAccountContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated() {
        addRepositories(userRepository, newAccountRepository);
        model = newAccountRepository.getModel();
        updateViewControlsState();
    }

    @Override
    public void onClickCurrency() {
        List<CurrencyUtils.Currency> currencies = CurrencyUtils.Currency.asList();
        int selectedIndex = currencies.indexOf(CurrencyUtils.getCurrency(model.getCurrency()));
        view.get().showCurrencySelectorDialog(currencies, selectedIndex);
    }

    @Override
    public void onCurrencySelected(CurrencyUtils.Currency currency) {
        model.setCurrency(currency.getStrCode());
        view.get().setCurrencyText(currency.name());
        updateViewControlsState();
        updateBankIssuersList();
    }

    private void updateBankIssuersList() {
        newAccountRepository.getBankIssuers(model.getCurrency(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                updateViewControlsState();

                List<Bank> banks = userRepository.getUser().getBanks(model.getCurrency());
                    if (banks != null && !banks.isEmpty()) {
                        Bank bank = banks.get(0);
                        onBankSelected(bank);
                        List<Issuer> issuers = userRepository.getUser().getIssuersByBank(model.getCurrency(), bank);
                        if (issuers != null && !issuers.isEmpty()) {
                            Issuer issuer = issuers.get(0);
                            onIssuerSelected(issuer);
                    }
                }
            }
        });
    }

    @Override
    @SuppressLint("CheckResult")
    public void setDataInput(Observable<String> nameObservable) {
        nameObservable.subscribe(name -> {
            model.setName(name);
            updateViewControlsState();
        });
    }

    @Override
    public void onClickBank() {
        List<Bank> banks = userRepository.getUser().getBanks(model.getCurrency());
        if (banks != null && !banks.isEmpty()){
            int selectedIndex = banks.indexOf(model.getBank());
            view.get().showBankSelectorDialog(banks, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_banks_found);
        }
    }

    @Override
    public void onBankSelected(Bank bank) {
        model.setBank(bank);
        view.get().setBankText(bank.toString());

        if (model.getIssuer() != null && !userRepository.getUser().isBankAndIssuerHasRelations(model.getIssuer(), model.getBank())){
            onIssuerSelected(null);
        }

        updateViewControlsState();
    }

    @Override
    public void onClickIssuer() {
        List<Issuer> issuers = userRepository.getUser().getIssuersByBank(model.getCurrency(), model.getBank());
        if (issuers != null && !issuers.isEmpty()){
            int selectedIndex = issuers.indexOf(model.getIssuer());
            view.get().showIssuerSelectorDialog(issuers, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_issuers_found);
        }
    }

    @Override
    public void onIssuerSelected(Issuer issuer) {
        model.setIssuer(issuer);
        view.get().setIssuerText(issuer != null ? issuer.toString() : "");
        updateViewControlsState();
    }

    @Override
    public void onClickProceed() {
        newAccountRepository.createAccount(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exitWithMessage(
                        appContext.getString(R.string.fragment_new_account_success_message, model.getName()));
            }
        });
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    private void updateViewControlsState() {
        if (view == null) return;

        view.get().disableBankInput(model.getCurrency() == null);
        view.get().disableIssuerInput(model.getBank() == null);
        view.get().disableProceed(!model.isAllDataReceived());
    }

    private void recoverViewData() {
        if (view == null) return;

        view.get().setCurrencyText(CurrencyUtils.getName(model.getCurrency()));
        view.get().setAccountNameText(model.getName());
        view.get().setBankText(model.getBank() != null ? model.getBank().toString() : null);
        view.get().setIssuerText(model.getIssuer() != null ? model.getIssuer().toString() : null);
    }

    @Override
    public void onResume() {
        recoverViewData();
    }

    private void goBack() {
        newAccountRepository.clearModel();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
