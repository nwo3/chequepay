package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {OperationDetailModule.class, ChequepayApiModule.class})
public interface OperationDetailComponent {
    void inject(OperationDetailFragment fragment);
}
