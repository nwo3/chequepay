package com.rusoft.chequepay.ui;

public class Screens {
    public static final String EXTRA_NAME = "tcf_extra_name";
    public static final String CONTAINER_NAME = "container_name";
    public static final String KEY_DATA = "data_key";
    public static final String KEY_FROM = "key_from";

    public static final String ACCOUNTS_CONTAINER = "accounts_container";
    public static final String PAYMENTS_CONTAINER = "payments_container";
    public static final String EVENTS_CONTAINER = "events_container";
    public static final String CONTACTS_CONTAINER = "contacts_container";
    public static final String MORE_CONTAINER = "more_container";

    public static final String ACCOUNTS_FRAGMENT = "accounts_fragment";
    public static final String PAYMENTS_FRAGMENT = "payments_fragment";
    public static final String CONTACTS_FRAGMENT = "contacts_fragment";
    public static final String CONTACT_DETAIL_FRAGMENT = "contact_detail_fragment";
    public static final String LIST_OF_CONTACT_OPERATIONS = "list_of_contact_operations";
    public static final String EVENTS_FRAGMENT = "events_fragment";
    public static final String MORE_FRAGMENT = "more_fragment";
    public static final String HELP_FRAGMENT = "help_fragment";
    public static final String OPERATION_DETAIL_FRAGMENT = "operation_detail_fragment";
    public static final String ACTIVITIES_FRAGMENT = "activities_fragment";
    public static final String MONEY_TRANSFER_FRAGMENT = "money_transfer_fragment";
    public static final String MONEY_DEPOSIT_FRAGMENT = "money_deposit_fragment";
    public static final String MONEY_WITHDRAWAL_FRAGMENT = "money_withdrawal_fragment";
    public static final String INVOICE_FRAGMENT = "invoice_fragment";
    public static final String INVOICE_PAY_FRAGMENT = "invoice_pay_fragment";
    public static final String NEW_ACCOUNT_FRAGMENT = "new_account_fragment";
    public static final String ACCOUNT_DETAIL_FRAGMENT = "account_detail_fragment";
    public static final String ACCOUNT_DETAILED_INFO_FRAGMENT = "account_detailed_info_fragment";
    public static final String CHEQUE_FRAGMENT = "cheque_fragment";
    public static final String OPERATION_LIST_FRAGMENT = "operation_list_fragment";
    public static final String CORRESPONDENT_DETAIL_FRAGMENT = "correspondent_detail_fragment";
    public static final String ADD_CONTACT_FRAGMENT = "add_contact_fragment";
    public static final String ACCOUNT_LIST_FRAGMENT = "account_list_fragment";
    public static final String FAQ_FRAGMENT = "faq_fragment";
    public static final String SECURITY_FRAGMENT = "security_fragment";
    public static final String PROFILE_FRAGMENT = "profile_fragment";
    public static final String DOCS_FRAGMENT = "docs_fragment";
    public static final String FEEDBACK_FRAGMENT = "feedback_fragment";
    public static final String PDF_FRAGMENT = "pdf_fragment";

    public static final String MAIN_ACTIVITY = "main_activity";
    public static final String AUTH_ACTIVITY = "auth_activity";
    public static final String PIN_ACTIVITY = "pin_activity";

    public static final String REGISTRATION_CONTAINER = "registration_container";
    public static final String REG_ROOT_FRAGMENT = "registration_first_fragment";
    public static final String REG_PERSON_FRAGMENT = "registration_person_fragment";
    public static final String REG_PASSPORT_FRAGMENT = "registration_passport_fragment";
    public static final String REG_ADDRESS_FRAGMENT = "registration_address_fragment";
    public static final String REG_HOME_ADDRESS_FRAGMENT = "registration_home_address_fragment";
    public static final String REG_BANK_FRAGMENT = "registration_bank_fragment";
    public static final String REG_ACCOUNT_FRAGMENT = "registration_account_fragment";
}