package com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface InvoicePayContract {

    interface IView extends IBaseView {
        void setAccountText(String text);
        void setDateTimeText(String text);
        void setConteragentText(String text);
        void setConteragentAccountText(String text);
        void setValidityText(String text);
        void setAmountText(String text);
        void setAmountTextColor(int textColor);
        void setCommentText(String text);
        void setStatusText(String text);
        void setCurrencyText(String text);
        void setCommissionText(String text);
        void setTotalText(String text);
        void showAddContragentButton(boolean show);

        void disableProceedButton(boolean disable);
        void hideProceedButton(boolean hide);
        void hideRefuseButton(boolean hide);
        void showDownloadPdfButton(boolean show);

        void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, Integer currencyFilter);
        void showAddNewAccountDialog();
        void showDepositDialog(Account account);
        void showRefuseDialog();

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(Serializable data);

        void onClickAccount();
        void onAccountSelected(Account account);

        void onClickProceed();
        void onClickAddContragent();
        void onRefuseConfirmed();

        void goToNewAccount();
        void goToDeposit(Account account);
        void goToPdfViewer();
        void clearModel();
    }
}
