package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.Date;
import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ActivitiesViewHolder> {

    public interface ItemClickListener{
        void onActivitySelected(Application invoiceInfoData);
    }

    private Context context;
    private List<Application> activities;
    private ItemClickListener listener;

    public ActivitiesAdapter(Context context, List<Application> activities, ItemClickListener listener) {
        this.context = context;
        this.activities = activities;
        this.listener = listener;
    }

    @Override
    public ActivitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_activity_item, parent, false);
        return new ActivitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ActivitiesViewHolder holder, int position) {
        Application activityItem = activities.get(position);

        holder.title.setText(activityItem.getStateName());
        holder.type.setText(activityItem.getPaymentTypeName());
        holder.name.setText(CommonUtils.splitContragent(activityItem.senderName));
        holder.amount.setText(CurrencyUtils.format(activityItem.getAmount(), activityItem.getCurrency()));
        holder.date.setText(AppConstants.SDF_ddMMyyyy_HHmm.format(new Date(activityItem.getDate())));

        holder.container.setOnTouchListener(UiUtils.getTouchListener());
        holder.container.setOnClickListener(v -> {
            if (listener != null) listener.onActivitySelected(activityItem);
        });

        holder.container.setBackgroundColor(activityItem.isNew() ? context.getResources().getColor(R.color.dark_slate_blue) : Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    class ActivitiesViewHolder extends RecyclerView.ViewHolder {
        View container;
        TextView title, type, date, amount, name;

        public ActivitiesViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            title = itemView.findViewById(R.id.title);
            type = itemView.findViewById(R.id.type);
            date = itemView.findViewById(R.id.date);
            amount = itemView.findViewById(R.id.amount);
            name = itemView.findViewById(R.id.name);
        }
    }
}
