package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class WithdrawalFragment extends BaseBranchFragment implements WithdrawalContract.IView {

    @Inject
    WithdrawalPresenter presenter;

    @BindView(R.id.account) StaticInputView account;
    @BindView(R.id.mode) StaticInputView mode;
    @BindView(R.id.amount) StaticInputView amount;

    @BindView(R.id.issuer_label) TextView issuerLabel;
    @BindView(R.id.currency) TextView currency;
    @BindView(R.id.total) TextView total;
    @BindView(R.id.commission_container) View commissionContainer;
    @BindView(R.id.commission) TextView commission;

    //region Вывод на счёт третьего лица (third-party account),
    //функционал на данный момент не доступен, ждём бэк
    @BindView(R.id.third_party_switch) SwitchCompat tpSwitch;
    @BindView(R.id.third_party_container) View tpContainer;
    @BindView(R.id.acc_num) StaticInputView tpAccount;
    @BindView(R.id.bank) StaticInputView tpBank;
    @BindView(R.id.kor) StaticInputView tpKor;
    @BindView(R.id.inn) StaticInputView tpInn;
    @BindView(R.id.kpp) StaticInputView tpKpp;
    @BindView(R.id.bik) StaticInputView tpBik;
    //endregion

    @BindView(R.id.proceed) TextView proceed;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_withdrawal;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_money_withdrawal);
    }

    public static WithdrawalFragment getNewInstance(String containerName, Serializable data) {
        WithdrawalFragment fragment = new WithdrawalFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerWithdrawalComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .withdrawalModule(new WithdrawalModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        account.setOnClickListener(view -> presenter.onClickAccount());
        mode.setOnClickListener(view -> presenter.onClickMode());

        amount.setOnFocusChangedListener((view, focused) -> {
            if (amount != null){
                if (focused) amount.setText("");
                else presenter.onAmountFocusLost(amount.getText());
            }
        });

        amount.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                UiUtils.hideKeyboard(getContext());
                v.setFocusableInTouchMode(false);
                v.setFocusable(false);
                v.setFocusableInTouchMode(true);
                v.setFocusable(true);
                proceed.requestFocus();
                return true;
            }
            return false;
        });

        tpSwitch.setOnCheckedChangeListener((compoundButton, state) -> {
            presenter.onThirdPartySwitchStateChanged(state);
            tpContainer.setVisibility(state ? View.VISIBLE : View.GONE);
        });

        proceed.setOnClickListener(view -> presenter.onClickProceed());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void setAccountText(String text) {
        account.setText(text);
    }

    @Override
    public void setModeText(Integer textResId) {
        if (textResId != null){
            mode.setText(getString(textResId));
        }
    }

    @Override
    public void setAmountText(String text) {
        amount.setText(text);
    }

    @Override
    public void setIssuerLabelText(String text) {
        issuerLabel.setText(text);
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
    }

    @Override
    public void setCommissionText(String text) {
        commission.setText(text);
    }

    @Override
    public void showCommission(boolean enabled) {
        commissionContainer.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setTotalText(String text) {
        total.setText(text);
    }

    @Override
    public void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onAccountSelected(accounts.get(selected)));
    }

    @Override
    public void showModeSelectorDialog(List<BusinessConstants.RefundMode> modes, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), modes, selectedIndex,
                selected -> presenter.onModeSelected(modes.get(selected)));
    }

    @Override
    public void setTpAccountText(String text) {
        tpAccount.setText(text);
    }

    @Override
    public void setTpBankText(String text) {
        tpBank.setText(text);
    }

    @Override
    public void setTpKorText(String text) {
        tpKor.setText(text);
    }

    @Override
    public void setTpInnText(String text) {
        tpInn.setText(text);
    }

    @Override
    public void setTpKppText(String text) {
        tpKpp.setText(text);
    }

    @Override
    public void setTpBikText(String text) {
        tpBik.setText(text);
    }

    @Override
    public void disableAccountInput(boolean disabled) {
        account.setEnabled(!disabled);
    }

    @Override
    public void disableModeInput(boolean disabled) {
        mode.setEnabled(!disabled);
    }

    @Override
    public void disableAmountInput(boolean disabled) {
        amount.setEnabled(!disabled);
    }

    @Override
    public void disableProceedButton(boolean disabled) {
        proceed.setEnabled(!disabled);
    }

    @Override
    public void showAddNewAccountDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_accounts, null,
                R.string.cancel, R.string.dialog_on_empty_accounts_accept,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToNewAccount();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showDepositDialog(Account account, boolean isInfoDialogType) {
        if (isInfoDialogType){
            UiUtils.showInfoDialog(getContext(), R.string.dialog_on_account_with_not_enough_funds, R.string.ok);

        } else {
            UiUtils.showQuestionDialog(getContext(),
                    R.string.dialog_on_account_with_not_enough_funds, null,
                    R.string.cancel, R.string.deposit_1,
                    true, (dialog, i) -> {
                        switch (i) {
                            case DialogInterface.BUTTON_POSITIVE:
                                presenter.goToDeposit(account);
                                dialog.dismiss();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    });
        }
    }


    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_WITHDRAWAL);
        startActivity(intent);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

}
