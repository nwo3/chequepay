package com.rusoft.chequepay.ui.registration.fragment.individual.passport;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.PassportSubModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

public class RegPassportPresenter extends BasePresenter implements RegPassportContract.IPresenter {

    @Inject
    RegistrationRepository repository;

    private WeakReference<RegPassportContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private PassportSubModel subModel;

    @Inject
    public RegPassportPresenter(RegPassportContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated() {
        model = repository.getModel();
        subModel = model.passport;
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        view.get().updateViewByResidency(model.isResident());
    }

    @Override
    public void onNumFocusLost(String text) {
        subModel.setNumber(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onSeriesFocusLost(String text) {
        subModel.setSeries(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onIssuerFocusLost(String text) {
        subModel.setIssuer(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onDateSelected(long date) {
        subModel.setIssueDate(validateIssuerDate(date) ? date : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    private boolean validateIssuerDate(Long date) {
        boolean isValid = date != null && (date > 0 && date < System.currentTimeMillis());
        if (!isValid) view.get().setErrorOnIssueDateInput(R.string.error_wrong_data);
        return isValid;
    }

    @Override
    public void onCodeFocusLost(String text) {
        subModel.setDepartmentCode(!text.isEmpty() ? text : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickNext() {
        if (subModel.isRequiredDataReceived()){
            router.navigateTo(Screens.REG_ADDRESS_FRAGMENT);
        } else {
            validateIssuerDate(subModel.getIssueDate());
            view.get().showSnackbar(R.string.error_on_check_required_data_fail);
        }
    }

    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        view.get().restoreState(subModel);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        view.get().initializeInputListeners();
    }

}
