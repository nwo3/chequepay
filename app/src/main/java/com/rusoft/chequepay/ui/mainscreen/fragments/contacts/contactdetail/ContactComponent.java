package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.contactdetail;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ContactModule.class, ChequepayApiModule.class})
public interface ContactComponent {
    void inject(ContactFragment fragment);
}
