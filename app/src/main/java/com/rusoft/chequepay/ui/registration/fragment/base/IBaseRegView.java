package com.rusoft.chequepay.ui.registration.fragment.base;

import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface IBaseRegView extends IBaseView {

    void updateNextButtonState(boolean enabled);

}
