package com.rusoft.chequepay.ui.registration;


import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.ErrorUtils;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

public class RegistrationPresenter extends BasePresenter implements RegistrationContract.IPresenter {

    private WeakReference<RegistrationContract.IView> view;
    private Router router;
    private RegistrationRepository repository;

    @Inject
    public RegistrationPresenter(RegistrationContract.IView view, Router router, RegistrationRepository repository) {
        this.view = new WeakReference<>(view);
        this.router = router;
        this.repository = repository;
    }

    @Override
    public void onViewCreated(int key) {
        boolean isInRegistrationMode = key == AppConstants.REG_ACTIVITY_REGULAR;
        repository.setRegistrationMode(isInRegistrationMode);
        view.get().setNextButtonTitle(isInRegistrationMode ? R.string.next : R.string.save);

        switch (key) {
            case AppConstants.REG_ACTIVITY_REGULAR:
                router.replaceScreen(Screens.REG_ROOT_FRAGMENT);
                break;

            case AppConstants.REG_ACTIVITY_PROFILE_EDIT_PERSONAL:
                router.replaceScreen(Screens.REG_PERSON_FRAGMENT);
                break;

            case AppConstants.REG_ACTIVITY_PROFILE_EDIT_HOME_ADDRESS:
                router.replaceScreen(Screens.REG_HOME_ADDRESS_FRAGMENT);
                break;

            case AppConstants.REG_ACTIVITY_PROFILE_EDIT_BANK:
                router.replaceScreen(Screens.REG_BANK_FRAGMENT);
                break;

            default:
                throw new InternalError(ErrorUtils.INTERNAL_ERROR);
        }
    }

    @Override
    public void onClickNext() {
        EventBus.getDefault().post(new NextPressedEvent());
        //LogUtil.i("REG_MODEL:\n" + repository.getModel());
    }

    @Override
    public void onDestroy() {
        repository.clearModel();
    }
}
