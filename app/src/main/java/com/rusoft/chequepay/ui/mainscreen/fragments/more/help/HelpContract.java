package com.rusoft.chequepay.ui.mainscreen.fragments.more.help;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface HelpContract {

    interface IView extends IBaseView {
        void goToAuth();
        void goToPin();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(String from);

        void onClickFaq();
        void onClickFeedback();

    }

}
