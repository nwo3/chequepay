package com.rusoft.chequepay.ui.mainscreen.fragments.more;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface MoreContract {

    interface IView extends IBaseView {
        void goToAuth();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(String keyFrom);

        void onClickProfile();
        void onClickSecurity();
        void onClickDocs();
        void onClickFaq();
        void onClickFeedback();
        void onClickLogout();
    }
}
