package com.rusoft.chequepay.ui.dialogs;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;

import butterknife.BindView;
import lombok.Setter;

public class RadioButtonPickerDialog<T> extends BasePickerDialog {

    public interface PickerListener {
        void onPickerConfirmClicked(int selectedIndex);
    }

    @BindView(R.id.container) View container;
    @BindView(R.id.radio_button_group) RadioGroup radioGroup;

    @Setter protected PickerListener listener;

    @Setter private int defItemNum;

    @Override
    protected void initializeContent() {
        for (int i = 0; i < items.size(); i++) {
            View rbView = getActivity().getLayoutInflater().inflate(R.layout.dialog_picker_radiobutton, null);
            RadioButton button = rbView.findViewById(R.id.radio_button);
            T item = (T) items.get(i);
            button.setId(i + 100);
            button.setText(getItemTitle(item));
            checkSpecialCases(item, button);
            radioGroup.addView(button);
        }

        RadioButton buttonCheckedByDefault = (RadioButton) radioGroup.getChildAt(defItemNum);
        buttonCheckedByDefault.setChecked(true); // set checked to item that must be checked by default
        scroll.post(() -> scroll.scrollTo(0, buttonCheckedByDefault.getTop())); // scroll to checked RadioButton

        confirm.setOnClickListener(view -> {
            listener.onPickerConfirmClicked(getSelectedIndex());
            dismiss();
        });
    }

    private int getSelectedIndex(){
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            if (((RadioButton) radioGroup.getChildAt(i)).isChecked()){
                return i;
            }
        }

        throw new InternalError(ErrorUtils.INTERNAL_ERROR);
    }

    private void checkSpecialCases(T item, RadioButton button) {
        if (item instanceof Account) {
            CurrencyUtils.Currency currency = CurrencyUtils.getCurrency(((Account) item).getCurrency());
            boolean isEnabled = true;

            if (isResident != null && currency != null) isEnabled = isResident == currency.isEnabledForResident();
            if (currencyFilter != null && currency != null) isEnabled = currency.getCode() == currencyFilter;

            button.setAlpha(isEnabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
            button.setFocusable(isEnabled);
            button.setClickable(isEnabled);
        }
    }
}
