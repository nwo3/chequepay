package com.rusoft.chequepay.ui.registration.fragment.individual.account;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegAccountModule.class, ChequepayApiModule.class})
public interface RegAccountComponent {
    void inject(RegAccountFragment fragment);
}
