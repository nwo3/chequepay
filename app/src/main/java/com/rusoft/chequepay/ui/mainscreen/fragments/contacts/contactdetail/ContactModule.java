package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.contactdetail;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactModule {
    private final ContactContract.IView view;

    public ContactModule(ContactContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ContactContract.IView provideContactView() {
        return view;
    }
}
