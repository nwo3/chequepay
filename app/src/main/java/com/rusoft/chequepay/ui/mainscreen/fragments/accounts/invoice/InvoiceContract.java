package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface InvoiceContract {

    interface IView extends IBaseView {
        void setAccountText(String title);
        void setTargetManualText(String text);
        void setTargetSelectorText(String text);
        void setAmountText(String text);
        void setLifetimeText(String text);
        void setCommentText(String text);
        void setDateText(String text);

        void changeTargetInputMode(boolean state);
        void applyTemplateMode();

        void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident);
        void showContactsSelectorDialog(List<Correspondent> correspondents, int selectedIndex);
        void showDatePicker();

        void showTargetInputError(Integer textResId);

        void disableAccountInput(boolean disabled);
        void disableModeSwitch(boolean disabled);
        void disableContractorInput(boolean disabled);
        void disableOtherInputs(boolean disabled);
        void disableProceedButton(boolean disabled);
        void disableTargetSelectorAndSwitch();

        void showAddNewAccountDialog();

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(Serializable data);

        void onClickAccount();
        void onAccountSelected(Account account);

        void onModeSwitchStateChanged(boolean state); //false: manual, true: from list

        void onClickContactsSelector();
        void onContactSelected(Correspondent correspondent);
        void onContractorFocusLost(String inputData);

        void onLifetimeFocusLost(String inputData);
        void onAmountFocusLost(String inputData);

        void onCommentFocusLost(String inputData);
        void onContractNumFocusLost(String inputData);

        void onClickDate();
        void onDateSelected(long date);

        void goToNewAccount();

        void onClickProceed();

    }
}
