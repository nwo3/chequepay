package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface EventsContract {

    interface IView extends IBaseView {
        void showOperationsShortList(List<Operation> operationShortList);
        void showEmptyOperationsShortList();
        void showUnreadActivities(int count);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickAllOperations();
        void onClickActivities();
        void onClickOperation(long operationId);
        void onRefresh();

        void getOperationsShortList();
    }
}
