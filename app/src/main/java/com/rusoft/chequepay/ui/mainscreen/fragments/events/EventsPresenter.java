package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.flagsmanager.Flags;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class EventsPresenter extends BasePresenter implements EventsContract.IPresenter {

    private WeakReference<EventsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private DetailOperationModel detailOperationModel;

    @Inject
    OperationRepository operationRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    public EventsPresenter(EventsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(operationRepository, userRepository);
        detailOperationModel = operationRepository.getDetailOperationModel();

        showUnreadActivitiesCount();
    }

    @Override
    public void onClickAllOperations() {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().navigateTo(Screens.OPERATION_LIST_FRAGMENT);
    }

    @Override
    public void onClickActivities() {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().replaceScreen(Screens.ACTIVITIES_FRAGMENT);
    }

    @Override
    public void onClickOperation(long operationId) {
        detailOperationModel.setOperationId(operationId);
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().navigateTo(Screens.OPERATION_DETAIL_FRAGMENT);
    }

    @Override
    public void onRefresh() {
        FlagsManager.getInstance().turnOn(Flags.EVENT);
        getOperationsShortList();
        showUnreadActivitiesCount();
    }

    @Override
    public void getOperationsShortList() {
            operationRepository.getOperationShortList(new RequestCallbackListener(){
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showLoadingIndicator(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showLoadingIndicator(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    showOperations();
                }
            });
    }

    private void showOperations() {
        if (userRepository.getUser().getShortOperations().size() > 0) {
            view.get().showOperationsShortList(userRepository.getUser().getShortOperations());
        } else {
            view.get().showEmptyOperationsShortList();
        }
    }

    private void showUnreadActivitiesCount() {
        int unreadActivitiesCount = 0;

        for (Application application : userRepository.getUser().getInvoices()) {
            if (application.isNew()) unreadActivitiesCount++;
        }

        view.get().showUnreadActivities(unreadActivitiesCount);
    }
}
