package com.rusoft.chequepay.ui.pinscreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.androidessence.fingerprintdialogsample.FingerprintController;
import com.androidessence.fingerprintdialogsample.FingerprintDialog;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.PinView;
import com.rusoft.chequepay.data.event.TokenExpiredEvent;
import com.rusoft.chequepay.data.event.UserDataServiceEvent;
import com.rusoft.chequepay.data.service.userdata.UserDataService;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;

public
class PinActivity extends BaseActivity implements PinScreenContract.IView, PinView.CallbacksListener, FingerprintController.Callback {

    @Inject
    PinScreenPresenter presenter;

    @BindView(R.id.title) TextView title;
    @BindView(R.id.pin) PinView pin;
    @BindView(R.id.help) View help;

    @Override
    protected int layoutResId() {
        return R.layout.activity_pin;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerPinScreenComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .pinScreenModule(new PinScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        pin.setListener(this);

        help.setOnClickListener(view -> presenter.onClickHelp());
        help.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void enableFingerprint(boolean enabled) {
        new Handler().post(() -> pin.setFingerprintEnabled(enabled));
    }

    @Override
    public void setTitleText(int titleResId) {
        title.setText(getString(titleResId));
    }

    @Override
    public void clearPin() {
        pin.clear();
    }

    @Override
    public void onWrongPin() {
        pin.onWrongPin();
    }

    @Override
    public void startUserDataReceiving() {
        startService(new Intent(this, UserDataService.class));
    }

    @Override
    public void onPinEntered(String code) {
        presenter.onPinEntered(code);
    }

    @Override
    public void onFingerprintClicked() {
        showFingerprintDialog();
    }

    @Override
    public void showFingerprintDialog() {
        FingerprintDialog dialog = FingerprintDialog.Companion.newInstance(getString(R.string.fingerprint_sign_in), getString(R.string.fingerprint_confirm));;
        dialog.setCallback(this);
        dialog.show(getSupportFragmentManager(), FingerprintDialog.Companion.getFRAGMENT_TAG());
    }

    @Override
    public void onAuthenticated() {
        //on fingerprint authentication success
        presenter.onFingerprintAuthenticationSuccess();
    }

    @Override
    public void onError() {
        //on fingerprint authentication error
        showSnackbar(getString(R.string.fingerprint_authentication_failed));
    }

    @Override
    public void onCancelButtonClicked() {
        //диалог закрывается в классе FingerprintDialog
    }

    @Override
    public void askToEnableFingerprintAuthentication() {
        UiUtils.showQuestionDialog(this, R.string.security_activate_fingerprint_authentication, null,
                R.string.no, R.string.yes,
                false, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.enableFingerprintAuthentication(true);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            presenter.enableFingerprintAuthentication(false);
                            break;
                    }

                    startUserDataReceiving();
                    dialog.dismiss();
                });
    }


    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Screens.KEY_FROM, Screens.PIN_ACTIVITY);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToAuth() {
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }

    @Override
    public void goToHelp() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Screens.KEY_FROM, AppConstants.FROM_PIN_TO_HELP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UserDataServiceEvent event){
        switch (event.getMessage()){
            case AppConstants.EVENT_USER_DATA_RECEIVING_START:
                showProgressDialog(true);
                break;

            case AppConstants.EVENT_USER_DATA_RECEIVING_END:
                showProgressDialog(false);
                break;

            case AppConstants.EVENT_USER_DATA_RECEIVING_SUCCESS:
                goToMain();
                break;

            case AppConstants.EVENT_USER_DATA_RECEIVING_ERROR:
                showError(event.getThrowable());
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TokenExpiredEvent event){
        presenter.onTokenExpired();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
