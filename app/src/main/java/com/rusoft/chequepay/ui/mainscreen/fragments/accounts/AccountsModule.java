package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountsModule {
    private final AccountsContract.IView view;

    public AccountsModule(AccountsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    AccountsContract.IView provideAccountsView() {
        return view;
    }
}
