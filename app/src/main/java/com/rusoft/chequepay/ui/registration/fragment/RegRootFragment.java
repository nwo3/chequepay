package com.rusoft.chequepay.ui.registration.fragment;

import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.OnTextChangedWatcher;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.RootSubModel;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class RegRootFragment extends BaseRootFragment implements RegRootContract.IView {

    @Inject
    RegRootPresenter presenter;

    @BindView(R.id.status) StaticInputView status;
    @BindView(R.id.resident) StaticInputView residency;
    @BindView(R.id.category) StaticInputView category;
    @BindView(R.id.pass) DynamicInputView pass;
    @BindView(R.id.pass_confirm) DynamicInputView passConfirm;
    @BindView(R.id.focus_handler) View focus_handler;


    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_root;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.reg_title_registration);
    }

    public static RegRootFragment getNewInstance() {
        return new RegRootFragment();
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegRootComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regRootModule(new RegRootModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ((RegistrationActivity) getActivity()).scrollToStart();

        status.setOnClickListener(v -> presenter.onClickStatus());
        residency.setOnClickListener(v -> presenter.onClickResidency());
        category.setOnClickListener(v -> presenter.onClickCategory());

        pass.setValidationPattern(ValidationPatterns.AMC_PASSWORD);
        passConfirm.setValidationPattern(ValidationPatterns.AMC_PASSWORD);

        pass.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (pass != null && pass.validate()) presenter.onPassFocusLost(pass.getText());
        });

        passConfirm.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (passConfirm != null) presenter.onPassConfirmFocusLost(passConfirm.getText());
        });
    }

    @Override
    public void showStatusPicker(List<BusinessConstants.RegType> types, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), types, selectedIndex,
                selected -> presenter.onStatusSelected(types.get(selected)));
    }

    @Override
    public void showResidencyPicker(List<Country> countries, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), countries, selectedIndex,
                selected -> presenter.onResidentSelected(countries.get(selected)));
    }

    @Override
    public void showCategoryPicker(List<BusinessConstants.ClientCategory> categories, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), categories, selectedIndex,
                selected -> presenter.onCategorySelected(categories.get(selected)));
    }

    @Override
    public void setStatusText(int textResId) {
        status.setText(getContext().getString(textResId));
    }

    @Override
    public void setResidencyText(String text) {
        residency.setText(text);
    }

    @Override
    public void setCategoryText(int textResId) {
        category.setText(getContext().getString(textResId));
    }

    @Override
    public void lockStatusAndCategory() {
        status.setEnabled(false);
        category.setEnabled(false);
    }

    @Override
    public void showPassConfirmError(Integer textResId) {
        passConfirm.setError(textResId == null ? null : getString(textResId));
    }

    @Override
    public void disableResidencySelector(boolean disable) {
        residency.setEnabled(!disable);

        if (disable){
            residency.setText(getString(R.string.data_loading_in_progress));
        }
    }

    @Override
    public void restoreState(RootSubModel model) {
        status.setText(model.getRegType() != null ? model.getRegType().toString(getContext()) : "");
        residency.setText(model.getResidencyCountry() != null ? model.getResidencyCountry().getName() : "");
        category.setText(model.getCategory() != null ? model.getCategory().toString(getContext()) : "");
        pass.setText(model.getPass());
        passConfirm.setText(model.getPassConfirm());
    }

    @Override
    public void updateNextButtonState(boolean enabled) {
        new android.os.Handler().post(() -> ((RegistrationActivity) getActivity()).updateNextButtonState(enabled));
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        focus_handler.requestFocus();

        if (UiUtils.validateInputViews(status, residency, category, pass, passConfirm)){
            presenter.onClickNext();
        }
    }

    @Override
    public void exit() {
        UiUtils.hideKeyboard(getContext());
        ((RegistrationActivity) getActivity()).goToAuth();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }
}
