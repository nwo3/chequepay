package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {WithdrawalModule.class, ChequepayApiModule.class})
public interface WithdrawalComponent {
    void inject(WithdrawalFragment fragment);
}
