package com.rusoft.chequepay.ui.registration.fragment.individual.address;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegAddressModule {
    private final RegAddressContract.IView view;

    public RegAddressModule(RegAddressContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegAddressContract.IView provideView() {
        return view;
    }
}
