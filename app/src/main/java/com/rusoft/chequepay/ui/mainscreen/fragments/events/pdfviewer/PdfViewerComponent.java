package com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {PdfViewerModule.class, ChequepayApiModule.class})
public interface PdfViewerComponent {
    void inject(PdfViewerFragment fragment);
}
