package com.rusoft.chequepay.ui.passrecovery;

import com.rusoft.chequepay.ui.base.IBaseActivityView;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface PassRecoveryScreenContract {

    interface IView extends IBaseActivityView{
        void updateScreenState(PassRecoveryScreenPresenter.ScreenState state);
        void setProceedEnabled(boolean enabled);
        void showPassConfirmError(int textResId);

        void goToAuth();
        void goToOtp();
        void goToSuccessScreen();
        void goToHelp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onPhoneTextChanged(String text, boolean isValid);
        void onPassFocusLost(String text);
        void onPassConfirmFocusLost(String text);

        void onClickProceed();
        void onClickHelp();

    }

}
