package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import android.os.Handler;

import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ActivitiesPresenter extends BasePresenter implements ActivitiesContract.IPresenter {

    private WeakReference<ActivitiesContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    InvoiceRepository invoiceRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    public ActivitiesPresenter(ActivitiesContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(userRepository, invoiceRepository);

        view.get().showActivitiesList(userRepository.getUser().getInvoices());
    }

    @Override
    public void onClickOperations() {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().replaceScreen(Screens.EVENTS_FRAGMENT);
    }

    @Override
    public void onClickItem(Application invoiceInfoData) {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter()
                .newScreenChain(Screens.INVOICE_PAY_FRAGMENT, invoiceInfoData);
    }

    @Override
    public void onRefresh() {
        updateInvoicesList(true);
    }

    @Override
    public void onResume() {
        new Handler().post(() -> updateInvoicesList(false));
    }

    private void updateInvoicesList(boolean forced) {
        invoiceRepository.updateInvoicesList(forced, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().showActivitiesList(userRepository.getUser().getInvoices());
            }
        });
    }


}
