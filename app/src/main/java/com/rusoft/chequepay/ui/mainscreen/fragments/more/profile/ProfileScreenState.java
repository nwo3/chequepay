package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import com.rusoft.chequepay.R;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
enum ProfileScreenState {

    MENU(
            false, R.string.mainscreen_title_profile,
            new int[]{}),

    PERSONAL(
            true, R.string.mainscreen_title_personal_info,
            new int[]{
                    R.string.client_num,
                    R.string.client_credentials,
                    R.string.email_1,
                    R.string.phone,
                    R.string.status,
                    R.string.inn,
                    R.string.web_site
            }),

    PASSPORT(
            false, R.string.mainscreen_title_passport_info,
            new int[]{
                    R.string.passport_num,
                    R.string.passport_whom,
                    R.string.passport_code,
                    R.string.passport_date,
            }),

    REG_ADDRESS(
            false, R.string.reg_title_registration_address,
            new int[]{
                    R.string.country,
                    R.string.country_subject_1,
                    R.string.settlement,
                    R.string.postcode,
                    R.string.address
            }),

    HOME_ADDRESS(
            true, R.string.reg_title_home_address,
            new int[]{
                    R.string.country,
                    R.string.country_subject_1,
                    R.string.settlement,
                    R.string.postcode,
                    R.string.address
            }),

    REQUISITES_RESIDENT(
            true, R.string.bank_req,
            new int[]{
                    R.string.title,
                    R.string.bik_1,
                    R.string.kor_acc,
                    R.string.account_number_3,
                    R.string.bank_inn,
                    R.string.bank_kpp,
                    R.string.payment_receiver
            }),

    REQUISITES_NON_RESIDENT(
            true, R.string.company_req,
            new int[]{
                    R.string.title,
                    R.string.bik_2,
                    R.string.swift,
                    R.string.bank_address,
                    R.string.account_number_2,
                    R.string.bank_additional_req
            });

    @Getter boolean hasWritableFields;
    @Getter int toolbarTitleResId;
    @Getter int[] titlesResIds;

}
