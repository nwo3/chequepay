package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.clientdata.response.Contractor;
import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.event.ToolbarBtnOnClickEvent;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.EventsAdapter;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

public class OperationListFragment extends BaseBranchFragment implements OperationListContract.IView, EventsAdapter.ItemClickListener, DatePickerDialog.OnDateSetListener {

    @Inject
    OperationListPresenter presenter;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.label) TextView emptyExcerptLabel;
    @BindView(R.id.btn_all_operations) TextView btnSearch;
    @BindView(R.id.filter_container) LinearLayout llFilter;
    @BindView(R.id.iv_account_name) StaticInputView ivAccountName;
    @BindView(R.id.iv_contragent) StaticInputView ivContragent;
    @BindView(R.id.iv_currency) StaticInputView ivCurrency;
    @BindView(R.id.iv_period) StaticInputView ivPeriod;
    @BindView(R.id.iv_type_operation) StaticInputView ivOperationTypes;
    @BindView(R.id.fl_view) FrameLayout frameLayout;
    @BindView(R.id.filter_scrollview) ScrollView scrollView;

    private DatePickerDialog datePickerDialog;
    private List<Operation> operationList = new ArrayList<>();
    private EventsAdapter adapter;

    @Getter @Setter
    private boolean filterHidden = false;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_operations_list;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.fragment_events_segmented_contrl_tab1);
    }

    public static OperationListFragment getNewInstance(String name) {
        OperationListFragment operationDetailFragment = new OperationListFragment();

        Bundle arguments = new Bundle();
        operationDetailFragment.setArguments(arguments);

        return operationDetailFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerOperationListComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .operationListModule(new OperationListModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        scrollView.setVisibility(View.GONE);

        adapter = new EventsAdapter(getContext(), operationList, this, false);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.addItemDecoration(new EventsAdapter.DividerCustomItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        presenter.onViewCreated();
        presenter.initDefaultData();

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                OperationListFragment.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setThemeDark(true);
        datePickerDialog.setStartTitle(getString(R.string.from));
        datePickerDialog.setEndTitle(getString(R.string.to));
        datePickerDialog.setMaxDate(calendar);

        presenter.getOperationList();

        btnSearch.setOnTouchListener(UiUtils.getTouchListener());

        ivPeriod.setOnClickListener(v -> presenter.onClickPeriod());

        ivAccountName.setOnClickListener(v -> presenter.onClickAccountName());

        ivContragent.setOnClickListener(v -> presenter.onClickContragent());

        ivCurrency.setOnClickListener(v -> presenter.onClickCurrency());

        ivOperationTypes.setOnClickListener(v -> presenter.onClickTypeOperation());

        btnSearch.setOnClickListener(v -> presenter.getOperationList());
    }

    @Override
    protected void onNavigationClicked() {
        if (llFilter != null) {
            presenter.onClickBack(filterHidden);
        }
    }

    @Override
    public void showOperationList(List<Operation> operations) {
        recyclerView.setVisibility(View.VISIBLE);
        emptyExcerptLabel.setVisibility(View.GONE);
        operationList.clear();
        operationList.addAll(operations);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyExcerptLabel() {
        recyclerView.setVisibility(View.GONE);
        emptyExcerptLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFilter(boolean show) {
        scrollView.setVisibility(View.VISIBLE);

        if (show) {
            llFilter.animate().translationY(-llFilter.getHeight());
            scrollView.animate().translationY(-scrollView.getHeight());
        } else {
            llFilter.animate().translationY(0);
            scrollView.animate().translationY(0);
        }
        setFilterHidden(!show);
        presenter.stateFilter(!show);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(AppConstants.SDF_ddMMyyyy.parse(dayOfMonth + "." + (++monthOfYear) + "." + year));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            Date startDate = calendar.getTime();

            calendar.setTime(AppConstants.SDF_ddMMyyyy.parse(dayOfMonthEnd + "." + (++monthOfYearEnd) + "." + yearEnd));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            Date endDate = calendar.getTime();

            if (startDate.getTime() <= endDate.getTime()) {
                ivPeriod.setText(AppConstants.SDF_ddMMyyyy.format(startDate) + " - " + AppConstants.SDF_ddMMyyyy.format(endDate));
                presenter.saveDate(startDate.getTime(), endDate.getTime());
            } else {
                showSnackbar(R.string.error_period_date_1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showAccountNameSelectorDialog(List<Account> accounts, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onAccountSelected(accounts.get(selected)));
    }

    @Override
    public void showTypeOperationSelectorDialog(List<BusinessConstants.OperationGroupType> typeOperationList, List<BusinessConstants.OperationGroupType> checkedTypeOperationList) {
        UiUtils.showCheckBoxPickerDialog(getActivity(), R.string.fragment_operations_all_name_account, typeOperationList, checkedTypeOperationList,
                selected -> presenter.onOperationTypesSelected(selected));
    }

    @Override
    public void showContragentSelectorDialog(List<Contractor> contractorList, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), contractorList, selectedIndex,
                selected -> presenter.onContractorSelected(contractorList.get(selected)));
    }

    @Override
    public void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, List<CurrencyUtils.Currency> checkedCurrencies) {
        UiUtils.showCheckBoxPickerDialog(getActivity(), R.string.fragment_operations_all_currency, currencies, checkedCurrencies,
                selected -> presenter.onCurrencySelected(selected));
    }

    @Override
    public void showDefaultData(Account account, Long dateStart, Long dateEnd, Contractor contractor, String operationTypes, String currencyList) {
        ivPeriod.setText(AppConstants.SDF_ddMMyyyy.format(new Date(dateStart)) + " - " + AppConstants.SDF_ddMMyyyy.format(new Date(dateEnd)));
        ivContragent.setText(contractor != null ? contractor.toString() : getString(R.string.fragment_operations_all_contragent));
        ivAccountName.setText(account != null ? account.toString() : getString(R.string.fragment_operations_all_name_account));
        ivOperationTypes.setText(operationTypes);
        ivCurrency.setText(currencyList);
    }

    @Override
    public void setAccountText(String accountName) {
        ivAccountName.setText(accountName);
    }

    @Override
    public void setContractorText(String contractorName) {
        ivContragent.setText(contractorName);
    }

    @Override
    public void setOperationTypesText(String operationTypes) {
        ivOperationTypes.setText(operationTypes);
    }

    @Override
    public void setCurrencyText(String currency) {
        ivCurrency.setText(currency);
    }

    @Override
    public void showPeriodDialog() {
        datePickerDialog.show(getActivity().getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack(filterHidden);
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag(DatePickerDialog.class.getSimpleName());
        if (dpd != null) dpd.setOnDateSetListener(this);

        presenter.initDefaultData();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).showToolbarBtn(R.drawable.filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity) getActivity()).hideToolbarBtn();
        presenter.unsubscribe();
    }

    @Subscribe
    public void onEvent(ToolbarBtnOnClickEvent filterToggleEvent) {
        llFilter.setVisibility(View.VISIBLE);
        showFilter(isFilterHidden());
    }

    @Override
    public void eventSelected(long operationId, boolean isNewOperation) {
        if (!isFilterHidden()) {
            if (isNewOperation) {
                int countUnreadMessages = ((MainActivity) getActivity()).getCountUnreadMessages();
                ((MainActivity) getActivity()).showUnreadIndicator(--countUnreadMessages);
            }
            presenter.onClickOperation(operationId);
        }
    }
}
