package com.rusoft.chequepay.ui.mainscreen.fragments.more.docs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.document.response.Document;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

public class DocsAdapter extends RecyclerView.Adapter<DocsAdapter.DocsViewHolder> {

    public interface ItemClickListener {
        void onDocSelected(long docId);
    }

    private Context context;
    private ItemClickListener callback;
    private List<Document> docs;

    public DocsAdapter(Context context, ItemClickListener callback, List<Document> docs) {
        this.context = context;
        this.callback = callback;
        this.docs = docs;
    }

    @Override
    public DocsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_doc_item, parent, false);
        return new DocsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DocsViewHolder holder, int position) {
        Document doc = docs.get(position);
        holder.text.setText(BusinessConstants.DocType.getTextByVal(context, doc.getDocumentType()) + " №" + doc.getDocumentNumber());
        holder.date.setText(AppConstants.SDF_ddMMyyyy_HHmm.format(doc.getDocTime()));

        if (position == docs.size() - 1) {
            holder.line.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return docs.size();
    }

    class DocsViewHolder extends RecyclerView.ViewHolder {
        TextView text, date;
        View line;

        public DocsViewHolder(View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text);
            date = itemView.findViewById(R.id.date);
            line = itemView.findViewById(R.id.line);

            itemView.setOnClickListener(v -> callback.onDocSelected(docs.get(getAdapterPosition()).getDocId()));
            itemView.setOnTouchListener(UiUtils.getTouchListener());
        }
    }
}
