package com.rusoft.chequepay.ui.mainscreen.fragments.more.profile;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ProfileModule.class, ChequepayApiModule.class})
public interface ProfileComponent {
    void inject(ProfileFragment fragment);
}
