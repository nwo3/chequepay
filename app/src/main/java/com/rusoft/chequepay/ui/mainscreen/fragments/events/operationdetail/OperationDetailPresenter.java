package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail;

import android.content.Context;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class OperationDetailPresenter extends BasePresenter implements OperationDetailContract.IPresenter {

    @Inject
    OperationRepository operationRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    ContractorRepository contractorRepository;

    private WeakReference<OperationDetailContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;
    private ItemOperationResponse itemOperationResponseUpdated;
    private DetailOperationModel detailOperationModel;


    @Inject
    public OperationDetailPresenter(OperationDetailContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated() {
        addRepositories(operationRepository, userRepository, contractorRepository);
        detailOperationModel = operationRepository.getDetailOperationModel();
    }

    @Override
    public void addCorrespondent() {
        if (itemOperationResponseUpdated != null) {
            if (userRepository.getUser().getWalletId() != itemOperationResponseUpdated.getSenderId()) {
                newContact(itemOperationResponseUpdated.getSenderName(), itemOperationResponseUpdated.getSenderId(), itemOperationResponseUpdated.getSenderAccount());
            } else {
                newContact(itemOperationResponseUpdated.getReceiverName(), itemOperationResponseUpdated.getReceiverId(), itemOperationResponseUpdated.getReceiverAccount());
            }
        }
    }

    @Override
    public void getOperationInfo() {
        operationRepository.getOperation(detailOperationModel.getOperationId(), new RequestCallbackListener() {

            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showSnackbar(R.string.error_operation_empty);
            }

            @Override
            public void onSuccess(ItemOperationResponse itemOperationResponse, Operation operation) {
                itemOperationResponseUpdated = itemOperationResponse;
                updateOperationShortList(detailOperationModel.getOperationId());
                updateOperationFilteredList(detailOperationModel.getOperationId());

                boolean isHideAddBtn = true;
                boolean isHidePdfLoadBtn = true;

                List<Long> correspondentList = new ArrayList<>();
                for (Correspondent correspondent : userRepository.getUser().getCorrespondents()) {
                    correspondentList.add(correspondent.getWalletId());
                }
                if (userRepository.getUser().getWalletId() != itemOperationResponse.getSenderId()) {
                    isHideAddBtn = correspondentList.contains(itemOperationResponse.getSenderId());
                } else {
                    isHideAddBtn = correspondentList.contains(itemOperationResponse.getReceiverId());
                }
                if (itemOperationResponse.getDocument() != null) isHidePdfLoadBtn = false;

                view.get().showOperationInfo(itemOperationResponse, operation, isHideAddBtn, isHidePdfLoadBtn);
            }
        });
    }

    @Override
    public void downloadPdf() {
        if (itemOperationResponseUpdated.getDocument() != null) {
            localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().navigateTo(Screens.PDF_FRAGMENT, itemOperationResponseUpdated.getDocument().getIdDownload());
        }
    }

    private void updateOperationShortList(long operationId) {
        List<Operation> operationShortList = userRepository.getUser().getShortOperations();
        for (int i = 0; i < operationShortList.size();i++) {
            Operation item = operationShortList.get(i);
            if (item.getOperationId() == operationId && item.getNewOperation()) {
                userRepository.getUser().getShortOperations().get(i).setNewOperation(false);
                break;
            }
        }
    }

    private void updateOperationFilteredList(long operationId) {
        List<Operation> operationFilteredList = userRepository.getUser().getFilteredOperations();
        for (int i = 0; i < operationFilteredList.size();i++) {
            Operation item = operationFilteredList.get(i);
            if (item.getOperationId() == operationId && item.getNewOperation()) {
                userRepository.getUser().getFilteredOperations().get(i).setNewOperation(false);
                break;
            }
        }
    }

    private void newContact(String contactName, Long walletId, String numberAccount) {
        CorrespondentModel correspondentModel = contractorRepository.getCorrespondentModel();
        correspondentModel.setCorrespondentName(CommonUtils.splitContragent(contactName));
        correspondentModel.setAccountNumber(String.valueOf(walletId));
        List<Account> accountList = new ArrayList<>();
        accountList.add(new Account(appContext.getString(R.string.account), numberAccount));
        correspondentModel.setAccounts(accountList);
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().navigateTo(Screens.ADD_CONTACT_FRAGMENT, null);
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
