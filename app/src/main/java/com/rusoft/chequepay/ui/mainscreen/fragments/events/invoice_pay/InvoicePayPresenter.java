package com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay;

import android.content.Context;
import android.os.Handler;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.entities.application.invoice_3.response.InvoiceItemResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.data.repository.transactions.invoice.InvoiceRepository;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransfersRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class InvoicePayPresenter extends BasePresenter implements InvoicePayContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    TransfersRepository transfersRepository;

    @Inject
    InvoiceRepository invoiceRepository;

    @Inject
    ContractorRepository contractorRepository;

    private WeakReference<InvoicePayContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;
    private TransferModel model;

    @Inject
    public InvoicePayPresenter(InvoicePayContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, transfersRepository);
        this.model = transfersRepository.getModel();

        if (data instanceof Application) {
            model.applyShortInvoiceData((Application) data);
        }

        UserModel user = userRepository.getUser();
        onAccountSelected(user.getOldestAccount());

        updateViewControlsState();
        recoverViewData();
        updateCommission();
        getInvoiceDetail();
    }

    private void updateUserAccounts() {
        userRepository.updateAccounts(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onClickAccount() {
        List<Account> accounts = userRepository.getUser().getAccounts();
        if (accounts != null && accounts.size() > 0) {
            int selectedIndex = accounts.indexOf(model.getSenderAccount());
            CurrencyUtils.Currency currency = CurrencyUtils.getCurrency(model.getCurrency());
            view.get().showAccountSelectorDialog(accounts, selectedIndex, currency != null ? currency.getCode() : null);
        } else {
            view.get().showAddNewAccountDialog();
        }
    }

    @Override
    public void onAccountSelected(Account account) {
        if (account == null) {
            return;
        }

        model.setSenderAccount(account);
        view.get().setAccountText(account.toString());
        updateViewControlsState();
        updateCommission();
    }

    private void updateCommission() {
        transfersRepository.getCommission(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
                model.setCommission(null);
                updateModelAndViewNumericData();
                updateViewControlsState();
            }

            @Override
            public void onSuccess() {
                updateModelAndViewNumericData();
                updateViewControlsState();
            }
        });
    }

    private void getInvoiceDetail() {
        Application data = model.getShortInvoiceData();
        invoiceRepository.getInvoiceDetail(data.getApplicationCoreId(), data.getApplicationType(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(BaseResponse baseResponse) {
                model.applyFullInvoiceData((InvoiceItemResponse) baseResponse);
                recoverViewData();
            }
        });
    }

    private void updateModelAndViewNumericData() {
        if (view == null) return;

        boolean isUpdateSuccess = model.updateNumericData();
        view.get().setAmountText(isUpdateSuccess ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setAmountTextColor(model.getAmount() > 0 ? R.color.bright_turquoise_text : R.color.white);
        view.get().setCommissionText(isUpdateSuccess ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setCurrencyText(isUpdateSuccess ? CurrencyUtils.getName(model.getCurrency()) : null);
        view.get().setTotalText(isUpdateSuccess ? CurrencyUtils.format(model.getTotal(), model.getCurrency()) : null);
    }

    @Override
    public void onClickProceed() {
        Account account = model.getSenderAccount();
        if (account.getAmount() < model.getAmount()) {
            view.get().showDepositDialog(account);
            return;
        }

        transfersRepository.sendInvoiceTransfer(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    @Override
    public void onClickAddContragent() {
        CorrespondentModel correspondentModel = contractorRepository.getCorrespondentModel();
        correspondentModel.setCorrespondentName(CommonUtils.splitContragent(model.getFullInvoiceData().getSenderName()));
        correspondentModel.setAccountNumber(String.valueOf(model.getFullInvoiceData().getSenderId()));
        List<com.rusoft.chequepay.data.entities.correspondents.request.Account> accountList = new ArrayList<>();
        accountList.add(new com.rusoft.chequepay.data.entities.correspondents.request.Account(appContext.getString(R.string.account), model.getReceiverAccountId()));
        correspondentModel.setAccounts(accountList);
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().navigateTo(Screens.ADD_CONTACT_FRAGMENT, null);
    }

    @Override
    public void onRefuseConfirmed() {
        Application data = model.getShortInvoiceData();
        invoiceRepository.refuseInvoice(data.getApplicationCoreId(), data.getApplicationType(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                goBack();
            }
        });
    }

    private void updateViewControlsState() {
        if (view == null) return;
        view.get().disableProceedButton(!model.isAllDataReceived());
    }

    private void recoverViewData() {
        if (view == null) return;

        Application data = model.getShortInvoiceData();
        InvoiceItemResponse fullData = model.getFullInvoiceData();

        view.get().setAccountText(model.getSenderAccount() != null ? model.getSenderAccount().toString() : null);

        if (data != null) {
            view.get().setDateTimeText(data.getDate() != null ? AppConstants.SDF_ddMMyyyy_HHmm.format(data.date) : null);
            view.get().setConteragentText(data.getSenderName() != null ? CommonUtils.splitContragent(data.getSenderName()) : null);
            view.get().setConteragentAccountText(data.getAccountNumber());
            view.get().setAmountText(CurrencyUtils.format(data.getAmount(), data.getCurrency()));
            view.get().setAmountTextColor(data.getAmount() > 0 ? R.color.bright_turquoise_text : R.color.white);
            view.get().setCommentText(data.getComment());
            view.get().setStatusText(data.stateName);

            List<Long> correspondentList = new ArrayList<>();
            for (Correspondent correspondent : userRepository.getUser().getCorrespondents()) {
                correspondentList.add(correspondent.getWalletId());
            }

            view.get().showAddContragentButton(!correspondentList.contains(data.getSenderId()));
        }

        if (fullData != null) {
            view.get().setValidityText(fullData.getLifetime().toString());
            List<Integer> statusAvailable = fullData.getStatusAvailable();
            view.get().hideProceedButton(statusAvailable == null || !statusAvailable.contains(3));
            view.get().hideRefuseButton(statusAvailable == null || !statusAvailable.contains(2));
            view.get().showDownloadPdfButton(fullData.documents != null && !fullData.documents.isEmpty());
        }
    }


    @Override
    public void goToNewAccount() {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter()
                .navigateTo(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void goToDeposit(Account account) {
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter()
                .navigateTo(Screens.MONEY_DEPOSIT_FRAGMENT, account);
    }

    @Override
    public void goToPdfViewer() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().navigateTo(Screens.PDF_FRAGMENT,
                model.getFullInvoiceData().getDocuments().get(0).getIdDownload());
    }

    @Override
    public void clearModel() {
        transfersRepository.clearModel();
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model.isTransactionDone()) {
            new Handler().postDelayed(this::goBack, AppConstants.DURATION_FAST);
        } else {
            updateUserAccounts();
            model.clearFlowToken();
            recoverViewData();
        }
    }

    private void goBack() {
        transfersRepository.clearModel();
        transfersRepository.clearDisposable();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
