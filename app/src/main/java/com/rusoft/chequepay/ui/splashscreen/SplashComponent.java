package com.rusoft.chequepay.ui.splashscreen;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {SplashModule.class, ChequepayApiModule.class})
public interface SplashComponent {
    void inject(SplashActivity splashActivity);
}
