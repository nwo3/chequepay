package com.rusoft.chequepay.ui.registration.fragment.individual.bank;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegBankModule {
    private final RegBankContract.IView view;

    public RegBankModule(RegBankContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegBankContract.IView provideView() {
        return view;
    }
}
