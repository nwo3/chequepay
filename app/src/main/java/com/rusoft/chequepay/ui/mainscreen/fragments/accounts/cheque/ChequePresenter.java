package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.cheque;

import android.content.Context;
import android.support.annotation.NonNull;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.cheques.Cheque;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.Amount2Words;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class ChequePresenter extends BasePresenter implements ChequeContract.IPresenter {

    @Inject
    UserRepository userRepository;

    private WeakReference<ChequeContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;

    @Inject
    public ChequePresenter(ChequeContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository);

        if (data instanceof Account){
            UserModel user = userRepository.getUser();
            String accountNumber = ((Account) data).getAccountNumber();
            Account account = user.getAccountByNum(accountNumber);
            Cheque cheque = user.getCheque(accountNumber);

            if (account != null && cheque != null) {
                view.get().setChequeIdText("#" + account.getChequeId());
                view.get().setIssuerAddressText(cheque.getIssuerInfoText());
                view.get().setIssuerRequisitesText(getRequisitesText(cheque));
                view.get().setPayerText(cheque.getBankFullName());
                view.get().setAmountWordRepresentationText(getAmountText(account));
                view.get().setBankAccountText(cheque.getBankAccount());
                view.get().setSignText(getSignText(cheque));
                view.get().setEmailText(cheque.getIssuerEmail());
                view.get().setPhoneText(cheque.getIssuerPhone());
            } else {
                throw new RuntimeException(ErrorUtils.ACCOUNT_NOT_FOUND);
            }
        } else {
            throw new IllegalArgumentException(ErrorUtils.WRONG_DATA_TYPE);
        }
    }

    @NonNull
    private String getRequisitesText(Cheque cheque) {
        StringBuilder sCheque = new StringBuilder();
        if (cheque.getIssuerOrgn() != null) sCheque.append(appContext.getString(R.string.ogrn_arg, cheque.getIssuerOrgn()) + "\n");
        if (cheque.getIssuerTaxNumber() != null) sCheque.append(appContext.getString(R.string.inn_arg, cheque.getIssuerTaxNumber()) + "\n");
        if (cheque.getIssuerKpp() != null) sCheque.append(appContext.getString(R.string.kpp_arg, cheque.getIssuerKpp()));
        return sCheque.toString();
    }

    @NonNull
    private String getAmountText(Account account) {
        Double amount = account.getAmount();
        Integer currency = account.getCurrency();
        return appContext.getString(R.string.fragment_cheque_amount_text,
                "<br/><b>" + CurrencyUtils.format(amount, currency) + "</b>",
                "<b>(" + Amount2Words.num2words(amount, CurrencyUtils.getCurrency(currency)) + ")</b><br/> ");
    }

    @NonNull
    private String getSignText(Cheque cheque) {
        return "<b>" + appContext.getString(R.string.fragment_cheque_sign_text,
                "</b><br/>" + cheque.getIssuerDirectorPost() + "<br/>",
                "<b>" + cheque.getIssuerDirectorName() + "</b><br/>")
                + "<i>" + appContext.getString(R.string.fragment_cheque_sign_text_2) + "</i>";
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        //ignore
    }

    private void goBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
