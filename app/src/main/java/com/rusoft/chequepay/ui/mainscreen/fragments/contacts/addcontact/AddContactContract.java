package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact;

import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface AddContactContract {

    interface IView extends IBaseView {
        void addAccount(String accountName, String accountNumber);
        void updateProceedButtonState();
        void updateFields(String comment, List<Account> accounts);
        void updateLockedFields(String name, String walletId, String inn);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void saveContractor();
        void saveFieldData(String nameContractor, String numberClient, String inn, String comment, List<Account> accounts);
        void clearFieldData();
        void startAccountList(String numberAccount);
        void onUpdateFields();
    }
}
