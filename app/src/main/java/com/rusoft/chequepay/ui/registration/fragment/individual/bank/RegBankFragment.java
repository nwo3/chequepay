package com.rusoft.chequepay.ui.registration.fragment.individual.bank;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.OnTextChangedWatcher;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.BankSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.ui.registration.fragment.base.BaseRegFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;

public class RegBankFragment extends BaseRegFragment implements RegBankContract.IView {

    @Inject
    RegBankPresenter presenter;

    @BindView(R.id.bik) DynamicInputView bik;
    @BindView(R.id.bank_name) DynamicInputView bank_name;
    @BindView(R.id.cor_acc) DynamicInputView cor_acc;
    @BindView(R.id.swift) DynamicInputView swift;
    @BindView(R.id.bank_address) DynamicInputView bank_address;
    @BindView(R.id.bank_req) DynamicInputView bank_req;
    @BindView(R.id.account_num) DynamicInputView account_num;
    @BindView(R.id.bank_inn) DynamicInputView bank_inn;
    @BindView(R.id.bank_kpp) DynamicInputView bank_kpp;
    @BindView(R.id.receiver) DynamicInputView receiver;
    @BindView(R.id.focus_handler) View focus_handler;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_bank;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.bank_req);
    }

    public static RegBankFragment getNewInstance() {
        return new RegBankFragment();
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegBankComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regBankModule(new RegBankModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ((RegistrationActivity) getActivity()).scrollToStart();

        bik.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bik != null && bik.validate())
                presenter.onBikFocusLost(bik.validate() ? bik.getText() : null);
        });

        bank_name.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bank_name != null && bank_name.validate())
                presenter.onBankNameFocusLost(bank_name.validate() ? bank_name.getText() : null);
        });

        cor_acc.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (cor_acc != null && cor_acc.validate())
                presenter.onCorAccFocusLost(cor_acc.validate() ? cor_acc.getText() : null);
        });

        swift.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (swift != null && swift.validate())
                presenter.onSwiftFocusLost(swift.validate() ? swift.getText() : null);
        });

        bank_address.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bank_address != null && bank_address.validate())
                presenter.onBankAddressFocusLost(bank_address.validate() ? bank_address.getText() : null);
        });

        bank_req.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bank_req != null && bank_req.validate())
                presenter.onBankReqFocusLost(bank_req.validate() ? bank_req.getText() : null);
        });

        account_num.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (account_num != null && account_num.validate())
                presenter.onAccountNumFocusLost(account_num.validate() ? account_num.getText() : null);
        });

        bank_inn.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bank_inn != null && bank_inn.validate())
                presenter.onBankInnFocusLost(bank_inn.validate() ? bank_inn.getText() : null);
        });

        bank_kpp.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (bank_kpp != null && bank_kpp.validate())
                presenter.onBankKppFocusLost(bank_kpp.validate() ? bank_kpp.getText() : null);
        });

        receiver.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (receiver != null && receiver.validate())
                presenter.onReceiverFocusLost(receiver.validate() ? receiver.getText() : null);
        });

        bank_inn.setValidationPattern(ValidationPatterns.ENTITY_INN);
        swift.setValidationPattern(ValidationPatterns.SWIFT);
        bank_req.setValidationPattern(ValidationPatterns.BANK_INFO);
        bank_address.setValidationPattern(ValidationPatterns.BANK_INFO);
        cor_acc.setValidationPattern(ValidationPatterns.CORR_ACC);
        bank_inn.setValidationPattern(ValidationPatterns.BANK_INN);
        bank_kpp.setValidationPattern(ValidationPatterns.BANK_KPP);
        receiver.setValidationPattern(ValidationPatterns.BANK_OWNER);
    }

    @Override
    public void updateViewByResidency(boolean isResident) {
        //Resident
        cor_acc.setEnabled(false);
        cor_acc.setOptional(!isResident);
        cor_acc.setVisibility(isResident ? View.VISIBLE : View.GONE);
        bank_inn.setOptional(!isResident);
        bank_inn.setVisibility(isResident ? View.VISIBLE : View.GONE);
        bank_kpp.setOptional(!isResident);
        bank_kpp.setVisibility(isResident ? View.VISIBLE : View.GONE);

        //Not a Resident
        swift.setOptional(!isResident);
        swift.setVisibility(isResident ? View.GONE : View.VISIBLE);
        bank_address.setOptional(isResident);
        bank_address.setVisibility(isResident ? View.GONE : View.VISIBLE);
        bank_req.setOptional(true);
        bank_req.setVisibility(isResident ? View.GONE : View.VISIBLE);

        //Common
        bik.setOptional(!isResident);
        bik.setTitle(getString(isResident ? R.string.bik_1 : R.string.bik_2));
        bank_name.setEnabled(!isResident);
        bank_name.setOptional(false);
        account_num.setOptional(false);
        receiver.setOptional(false);

        //Validation
        bik.setValidationPattern(isResident ? ValidationPatterns.BIC_RUS : ValidationPatterns.BIC_OTHER);
        bank_name.setValidationPattern(ValidationPatterns.EMPTY_PATTERN);
        account_num.setValidationPattern(isResident ? ValidationPatterns.BANK_ACCOUNT_RUS : ValidationPatterns.BANK_ACCOUNT_OTHER);
    }

    @Override
    public void restoreView(BankSubModel model) {
        bik.setText(model.getBik());
        bank_name.setText(model.getName());
        cor_acc.setText(model.getCorrAcc());
        swift.setText(model.getSwift());
        bank_address.setText(model.getAddress());
        bank_req.setText(model.getAdditionalInfo());
        account_num.setText(model.getAccNum());
        bank_inn.setText(model.getInn());
        bank_kpp.setText(model.getKpp());
        receiver.setText(model.getReceiver());

        bank_name.setEnabled(!model.isBikFound());
        cor_acc.setEnabled(!model.isBikFound());
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_PROFILE_EDIT);
        startActivity(intent);
    }

    @Override
    public void exit() {
        getActivity().finish();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        focus_handler.requestFocus();

        if (UiUtils.validateInputViews(bik, bank_name, cor_acc, swift, bank_address,
                bank_req, account_num, bank_inn, bank_kpp, receiver)){

            presenter.onClickNext();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public String getContainerName() {
        return Screens.REGISTRATION_CONTAINER;
    }
}
