package com.rusoft.chequepay.ui.authscreen;


import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {AuthScreenModule.class, ChequepayApiModule.class})
public interface AuthScreenComponent {
    void inject(AuthActivity activity);
}
