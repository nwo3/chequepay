package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ChoiceAccountModule.class, ChequepayApiModule.class})
public interface ChoiceAccountComponent {
    void inject(ChoiceAccountFragment fragment);
}
