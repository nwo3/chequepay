package com.rusoft.chequepay.ui.pinscreen;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.fingerprint.FingerprintManager;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class PinScreenPresenter extends BasePresenter implements PinScreenContract.IPresenter {

    @Inject
    UserRepository userRepository;

    private WeakReference<PinScreenContract.IView> view;
    private Context appContext;

    private int pinAttempts;
    private String pinToConfirm = "";
    private FingerprintManager fingerprintManager;

    @Inject
    public PinScreenPresenter(PinScreenContract.IView view, UserRepository userRepository, Context appContext) {
        this.view = new WeakReference<>(view);
        this.userRepository = userRepository;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && CommonUtils.checkFingerprintCompatibility(appContext)) {
            fingerprintManager = (FingerprintManager) appContext.getSystemService(Activity.FINGERPRINT_SERVICE);
        }

        if (userRepository.isPinAlreadySet()) view.get().setTitleText(R.string.enter_code);
        else view.get().setTitleText(R.string.set_code);

        view.get().enableFingerprint(isFingerprintAuthEnabled());
    }

    @Override
    public void onPinEntered(String code) {
        if (userRepository.isPinAlreadySet()) {
            if (userRepository.getPinCode().equals(code)) {
                view.get().startUserDataReceiving();
            } else {
                pinAttempts++;
                view.get().onWrongPin();
            }

            if (pinAttempts == BusinessConstants.PIN_ATTEMPTS_COUNT) {
                pinAttempts = 0;
                UiUtils.showQuestionDialog((Context) view.get(),
                        R.string.dialog_on_pin_attempts_exceeded, null,
                        R.string.cancel, R.string.reset,
                        true, (dialog, i) -> {
                            switch (i) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    logout();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        });
            }

        } else {
            //Установка пинкода
            if (pinToConfirm.isEmpty()) {
                pinToConfirm = code;
                view.get().clearPin();
                view.get().setTitleText(R.string.confirm_code);

            } else {
                //Подтверждение пинкода
                if (code.equals(pinToConfirm)) {
                    userRepository.storePinCode(code);

                    if (checkFingerprintPossibility() && !userRepository.isFingerprintAuthenticationEnabled() && !userRepository.IsFingerprintAuthenticationDialogShown()) {
                        view.get().askToEnableFingerprintAuthentication();
                    } else {
                        view.get().startUserDataReceiving();
                    }

                } else {
                    view.get().onWrongPin();
                    view.get().setTitleText(R.string.set_code);
                    view.get().showSnackbar(R.string.snackbar_pin_confirmation_error);
                    pinToConfirm = "";
                }
            }
        }
    }

    @Override
    public void onFingerprintAuthenticationSuccess() {
        userRepository.setAuthenticatedByFingerprint(true);
        view.get().startUserDataReceiving();
    }

    @Override
    public void enableFingerprintAuthentication(boolean enable) {
        userRepository.setIsFingerprintAuthenticationDialogShown(true);
        userRepository.setFingerprintAuthenticationEnabled(enable);
        view.get().enableFingerprint(enable);
    }

    private boolean checkFingerprintPossibility() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && CommonUtils.checkFingerprintCompatibility(appContext)
                && fingerprintManager != null && fingerprintManager.hasEnrolledFingerprints();
    }

    private boolean isFingerprintAuthEnabled() {
        return checkFingerprintPossibility() && userRepository.isFingerprintAuthenticationEnabled();
    }

    @Override
    public void onTokenExpired() {
        logout();
    }

    private void logout() {
        userRepository.logout();
        view.get().goToAuth();
    }

    @Override
    public void onClickHelp() {
        view.get().goToHelp();
    }

    @Override
    public void onBackPressed() {
        UiUtils.showOnExitDialog((Context) view.get());
    }
}
