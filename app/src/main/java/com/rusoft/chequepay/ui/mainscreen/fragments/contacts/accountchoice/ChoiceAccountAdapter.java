package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

public class ChoiceAccountAdapter extends RecyclerView.Adapter<ChoiceAccountAdapter.OperationListViewHolder> {

    interface IAccountsAdapter {
        void onClickItem(Operation operation, Integer position);
    }

    private IAccountsAdapter callback;
    private List<Operation> operations;

    public ChoiceAccountAdapter(IAccountsAdapter callback, List<Operation> operations) {
        this.callback = callback;
        this.operations = operations;
    }

    @Override
    public OperationListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_accounts_item_addcontact, parent, false);
        return new OperationListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OperationListViewHolder holder, int position) {
        Operation operation = operations.get(position);
        holder.tvContractorName.setText(CommonUtils.splitContragent(operation.getContractor()));
        holder.tvAccountName.setText(operation.getContractorAccount());
        holder.indicator.setVisibility(operation.getIsSelectedAccount() ? View.VISIBLE : View.INVISIBLE);

        ImageView iv = holder.ivIcon;
        iv.setImageResource(CurrencyUtils.getCurrencyDrawableFromAccNumber(operation.getContractorAccount()));
    }

    @Override
    public int getItemCount() {
        return operations.size();
    }

    class OperationListViewHolder extends RecyclerView.ViewHolder {
        TextView tvContractorName, tvAccountName;
        ImageView ivIcon;
        View indicator;
        public OperationListViewHolder(View itemView) {
            super(itemView);
            tvContractorName = itemView.findViewById(R.id.title);
            tvAccountName = itemView.findViewById(R.id.account);
            indicator = itemView.findViewById(R.id.indicator);
            ivIcon = itemView.findViewById(R.id.icon);
            itemView.setOnTouchListener(UiUtils.getTouchListener());
            itemView.setOnClickListener(v -> {
                callback.onClickItem(operations.get(getAdapterPosition()), getAdapterPosition());
            });
        }
    }
}
