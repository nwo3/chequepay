package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


public class ContactsFragment extends BaseRootFragment implements ContactsContract.IView, ContactsAdapter.IContactsAdapter {

    @Inject
    ContactsPresenter presenter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.add_counteragent) View add;
    @BindView(R.id.recyclerContacts) RecyclerView recyclerViewContacts;

    private List<Correspondent> correspondentList = new ArrayList<>();
    private ContactsAdapter contactsAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_contacts;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_contacts);
    }

    public static ContactsFragment getNewInstance(String name) {
        ContactsFragment contactsFragment = new ContactsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.EXTRA_NAME, name);
        contactsFragment.setArguments(arguments);

        return contactsFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerContactsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .contactsModule(new ContactsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        contactsAdapter = new ContactsAdapter(this, correspondentList);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewContacts.setLayoutManager(llm);
        recyclerViewContacts.setAdapter(contactsAdapter);

        add.setOnClickListener(v -> presenter.onClickAddContact());
        add.setOnTouchListener(UiUtils.getTouchListener());

        presenter.getContractorsList();

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
    }

    @Override
    public void showContractorsList(List<Correspondent> contractors) {
        correspondentList.clear();
        correspondentList.addAll(contractors);
        contactsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickItem(long contractorId) {
        presenter.onClickContact(contractorId);
    }

    @Override
    public void showEmptyContractorsList() {
        correspondentList.clear();
        contactsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        super.showLoadingIndicator(enabled);
        if (!enabled) swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

}
