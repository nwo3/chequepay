package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface DepositContract {

    interface IView extends IBaseView {
        void setAccountText(String text);
        void setIssuerText(String text);
        void setBankText(String text);
        void setModeText(Integer textResId);
        void setCurrencyText(String text);
        void setAmountText(String text);
        void setCommissionText(String text);
        void setTotalText(String text);

        void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident);
        void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex);
        void showBankSelectorDialog(List<Bank> banks, int selectedIndex);
        void showModeSelectorDialog(List<BusinessConstants.RefundMode> modes, int selectedIndex);

        void disableAccountInput(boolean disabled);
        void disableIssuerInput(boolean disabled);
        void disableBankInput(boolean disabled);
        void disableModeInput(boolean disabled);
        void disableAmountInput(boolean disabled);
        void disableProceedButton(boolean disabled);

        void showAddNewAccountDialog();

        String getContainerName();

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {

        void onViewCreated(Serializable data);

        void onClickAccount();
        void onAccountSelected(Account account);

        void onClickIssuer();
        void onIssuerSelected(Issuer issuer);

        void onClickBank();
        void onBankSelected(Bank bank);

        void onClickMode();
        void onModeSelected(BusinessConstants.RefundMode mode);

        void onAmountFocusLost(String inputData);

        void goToNewAccount();

        void onClickProceed();

    }
}
