package com.rusoft.chequepay.ui.otpscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class OtpScreenModule {
    private final OtpScreenContract.IView view;

    public OtpScreenModule(OtpScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    OtpScreenContract.IView provideView() {
        return view;
    }
}
