package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.invoice;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class InvoiceModule {
    private final InvoiceContract.IView view;

    public InvoiceModule(InvoiceContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    InvoiceContract.IView provideInvoiceView() {
        return view;
    }
}
