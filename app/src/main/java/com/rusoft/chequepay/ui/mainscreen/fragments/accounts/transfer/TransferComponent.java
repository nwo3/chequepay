package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {TransferModule.class, ChequepayApiModule.class})
public interface TransferComponent {
    void inject(TransferFragment fragment);
}
