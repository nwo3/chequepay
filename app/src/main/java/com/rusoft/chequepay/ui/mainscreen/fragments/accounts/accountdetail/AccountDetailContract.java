package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetail;

import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;
import java.util.List;

public interface AccountDetailContract {

    interface IView extends IBaseView{
        void showOperationsTab();
        void showExcerptTab(List<Operation> operations);
        void setDetailsData(String title, String number, String amount);

        void enableDeposit(boolean enabled);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(Serializable data);

        void onClickOperationsTab();
        void onClickExcerptTab();

        void onClickDetails();
        void onClickDeposit();
        void onClickWithdraw();
        void onClickTransfer();
        void onClickInvoice();
        void onClickCheque();
        void onAccountCloseConfirmed();

        void onClickOperation(long operationId);
    }
}
