package com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback;

import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class FeedbackPresenter extends BasePresenter implements FeedbackContract.IPresenter {

    private WeakReference<FeedbackContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public FeedbackPresenter(FeedbackContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
