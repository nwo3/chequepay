package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface AccountsContract {

    interface IView extends IBaseView {
        void updateTotalBalanceSection(double rubAmount, double usdAmount, double eurAmount);
        void updateAccountsRecyclerView(List<Account> accounts);
        void showRefillAccountsDialog();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void onRefresh();

        void onClickDeposit();
        void onClickTransfer();
        void onClickWithdraw();
        void onClickInvoice();

        void onClickNewAccount();

        void onAccountSelected(Account account);

    }
}
