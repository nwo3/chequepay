package com.rusoft.chequepay.ui.registration.fragment.individual.personal;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.PersonSubModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class RegPersonalPresenter extends BasePresenter implements RegPersonalContract.IPresenter {

    @Inject
    RegistrationRepository repository;

    @Inject
    UserRepository userRepository;

    private WeakReference<RegPersonalContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private PersonSubModel subModel;

    @Inject
    public RegPersonalPresenter(RegPersonalContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated() {
        model = repository.getModel();
        subModel = model.person;

        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        view.get().updateViewByResidency(model.isResident());
        view.get().updateScreenIfInProfileMode(model.isInProfileEditingMode());
    }

    @Override
    public void onLastNameFocusLost(String text) {
        subModel.setLastName(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onFirstNameFocusLost(String text) {
        subModel.setFirstName(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onPatronymicFocusLost(String text) {
        subModel.setPatronymic(!text.isEmpty() ? text : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onBirthDateSelected(Calendar calendar) {
        subModel.setBirthDate(validateBirthDate(calendar) ? calendar : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    private boolean validateBirthDate(Calendar birthDate){
        boolean isValid = birthDate != null && CommonUtils.getAge(birthDate) >= BusinessConstants.MIN_AGE;
        if (!isValid) view.get().setErrorOnBirthDateSelector(R.string.error_age_under_18);
        return isValid;
    }

    @Override
    public void onInnFocusLost(String text) {
        subModel.setInn(!text.isEmpty() ? text : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onPhoneFocusLost(String phone) {
        if (phone == null || phone.isEmpty()) {
            subModel.setMobPhone(null);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            return;

        } else if (model.isInProfileEditingMode() && !subModel.isPhoneChanged(phone)) {
            subModel.setMobPhone(phone);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            return;
        }

        repository.checkLoginParams(phone, null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
                view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().setErrorOnPhoneInput(R.string.error_on_phone_uniqueness_check);
                subModel.setMobPhone(null);
            }

            @Override
            public void onSuccess() {
                subModel.setMobPhone(phone);
            }
        });
    }

    @Override
    public void onEmailFocusLost(String email) {
        if (email == null || email.isEmpty()) {
            subModel.setEmail(null);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            return;

        } else if (model.isInProfileEditingMode() && !subModel.isEmailChanged(email)) {
            subModel.setEmail(email);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            return;
        }

        repository.checkLoginParams(null, email, new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showLoadingIndicator(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showLoadingIndicator(false);
                    view.get().updateNextButtonState(subModel.isRequiredDataReceived());
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().setErrorOnEmailInput(R.string.error_on_email_uniqueness_check);
                    subModel.setEmail(null);
                }

                @Override
                public void onSuccess() {
                    subModel.setEmail(email);
                }
            });
    }

    @Override
    public void onWebSiteFocusLost(String webSite) {
        subModel.setWebSite(!(webSite == null || webSite.isEmpty()) ? webSite : null);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickNext(String phone, String email) {
        if (!model.isInProfileEditingMode()) {

            if (subModel.isRequiredDataReceived()){
                router.navigateTo(Screens.REG_PASSPORT_FRAGMENT);
            } else {
                view.get().showSnackbar(R.string.error_on_check_required_data_fail);
                validateBirthDate(subModel.getBirthDate());
                onPhoneFocusLost(phone);
                onEmailFocusLost(email);
            }

        } else {
            if (!subModel.isRequiredDataReceived()) {
                return;
            }

            repository.editProfile(null, new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    view.get().goToOtp();
                }
            });
        }
    }

    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        if (model.isInProfileEditingMode() && model.isActionDone()){
            repository.clearModel();
            view.get().exit();
            return;
        }

        view.get().restoreState(subModel);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onDestroy() {
        if (model.isInProfileEditingMode()) {
            repository.clearModel();
        }
    }
}
