package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ContactsModule.class, ChequepayApiModule.class})
public interface ContactsComponent {
    void inject(ContactsFragment fragment);
}
