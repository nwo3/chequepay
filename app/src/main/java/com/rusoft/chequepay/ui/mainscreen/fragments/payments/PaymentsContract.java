package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface PaymentsContract {

    interface IView extends IBaseView {
        void updateTemplatesRecyclerView(List<ShortTemplate> templates);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreate();

        void onRefresh();

        void onClickMoneyDeposit();
        void onClickMoneyTransfer();
        void onClickMoneyWithdraw();
        void onClickInvoice();

        void onTemplateSelected(ShortTemplate template);
        void onTemplateDeleteConfirmed(ShortTemplate template);
    }
}
