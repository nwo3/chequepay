package com.rusoft.chequepay.ui.otpscreen;

import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.pinscreen.PinActivity;
import com.rusoft.chequepay.ui.successscreen.SuccessActivity;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.LogUtil;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class OtpActivity extends BaseActivity implements OtpScreenContract.IView {

    @Inject
    OtpScreenPresenter presenter;

    @BindView(R.id.scroll) ScrollView scroll;
    @BindView(R.id.otp) EditText otpBox;
    @BindView(R.id.proceed) View proceed;

    private int actionType;

    @Override
    protected int layoutResId() {
        return R.layout.activity_otp;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerOtpScreenComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .otpScreenModule(new OtpScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        actionType = getIntent().getIntExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.NO_DATA);
        setProceedEnabled(false);
        proceed.setOnTouchListener(UiUtils.getTouchListener());
        proceed.setOnClickListener(view -> {
            String otp = otpBox.getText().toString();
            if (!otp.isEmpty()){
                UiUtils.hideKeyboard(this);
                presenter.onClickProceed(actionType, otp);
            } else {
                showSnackbar(getString(R.string.error_otp_box_empty));
            }
        });

        UiUtils.requestFocusAndShowKeyboard(this, otpBox);
        otpBox.setFilters(new InputFilter[] {new InputFilter.LengthFilter(BusinessConstants.OTP_LENGTH)});
        otpBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //ignore
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                presenter.enteredOtpLengthChanged(length);

                if (length == BusinessConstants.OTP_LENGTH) {
                    UiUtils.hideKeyboard(OtpActivity.this);
                    presenter.onClickProceed(actionType, otpBox.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //ignore
            }
        });

        UiUtils.setKeyboardListener(scroll, new UiUtils.KeyboardListener() {
            @Override
            public void onKeyboardOpened() {
                //ignore
            }

            @Override
            public void onKeyboardClosed() {
                //ignore
            }
        });
    }

    @Override
    public void setProceedEnabled(boolean enabled) {
        proceed.setEnabled(enabled);
        proceed.setAlpha(enabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
    }

    @Override
    public void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void goToAuth() {
        finish();
    }

    @Override
    public void goToPin() {
        startActivity(new Intent(this, PinActivity.class));
        finishAffinity();
    }

    @Override
    public void goToSuccessScreen() {
        Intent intent = new Intent(this, SuccessActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, actionType);
        startActivity(intent);
        finish();
    }

    @Override
    public void exit() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        switch (actionType){
            case AppConstants.ACTION_TYPE_AUTH:
                goToAuth();
                break;

            case AppConstants.ACTION_TYPE_TRANSFER:
            case AppConstants.ACTION_TYPE_DEPOSIT:
            case AppConstants.ACTION_TYPE_WITHDRAWAL:
            case AppConstants.ACTION_TYPE_INVOICE:
            case AppConstants.ACTION_TYPE_INVOICE_TRANSFER:
                finish();
                break;

            case AppConstants.NO_DATA:
                LogUtil.e(ErrorUtils.INTERNAL_ERROR);
                break;
        }
    }
}
