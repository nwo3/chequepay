package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetailedinfo;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class AccountDetailedInfoFragment extends BaseBranchFragment implements AccountDetailedInfoContract.IView {

    @Inject
    AccountDetailedInfoPresenter presenter;

    @BindView(R.id.amount) TextView amount;
    @BindView(R.id.num) TextView number;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.reg_date) TextView regdate;
    @BindView(R.id.issuer) TextView issuer;
    @BindView(R.id.payer) TextView payer;
    @BindView(R.id.ll_container) LinearLayout llContainer;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_account_detailed_info;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_detailed_info);
    }

    public static AccountDetailedInfoFragment getNewInstance(String containerName, Serializable data) {
        AccountDetailedInfoFragment fragment = new AccountDetailedInfoFragment();
        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerAccountDetailedInfoComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .accountDetailedInfoModule(new AccountDetailedInfoModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));
    }

    @Override
    public void setFieldsText(String amount, String number, String name, String regdate, String issuer, String payer) {
        this.amount.setText(amount);
        this.number.setText(number);
        this.name.setText(name);
        this.regdate.setText(regdate);
        this.issuer.setText(issuer);
        this.payer.setText(payer);

        if (issuer == null) {
            llContainer.getChildAt(llContainer.indexOfChild(this.issuer) + 1).setVisibility(View.GONE);
            llContainer.getChildAt(llContainer.indexOfChild(this.issuer) - 1).setVisibility(View.GONE);
        }

        if (payer == null) {
            llContainer.getChildAt(llContainer.indexOfChild(this.payer) + 1).setVisibility(View.GONE);
            llContainer.getChildAt(llContainer.indexOfChild(this.payer) - 1).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
