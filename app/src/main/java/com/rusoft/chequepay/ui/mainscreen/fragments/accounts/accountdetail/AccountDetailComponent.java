package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetail;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {AccountDetailModule.class, ChequepayApiModule.class})
public interface AccountDetailComponent {
    void inject(AccountDetailFragment fragment);
}
