package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist;

import android.content.Context;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.clientdata.response.Contractor;
import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.FilterDataModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class OperationListPresenter extends BasePresenter implements OperationListContract.IPresenter {

    @Inject
    OperationRepository operationRepository;

    @Inject
    ContractorRepository contractorRepository;

    @Inject
    UserRepository userRepository;

    private WeakReference<OperationListContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;
    private FilterDataModel filterDataModel;
    private DetailOperationModel detailOperationModel;
    private boolean filterIsShown;

    @Inject
    public OperationListPresenter(OperationListContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated() {
        addRepositories(operationRepository, contractorRepository, userRepository);
        filterDataModel = operationRepository.getFilterDataModel();
        detailOperationModel = operationRepository.getDetailOperationModel();
    }

    @Override
    public void onClickPeriod() {
        view.get().showPeriodDialog();
    }

    @Override
    public void onClickAccountName() {
        List<com.rusoft.chequepay.data.entities.accounts.Account> accounts = userRepository.getUser().getAccounts();
        if (accounts.size() > 0) {
            int selectedIndex = 0;
            List<Account> alteredAccounts = new ArrayList<>();
            alteredAccounts.add(new Account(null, appContext.getString(R.string.fragment_operations_all_name_account),null));

            for (int i = 0; i < accounts.size(); i++) {
                alteredAccounts.add(new Account(null, accounts.get(i).getName(), accounts.get(i).getAccountNumber()));
                if (filterDataModel.getAccount() != null && filterDataModel.getAccount().getNumber().equals(accounts.get(i).getAccountNumber())) {
                    selectedIndex = i;
                    selectedIndex++;
                }
            }
            view.get().showAccountNameSelectorDialog(alteredAccounts, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_account_list_empty);
        }
    }

    @Override
    public void onClickContragent() {
        if (userRepository.getUser().getContractors().size() > 0) {
            Contractor contractor = new Contractor();
            contractor.setContractorName(appContext.getString(R.string.fragment_operations_all_contragent));
            contractor.setContractorId(0);
            List<Contractor> contractors = new ArrayList<>();
            contractors.add(contractor);
            contractors.addAll(userRepository.getUser().getContractors());

            int selectedIndex = filterDataModel.getContractorId() != null ?
                    contractors.indexOf(contractorRepository.getContractorFromList(filterDataModel.getContractorId())) : 0;
            view.get().showContragentSelectorDialog(contractors, selectedIndex);

        } else {

        }
    }

    @Override
    public void onClickTypeOperation() {
        view.get().showTypeOperationSelectorDialog(BusinessConstants.OperationGroupType.asList(), filterDataModel.getOperationTypes());
    }

    @Override
    public void onClickCurrency() {
        view.get().showCurrencySelectorDialog(CurrencyUtils.Currency.asList(), filterDataModel.getCurrency());
    }

    @Override
    public void onClickOperation(long operationId) {
        detailOperationModel.setOperationId(operationId);
        localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().navigateTo(Screens.OPERATION_DETAIL_FRAGMENT);
    }

    @Override
    public void saveDate(Long dataStart, Long dataEnd) {
        filterDataModel.setDateStart(dataStart);
        filterDataModel.setDateEnd(dataEnd);
        filterDataModel.setUpdated(false);
    }

    @Override
    public void onAccountSelected(Account account) {
        filterDataModel.setAccount(account);
        filterDataModel.setUpdated(false);
        view.get().setAccountText(account.getName());
    }

    @Override
    public void onContractorSelected(Contractor contractor) {
        filterDataModel.setContractorId(contractor.getContractorId() != 0 ? contractor.getContractorId() : null);
        filterDataModel.setUpdated(false);
        view.get().setContractorText(CommonUtils.splitContragent(contractor.getContractorName()));
    }

    @Override
    public void onOperationTypesSelected(List<BusinessConstants.OperationGroupType> operationTypes) {
        filterDataModel.setOperationTypes(operationTypes);
        filterDataModel.setUpdated(false);
        view.get().setOperationTypesText(operationTypesToString(operationTypes));
    }

    @Override
    public void onCurrencySelected(List<CurrencyUtils.Currency> currencyList) {
        filterDataModel.setCurrency(currencyList);
        filterDataModel.setUpdated(false);
        view.get().setCurrencyText(currencyListToString(currencyList));
    }

    @Override
    public void initDefaultData() {
        if (filterDataModel.getDateStart() == null) {
            Calendar calendar = Calendar.getInstance();
            filterDataModel.setDateEnd(calendar.getTimeInMillis());
            calendar.add(Calendar.MONTH, -1);
            filterDataModel.setDateStart(calendar.getTimeInMillis());
        }

        view.get().showDefaultData(filterDataModel.getAccount(),
                filterDataModel.getDateStart(),
                filterDataModel.getDateEnd(),
                contractorRepository.getContractorFromList(filterDataModel.getContractorId()),
                operationTypesToString(filterDataModel.getOperationTypes()),
                currencyListToString(filterDataModel.getCurrency()));
    }

    @Override
    public void getOperationList() {
        operationRepository.getOperationList(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                if (!userRepository.getUser().getFilteredOperations().isEmpty()) {
                    operationRepository.getFilterDataModel().setUpdated(true);
                    view.get().showOperationList(userRepository.getUser().getFilteredOperations());
                } else {
                    view.get().showEmptyExcerptLabel();
                }

                if (filterIsShown) view.get().showFilter(true);
            }
        });
    }

    @Override
    public void stateFilter(boolean isShown) {
        filterIsShown = isShown;
    }

    private String operationTypesToString(List<BusinessConstants.OperationGroupType> operationTypes) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (operationTypes.size() == 0 || operationTypes.size() == BusinessConstants.OperationType.values().length) {
            stringBuilder.append(appContext.getString(R.string.fragment_operations_all_type_operation));
        } else {
            for (int i = 0; i < operationTypes.size(); i++) {
                stringBuilder.append(i == 0 ? appContext.getString(operationTypes.get(i).getTitleResId()) : ", " + appContext.getString(operationTypes.get(i).getTitleResId()));
            }
        }
        return stringBuilder.toString();
    }

    private String currencyListToString(List<CurrencyUtils.Currency> currencyList) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (currencyList.size() == 0 || currencyList.size() == CurrencyUtils.Currency.values().length) {
            stringBuilder.append(appContext.getString(R.string.fragment_operations_all_currency));
        } else {
            for (int i = 0; i < currencyList.size(); i++) {
                stringBuilder.append(i == 0 ? currencyList.get(i).toString() : ", " + currencyList.get(i).toString());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public void onClickBack(boolean filterIsShown) {
        if (filterIsShown) {
            view.get().showFilter(true);
        } else {
            filterDataModel.clear();
            localCiceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().exit();
        }
    }

    @Override
    public void onDestroyView() {
        operationRepository.clearDisposable();
        contractorRepository.clearDisposable();
    }
}
