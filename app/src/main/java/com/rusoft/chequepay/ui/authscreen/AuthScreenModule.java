package com.rusoft.chequepay.ui.authscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthScreenModule {
    private final AuthScreenContract.IView view;

    public AuthScreenModule(AuthScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    AuthScreenContract.IView provideView() {
        return view;
    }
}
