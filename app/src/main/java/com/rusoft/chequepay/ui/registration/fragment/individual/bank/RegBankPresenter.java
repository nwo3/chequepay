package com.rusoft.chequepay.ui.registration.fragment.individual.bank;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.info.response.GetBankInfoResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.BankSubModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class RegBankPresenter extends BasePresenter implements RegBankContract.IPresenter {

    @Inject
    RegistrationRepository repository;

    private WeakReference<RegBankContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private BankSubModel subModel;

    @Inject
    public RegBankPresenter(RegBankContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated() {
        model = repository.getModel();
        subModel = model.bank;

        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        view.get().updateViewByResidency(model.isResident());
    }

    @Override
    public void onBikFocusLost(String bik) {
        if (bik == null || bik.isEmpty()) {
            subModel.setBik(null);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
            return;

        } else {

            if (subModel.getBik() != null && subModel.getBik().equals(bik)) {
                return;
            }

            subModel.setBik(bik);
            view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        }

        if (model.isResident()){
            repository.getBankInfo(bik, new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showLoadingIndicator(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showLoadingIndicator(false);
                    view.get().updateNextButtonState(subModel.isRequiredDataReceived());
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                    subModel.setName(null);
                    subModel.setCorrAcc(null);
                    subModel.setBikFound(false);
                    view.get().restoreView(subModel);
                }

                @Override
                public void onSuccess(BaseResponse response) {
                    GetBankInfoResponse bankInfo = (GetBankInfoResponse) response;
                    subModel.setName(bankInfo.getName());
                    subModel.setCorrAcc(bankInfo.getKorAccount());
                    subModel.setBikFound(true);
                    view.get().restoreView(subModel);
                }
            });
        }
    }

    @Override
    public void onBankNameFocusLost(String text) {
        subModel.setName(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onCorAccFocusLost(String text) {
        subModel.setCorrAcc(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onSwiftFocusLost(String text) {
        subModel.setSwift(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onBankAddressFocusLost(String text) {
        subModel.setAddress(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onBankReqFocusLost(String text) {
        subModel.setAdditionalInfo(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onAccountNumFocusLost(String text) {
        subModel.setAccNum(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onBankInnFocusLost(String text) {
        subModel.setInn(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onBankKppFocusLost(String text) {
        subModel.setKpp(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onReceiverFocusLost(String text) {
        subModel.setReceiver(text);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
    }

    @Override
    public void onClickNext() {
        if (!model.isInProfileEditingMode()) {
            if (subModel.isRequiredDataReceived()){
                router.navigateTo(Screens.REG_ACCOUNT_FRAGMENT);
            } else {
                view.get().showSnackbar(R.string.error_on_check_required_data_fail);
            }
        } else {
            if (!subModel.isRequiredDataReceived()) {
                return;
            }

            repository.editProfile(null, new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    view.get().goToOtp();
                }
            });
        }
    }

    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        if (model.isInProfileEditingMode() && model.isActionDone()){
            repository.clearModel();
            view.get().exit();
            return;
        }

        view.get().restoreView(subModel);
        view.get().updateNextButtonState(subModel.isRequiredDataReceived());
        view.get().updateViewByResidency(model.isResident());
    }

    @Override
    public void unsubscribe() {
        if (model.isInProfileEditingMode()) {
            repository.clearModel();
        }
    }
}
