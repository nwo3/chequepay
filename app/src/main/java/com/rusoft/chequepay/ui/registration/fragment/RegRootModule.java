package com.rusoft.chequepay.ui.registration.fragment;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegRootModule {
    private final RegRootContract.IView view;

    public RegRootModule(RegRootContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegRootContract.IView provideView() {
        return view;
    }
}
