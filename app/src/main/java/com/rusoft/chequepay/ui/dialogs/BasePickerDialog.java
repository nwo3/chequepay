package com.rusoft.chequepay.ui.dialogs;

import android.app.Dialog;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.base.BaseDialogFragment;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.LogUtil;

import java.util.List;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

abstract class BasePickerDialog<T> extends BaseDialogFragment {

    @BindView(R.id.dialog_container) RelativeLayout dialog_container;
    @BindView(R.id.scroll) ScrollView scroll;
    @BindView(R.id.title) TextView title;

    @BindView(R.id.actions_container) View actionsContainer;
    @BindView(R.id.cancel) View cancel;
    @BindView(R.id.confirm) protected View confirm;

    @Setter protected List<T> items;

    //Special case's, used only for Accounts list
    @Getter @Setter protected Boolean isResident;
    @Getter @Setter protected Integer currencyFilter;

    protected abstract void initializeContent();

    @Override
    protected AlertDialog.Builder getDialogBuilder() {
        return new AlertDialog.Builder(getActivity(), R.style.TransparentDialogFragment);
    }

    @Override
    protected int layoutResId() {
        return R.layout.dialog_picker;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCanceledOnTouchOutside(true);
        }
    }

    @Override
    protected void initViews() {
        if (items == null){
            LogUtil.e(ErrorUtils.INTERNAL_ERROR);
            dismiss();
            return;
        }

        initializeContent();
        cancel.setOnClickListener(view -> dismiss());
    }

    protected String getItemTitle(T item) {
        String result;

        if (item instanceof BusinessConstants.IBusinessEnumInterface) {
            result = ((BusinessConstants.IBusinessEnumInterface) item).toString(getActivity());

        } else {
            result = item.toString();
        }

        return result;
    }
}
