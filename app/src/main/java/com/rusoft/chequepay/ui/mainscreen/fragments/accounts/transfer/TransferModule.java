package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class TransferModule {
    private final TransferContract.IView view;

    public TransferModule(TransferContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    TransferContract.IView provideMoneyTransferView() {
        return view;
    }
}
