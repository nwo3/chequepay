package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.listofcontactoperations;

import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.util.List;

public interface ListOfContactOperationsContract {

    interface IView extends IBaseView {
        void showOperationList(List<Operation> operations);
        void showEmptyOperationList();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void getOperationList(long correspondentId);
        void onClickOperation(long operationId);
    }
}
