package com.rusoft.chequepay.ui.authscreen;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.flagsmanager.Flags;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;


public class AuthScreenPresenter extends BasePresenter implements AuthScreenContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    RegistrationRepository registrationRepository;

    private WeakReference<AuthScreenContract.IView> view;

    @Inject
    public AuthScreenPresenter(AuthScreenContract.IView view, UserRepository userRepository) {
        this.view = new WeakReference<>(view);
        this.userRepository = userRepository;
    }

    @Override
    public void onViewCreated(int actionType) {
        if (actionType == AppConstants.ACTION_TYPE_REGISTRATION){
            registrationRepository.clearModel();
        }
    }

    @Override
    public void onClickProceed(String login, String password) {
        userRepository.auth(login, password, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onClickReg() {
        view.get().goToReg();
    }

    @Override
    public void onClickForgotPass() {
        view.get().goToPassRecovery();
    }

    @Override
    public void onClickHelp() {
        view.get().goToHelp();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (FlagsManager.getInstance().isFlagOn(Flags.TOKEN_EXPIRED)) {
            view.get().showInfoDialog(R.string.error_token_expired);
            FlagsManager.getInstance().turnOff(Flags.TOKEN_EXPIRED);

        } else if (FlagsManager.getInstance().isFlagOn(Flags.IP_RESTRICTION)) {
            view.get().showInfoDialog(R.string.error_ip_restriction);
            FlagsManager.getInstance().turnOff(Flags.IP_RESTRICTION);
        }
    }
}
