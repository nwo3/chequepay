package com.rusoft.chequepay.ui.pinscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PinScreenModule {
    private final PinScreenContract.IView view;

    public PinScreenModule(PinScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    PinScreenContract.IView provideView() {
        return view;
    }
}
