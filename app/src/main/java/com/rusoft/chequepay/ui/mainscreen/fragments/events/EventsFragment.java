package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.event.UpdateDataEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;

import static com.rusoft.chequepay.AppConstants.UPDATE_OPERATIONS_DELAY;

public class EventsFragment extends BaseRootFragment implements EventsContract.IView, EventsAdapter.ItemClickListener {

    @Inject
    EventsPresenter presenter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.btn_all_operations) TextView btnAllOperations;
    @BindView(R.id.segmented1) SegmentedGroup segmentedGroup;
    @BindView(R.id.label) TextView emptyExcerptLabel;
    @BindView(R.id.rv_operations) RecyclerView recyclerViewOperations;
    @BindView(R.id.tv_count_activities) TextView tvCountActivities;
    @BindView(R.id.btn_segmented_control_operation) RadioButton rbOperations;
    @BindView(R.id.btn_segmented_control_action) RadioButton rbActivities;

    private List<Operation> operationList = new ArrayList<>();
    private EventsAdapter eventsAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_events;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_events);
    }

    public static EventsFragment getNewInstance(String name) {
        EventsFragment eventsFragment = new EventsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.EXTRA_NAME, name);
        eventsFragment.setArguments(arguments);

        return eventsFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerEventsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .eventsModule(new EventsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        eventsAdapter = new EventsAdapter(getContext(), operationList, this, true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewOperations.setLayoutManager(llm);
        recyclerViewOperations.setAdapter(eventsAdapter);
        recyclerViewOperations.addItemDecoration(new EventsAdapter.DividerCustomItemDecoration(getContext()));

        btnAllOperations.setOnClickListener(v -> presenter.onClickAllOperations());
        btnAllOperations.setOnTouchListener(UiUtils.getTouchListener());
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());

        rbOperations.setTextColor(getResources().getColor(R.color.white));
        rbActivities.setTextColor(getResources().getColor(R.color.white));
        segmentedGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.btn_segmented_control_operation:

                    break;
                case R.id.btn_segmented_control_action:
                    presenter.onClickActivities();
                    break;
            }
        });

        presenter.getOperationsShortList();
    }

    @Override
    public void showOperationsShortList(List<Operation> operationShortList) {
        operationList.clear();
        operationList.addAll(operationShortList);
        eventsAdapter.notifyDataSetChanged();

        recyclerViewOperations.setVisibility(View.VISIBLE);
        btnAllOperations.setVisibility(View.VISIBLE);
        emptyExcerptLabel.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        super.showLoadingIndicator(enabled);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(enabled);
        }
    }

    @Override
    public void showEmptyOperationsShortList() {
        operationList.clear();
        eventsAdapter.notifyDataSetChanged();

        recyclerViewOperations.setVisibility(View.INVISIBLE);
        btnAllOperations.setVisibility(View.INVISIBLE);
        emptyExcerptLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUnreadActivities(int count) {
        tvCountActivities.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
        tvCountActivities.setText(String.valueOf(count));
    }

    @Override
    public void eventSelected(long operationId, boolean isNewOperation) {
        if (isNewOperation) {
            int countUnreadMessages = ((MainActivity) getActivity()).getCountUnreadMessages();
            ((MainActivity) getActivity()).showUnreadIndicator(--countUnreadMessages);
        }
        presenter.onClickOperation(operationId);
    }

    @Subscribe
    public void onEvent(UpdateDataEvent updateDataEvent) {
        new Handler().postDelayed(() -> {
            presenter.getOperationsShortList();
        }, UPDATE_OPERATIONS_DELAY);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
