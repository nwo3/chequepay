package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class SecurityModule {
    private final SecurityContract.IView view;

    public SecurityModule(SecurityContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    SecurityContract.IView provideView() {
        return view;
    }
}
