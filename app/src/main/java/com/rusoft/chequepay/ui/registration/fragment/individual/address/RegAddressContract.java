package com.rusoft.chequepay.ui.registration.fragment.individual.address;

import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.repository.registration.model.AddressSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;

import java.util.List;

public interface RegAddressContract {

    interface IView extends IBaseRegView {
        void updateViewByResidency(boolean isResident);

        void showCountriesPicker(List<Country> countries, int selectedIndex);
        void showRegionsPicker(List<Region> regions, int selectedIndex);

        void setCountryText(String text);
        void setRegionText(String text);

        void lockFields(boolean locked);

        void showSwitcher(boolean show);

        void updateCountrySubjectState(boolean isFromRussia);

        void restoreState(AddressSubModel model);

        void goToOtp();
        void exit();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(RegAddressFragment.ScreenType type);

        void onSwitchStateChanged(boolean enabled);

        void onClickCountry();
        void onCountrySelected(Country country);

        void onClickRegion();
        void onRegionSelected(Region region);
        void onDistrictFocusLost(String text);

        void onPostCodeFocusLost(String text);
        void onSettlementFocusLost(String text);
        void onStreetFocusLost(String text);

        void onClickNext();

    }
}
