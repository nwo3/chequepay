package com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class InvoicePayModule {
    private final InvoicePayContract.IView view;

    public InvoicePayModule(InvoicePayContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    InvoicePayContract.IView provideInvoiceView() {
        return view;
    }
}
