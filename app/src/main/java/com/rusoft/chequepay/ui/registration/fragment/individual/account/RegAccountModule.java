package com.rusoft.chequepay.ui.registration.fragment.individual.account;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegAccountModule {
    private final RegAccountContract.IView view;

    public RegAccountModule(RegAccountContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegAccountContract.IView provideView() {
        return view;
    }
}
