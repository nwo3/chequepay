package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.operations.response.itemOperationResponse.ItemOperationResponse;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;

public class OperationDetailFragment extends BaseBranchFragment implements OperationDetailContract.IView {

    @Inject
    OperationDetailPresenter presenter;

    @BindView(R.id.ll_container_operation) LinearLayout llOperation;
    @BindView(R.id.btn_add_contragent) View tvAdd;
    @BindView(R.id.btn_download_pdf) View tvPdf;


    @Override
    protected int layoutResId() {
        return R.layout.fragment_operation_detail;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_operation_info);
    }

    public static OperationDetailFragment getNewInstance(String containerKey) {
        OperationDetailFragment operationDetailFragment = new OperationDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerKey);
        operationDetailFragment.setArguments(arguments);

        return operationDetailFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerOperationDetailComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .operationDetailModule(new OperationDetailModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        presenter.getOperationInfo();

        tvAdd.setOnClickListener(v -> presenter.addCorrespondent());
        tvPdf.setOnClickListener(v -> presenter.downloadPdf());

        tvAdd.setOnTouchListener(UiUtils.getTouchListener());
        tvPdf.setOnTouchListener(UiUtils.getTouchListener());

    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Override
    public void showOperationInfo(ItemOperationResponse itemOperationResponse, Operation operation, boolean isHideAddBtn, boolean isHidePdfLoadBtn) {
        Map<String, String> operationInfo = new LinkedHashMap<>();
        operationInfo.put(getString(R.string.account_number_1), itemOperationResponse.getReceiverAccount());

        if (operation != null) {
            operationInfo.put(getString(R.string.account_name), operation.getAccountName());
            operationInfo.put(getString(R.string.fragment_detail_operation_deposit), operation.getDepositAmount());
            operationInfo.put(getString(R.string.fragment_detail_operation_withdrawal), operation.getWithdrawAmount());
            operationInfo.put(getString(R.string.fragment_detail_operation_in_balance), operation.getSaldoPrevious());
            operationInfo.put(getString(R.string.fragment_detail_operation_out_balance), operation.getSaldo());

            operationInfo.put(getString(R.string.fragment_detail_operation_currency), CurrencyUtils.getName((int) itemOperationResponse.getCurrency()));
            operationInfo.put(getString(R.string.fragment_detail_operation_number_operation), itemOperationResponse.getOperationUid());
            operationInfo.put(getString(R.string.fragment_detail_operation_type_operation), itemOperationResponse.getOperationTypeName());
            operationInfo.put(getString(R.string.fragment_detail_operation_date), AppConstants.SDF_ddMMyyyy_HHmm.format(new Date(itemOperationResponse.getDate())));
        }

        operationInfo.put(getString(R.string.fragment_detail_operation_target_payment), CommonUtils.splitContragent(itemOperationResponse.getReceiverName()));
        operationInfo.put(getString(R.string.fragment_detail_operation_contragent), CommonUtils.splitContragent(itemOperationResponse.getSenderName()));

        for (Map.Entry<String, String> entry : operationInfo.entrySet()) {
            String keyLabel = entry.getKey();
            String value = entry.getValue();
            View operationView = getLayoutInflater().inflate(R.layout.view_info_operation, null, false);
            StaticInputView inputView = operationView.findViewById(R.id.iv_value);
            inputView.setTitle(keyLabel);
            inputView.setText(value);
            llOperation.addView(operationView);
        }

        tvAdd.setVisibility(isHideAddBtn ? View.GONE : View.VISIBLE);
        tvPdf.setVisibility(isHidePdfLoadBtn ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
