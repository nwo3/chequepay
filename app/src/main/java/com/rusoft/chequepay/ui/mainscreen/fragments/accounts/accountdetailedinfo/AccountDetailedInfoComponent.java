package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetailedinfo;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {AccountDetailedInfoModule.class, ChequepayApiModule.class})
public interface AccountDetailedInfoComponent {
    void inject(AccountDetailedInfoFragment fragment);
}
