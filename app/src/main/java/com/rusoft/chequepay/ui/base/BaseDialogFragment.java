package com.rusoft.chequepay.ui.base;


import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseDialogFragment extends DialogFragment {

    protected abstract AlertDialog.Builder getDialogBuilder();
    protected abstract int layoutResId();
    protected abstract void initViews();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return prepareDialog();
    }

    private Dialog prepareDialog() {
        AlertDialog.Builder builder = getDialogBuilder();
        builder.setView(inflateView());
        return builder.create();
    }


    protected View inflateView() {
        View view = LayoutInflater.from(getActivity()).inflate(layoutResId(), null, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }
}
