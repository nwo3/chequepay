package com.rusoft.chequepay.ui.registration.fragment.individual.address;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegAddressModule.class, ChequepayApiModule.class})
public interface RegAddressComponent {
    void inject(RegAddressFragment fragment);
}
