package com.rusoft.chequepay.ui.mainscreen.containers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseContainerFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer.PdfViewerFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.MoreFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.docs.DocsFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.faq.FaqFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback.FeedbackFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.help.HelpFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.profile.ProfileFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.more.security.SecurityFragment;
import com.rusoft.chequepay.utils.Preferences;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

import static com.rusoft.chequepay.ui.Screens.KEY_FROM;

public class MoreContainer extends BaseContainerFragment {

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    @Inject
    Preferences preferences;

    private Navigator navigator;

    public static MoreContainer getNewInstance(String name, String from) {
        MoreContainer moreContainer = new MoreContainer();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.EXTRA_NAME, name);
        arguments.putString(KEY_FROM, from);
        moreContainer.setArguments(arguments);

        return moreContainer;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerContainersComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_container;
    }

    @Override
    protected String toolbarTitle() {
        return null;
    }

    @Override
    protected void initViews() {
        //ignore
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.container) {
                @Override
                protected Intent createActivityIntent(Context context, String screenKey, Object data) {
                    return null;
                }

                @Override
                protected Fragment createFragment(String screenKey, Object data) {
                    switch (screenKey) {
                        case Screens.MORE_FRAGMENT:
                            return MoreFragment.getNewInstance(Screens.MORE_CONTAINER, (String) data);

                        case Screens.HELP_FRAGMENT:
                            return HelpFragment.getNewInstance(Screens.MORE_CONTAINER, (String) data);

                        case Screens.FAQ_FRAGMENT:
                            return FaqFragment.getNewInstance(Screens.MORE_CONTAINER);

                        case Screens.PROFILE_FRAGMENT:
                            return ProfileFragment.getNewInstance(Screens.MORE_CONTAINER);

                        case Screens.FEEDBACK_FRAGMENT:
                            return FeedbackFragment.getNewInstance(Screens.MORE_CONTAINER);

                        case Screens.SECURITY_FRAGMENT:
                            return SecurityFragment.getNewInstance(Screens.MORE_CONTAINER);

                        case Screens.DOCS_FRAGMENT:
                            return DocsFragment.getNewInstance(Screens.MORE_CONTAINER);

                        case Screens.PDF_FRAGMENT:
                            return PdfViewerFragment.getNewInstance(Screens.MORE_CONTAINER, (long) data);

                        default:
                            throw new RuntimeException();
                    }
                }

                @Override
                protected void showSystemMessage(String message) {
                    showSnackbar(message);
                }

                @Override
                protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
                    if (!(command instanceof Replace)) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right);
                    }
                }

                @Override
                protected void exit() {
                    //ignore
                }
            };
        }
        return navigator;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.container) == null) {
            ciceroneHolder.getCicerone(Screens.MORE_CONTAINER).getRouter()
                    .replaceScreen(Screens.MORE_FRAGMENT, getArguments().getString(KEY_FROM));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ciceroneHolder.getCicerone(Screens.MORE_CONTAINER).getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        ciceroneHolder.getCicerone(Screens.MORE_CONTAINER).getNavigatorHolder().removeNavigator();
        super.onPause();
    }
}
