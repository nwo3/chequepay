package com.rusoft.chequepay.ui.splashscreen;


import com.rusoft.chequepay.data.network.methods.SignInMethods;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class SplashPresenter extends BasePresenter implements SplashContract.IPresenter {

    @Inject
    UserRepository repository;

    private WeakReference<SplashContract.IView> view;

    @Inject
    public SignInMethods signInMethods;

    @Inject
    public SplashPresenter(SplashContract.IView view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onViewCreated() {
        if (repository.isAuthorized()){
            view.get().goToPin();
        } else {
            view.get().goToAuth();
        }
    }
}
