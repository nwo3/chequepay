package com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback;

import android.os.Bundle;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

public class FeedbackFragment extends BaseBranchFragment implements FeedbackContract.IView {

    @Inject
    FeedbackPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_feedback;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_feedback);
    }

    public static FeedbackFragment getNewInstance(String containerName) {
        FeedbackFragment fragment = new FeedbackFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerFeedbackComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .feedbackModule(new FeedbackModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        //ignore
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

}
