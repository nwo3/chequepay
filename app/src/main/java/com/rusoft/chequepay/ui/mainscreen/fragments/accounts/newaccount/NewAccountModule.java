package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class NewAccountModule {
    private final NewAccountContract.IView view;

    public NewAccountModule(NewAccountContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    NewAccountContract.IView provideNewAccountView() {
        return view;
    }
}
