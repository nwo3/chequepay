package com.rusoft.chequepay.ui.registration.fragment.individual.personal;

import com.rusoft.chequepay.data.repository.registration.model.PersonSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;

import java.util.Calendar;

public interface RegPersonalContract {

    interface IView extends IBaseRegView {
        void updateViewByResidency(boolean isResident);
        void updateScreenIfInProfileMode(boolean isInProfileEditMode);


        void setErrorOnBirthDateSelector(int textResId);
        void setErrorOnPhoneInput(int textResId);
        void setErrorOnEmailInput(int textResId);

        void restoreState(PersonSubModel model);

        void goToOtp();
        void exit();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onLastNameFocusLost(String text);
        void onFirstNameFocusLost(String text);
        void onPatronymicFocusLost(String text);

        void onBirthDateSelected(Calendar calendar);

        void onInnFocusLost(String text);
        void onPhoneFocusLost(String text);
        void onEmailFocusLost(String text);
        void onWebSiteFocusLost(String text);

        void onClickNext(String phone, String email);

        void onDestroy();
    }
}
