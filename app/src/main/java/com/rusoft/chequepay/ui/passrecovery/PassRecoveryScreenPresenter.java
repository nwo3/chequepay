package com.rusoft.chequepay.ui.passrecovery;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.passrecovery.PassRecoveryModel;
import com.rusoft.chequepay.data.repository.passrecovery.PassRecoveryRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

import static com.rusoft.chequepay.ui.passrecovery.PassRecoveryScreenPresenter.ScreenState.PASS;
import static com.rusoft.chequepay.ui.passrecovery.PassRecoveryScreenPresenter.ScreenState.PHONE;


public class PassRecoveryScreenPresenter extends BasePresenter implements PassRecoveryScreenContract.IPresenter {

    @Inject
    PassRecoveryRepository repository;

    private WeakReference<PassRecoveryScreenContract.IView> view;

    private PassRecoveryModel model;

    @Inject
    public PassRecoveryScreenPresenter(PassRecoveryScreenContract.IView view) {
        this.view = new WeakReference<>(view);
    }

    enum ScreenState {PHONE, PASS}
    private ScreenState screenState;

    public void setScreenState(ScreenState screenState) {
        this.screenState = screenState;
        view.get().updateScreenState(screenState);
        updateProceedButtonState();
    }

    @Override
    public void onViewCreated() {
        model = repository.getModel();
        setScreenState(PHONE);
    }

    @Override
    public void onPhoneTextChanged(String text, boolean isValid) {
        model.setSource(isValid ? text : null);
        updateProceedButtonState();
    }

    @Override
    public void onPassFocusLost(String text) {
        model.setPass(text);
        updateProceedButtonState();
    }

    @Override
    public void onPassConfirmFocusLost(String text) {
        model.setPassConfirm(text);
        updateProceedButtonState();

        if (model.getPass() != null && !model.isPassConfirmed()) {
            view.get().showPassConfirmError(R.string.error_passwords_do_not_match);
        }
    }

    private void updateProceedButtonState() {
        switch (screenState) {
            case PHONE:
                view.get().setProceedEnabled(model.getSource() != null);
                break;

            case PASS:
                view.get().setProceedEnabled(model.isPassConfirmed());
                break;
        }
    }

    @Override
    public void onClickProceed() {
        switch (screenState) {
            case PHONE:
                repository.recoveryStart(new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showProgressDialog(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showProgressDialog(false);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        view.get().showError(throwable);
                    }

                    @Override
                    public void onSuccess() {
                        view.get().goToOtp();
                    }
                });
                break;

            case PASS:
                repository.changePassword(new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showProgressDialog(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showProgressDialog(false);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        view.get().showError(throwable);
                    }

                    @Override
                    public void onSuccess() {
                        view.get().goToSuccessScreen();
                    }
                });
                break;
        }
    }

    @Override
    public void onClickHelp() {
        view.get().goToHelp();
    }

    @Override
    public void onResume() {
        setScreenState(model.isAccessConfirmed() ? PASS : PHONE); //on return after otp screen
    }

    @Override
    public void unsubscribe() {
        repository.clearModel();
    }
}
