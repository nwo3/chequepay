package com.rusoft.chequepay.ui.authscreen;

import com.rusoft.chequepay.ui.base.IBaseActivityView;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface AuthScreenContract {

    interface IView extends IBaseActivityView{
        void goToOtp();
        void goToHelp();
        void goToReg();
        void goToPassRecovery();

        void showInfoDialog(int textResId);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(int actionType);
        void onClickProceed(String login, String password);
        void onClickReg();
        void onClickForgotPass();
        void onClickHelp();
    }

}
