package com.rusoft.chequepay.ui.registration.fragment.individual.bank;

import com.rusoft.chequepay.data.repository.registration.model.BankSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;

public interface RegBankContract {

    interface IView extends IBaseRegView {
        void updateViewByResidency(boolean isResident);

        void restoreView(BankSubModel model);

        void goToOtp();
        void exit();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onBikFocusLost(String text);
        void onBankNameFocusLost(String text);
        void onCorAccFocusLost(String text);
        void onSwiftFocusLost(String text);
        void onBankAddressFocusLost(String text);
        void onBankReqFocusLost(String text);
        void onAccountNumFocusLost(String text);
        void onBankInnFocusLost(String text);
        void onBankKppFocusLost(String text);
        void onReceiverFocusLost(String text);

        void onClickNext();

    }
}
