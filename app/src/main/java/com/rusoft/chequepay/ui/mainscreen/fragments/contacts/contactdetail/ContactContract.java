package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.contactdetail;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface ContactContract {

    interface IView extends IBaseView {
        void hideTransferAndInvoice();
    }

    interface IPresenter extends IBasePresenter {
        void onClickInfo(long contractorId);
        void onClickShowStatement(long contractorId);
        void onClickTransfer();
        void onClickInvoice();
        void onClickEdit();
        void onViewCreated(long correspondentId);
        void onDeleteCorrespondent();
    }
}
