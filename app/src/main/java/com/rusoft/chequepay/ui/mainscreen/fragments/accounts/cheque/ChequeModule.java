package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.cheque;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ChequeModule {
    private final ChequeContract.IView view;

    public ChequeModule(ChequeContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ChequeContract.IView provideView() {
        return view;
    }
}
