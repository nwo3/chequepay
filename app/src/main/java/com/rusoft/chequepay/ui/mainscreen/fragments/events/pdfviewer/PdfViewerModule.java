package com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PdfViewerModule {
    private final PdfViewerContract.IView view;

    public PdfViewerModule(PdfViewerContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    PdfViewerContract.IView provideView() {
        return view;
    }
}
