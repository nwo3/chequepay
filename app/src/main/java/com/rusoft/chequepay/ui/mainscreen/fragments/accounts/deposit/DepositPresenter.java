package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit;

import android.os.Handler;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.entities.templates.BuyTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.account.AccountRepository;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositModel;
import com.rusoft.chequepay.data.repository.transactions.deposit.DepositRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class DepositPresenter extends BasePresenter implements DepositContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    DepositRepository depositRepository;

    @Inject
    AccountRepository accountRepository;

    private WeakReference<DepositContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    private DepositModel model;

    @Inject
    public DepositPresenter(DepositContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, depositRepository, accountRepository);
        this.model = depositRepository.getModel();

        UserModel user = userRepository.getUser();

        if (data == null) {
            onAccountSelected(user.getOldestAccount());
            onModeSelected(BusinessConstants.RefundMode.TRANSFER);

        } else if (data instanceof Account) {
            onAccountSelected((Account) data);
            onModeSelected(BusinessConstants.RefundMode.TRANSFER);

        } else if (data instanceof BuyTemplate) {
            applyTemplate((BuyTemplate) data);
            recoverViewData();
            updateTariff();
        }

        updateViewControlsState();
    }

    private void applyTemplate(BuyTemplate template) {
        UserModel user = userRepository.getUser();
        Account account = user.getAccountByNum(template.getAccountNumber());
        if (account != null) {
            model.setAccount(account);
            model.setAmount(template.getAmount());
            model.setMode(BusinessConstants.RefundMode.getMode(template.getRefundMode()));
            model.setTemplateUsed(true);

            accountRepository.getBankIssuers(model.getCurrency(), new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showLoadingIndicator(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showLoadingIndicator(false);
                    recoverViewData();
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    model.setBank(user.getBankById(model.getCurrency(), template.getBankId()));
                    model.setIssuer(user.getIssuerById(model.getCurrency(), template.getReceiverId()));
                    updateTariff();
                }
            });
        } else {
            view.get().showSnackbar(R.string.error_account_not_found);
        }
    }

    private void updateUserAccounts() {
        userRepository.updateAccounts(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onClickAccount() {
        List<Account> accounts = userRepository.getUser().getAccounts();
        if (accounts != null && accounts.size() > 0){
            int selectedIndex = accounts.indexOf(model.getAccount());
            boolean isResident = userRepository.getUser().isResident();
            view.get().showAccountSelectorDialog(accounts, selectedIndex, isResident);
        } else {
            view.get().showAddNewAccountDialog();
        }
    }

    @Override
    public void onAccountSelected(Account account) {
        if (account == null) {
            return;
        }

        model.setAccount(account);
        view.get().setAccountText(account.toString());
        view.get().setCurrencyText(CurrencyUtils.getName(account.getCurrency()));
        updateBankIssuersList();
        updateTariff();
    }

    @Override
    public void onClickIssuer() {
        List<Issuer> issuers = userRepository.getUser().getIssuers(model.getCurrency());
        if (issuers != null && !issuers.isEmpty()){
            int selectedIndex = issuers.indexOf(model.getIssuer());
            view.get().showIssuerSelectorDialog(issuers, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_issuers_found);
        }
    }

    @Override
    public void onIssuerSelected(Issuer issuer) {
        model.setIssuer(issuer);
        view.get().setIssuerText(issuer.toString());
        updateTariff();

        if (model.getBank() != null && !userRepository.getUser().isBankAndIssuerHasRelations(model.getIssuer(), model.getBank())){
            onBankSelected(null);
        }

        updateViewControlsState();
    }

    @Override
    public void onClickBank() {
        List<Bank> banks = userRepository.getUser().getBankByIssuer(model.getCurrency(), model.getIssuer());
        if (banks != null && !banks.isEmpty()){
            int selectedIndex = banks.indexOf(model.getBank());
            view.get().showBankSelectorDialog(banks, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_banks_found);
        }
    }

    @Override
    public void onBankSelected(Bank bank) {
        model.setBank(bank);
        view.get().setBankText(bank != null ? bank.toString() : "");
        if (bank != null) {
            updateTariff();
        }
        updateViewControlsState();
    }

    @Override
    public void onClickMode() {
        List<BusinessConstants.RefundMode> modes = BusinessConstants.RefundMode.asList();
        int selectedIndex = modes.indexOf(model.getMode());
        view.get().showModeSelectorDialog(modes, selectedIndex);
    }

    @Override
    public void onModeSelected(BusinessConstants.RefundMode mode) {
        model.setMode(mode);
        view.get().setModeText(mode.getTitleResId());
        updateViewControlsState();
    }

    @Override
    public void onAmountFocusLost(String inputData) {
        if (ValidationPatterns.validate(inputData, ValidationPatterns.DOUBLE)) {
            Double amount = Double.valueOf(inputData);

            if (CurrencyUtils.isAmountInRange(model.getCurrency(), amount)){
                model.setAmount(amount);
            } else {
                view.get().showSnackbar(R.string.error_on_unacceptable_amount);
                model.setAmount(null);
            }
        } else {
            model.setAmount(null);
        }

        updateModelAndViewNumericData();
        updateViewControlsState();
    }

    private void updateBankIssuersList() {
        accountRepository.getBankIssuers(model.getCurrency(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                updateViewControlsState();

                List<Issuer> issuers = userRepository.getUser().getIssuers(model.getCurrency());
                if (issuers != null && !issuers.isEmpty()) {
                    Issuer issuer = issuers.get(0);
                    onIssuerSelected(issuer);
                    List<Bank> banks = userRepository.getUser().getBankByIssuer(model.getCurrency(), issuer);
                    if (banks != null && !banks.isEmpty()) {
                        Bank bank = banks.get(0);
                        onBankSelected(bank);
                    }
                }
            }
        });
    }


    private void updateTariff() {
        depositRepository.getTariff(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }


            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(Double tariff) {
                model.setTariff(tariff);
                updateModelAndViewNumericData();
                updateViewControlsState();
            }
        });
    }

    @Override
    public void onClickProceed() {
        depositRepository.createDeposit(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    private void updateModelAndViewNumericData() {
        if (view == null) return;

        boolean isUpdateSuccess = model.updateNumericData();
        view.get().setAmountText(isUpdateSuccess ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommissionText(isUpdateSuccess ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setTotalText(isUpdateSuccess ? CurrencyUtils.format(model.getTotal(), model.getCurrency()) : null);
    }

    private void updateViewControlsState() {
        if (view == null) return;

        view.get().disableIssuerInput(model.getAccount() == null);
        view.get().disableBankInput(model.getAccount() == null || model.getIssuer() == null);
        view.get().disableModeInput(model.getAccount() == null || model.getIssuer() == null);
        view.get().disableAmountInput(model.getAccount() == null || model.getIssuer() == null || model.getTariff() == null);
        view.get().disableProceedButton(!model.isAllDataReceived());
    }

    private void recoverViewData(){
        if (view == null) return;

        view.get().setAccountText(model.getAccount() != null ? model.getAccount().toString() : null);
        view.get().setCurrencyText(CurrencyUtils.getName(model.getCurrency()));
        view.get().setIssuerText(model.getIssuer() != null ? model.getIssuer().toString() : null);
        view.get().setBankText(model.getBank() != null ? model.getBank().toString() : null);
        view.get().setModeText(model.getMode() != null ? model.getMode().getTitleResId() : null);
        view.get().setAmountText(model.getAmount() != null ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommissionText(model.getCommissionAmount() != null ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setTotalText(model.getTotal() != null ? CurrencyUtils.format(model.getTotal(), model.getCurrency()) : null);
    }

    @Override
    public void goToNewAccount() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model.isTransactionDone()){
            new Handler().post(this::goBack);
        } else {
            updateUserAccounts();
            model.clearFlowToken();
            recoverViewData();
        }
    }

    private void goBack() {
        depositRepository.clearModel();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
