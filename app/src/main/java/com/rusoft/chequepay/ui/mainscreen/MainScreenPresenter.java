package com.rusoft.chequepay.ui.mainscreen;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.messages.MessageRepository;
import com.rusoft.chequepay.data.repository.notifications.NotificationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.flagsmanager.Flags;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class MainScreenPresenter extends BasePresenter implements MainScreenContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    MessageRepository messageRepository;

    @Inject
    NotificationRepository notificationRepository;

    private WeakReference<MainScreenContract.IView> view;
    private Router router;

    @Inject
    public MainScreenPresenter(MainScreenContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated(String from) {
        addRepositories(userRepository, messageRepository, notificationRepository);

        switch (from) {
            case Screens.PIN_ACTIVITY:
                router.replaceScreen(Screens.ACCOUNTS_CONTAINER);
                messageRepository.initLongPolling();
                getUnreadMessages();
                break;

            case Screens.AUTH_ACTIVITY:
            case AppConstants.FROM_PIN_TO_HELP:
                router.replaceScreen(Screens.MORE_CONTAINER, from); //from AUTH to HELP screen
                break;

            default:
                throw new RuntimeException();
        }
    }

    @Override
    public void onClickNavAccounts() {
        router.replaceScreen(Screens.ACCOUNTS_CONTAINER);
    }

    @Override
    public void onClickNavPayments() {
        router.replaceScreen(Screens.PAYMENTS_CONTAINER);
    }

    @Override
    public void onClickNavEvents() {
        router.replaceScreen(Screens.EVENTS_CONTAINER);
    }

    @Override
    public void onClickNavContacts() {
        router.replaceScreen(Screens.CONTACTS_CONTAINER);
    }

    @Override
    public void onClickNavMore() {
        router.replaceScreen(Screens.MORE_CONTAINER, Screens.MAIN_ACTIVITY);
    }

    @Override
    public void onClickBack() {
        //ignore
    }

    @Override
    public void getUnreadMessages() {
        notificationRepository.getCountNotification(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                //ignore
            }

            @Override
            public void onPostExecute() {
                //ignore
            }

            @Override
            public void onError(Throwable throwable) {
                //ignore
            }

            @Override
            public void onSuccess(Integer result) {
                if (result > 0) {
                    view.get().showUnreadIndicator(result);
                }
            }
        });
    }

    @Override
    public void onCatchTokenExpiredEvent() {
        FlagsManager.getInstance().turnOn(Flags.TOKEN_EXPIRED);
        logout();
    }

    @Override
    public void onCatchIpRestrictionEvent() {
        FlagsManager.getInstance().turnOn(Flags.IP_RESTRICTION);
        logout();
    }

    private void logout() {
        userRepository.logout();
        view.get().goToAuth();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (FlagsManager.getInstance().isFlagOn(Flags.TOKEN_EXPIRED)
                || FlagsManager.getInstance().isFlagOn(Flags.IP_RESTRICTION)) {
            logout();
        }
    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();
        messageRepository.removeLongPolling();
    }
}
