package com.rusoft.chequepay.ui.base.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.DepositScreenEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.utils.LogUtil;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements IBaseView {
    private Unbinder unbinder;

    protected abstract void initializeDaggerComponent();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.i(getName() + " INITIALIZED");
        initializeDaggerComponent();
        View view = inflater.inflate(layoutResId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    protected abstract int layoutResId();

    protected abstract String toolbarTitle();

    protected abstract void initViews();

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        LogUtil.i(getName() + " ON_START");
    }


    @Override
    public void onResume() {
        super.onResume();
        LogUtil.i(getName() + " ON_RESUME");
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtil.i(getName() + " ON_PAUSE");
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        LogUtil.i(getName() + " ON_STOP");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LogUtil.i(getName() + " DESTROYED");
        unbinder.unbind();
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        if (!(getActivity() instanceof BaseActivity)){
            return;
        }

        ((BaseActivity) getActivity()).showProgress(enabled);
    }

    @Override
    public void showProgressDialog(boolean enabled) {
        if (!(getActivity() instanceof BaseActivity)){
            return;
        }

        ((BaseActivity) getActivity()).showProgressDialog(enabled);
    }

    @Override
    public void showError(Throwable throwable) {
        if (!(getActivity() instanceof BaseActivity)){
            return;
        }

        ((BaseActivity) getActivity()).showError(throwable);
    }

    @Override
    public void showSnackbar(int textResID) {
        if (!(getActivity() instanceof BaseActivity)){
            return;
        }

        ((BaseActivity) getActivity()).showSnackbar(textResID);
    }

    @Override
    public void showSnackbar(String text){
        ((BaseActivity) getActivity()).showSnackbar(text);
    }

    @Override
    public void updateToolbarTitle(String text) {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        if (toolbar != null){
            TextView title = toolbar.findViewById(R.id.toolbar_title);
            if (title != null){
                title.setText(text);
            }
        }
    }

    @Override
    public void showRefillAccountsDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_amount_accounts, null,
                R.string.cancel, R.string.mainscreen_title_money_deposit,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            EventBus.getDefault().post(new DepositScreenEvent());
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    @Nullable
    public String getContainerName() {
        return getArguments().getString(Screens.CONTAINER_NAME);
    }
}
