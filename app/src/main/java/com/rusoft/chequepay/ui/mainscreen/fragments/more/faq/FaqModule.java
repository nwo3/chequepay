package com.rusoft.chequepay.ui.mainscreen.fragments.more.faq;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class FaqModule {
    private final FaqContract.IView view;

    public FaqModule(FaqContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    FaqContract.IView provideView() {
        return view;
    }
}
