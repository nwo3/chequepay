package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ActivitiesModule.class, ChequepayApiModule.class})
public interface ActivitiesComponent {
    void inject(ActivitiesFragment fragment);
}
