package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.contactdetail;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ContactPresenter extends BasePresenter implements ContactContract.IPresenter {

    private WeakReference<ContactContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Correspondent correspondent;

    @Inject
    ContractorRepository contractorRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    public ContactPresenter(ContactContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(long correspondentId) {
        addRepositories(contractorRepository, userRepository);

        correspondent = contractorRepository.getCorrespondentFromList(correspondentId);
        view.get().updateToolbarTitle(CommonUtils.splitContragent(correspondent.getName()));

        if (correspondent.getRegistrationType() != BusinessConstants.RegType.STANDART.getVal()) {
            view.get().hideTransferAndInvoice();
        }
    }

    @Override
    public void onClickInfo(long contractorId) {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().navigateTo(
                Screens.CORRESPONDENT_DETAIL_FRAGMENT,
                contractorId);
    }

    @Override
    public void onClickShowStatement(long contractorId) {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter()
                .navigateTo(Screens.LIST_OF_CONTACT_OPERATIONS, contractorId);
    }

    @Override
    public void onClickEdit() {
        CorrespondentModel correspondentModel = contractorRepository.getCorrespondentModel();
        correspondentModel.setEdit(true);
        correspondentModel.setCorrespondentName(correspondent.getName());
        correspondentModel.setAccountNumber(String.valueOf(correspondent.getWalletId()));

        List<Account> accountListForRequest = new ArrayList<>();
        for (com.rusoft.chequepay.data.entities.correspondents.response.Account account : correspondent.getAccounts()) {
            accountListForRequest.add(new Account(account.getName(), account.getNumber()));
        }
        correspondentModel.setAccounts(accountListForRequest);

        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().navigateTo(Screens.ADD_CONTACT_FRAGMENT);
    }

    @Override
    public void onClickTransfer() {
        correspondent.setFromContact(true);
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter()
                .navigateTo(Screens.MONEY_TRANSFER_FRAGMENT, correspondent);
    }

    @Override
    public void onClickInvoice() {
        correspondent.setFromContact(true);
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter()
                .navigateTo(Screens.INVOICE_FRAGMENT, correspondent);
    }

    @Override
    public void onDeleteCorrespondent() {
        contractorRepository.deleteContractor(correspondent.getWalletId(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onSuccess() {
                localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().backTo(Screens.CONTACTS_FRAGMENT);
            }
        });
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().backTo(Screens.CONTACTS_FRAGMENT);
    }
}
