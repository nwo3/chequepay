package com.rusoft.chequepay.ui.registration.fragment.individual.passport;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegPassportModule.class, ChequepayApiModule.class})
public interface RegPassportComponent {
    void inject(RegPassportFragment fragment);
}
