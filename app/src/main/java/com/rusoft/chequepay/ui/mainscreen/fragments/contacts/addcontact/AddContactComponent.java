package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {AddContactModule.class, ChequepayApiModule.class})
public interface AddContactComponent {
    void inject(AddContactFragment fragment);
}
