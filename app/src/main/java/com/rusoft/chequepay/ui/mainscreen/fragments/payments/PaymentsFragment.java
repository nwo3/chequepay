package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.data.event.DepositScreenEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


public class PaymentsFragment extends BaseRootFragment implements PaymentsContract.IView, TemplatesAdapter.ItemClickListener {

    @Inject
    PaymentsPresenter presenter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.deposit) View deposit;
    @BindView(R.id.withdraw) View withdraw;
    @BindView(R.id.transfer) View transfer;
    @BindView(R.id.invoice) View invoice;

    @BindView(R.id.recycler) RecyclerView recycler;
    @BindView(R.id.label) View emptyTemplates;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_payments;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_payments);
    }

    public static PaymentsFragment getNewInstance(String containerName) {
        PaymentsFragment paymentsFragment = new PaymentsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        paymentsFragment.setArguments(arguments);

        return paymentsFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerPaymentsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .paymentsModule(new PaymentsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreate();

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());

        deposit.setOnClickListener(view -> presenter.onClickMoneyDeposit());
        withdraw.setOnClickListener(view -> presenter.onClickMoneyWithdraw());
        transfer.setOnClickListener(view -> presenter.onClickMoneyTransfer());
        invoice.setOnClickListener(view -> presenter.onClickInvoice());

        deposit.setOnTouchListener(UiUtils.getTouchListener());
        withdraw.setOnTouchListener(UiUtils.getTouchListener());
        transfer.setOnTouchListener(UiUtils.getTouchListener());
        invoice.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void updateTemplatesRecyclerView(List<ShortTemplate> templates) {
        if (templates.size() > 0){
            emptyTemplates.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            recycler.setAdapter(new TemplatesAdapter(this, templates));
            recycler.setHasFixedSize(false);
            recycler.setNestedScrollingEnabled(false);

        } else {
            emptyTemplates.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTemplateClicked(ShortTemplate template) {
        presenter.onTemplateSelected(template);
    }

    @Override
    public void onTemplateLongClicked(ShortTemplate template) {
        UiUtils.showQuestionDialog(getContext(), R.string.dialog_on_template_delete, template.getName(),
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            presenter.onTemplateDeleteConfirmed(template);
                            break;
                    }
                });
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        super.showLoadingIndicator(enabled);
        if (!enabled) swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showRefillAccountsDialog() {
        super.showRefillAccountsDialog();
    }

    @Override
    public void onResume() {
        presenter.onResume();
        super.onResume();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Subscribe
    public void onEvent(DepositScreenEvent depositScreenEvent) {
        presenter.onClickMoneyDeposit();
    }
}
