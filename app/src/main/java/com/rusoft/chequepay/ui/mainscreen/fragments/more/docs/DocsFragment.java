package com.rusoft.chequepay.ui.mainscreen.fragments.more.docs;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.document.response.Document;
import com.rusoft.chequepay.data.event.ToolbarBtnOnClickEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.utils.LogUtil;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class DocsFragment extends BaseBranchFragment implements DocsContract.IView, DocsAdapter.ItemClickListener {

    @Inject
    DocsPresenter presenter;

    @BindView(R.id.label) View label;
    @BindView(R.id.recycler) RecyclerView recycler;

    @BindView(R.id.filter_container) View filterContainer;
    @BindView(R.id.type_category) StaticInputView selectorCategory;
    @BindView(R.id.status_selector) StaticInputView selectorStatus;
    @BindView(R.id.period_selector) StaticInputView selectorPeriod;
    @BindView(R.id.search) View search;

    private boolean isFilterShown;
    private boolean isLoading;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_docs;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_documents);
    }

    public static DocsFragment getNewInstance(String containerName) {
        DocsFragment fragment = new DocsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerDocsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .docsModule(new DocsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        selectorCategory.setOnClickListener(v -> presenter.onClickCategory());
        selectorStatus.setOnClickListener(v -> presenter.onClickStatus());
        selectorPeriod.setOnClickListener(v -> presenter.onClickPeriod());

        search.setOnClickListener(v -> presenter.onClickSearch());

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    if (!isLoading) presenter.onRecyclerBottomReached();
                }
            }
        });
    }

    @Override
    public void initializeDocumentsRecycler(List<Document> documents) {
        if (recycler == null) return;
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(new DocsAdapter(getContext(), this, documents));
        recycler.setHasFixedSize(true);
        recycler.setNestedScrollingEnabled(false);
    }

    @Override
    public void updateDocumentsRecycler() {
        if (recycler.getAdapter() == null) return;
        recycler.getAdapter().notifyDataSetChanged();
        showEmptyListLabel(recycler.getAdapter().getItemCount() == 0);
    }

    private void showEmptyListLabel(boolean show) {
        label.setVisibility(show ? View.VISIBLE : View.GONE);
        recycler.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDocSelected(long docId) {
        if (!isFilterShown) {
            presenter.onDocSelected(docId);
        } else {
            showFilter(false);
        }
    }

    @Override
    public void showCategoryPickerDialog(List<BusinessConstants.DocCategory> categories, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), categories, selectedIndex,
                selected -> presenter.onCategorySelected(categories.get(selected)));
    }

    @Override
    public void showStatusPickerDialog(List<BusinessConstants.DocStatus> statuses, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), statuses, selectedIndex,
                selected -> presenter.onStatusSelected(statuses.get(selected)));
    }

    @Override
    public void showPeriodPickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = DatePickerDialog.newInstance(
                (view, year, monthOfYear, dayOfMonth, yearEnd, monthOfYearEnd, dayOfMonthEnd) -> {
                    Date startDate = null;
                    try {
                        calendar.setTime(AppConstants.SDF_ddMMyyyy.parse(dayOfMonth + "." + (++monthOfYear) + "." + year));
                        calendar.set(Calendar.HOUR_OF_DAY, 0);
                        startDate = calendar.getTime();

                        calendar.setTime(AppConstants.SDF_ddMMyyyy.parse(dayOfMonthEnd + "." + (++monthOfYearEnd) + "." + yearEnd));
                        calendar.set(Calendar.HOUR_OF_DAY, 23);
                        Date endDate = calendar.getTime();
                        presenter.onPeriodSelected(startDate.getTime(), endDate.getTime());
                    } catch (ParseException e) {
                        LogUtil.e(e.getMessage());
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        dialog.setThemeDark(true);
        dialog.setStartTitle(getString(R.string.from));
        dialog.setEndTitle(getString(R.string.to));
        dialog.setMaxDate(calendar);

        dialog.show(getActivity().getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void setCategoryText(int textResId) {
        selectorCategory.setText(getString(textResId));
    }

    @Override
    public void setStatusText(int textResId) {
        selectorStatus.setText(getString(textResId));
    }

    @Override
    public void setPeriodText(String text) {
        selectorPeriod.setText(text);
    }

    @Override
    public void setPeriodError(Integer textResId) {
        selectorPeriod.setError(textResId != null ? getString(textResId) : null);
    }

    @Override
    public void enableSearchButton(boolean enable) {
        search.setEnabled(enable);
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        isLoading = enabled;
        super.showLoadingIndicator(enabled);
    }

    @Subscribe
    public void onEvent(ToolbarBtnOnClickEvent filterToggleEvent) {
        showFilter(!isFilterShown);
    }

    @Override
    public void showFilter(boolean show) {
        isFilterShown = show;

        if (filterContainer != null) {
            filterContainer.animate().translationY(show ? 0 : -filterContainer.getHeight());
        }
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).showToolbarBtn(R.drawable.filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity) getActivity()).hideToolbarBtn();
        presenter.unsubscribe();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

}
