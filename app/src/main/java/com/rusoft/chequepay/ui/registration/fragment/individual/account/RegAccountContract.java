package com.rusoft.chequepay.ui.registration.fragment.individual.account;

import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.repository.registration.model.AccountSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.List;

public interface RegAccountContract {

    interface IView extends IBaseRegView {

        void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, int selectedIndex);
        void showBankSelectorDialog(List<Bank> banks, int selectedIndex);
        void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex);

        void setCurrencyText(String text);
        void setBankText(String text);
        void setIssuerText(String text);

        void disableBankInput(boolean disabled);
        void disableIssuerInput(boolean disabled);

        void disableNext(boolean disabled);

        void goToOtp();

        void restoreView(AccountSubModel model);

    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickCurrency();
        void onCurrencySelected(CurrencyUtils.Currency currency);

        void onClickBank();
        void onBankSelected(Bank bank);

        void onClickIssuer();
        void onIssuerSelected(Issuer issuer);

        void onClickNext();

    }
}
