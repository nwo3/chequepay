package com.rusoft.chequepay.ui.otpscreen;


import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {OtpScreenModule.class, ChequepayApiModule.class})
public interface OtpScreenComponent {
    void inject(OtpActivity activity);
}
