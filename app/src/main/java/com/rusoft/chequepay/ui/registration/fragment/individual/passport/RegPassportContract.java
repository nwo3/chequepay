package com.rusoft.chequepay.ui.registration.fragment.individual.passport;

import com.rusoft.chequepay.data.repository.registration.model.PassportSubModel;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.registration.fragment.base.IBaseRegView;

public interface RegPassportContract {

    interface IView extends IBaseRegView {
        void initializeInputListeners();

        void updateViewByResidency(boolean isResident);

        void setErrorOnIssueDateInput(int textResId);

        void restoreState(PassportSubModel model);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onNumFocusLost(String text);
        void onSeriesFocusLost(String text);
        void onIssuerFocusLost(String text);
        void onDateSelected(long date);
        void onCodeFocusLost(String text);

        void onClickNext();
;
    }
}
