package com.rusoft.chequepay.ui.mainscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MainScreenModule {
    private final MainScreenContract.IView view;

    public MainScreenModule(MainScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    MainScreenContract.IView provideMainView() {
        return view;
    }
}
