package com.rusoft.chequepay.ui.authscreen;

import android.content.Intent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BuildConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BaseActivity;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.passrecovery.PassRecoveryActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class AuthActivity extends BaseActivity implements AuthScreenContract.IView {

    @Inject
    AuthScreenPresenter presenter;

    @BindView(R.id.scroll) ScrollView scroll;
    @BindView(R.id.logo) View logo;
    @BindView(R.id.login) DynamicInputView login;
    @BindView(R.id.password) DynamicInputView pass;
    @BindView(R.id.proceed) View proceed;
    @BindView(R.id.registration) TextView registration;
    @BindView(R.id.forgot_pass) TextView forgotPass;
    @BindView(R.id.help) TextView help;

    @Override
    protected int layoutResId() {
        return R.layout.activity_auth;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerAuthScreenComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .authScreenModule(new AuthScreenModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        if (BuildConstants.isDev()) {
            logo.setOnClickListener(view -> {
                login.setText(BuildConstants.TEST_LOGIN);
                pass.setText(BuildConstants.TEST_PASSWORD);
            });
        }

        login.setValidationPattern(ValidationPatterns.EMAIL);
        pass.setValidationPattern(ValidationPatterns.AMC_PASSWORD);

        presenter.onViewCreated(getIntent().getIntExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.NO_DATA));

        pass.getTextView().setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_DONE) {
                proceed.performClick();
                return true;
            }
            return false;
        });

        proceed.setOnClickListener(v -> {
            if (login.validate(true) && pass.validate(true)){
                presenter.onClickProceed(login.getText(), pass.getText());
            }
        });

        registration.setOnClickListener(v -> presenter.onClickReg());

        forgotPass.setOnClickListener(v -> presenter.onClickForgotPass());

        help.setOnClickListener(view -> presenter.onClickHelp());

        proceed.setOnTouchListener(UiUtils.getTouchListener());

        UiUtils.setKeyboardListener(scroll, new UiUtils.KeyboardListener() {
            @Override
            public void onKeyboardOpened() {
                help.setVisibility(View.GONE);
                registration.setVisibility(View.GONE);
                forgotPass.setVisibility(View.GONE);
            }

            @Override
            public void onKeyboardClosed() {
                help.setVisibility(View.VISIBLE);
                registration.setVisibility(View.VISIBLE);
                forgotPass.setVisibility(View.VISIBLE);
            }
        });

        registration.setOnTouchListener(UiUtils.getTouchListener());
        forgotPass.setOnTouchListener(UiUtils.getTouchListener());
        help.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(this, OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_AUTH);
        startActivity(intent);
    }

    @Override
    public void goToHelp() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Screens.KEY_FROM, Screens.AUTH_ACTIVITY);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToReg() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.REG_ACTIVITY_REGULAR);
        startActivity(intent);
    }

    @Override
    public void goToPassRecovery() {
        startActivity(new Intent(this, PassRecoveryActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    public void showInfoDialog(int textResId) {
        UiUtils.showInfoDialog(this, textResId, R.string.ok);
    }
}
