package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.data.entities.clientdata.response.Contractor;
import com.rusoft.chequepay.data.entities.correspondents.response.Account;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.List;

public interface OperationListContract {

    interface IView extends IBaseView {
        void showOperationList(List<Operation> operations);
        void showEmptyExcerptLabel();
        void showFilter(boolean show);

        void showAccountNameSelectorDialog(List<Account> accountList, int selectedIndex);
        void showTypeOperationSelectorDialog(List<BusinessConstants.OperationGroupType> operationTypeList, List<BusinessConstants.OperationGroupType> checkedOperationTypeList);
        void showContragentSelectorDialog(List<Contractor> contractorList, int selectedIndex);
        void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, List<CurrencyUtils.Currency> checkedCurrencies);
        void showPeriodDialog();
        void showDefaultData(Account account, Long dateStart, Long dateEnd, Contractor contractor, String operationTypes, String currencyList);

        void setAccountText(String accountName);
        void setContractorText(String contractorName);
        void setOperationTypesText(String operationTypes);
        void setCurrencyText(String currency);
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onClickOperation(long operationId);
        void onClickPeriod();
        void onClickAccountName();
        void onClickContragent();
        void onClickTypeOperation();
        void onClickCurrency();
        void saveDate(Long dataStart, Long dataEnd);

        void onAccountSelected(Account account);
        void onContractorSelected(Contractor contractor);
        void onOperationTypesSelected(List<BusinessConstants.OperationGroupType> operationTypes);
        void onCurrencySelected(List<CurrencyUtils.Currency> currencies);

        void initDefaultData();
        void getOperationList();
        void stateFilter(boolean filterIsShown);

        void onClickBack(boolean filterIsShown);
        void onDestroyView();
    }
}
