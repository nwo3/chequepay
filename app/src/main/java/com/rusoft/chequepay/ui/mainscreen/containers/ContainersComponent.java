package com.rusoft.chequepay.ui.mainscreen.containers;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class)
public interface ContainersComponent {
    void inject(EventsContainer fragment);
    void inject(AccountsContainer fragment);
    void inject(PaymentsContainer fragment);
    void inject(MoreContainer fragment);
    void inject(ContactsContainer fragment);
}
