package com.rusoft.chequepay.ui.mainscreen.fragments.accounts;

import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class AccountsPresenter extends BasePresenter implements AccountsContract.IPresenter {

    @Inject
    UserRepository userRepository;

    private WeakReference<AccountsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public AccountsPresenter(AccountsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(userRepository);
        updateViewData();
        updateUserAccounts(false);
    }

    @Override
    public void onRefresh() {
        updateUserAccounts(true);
    }

    private void updateUserAccounts(boolean forced) {
        userRepository.updateAccounts(forced, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
                updateViewData();
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    private void updateViewData() {
        if (view == null) return;

        UserModel user = userRepository.getUser();
        view.get().updateTotalBalanceSection(
                user.getCurrencyTotalBalance(CurrencyUtils.Currency.RUB.getCode()),
                user.getCurrencyTotalBalance(CurrencyUtils.Currency.USD.getCode()),
                user.getCurrencyTotalBalance(CurrencyUtils.Currency.EUR.getCode())
        );

        view.get().updateAccountsRecyclerView(user.getAccounts());
    }

    @Override
    public void onClickDeposit() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                .getRouter().newScreenChain(Screens.MONEY_DEPOSIT_FRAGMENT);
    }

    @Override
    public void onClickTransfer() {
        if (userRepository.getUser().checkSumOfAccountsGreaterThanZero()) {
            localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                    .getRouter().newScreenChain(Screens.MONEY_TRANSFER_FRAGMENT);
        } else {
            view.get().showRefillAccountsDialog();
        }
    }

    @Override
    public void onClickWithdraw() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                .getRouter().newScreenChain(Screens.MONEY_WITHDRAWAL_FRAGMENT);
    }

    @Override
    public void onClickInvoice() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                .getRouter().newScreenChain(Screens.INVOICE_FRAGMENT);
    }

    @Override
    public void onClickNewAccount() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                .getRouter().newScreenChain(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void onAccountSelected(Account account) {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER)
                .getRouter().newScreenChain(Screens.ACCOUNT_DETAIL_FRAGMENT, account);
    }

}
