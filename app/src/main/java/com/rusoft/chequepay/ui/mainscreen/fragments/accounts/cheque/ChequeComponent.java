package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.cheque;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {ChequeModule.class, ChequepayApiModule.class})
public interface ChequeComponent {
    void inject(ChequeFragment fragment);
}
