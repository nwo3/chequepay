package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.correspondentdetail;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.correspondents.response.CorrespondentInfo;
import com.rusoft.chequepay.data.entities.correspondents.response.InfoCorrespondentResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CommonUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class CorrespondentDetailPresenter extends BasePresenter implements CorrespondentDetailContract.IPresenter {

    @Inject
    ContractorRepository contractorRepository;

    @Inject
    UserRepository userRepository;

    private WeakReference<CorrespondentDetailContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;


    @Inject
    public CorrespondentDetailPresenter(CorrespondentDetailContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(contractorRepository, userRepository);
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().backTo(Screens.CONTACT_DETAIL_FRAGMENT);
    }

    @Override
    public void getTitle(long contractorId) {
        Correspondent correspondent = contractorRepository.getCorrespondentFromList(contractorId);
        if (correspondent != null) {
            view.get().updateToolbarTitle(CommonUtils.splitContragent(correspondent.getName()));
        }
    }

    @Override
    public void getContractorInfo(long contractorId) {
        contractorRepository.getInfoContractor(contractorId, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showSnackbar(R.string.error_contact_empty);
            }

            @Override
            public void onSuccess(BaseResponse response) {
                CorrespondentInfo correspondentInfo = ((InfoCorrespondentResponse) response).getCorrespondentInfo();
                view.get().showContractorInfo(correspondentInfo.getName(),
                        correspondentInfo.getWalletId(),
                        correspondentInfo.getTaxNumber(),
                        correspondentInfo.getComment(),
                        correspondentInfo.getAccounts());
            }
        });
    }

}
