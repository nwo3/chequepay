package com.rusoft.chequepay.ui.mainscreen.fragments.more.docs;

import android.os.Handler;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.documents.DocsFilterModel;
import com.rusoft.chequepay.data.repository.documents.DocumentsRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class DocsPresenter extends BasePresenter implements DocsContract.IPresenter {

    @Inject
    DocumentsRepository repository;

    private WeakReference<DocsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    private DocsFilterModel model;

    @Inject
    public DocsPresenter(DocsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(repository);
        model = repository.getModel();
        view.get().initializeDocumentsRecycler(repository.getDocuments());
        updateData();
    }

    private void updateData() {
        repository.updateDocumentsList(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
                view.get().updateDocumentsRecycler();
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onRecyclerBottomReached() {
        model.incrementPageNumber();
        updateData();

    }

    @Override
    public void onDocSelected(long docId) {
        localCiceroneHolder.getCicerone(Screens.MORE_CONTAINER).getRouter()
                .navigateTo(Screens.PDF_FRAGMENT, docId);
    }

    @Override
    public void onClickCategory() {
        List<BusinessConstants.DocCategory> categories = BusinessConstants.DocCategory.asList();
        int selectedIndex = categories.indexOf(model.getDocCategory());
        view.get().showCategoryPickerDialog(categories, selectedIndex);
    }

    @Override
    public void onCategorySelected(BusinessConstants.DocCategory category) {
        model.setDocCategory(category);
        view.get().setCategoryText(category.getTitleResId());
        updateSearchButtonState();
    }

    @Override
    public void onClickStatus() {
        List<BusinessConstants.DocStatus> statuses = BusinessConstants.DocStatus.asList();
        int selectedIndex = statuses.indexOf(model.getDocStatus());
        view.get().showStatusPickerDialog(statuses, selectedIndex);
    }

    @Override
    public void onStatusSelected(BusinessConstants.DocStatus status) {
        model.setDocStatus(status);
        view.get().setStatusText(status.getTitleResId());
        updateSearchButtonState();
    }

    @Override
    public void onClickPeriod() {
        view.get().showPeriodPickerDialog();
    }

    @Override
    public void onPeriodSelected(long dateStart, long dateFinish) {
        model.setDateStart(validatePeriod(dateStart, dateFinish) ? dateStart : null);
        model.setDateFinish(validatePeriod(dateStart, dateFinish) ? dateFinish : null);
        updateSearchButtonState();
        view.get().setPeriodText(AppConstants.SDF_ddMMyyyy.format(dateStart) + " - " + AppConstants.SDF_ddMMyyyy.format(dateFinish));
    }

    private boolean validatePeriod(long dateStart, long dateFinish) {
        if (dateStart > dateFinish) view.get().setPeriodError(R.string.error_period_date_1);
        else if (dateStart == dateFinish) view.get().setPeriodError(R.string.error_period_date_2);
        else view.get().setPeriodError(null);

        return dateFinish > dateStart;
    }

    @Override
    public void onClickSearch() {
        view.get().showFilter(false);
        updateData();
    }

    private void updateSearchButtonState() {
        view.get().enableSearchButton(model.isSearchEnabled());
    }

    private void recoverViewData() {
        view.get().setCategoryText(model.getDocCategory().getTitleResId());
        view.get().setStatusText(model.getDocStatus().getTitleResId());
        view.get().setPeriodText(AppConstants.SDF_ddMMyyyy.format(model.getDateStart()) + " - " + AppConstants.SDF_ddMMyyyy.format(model.getDateFinish()));
    }

    @Override
    public void onResume() {
        recoverViewData();

        new Handler().post(() -> view.get().showFilter(false));
    }

    @Override
    public void onClickBack() {
        unsubscribe();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
        repository.clearModel();
    }

}
