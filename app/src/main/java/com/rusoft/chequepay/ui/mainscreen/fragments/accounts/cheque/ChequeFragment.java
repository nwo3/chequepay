package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.cheque;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class ChequeFragment extends BaseBranchFragment implements ChequeContract.IView {

    @Inject
    ChequePresenter presenter;

    @BindView(R.id.cheque_id) TextView chequeId;
    @BindView(R.id.issuer_address) TextView issuerAdress;
    @BindView(R.id.issuer_requisites) TextView issuerRequisites;
    @BindView(R.id.payer) TextView payer;
    @BindView(R.id.text_data) TextView textData;
    @BindView(R.id.bank_account) TextView bankAccount;
    @BindView(R.id.sign) TextView sign;
    @BindView(R.id.email) TextView email;
    @BindView(R.id.phone) TextView phone;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_cheque;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_cheque);
    }

    public static ChequeFragment getNewInstance(String containerName,  Serializable data) {
        ChequeFragment fragment = new ChequeFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);

        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerChequeComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .chequeModule(new ChequeModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));
    }

    @Override
    public void setChequeIdText(String text) {
        chequeId.setText(text);
    }

    @Override
    public void setIssuerAddressText(String text) {
        issuerAdress.setText(text);
    }

    @Override
    public void setIssuerRequisitesText(String text) {
        issuerRequisites .setText(text);
    }

    @Override
    public void setPayerText(String text) {
        payer.setText(text);
    }

    @Override
    public void setAmountWordRepresentationText(String text) {
        textData.setText(Html.fromHtml(text));
    }

    @Override
    public void setBankAccountText(String text) {
        bankAccount.setText(text);
    }

    @Override
    public void setSignText(String text) {
        sign.setText(Html.fromHtml(text));
    }

    @Override
    public void setPhoneText(String text) {
        phone.setText(text);
    }

    @Override
    public void setEmailText(String text) {
        email.setText(text);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
