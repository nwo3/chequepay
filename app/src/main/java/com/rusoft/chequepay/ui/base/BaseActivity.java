package com.rusoft.chequepay.ui.base;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.utils.ErrorUtils;
import com.rusoft.chequepay.utils.LogUtil;
import com.rusoft.chequepay.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

public abstract class BaseActivity extends AppCompatActivity implements IBaseActivityView{
    @Nullable @BindView(R.id.progress) protected ProgressBar progress;
    private Dialog progressDialog;

    protected abstract int layoutResId();

    protected abstract void initializeDaggerComponent();

    protected abstract void initViews();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.i(getName() + " INITIALIZED");
        initializeDaggerComponent();
        setContentView(layoutResId());
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onDestroy() {
        LogUtil.i(getName() + " DESTROYED");
        super.onDestroy();
    }

    private String getName() {
        return this.getClass().getSimpleName();
    }

    public void showProgress (boolean enabled){
        if (progress != null){
            progress.bringToFront();
            progress.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
        }
    }

    public void showProgressDialog(boolean enabled){
        if (progressDialog == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(R.layout.dialog_progress);
            builder.setCancelable(false);
            progressDialog = builder.create();
        }

        if (enabled) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void showSnackbar (String text){
        UiUtils.hideKeyboard(this);
        final ViewGroup root = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        Snackbar snackBar = Snackbar.make(root, text, LENGTH_LONG).setDuration(AppConstants.MESSAGE_DURATION);
        ((TextView) snackBar.getView().findViewById(android.support.design.R.id.snackbar_text)).setSingleLine(false);
        snackBar.show();
    }

    public void showSnackbar(int textResId){
        showSnackbar(getString(textResId));
    }

    public void showError(Throwable throwable) {
        showSnackbar(ErrorUtils.getErrorText(this, throwable));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
