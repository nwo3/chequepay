package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import android.os.Handler;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.security.SecurityEditModel;
import com.rusoft.chequepay.data.repository.security.SecurityRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class SecurityPresenter extends BasePresenter implements SecurityContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    SecurityRepository securityRepository;

    private WeakReference<SecurityContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    private SecurityEditModel model;

    @Inject
    public SecurityPresenter(SecurityContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(userRepository,securityRepository);
        updateModelAndViewData();
        updateControlsState();

        userRepository.updateFullProfile(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
                updateControlsState();
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
                goBack();
            }

            @Override
            public void onSuccess() {
                updateModelAndViewData();
            }
        });
    }

    private void updateModelAndViewData() {
        model = securityRepository.getModel();
        model.update(userRepository.getUser().getFullProfile());

        view.get().changeFingerprintAuthenticationSwitchState(userRepository.isFingerprintAuthenticationEnabled());

        view.get().initializeIpTab(model.getAllowedIpAddresses());
        view.get().changeIpRestrictionSwitchState(model.isIpRestrictionEnabled());

        view.get().changeOtpSwitchState(model.isAuthByOtpEnabled());
        view.get().changeSmsNotificationSwitchState(model.isSmsNotificationEnabled());

        onClickPassTab();
        updateControlsState();
    }

    @Override
    public void onClickPassTab() {
        model.setActiveTab(SecurityEditModel.SecurityScreenActiveTab.PASS);
        view.get().showPassTab();
        updateControlsState();
    }

    @Override
    public void onClickIpTab() {
        model.setActiveTab(SecurityEditModel.SecurityScreenActiveTab.IP);
        view.get().showIpTab();
        updateControlsState();
    }

    @Override
    public void onClickSmsTab() {
        model.setActiveTab(SecurityEditModel.SecurityScreenActiveTab.SMS);
        view.get().showSmsTab();
        updateControlsState();
    }

    @Override
    public void onFingerprintSwitchStateChangedConfirm(boolean state) {
        userRepository.setFingerprintAuthenticationEnabled(state);
    }

    @Override
    public void onNewPassFocusLost(String text) {
        model.setNewPass(text);
        updateControlsState();
    }

    @Override
    public void onRepeatPassFocusLost(String text) {
        model.setRepeatPass(text);
        if (!model.isPassConfirmed())
            view.get().showErrorOnRepeatPassView(R.string.error_passwords_do_not_match);
        updateControlsState();
    }

    @Override
    public void onOldPassFocusLost(String text) {
        model.setOldPass(text);
        updateControlsState();
    }

    @Override
    public void onIPSwitchStateChanged(boolean state) {
        model.setIpRestrictionEnabled(state);
        view.get().changeIpRestrictionSwitchState(state);
        updateControlsState();
    }

    @Override
    public void onAddIP(String ip) {
        model.getAllowedIpAddresses().add(ip);
        view.get().updateIpRecycler();
        updateControlsState();
    }

    @Override
    public void onEditIp(int index, String newIp) {
        model.getAllowedIpAddresses().set(index, newIp);
        view.get().updateIpRecycler();
        updateControlsState();
    }

    @Override
    public void onDeleteIp(int pos) {
        model.getAllowedIpAddresses().remove(pos);
        view.get().updateIpRecycler();
        updateControlsState();
    }

    @Override
    public void onSmsSwitchStateChanged(boolean state) {
        model.setSmsNotificationEnabled(state);
        updateControlsState();
    }

    @Override
    public void onOtpSwitchStateChanged(boolean state) {
        model.setAuthByOtpEnabled(state);
        updateControlsState();
    }

    @Override
    public void onClickProceed() {
        securityRepository.changeSettings(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    private void updateControlsState() {
        view.get().enableProceed(model.isProceedShouldBeEnabled());
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model != null) {
            if (model.isEditingDone()) {
                //when settings editing successfully done
                new Handler().postDelayed(this::goBack, AppConstants.DURATION_FAST);
            } else {
                //when returning from OTP screen, edits not completed
                model.clearFlowToken();
            }
        }
    }

    private void goBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

    @Override
    public void onDestroy() {
        securityRepository.clearModel();
        super.onDestroy();
    }
}
