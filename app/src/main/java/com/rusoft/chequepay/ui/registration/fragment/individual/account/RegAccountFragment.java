package com.rusoft.chequepay.ui.registration.fragment.individual.account;

import android.content.Intent;
import android.support.annotation.Nullable;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.AccountSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.ui.registration.fragment.base.BaseRegFragment;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class RegAccountFragment extends BaseRegFragment implements RegAccountContract.IView {

    @Inject
    RegAccountPresenter presenter;

    @BindView(R.id.currency) StaticInputView currency;
    @BindView(R.id.bank_payer) StaticInputView bank_payer;
    @BindView(R.id.issuer) StaticInputView issuer;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_account;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.reg_title_account_data);
    }

    public static RegAccountFragment getNewInstance() {
        return new RegAccountFragment();
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegAccountComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regAccountModule(new RegAccountModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        ((RegistrationActivity) getActivity()).scrollToStart();

        currency.setOnClickListener(v -> presenter.onClickCurrency());
        bank_payer.setOnClickListener(v -> presenter.onClickBank());
        issuer.setOnClickListener(v -> presenter.onClickIssuer());
    }

    @Override
    public void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), currencies, selectedIndex,
                selected -> presenter.onCurrencySelected(currencies.get(selected)));
    }

    @Override
    public void showBankSelectorDialog(List<Bank> banks, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), banks, selectedIndex,
                selected -> presenter.onBankSelected(banks.get(selected)));
    }

    @Override
    public void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), issuers, selectedIndex,
                selected -> presenter.onIssuerSelected(issuers.get(selected)));
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
    }

    @Override
    public void setBankText(String text) {
        bank_payer.setText(text);
    }

    @Override
    public void setIssuerText(String text) {
        issuer.setText(text != null ? text : getString(R.string.choose_from_list_1));
    }

    @Override
    public void disableBankInput(boolean disabled) {
        bank_payer.setEnabled(!disabled);
    }

    @Override
    public void disableIssuerInput(boolean disabled) {
        issuer.setEnabled(!disabled);
    }

    @Override
    public void disableNext(boolean disabled) {
        ((RegistrationActivity) getActivity()).disableNext(disabled);
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_REGISTRATION);
        startActivity(intent);
    }

    @Override
    public void restoreView(AccountSubModel model) {
        setCurrencyText(model.getCurrency() != null ? model.getCurrency().toString() : getString(R.string.choose_from_list_1));
        setBankText(model.getIssuer() != null ? model.getIssuer().toString() : getString(R.string.determined_automatically));
        setIssuerText(model.getBank() != null ? model.getBank().toString() : getString(R.string.determined_automatically));
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        presenter.onClickNext();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public String getContainerName() {
        return Screens.REGISTRATION_CONTAINER;
    }
}
