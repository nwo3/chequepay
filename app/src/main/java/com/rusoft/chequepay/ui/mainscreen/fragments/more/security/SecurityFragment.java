package com.rusoft.chequepay.ui.mainscreen.fragments.more.security;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import info.hoang8f.android.segmented.SegmentedGroup;

public class SecurityFragment extends BaseBranchFragment implements SecurityContract.IView, IPsAdapter.ItemClickListener {

    @Inject
    SecurityPresenter presenter;

    @BindView(R.id.tabs) SegmentedGroup tabs;
    @BindView(R.id.tab_pass) RadioButton tabPass;
    @BindView(R.id.tab_ip) RadioButton tabIp;
    @BindView(R.id.tab_sms) RadioButton tabSms;

    @BindView(R.id.switch_fingerprint) SwitchCompat fingerprintSwitch;
    @BindView(R.id.label_fingerprint) View labelFingerprint;

    @BindView(R.id.tab_container_pass) View tabContainerPass;
    @BindView(R.id.tab_container_ip) View tabContainerIp;
    @BindView(R.id.tab_container_sms) View tabContainerSms;

    @BindView(R.id.pass_new) StaticInputView passNew;
    @BindView(R.id.pass_repeat) StaticInputView passRepeat;
    @BindView(R.id.pass_old) StaticInputView passOld;

    @BindView(R.id.switch_ip) SwitchCompat ipSwitch;
    @BindView(R.id.ip_container) ViewGroup ipContainer;
    @BindView(R.id.recycler) RecyclerView ipRecycler;
    @BindView(R.id.add_ip) View ipAdd;

    @BindView(R.id.switch_sms) SwitchCompat smsSwitch;
    @BindView(R.id.switch_otp_auth) SwitchCompat otpSwitch;

    @BindView(R.id.proceed) View proceed;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_security;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_security);
    }

    public static SecurityFragment getNewInstance(String containerName) {
        SecurityFragment fragment = new SecurityFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerSecurityComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .securityModule(new SecurityModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        otpSwitch.setVisibility(View.GONE); // Функция отключена в рамках задачи CP-347

        tabPass.setTextColor(getResources().getColor(R.color.white));
        tabIp.setTextColor(getResources().getColor(R.color.white));
        tabSms.setTextColor(getResources().getColor(R.color.white));
        tabs.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id){
                case R.id.tab_pass:
                    presenter.onClickPassTab();
                    break;

                case R.id.tab_ip:
                    presenter.onClickIpTab();
                    break;

                case R.id.tab_sms:
                    presenter.onClickSmsTab();
                    break;
            }
        });

        if (CommonUtils.checkFingerprintCompatibility(getContext())) {
            labelFingerprint.setVisibility(View.VISIBLE);
            fingerprintSwitch.setVisibility(View.VISIBLE);
            fingerprintSwitch.setOnCheckedChangeListener((v, checked) -> showFingerprintAuthenticationStateChangingDialog(checked));
        }

        passNew.setValidationPattern(ValidationPatterns.AMC_PASSWORD);
        passRepeat.setValidationPattern(ValidationPatterns.AMC_PASSWORD);

        passNew.setOnFocusLostListener(() -> {
            if (passNew != null && passNew.validate(false)) {
                presenter.onNewPassFocusLost(passNew.validate(false) ? passNew.getText() : null);
            }
        });

        passRepeat.setOnFocusLostListener(() -> {
            if (passRepeat != null) {
                presenter.onRepeatPassFocusLost(passRepeat.validate(false) ? passRepeat.getText() : null);
            }
        });

        passOld.setOnFocusLostListener(() -> {
            if (passOld != null) {
                presenter.onOldPassFocusLost(passOld.validate(false) ? passOld.getText() : null);
            }
        });

        ipSwitch.setOnCheckedChangeListener((v, checked) -> presenter.onIPSwitchStateChanged(checked));
        ipAdd.setOnClickListener(v -> showIpDialog(null));

        smsSwitch.setOnCheckedChangeListener((v, checked) -> presenter.onSmsSwitchStateChanged(checked));
        otpSwitch.setOnCheckedChangeListener((v, checked) -> presenter.onOtpSwitchStateChanged(checked));

        proceed.setOnClickListener(v -> showAreYouSureDialog());

        ipAdd.setOnTouchListener(UiUtils.getTouchListener());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    private void showAreYouSureDialog() {
        UiUtils.showQuestionDialog(getContext(), R.string.dialog_on_security_edit, null,
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            presenter.onClickProceed();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showFingerprintAuthenticationStateChangingDialog(boolean state) {
        UiUtils.showQuestionDialog(getContext(), getString(R.string.security_fingerprint_authentication_switch_message, getString(state ? R.string.on : R.string.off)),
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.onFingerprintSwitchStateChangedConfirm(state);
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            fingerprintSwitch.setOnCheckedChangeListener(null);
                            fingerprintSwitch.setChecked(!state);
                            fingerprintSwitch.setOnCheckedChangeListener((v, checked) -> showFingerprintAuthenticationStateChangingDialog(checked));
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showPassTab() {
        tabContainerPass.setVisibility(View.VISIBLE);
        tabContainerIp.setVisibility(View.GONE);
        tabContainerSms.setVisibility(View.GONE);
    }

    @Override
    public void showIpTab() {
        tabContainerPass.setVisibility(View.GONE);
        tabContainerIp.setVisibility(View.VISIBLE);
        tabContainerSms.setVisibility(View.GONE);
    }

    @Override
    public void showSmsTab() {
        tabContainerPass.setVisibility(View.GONE);
        tabContainerIp.setVisibility(View.GONE);
        tabContainerSms.setVisibility(View.VISIBLE);

    }

    @Override
    public void changeFingerprintAuthenticationSwitchState(boolean state) {
        fingerprintSwitch.setChecked(state);
    }

    @Override
    public void showErrorOnRepeatPassView(int textResId) {
        passRepeat.setError(getString(textResId));
    }

    @Override
    public void initializeIpTab(List<String> ips) {
        ipRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        ipRecycler.setAdapter(new IPsAdapter(ips, this));
        ipRecycler.setHasFixedSize(true);
    }

    @Override
    public void changeIpRestrictionSwitchState(boolean state) {
        ipSwitch.setChecked(state);

        if (state){
            UiUtils.expand(ipContainer);
        } else {
            UiUtils.collapse(ipContainer);
        }
    }

    @Override
    public void updateIpRecycler() {
        ipRecycler.getAdapter().notifyDataSetChanged();
    }

    private void showIpDialog(String text){
        showIpDialog(text, -1);
    }

    private void showIpDialog(String text, int index) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(this.getString(R.string.dialog_on_creating_IP_restriction_title));
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog_with_input, null, false);
        final EditText input = viewInflated.findViewById(R.id.input);
        input.setText(text);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (d, w) -> {
            String ip = input.getText().toString();
            if (index == -1) {
                presenter.onAddIP(ip);
            } else {
                presenter.onEditIp(index, ip);
            }
            d.dismiss();
        });

        builder.setNegativeButton(android.R.string.cancel, (d, w) -> d.cancel());

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(dialogInterface -> {
            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            String s = input.getText().toString();
            button.setEnabled(!s.isEmpty() && ValidationPatterns.validate(s, ValidationPatterns.IP_ADDRESS));

            input.addTextChangedListener((AfterTextChangedWatcher) editable -> {
                        String ip = input.getText().toString();
                        button.setEnabled(!ip.isEmpty() && ValidationPatterns.validate(ip, ValidationPatterns.IP_ADDRESS));
                    }
            );
        });

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    @Override
    public void onIpSelected(int pos, String ip) {
        showIpDialog(ip, pos);
    }

    @Override
    public void onIpDelete(int pos) {
        presenter.onDeleteIp(pos);
    }

    @Override
    public void changeSmsNotificationSwitchState(boolean state) {
        smsSwitch.setChecked(state);
    }

    @Override
    public void changeOtpSwitchState(boolean state) {
        otpSwitch.setChecked(state);
    }

    @Override
    public void enableProceed(boolean enabled) {
        proceed.setEnabled(enabled);
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_SECURITY_SETTINGS_CHANGE);
        startActivity(intent);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
