package com.rusoft.chequepay.ui.registration.fragment.individual.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.components.AfterTextChangedWatcher;
import com.rusoft.chequepay.components.input.DynamicInputView;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.info.Country;
import com.rusoft.chequepay.data.entities.info.Region;
import com.rusoft.chequepay.data.event.NextPressedEvent;
import com.rusoft.chequepay.data.repository.registration.model.AddressSubModel;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.ui.registration.RegistrationActivity;
import com.rusoft.chequepay.ui.registration.fragment.base.BaseRegFragment;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import lombok.AllArgsConstructor;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class RegAddressFragment extends BaseRegFragment implements RegAddressContract.IView {

    @Inject
    RegAddressPresenter presenter;

    @BindView(R.id.switch_) SwitchCompat swicher;

    @BindView(R.id.country) StaticInputView country;
    @BindView(R.id.country_subject) StaticInputView country_subject;
    @BindView(R.id.district) DynamicInputView district;
    @BindView(R.id.post_code) DynamicInputView post_code;
    @BindView(R.id.locality) DynamicInputView settlement;
    @BindView(R.id.address) DynamicInputView street;
    @BindView(R.id.focus_handler) View focus_handler;

    @AllArgsConstructor
    public enum ScreenType {
        REGISTRATION_ADDRESS(R.string.reg_title_registration_address),
        HOME_ADDRESS(R.string.reg_title_home_address);

        int toolbarTitleResId;
    }

    private ScreenType type;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_registration_address;
    }

    @Override
    protected String toolbarTitle() {
        return null;
    }

    public static RegAddressFragment getNewInstance(ScreenType type) {
        RegAddressFragment fragment = new RegAddressFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable(KEY_DATA, type.name());
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerRegAddressComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .regAddressModule(new RegAddressModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        type = ScreenType.valueOf(getArguments().getString(KEY_DATA));
        updateToolbarTitle(getContext().getString(type.toolbarTitleResId));
        ((RegistrationActivity) getActivity()).scrollToStart();

        presenter.onViewCreated(type);

        swicher.setOnCheckedChangeListener((compoundButton, state) -> presenter.onSwitchStateChanged(state));

        country.setOnClickListener(v -> presenter.onClickCountry());

        country_subject.setOnClickListener(v -> presenter.onClickRegion());

        district.setOnFocusLostListener(() -> {
            if (district != null) presenter.onDistrictFocusLost(district.getText());
        });

        post_code.setOnFocusLostListener(() -> {
            if (post_code != null) presenter.onPostCodeFocusLost(post_code.getText());
        });

        settlement.setOnFocusLostListener(() -> {
            if (settlement != null) presenter.onSettlementFocusLost(settlement.getText());
        });

        street.setOnFocusLostListener(() -> {
            if (street != null) presenter.onStreetFocusLost(street.getText());
        });

        street.getTextView().setOnEditorActionListener((textView, action, keyEvent) -> {
            if (action == EditorInfo.IME_ACTION_NEXT) {
                presenter.onStreetFocusLost(street.getText());
                return true;
            }
            return false;
        });

        street.addTextWatcher((AfterTextChangedWatcher) editable -> {
            if (street != null) presenter.onStreetFocusLost(street.getText());
        });

        district.setValidationPattern(ValidationPatterns.DISTRICT);
        settlement.setValidationPattern(ValidationPatterns.SETTLEMENT);
        street.setValidationPattern(ValidationPatterns.ADDRESS);
    }

    @Override
    public void updateViewByResidency(boolean isResident) {
        post_code.setValidationPattern(isResident ?
                ValidationPatterns.POST_CODE_RUS :
                ValidationPatterns.POST_CODE_OTHER);
    }

    @Override
    public void updateCountrySubjectState(boolean isFromRussia) {
        country_subject.setVisibility(isFromRussia ? View.VISIBLE : View.GONE);
        district.setVisibility(isFromRussia ? View.GONE : View.VISIBLE);
        district.setText("");
    }

    @Override
    public void lockFields(boolean locked) {
        country.setEnabled(!locked);
        country_subject.setEnabled(!locked);
        district.setEnabled(!locked);
        post_code.setEnabled(!locked);
        settlement.setEnabled(!locked);
        street.setEnabled(!locked);
    }

    @Override
    public void showSwitcher(boolean show) {
        swicher.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showCountriesPicker(List<Country> countries, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), countries, selectedIndex,
                selected -> presenter.onCountrySelected(countries.get(selected)));
    }

    @Override
    public void showRegionsPicker(List<Region> regions, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getContext(), regions, selectedIndex,
                selected -> presenter.onRegionSelected(regions.get(selected)));
    }

    @Override
    public void setCountryText(String text) {
        country.setText(text);
    }

    @Override
    public void setRegionText(String text) {
        country_subject.setText(text);
    }

    @Override
    public void restoreState(AddressSubModel model) {
        country.setText(model.getCountry() != null ? model.getCountry().getName() : null);
        country_subject.setText(model.getRegion() != null ? model.getRegion().getName() : null);
        district.setText(model.getRegion() != null ? model.getRegion().getName() : null);
        post_code.setText(model.getPostCode());
        settlement.setText(model.getSettlement());
        street.setText(model.getStreet());
        focus_handler.requestFocus();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Subscribe
    public void onEvent(NextPressedEvent event) {
        focus_handler.requestFocus();

        if (UiUtils.validateInputViews(country, country_subject, district, post_code, settlement, street)){
            presenter.onClickNext();
        }
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_PROFILE_EDIT);
        startActivity(intent);
    }

    @Override
    public void exit() {
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public String getContainerName() {
        return Screens.REGISTRATION_CONTAINER;
    }
}
