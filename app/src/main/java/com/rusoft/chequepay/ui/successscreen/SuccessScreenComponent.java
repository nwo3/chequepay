package com.rusoft.chequepay.ui.successscreen;


import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {SuccessScreenModule.class, ChequepayApiModule.class})
public interface SuccessScreenComponent {
    void inject(SuccessActivity activity);
}
