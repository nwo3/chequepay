package com.rusoft.chequepay.ui.mainscreen.fragments.more;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MoreModule {
    private final MoreContract.IView view;

    public MoreModule(MoreContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    MoreContract.IView provideMoreView() {
        return view;
    }
}
