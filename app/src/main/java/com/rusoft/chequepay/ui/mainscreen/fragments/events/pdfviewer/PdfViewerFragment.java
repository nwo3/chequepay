package com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.event.ToolbarBtnOnClickEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.AppConstants.REQUEST_CODE_SUCCESS;

public class PdfViewerFragment extends BaseBranchFragment implements PdfViewerContract.IView {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @BindView(R.id.pdfView) PDFView pdfView;
    @BindView(R.id.tv_error) TextView tvError;

    @Inject
    PdfViewerPresenter presenter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_webview;
    }

    @Override
    protected String toolbarTitle() {
        return "PDF";
    }

    public static PdfViewerFragment getNewInstance(String containerKey, long docId) {
        PdfViewerFragment fragment = new PdfViewerFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerKey);
        arguments.putLong(Screens.KEY_DATA, docId);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerPdfViewerComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .pdfViewerModule(new PdfViewerModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();
        presenter.showPdf(getArguments().getLong(Screens.KEY_DATA));
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Override
    public void showPdf(byte[] pdf) {
        pdfView.fromBytes(pdf).load();
    }

    @Override
    public void showError() {
        pdfView.setVisibility(View.GONE);
        tvError.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).showToolbarBtn(R.drawable.ic_upload_file);
    }

    @Override
    public void onStop() {
        ((MainActivity) getActivity()).hideToolbarBtn();
        presenter.unsubscribe();
        super.onStop();
    }

    @Subscribe
    public void onEvent(ToolbarBtnOnClickEvent clickEvent) {
        verifyStoragePermissions(getActivity());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_SUCCESS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            presenter.downloadPdf(getArguments().getLong(Screens.KEY_DATA));
        } else {
            showSnackbar(R.string.error_permission_savepdf);
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            PdfViewerFragment.this.requestPermissions(PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        } else {
            presenter.downloadPdf(getArguments().getLong(Screens.KEY_DATA));
        }
    }
}
