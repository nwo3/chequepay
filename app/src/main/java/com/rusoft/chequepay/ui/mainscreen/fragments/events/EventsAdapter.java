package com.rusoft.chequepay.ui.mainscreen.fragments.events;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.Date;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    public interface ItemClickListener {
        void eventSelected(long operationId, boolean isNewOperation);
    }

    private ItemClickListener callback;
    private List<Operation> operations;
    private boolean isMainEvents;
    private Resources resources;

    public EventsAdapter(Context context, List<Operation> operations, ItemClickListener callback, boolean isMainEvents) {
        this.resources = context.getResources();
        this.callback = callback;
        this.operations = operations;
        this.isMainEvents = isMainEvents;
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_operation_item, parent, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {
        Operation operation = operations.get(position);

        holder.container.setOnTouchListener((view, motionEvent) -> {
            int action = motionEvent.getAction();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    holder.container.setBackgroundColor(resources.getColor(R.color.dark_slate_blue_50));
                    return true;

                case MotionEvent.ACTION_UP:
                    holder.container.setBackgroundColor(operation.getNewOperation() ? resources.getColor(R.color.dark_slate_blue) : Color.TRANSPARENT);
                    view.performClick();
                    return true;

                //case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_CANCEL:
                    holder.container.setBackgroundColor(operation.getNewOperation() ? resources.getColor(R.color.dark_slate_blue) : Color.TRANSPARENT);
                    return false;
            }
            return false;
        });

        holder.tvType1.setText(operation.getAccountName());
        holder.tvType2.setText(operation.getOperationTypeName());
        holder.tvTypeName.setText(CommonUtils.splitContragent(operation.getContractor()));
        holder.tvTypeDate.setText(AppConstants.SDF_ddMMyyyy_HHmm.format(new Date(operation.getDate())));

        holder.container.setBackgroundColor(operation.getNewOperation() ? resources.getColor(R.color.dark_slate_blue) : Color.TRANSPARENT);

        if (Double.valueOf(operation.getDepositAmount()) > 0) {
            holder.tvTypeAmount.setTextColor(resources.getColor(R.color.bright_turquoise_text));
            holder.tvTypeAmount.setText(String.format("+ %s %s", CurrencyUtils.format(Double.valueOf(operation.getDepositAmount())),
                    CurrencyUtils.getCurrencySymbolFromAccountNumber(operation.getAccountNumber())));
        }

        if (Double.valueOf(operation.getWithdrawAmount()) > 0) {
            holder.tvTypeAmount.setTextColor(resources.getColor(R.color.white));
            holder.tvTypeAmount.setText(String.format("- %s %s", CurrencyUtils.format(Double.valueOf(operation.getWithdrawAmount())),
                    CurrencyUtils.getCurrencySymbolFromAccountNumber(operation.getAccountNumber())));
        }

        int left = (int) resources.getDimension(R.dimen.space_medium_m);
        int right = (int) resources.getDimension(R.dimen.space_medium_m);
        int top = (int) resources.getDimension(R.dimen.events_item_padding_top_and_bottom);
        int bottom = (++position) == operations.size() ? (int) resources.getDimension(R.dimen.buttons_height)
                + (int) resources.getDimension(R.dimen.space_medium_m)
                + (int) resources.getDimension(R.dimen.space_small_s)
                : (int) resources.getDimension(R.dimen.events_item_padding_top_and_bottom);
        holder.container.setPadding(left, top, right, isMainEvents ? bottom : (int) resources.getDimension(R.dimen.events_item_padding_top_and_bottom));
    }

    @Override
    public int getItemCount() {
        return operations.size();
    }

    class EventsViewHolder extends RecyclerView.ViewHolder {
        View container;
        TextView tvType1, tvType2, tvTypeDate, tvTypeAmount, tvTypeName;

        public EventsViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            tvType1 = itemView.findViewById(R.id.title);
            tvType2 = itemView.findViewById(R.id.type);
            tvTypeDate = itemView.findViewById(R.id.date);
            tvTypeAmount = itemView.findViewById(R.id.amount);
            tvTypeName = itemView.findViewById(R.id.name);
            itemView.setOnClickListener(v -> callback.eventSelected(operations.get(getAdapterPosition()).getOperationId(), operations.get(getAdapterPosition()).getNewOperation()));
        }
    }

    public static class DividerCustomItemDecoration extends RecyclerView.ItemDecoration {

        private Drawable divider;
        private int sidesMargin;

        public DividerCustomItemDecoration(Context context) {
            divider = ContextCompat.getDrawable(context, R.drawable.background_divider);
            sidesMargin = (int) context.getResources().getDimension(R.dimen.space_medium_m);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = sidesMargin;
            int right = parent.getWidth() - sidesMargin;

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }
}
