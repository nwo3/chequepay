package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.entities.templates.TransferTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransferModel;
import com.rusoft.chequepay.data.repository.transactions.transfers.TransfersRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class TransferPresenter extends BasePresenter implements TransferContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    TransfersRepository transfersRepository;

    private WeakReference<TransferContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;

    private TransferModel model;
    private boolean clearModel = true;

    @Inject
    public TransferPresenter(TransferContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, transfersRepository);
        this.model = transfersRepository.getModel();

        onModeSwitchStateChanged(false);

        UserModel user = userRepository.getUser();
        view.get().disableModeSwitch(user.getCorrespondents() == null || user.getCorrespondents().isEmpty());

        if (data == null) {
            onSourceAccountSelected(user.getOldestAccount());

        } else if (data instanceof Account) {
            onSourceAccountSelected((Account) data);

        } else if (data instanceof Correspondent) {
            Correspondent correspondentFromContacts = (Correspondent) data;
            onSourceAccountSelected(user.getOldestAccount());
            onModeSwitchStateChanged(true);
            onContactSelected(correspondentFromContacts);
            if (correspondentFromContacts.isFromContact()) {
                view.get().disableTargetSelectorAndSwitch();
            }
        } else if (data instanceof TransferTemplate) {
            transfersRepository.applyTemplate((TransferTemplate) data);
            view.get().applyTemplateMode();
            updateCommission();
            recoverViewData();
        }

        updateViewControlsState();
    }

    private void updateUserAccounts() {
        userRepository.updateAccounts(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }
        });
    }

    @Override
    public void onClickSource() {
        List<Account> accounts = userRepository.getUser().getAccounts();
        if (accounts != null && accounts.size() > 0) {
            int selectedIndex = accounts.indexOf(model.getSenderAccount());
            boolean isResident = userRepository.getUser().isResident();
            view.get().showSourceAccountSelectorDialog(accounts, selectedIndex, isResident);
        } else {
            view.get().showAddNewAccountDialog();
        }
    }

    @Override
    public void onSourceAccountSelected(Account account) {
        if (account == null) {
            return;
        }

        model.setSenderAccount(account);
        view.get().setSourceText(account.toString());
        view.get().setCurrencyText(CurrencyUtils.getName(account.getCurrency()));
        updateCommission();
        updateViewControlsState();

    }

    @Override
    public void onModeSwitchStateChanged(boolean state) {
        view.get().changeTargetInputMode(state);
        model.setInputModeManual(!state);
        updateModelAndViewNumericData();
        updateViewControlsState();
    }

    @Override
    public void onTargetManualFocusLost(String targetData) {
        ValidationPatterns dataType = ValidationPatterns.getType(targetData);
        if (dataType == null) {
            model.setReceiverAccountId(null);
            view.get().showTargetInputError(R.string.error_incorrect_requisites);
            return;
        }

        switch (dataType) {
            case ACCOUNT_ID:
                model.setReceiverAccountId(targetData);
                updateCommission();
                updateViewControlsState();
                break;

            case EMAIL:
            case PHONE_RUS:
            case PHONE_OTHER:
            case WALLET_ID:
                transfersRepository.getWalletInfo(dataType, targetData, new RequestCallbackListener() {
                    @Override
                    public void onPreExecute(Disposable disposable) {
                        view.get().showLoadingIndicator(true);
                    }

                    @Override
                    public void onPostExecute() {
                        view.get().showLoadingIndicator(false);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        model.setReceiverAccountId(null);
                        view.get().showTargetInputError(R.string.error_requisites_not_found);
                        view.get().showError(throwable);
                    }

                    @Override
                    public void onSuccess() {
                        updateCommission();
                        updateViewControlsState();
                        //todo handle receiverAccountId belongs to user
                        //todo handle разные currency, ошибка 2316
                    }
                });
                break;
        }
    }


    @Override
    public void onClickContactSelector() {
        List<Correspondent> correspondents = userRepository.getUser().getCorrespondents();
        if (correspondents != null && correspondents.size() > 0) {
            int selectedIndex = correspondents.indexOf(model.getChosenContact());
            view.get().showContactsSelectorDialog(correspondents, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_empty_list);
        }
    }

    @Override
    public void onContactSelected(Correspondent correspondent) {
        if (model.getChosenContact() != null && !model.getChosenContact().equals(correspondent)) {
            model.setReceiverAccountId(null);
            view.get().setContactAccountSelectorText(null);
        }

        view.get().setContactSelectorText(correspondent.toString());
        model.setChosenContact(correspondent);
        updateViewControlsState();
    }

    @Override
    public void onClickContactAccountSelector() {
        List<com.rusoft.chequepay.data.entities.correspondents.response.Account> contactAccounts = model.getChosenContact().getAccountsByCurrency(model.getCurrency());
        if (contactAccounts != null && contactAccounts.size() > 0) {
            int selectedIndex = contactAccounts.indexOf(model.getChosenContact().getAccountByNumber(model.getReceiverAccountId()));
            view.get().showContactAccountsSelectorDialog(contactAccounts, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_empty_list);
        }
    }

    @Override
    public void onContactAccountSelected(com.rusoft.chequepay.data.entities.correspondents.response.Account account) {
        view.get().setContactAccountSelectorText(account.getName());
        onTargetManualFocusLost(String.valueOf(account.getNumber()));
    }

    @Override
    public void onAmountFocusLost(String inputData) {
        if (ValidationPatterns.validate(inputData, ValidationPatterns.DOUBLE)) {
            Double amount = CurrencyUtils.getFormatDouble(inputData);

            if (CurrencyUtils.isAmountInRange(model.getCurrency(), amount)) {
                model.setAmount(amount);
            } else {
                view.get().showSnackbar(R.string.error_on_unacceptable_amount);
                model.setAmount(null);
            }
        } else {
            model.setAmount(null);
        }

        updateModelAndViewNumericData();
        updateViewControlsState();
    }

    @Override
    public void onCommentFocusLost(String inputData) {
        model.setComment(inputData);
        updateViewControlsState();
    }

    private void updateCommission() {
        transfersRepository.getCommission(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
                model.setCommission(null);
                updateModelAndViewNumericData();
                updateViewControlsState();
            }

            @Override
            public void onSuccess() {
                updateModelAndViewNumericData();
                updateViewControlsState();
            }
        });
    }

    @Override
    public void onClickProceed() {
        Account account = model.getSenderAccount();

        if (account.getAmount() < model.getAmount()) {
            view.get().showDepositDialog(account, false);
            return;
        }

        transfersRepository.sendTransfer(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    @Override
    public void clearModel() {
        if (clearModel) transfersRepository.clearModel();
    }

    private void updateModelAndViewNumericData() {
        if (view == null) return;

        boolean isUpdateSuccess = model.updateNumericData();
        view.get().setAmountText(isUpdateSuccess ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommissionText(isUpdateSuccess ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setTotalText(isUpdateSuccess ? CurrencyUtils.format(model.getTotal(), model.getCurrency()) : null);
    }

    private void updateViewControlsState() {
        if (view == null) return;

        view.get().disableModeSwitch(model.getSenderAccount() == null);
        view.get().disableTargetManualInput(model.getSenderAccount() == null);

        if (!model.isInputModeManual()) {
            view.get().disableContactAccountSelector(!isContactAccountSelectEnabled());
            view.get().setContactAccountSelectorText(isReceiverChosen() ? model.getChosenContact().getAccountNameByNumber(model.getReceiverAccountId()) : null);
            view.get().setContactAccountSelectorHint(!isContactAccountListEmpty(model.getChosenContact()) ? appContext.getString(R.string.choose_from_list_1) : appContext.getString(R.string.fragment_money_transfer_target_acc_error_text, CurrencyUtils.getSymbol(model.getCurrency())));
        }

        view.get().disableAmountInput(!isReceiverChosen());
        view.get().disableProceedButton(!model.isAllDataReceived());
        if (model.getReceiverAccountId() != null) {
            view.get().showTargetInputError(null);
        }
    }

    private boolean isReceiverChosen() {
        return model.getReceiverAccountId() != null && !model.getReceiverAccountId().isEmpty();
    }

    private boolean isContactAccountSelectEnabled() {
        return model.getChosenContact() != null && !isContactAccountListEmpty(model.getChosenContact());
    }

    private boolean isContactAccountListEmpty(Correspondent chosenContact) {
        return chosenContact != null && chosenContact.getAccounts() != null && model.getCurrency() != null && chosenContact.getAccountsByCurrency(model.getCurrency()).isEmpty();
    }

    private void recoverViewData() {
        if (view == null) return;

        view.get().setSourceText(model.getSenderAccount() != null ? model.getSenderAccount().toString() : null);
        view.get().setCurrencyText(model.getSenderAccount() != null ? CurrencyUtils.getName(model.getSenderAccount().getCurrency()) : null);

        if (!model.isTemplateUsed()) {
            if (model.isInputModeManual()) {
                view.get().setTargetManualText(model.getReceiverAccountId());

            } else {
                view.get().setContactSelectorText(model.getChosenContact() != null ? model.getChosenContact().toString() : null);
                view.get().setContactAccountSelectorText(getContactAccountText());
            }
        } else {
            Correspondent correspondent = userRepository.getUser().getCorrespondentByAccountNumber(model.getReceiverAccountId());
            view.get().setContactSelectorText(correspondent != null ? correspondent.toString() : model.getReceiverAccountId());
        }

        view.get().setAmountText(model.getAmount() != null ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommentText(model.getComment());
        view.get().setCommissionText(model.getCommissionAmount() != null ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setCurrencyText(model.getSenderAccount() != null ? CurrencyUtils.getName(model.getSenderAccount().getCurrency()) : null);
        view.get().setTotalText(model.getTotal() != null ? CurrencyUtils.format(model.getTotal(), model.getCurrency()) : null);
    }

    @Nullable
    private String getContactAccountText() {
        return model.getChosenContact() != null ?
                model.getChosenContact().getAccountNameByNumber(model.getReceiverAccountId()) :
                null;
    }

    @Override
    public void goToDeposit(Account account) {
        clearModel = false;
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.MONEY_DEPOSIT_FRAGMENT, account);
    }

    @Override
    public void goToNewAccount() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model.isTransactionDone()) {
            new Handler().post(this::goBack);
        } else {
            updateUserAccounts();
            model.clearFlowToken();
            recoverViewData();
        }
    }

    private void goBack() {
        transfersRepository.clearModel();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
