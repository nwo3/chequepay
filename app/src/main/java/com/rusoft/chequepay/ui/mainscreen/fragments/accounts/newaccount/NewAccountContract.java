package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount;

import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.util.List;

import io.reactivex.Observable;

public interface NewAccountContract {

    interface IView extends IBaseView {

        void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, int selectedIndex);
        void showBankSelectorDialog(List<Bank> banks, int selectedIndex);
        void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex);

        void setCurrencyText(String text);
        void setAccountNameText(String text);
        void setBankText(String text);
        void setIssuerText(String text);

        void disableBankInput(boolean disabled);
        void disableIssuerInput(boolean disabled);
        void disableProceed(boolean disabled);
    }

    interface IPresenter extends IBasePresenter {

        void onViewCreated();

        void onClickCurrency();
        void onCurrencySelected(CurrencyUtils.Currency currency);

        void setDataInput(Observable<String> nameObservable);

        void onClickBank();
        void onBankSelected(Bank bank);

        void onClickIssuer();
        void onIssuerSelected(Issuer issuer);

        void onClickProceed();

    }
}
