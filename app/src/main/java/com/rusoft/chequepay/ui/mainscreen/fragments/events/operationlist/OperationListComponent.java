package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {OperationListModule.class, ChequepayApiModule.class})
public interface OperationListComponent {
    void inject(OperationListFragment fragment);
}
