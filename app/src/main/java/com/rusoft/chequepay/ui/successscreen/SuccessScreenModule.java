package com.rusoft.chequepay.ui.successscreen;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class SuccessScreenModule {
    private final SuccessScreenContract.IView view;

    public SuccessScreenModule(SuccessScreenContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    SuccessScreenContract.IView provideView() {
        return view;
    }
}
