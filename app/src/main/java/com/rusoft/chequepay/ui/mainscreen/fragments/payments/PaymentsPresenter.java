package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import android.os.Handler;

import com.rusoft.chequepay.data.entities.templates.BaseTemplate;
import com.rusoft.chequepay.data.entities.templates.ShortTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.transactions.templates.TemplatesRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.flagsmanager.Flags;
import com.rusoft.chequepay.utils.flagsmanager.FlagsManager;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class PaymentsPresenter extends BasePresenter implements PaymentsContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    TemplatesRepository templatesRepository;

    private WeakReference<PaymentsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    @Inject
    public PaymentsPresenter(PaymentsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreate() {
        addRepositories(userRepository, templatesRepository);
        updateTemplatesList(false);
    }

    private void updateTemplatesList(boolean forced) {
        templatesRepository.getAllTemplates(forced, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                if (view.get() != null) view.get().updateTemplatesRecyclerView(userRepository.getUser().getTemplatesList());
            }
        });
    }

    @Override
    public void onRefresh() {
        updateTemplatesList(true);
    }

    @Override
    public void onClickMoneyDeposit() {
        goTo(Screens.MONEY_DEPOSIT_FRAGMENT, null);
    }

    @Override
    public void onClickMoneyTransfer() {
        if (userRepository.getUser().checkSumOfAccountsGreaterThanZero()) {
            goTo(Screens.MONEY_TRANSFER_FRAGMENT, null);
        } else {
            view.get().showRefillAccountsDialog();
        }
    }

    @Override
    public void onClickMoneyWithdraw() {
        goTo(Screens.MONEY_WITHDRAWAL_FRAGMENT, null);
    }

    @Override
    public void onClickInvoice() {
        goTo(Screens.INVOICE_FRAGMENT, null);
    }

    @Override
    public void onTemplateSelected(ShortTemplate template) {
        templatesRepository.getTemplate(template, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                BaseTemplate temp = userRepository.getUser().getTemplate(template.getTemplateId());
                if (temp != null){
                    switch (template.getTemplateType()){

                        case BUY:
                            goTo(Screens.MONEY_DEPOSIT_FRAGMENT, temp);
                            break;

                        case SALE:
                            goTo(Screens.MONEY_WITHDRAWAL_FRAGMENT, temp);
                            break;

                        case TRANSFER:
                            if (userRepository.getUser().checkSumOfAccountsGreaterThanZero()) {
                                goTo(Screens.MONEY_TRANSFER_FRAGMENT, temp);
                            } else {
                                view.get().showRefillAccountsDialog();
                            }
                            break;

                        case INVOICE:
                            goTo(Screens.INVOICE_FRAGMENT, temp);
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void onTemplateDeleteConfirmed(ShortTemplate template) {
        templatesRepository.deleteTemplate(template, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(Long result) {
                updateTemplatesList(true);
            }
        });
    }

    private void goTo(String screenKey, Serializable data) {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().newScreenChain(screenKey, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (FlagsManager.getInstance().isFlagOn(Flags.TEMPLATE)) {
            new Handler().post(() -> updateTemplatesList(false));
        }
    }
}
