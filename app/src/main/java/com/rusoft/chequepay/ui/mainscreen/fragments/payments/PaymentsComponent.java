package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

@CustomScope
@dagger.Component(dependencies = AppComponent.class, modules = {PaymentsModule.class, ChequepayApiModule.class})
public interface PaymentsComponent {
    void inject(PaymentsFragment fragment);
}
