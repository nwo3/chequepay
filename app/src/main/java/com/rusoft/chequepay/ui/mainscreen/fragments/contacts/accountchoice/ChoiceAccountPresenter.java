package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.BaseResponse;
import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.data.entities.wallets.response.WalletInfoByAccountResponse;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.contractors.CorrespondentModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ChoiceAccountPresenter extends BasePresenter implements ChoiceAccountContract.IPresenter {

    @Inject
    OperationRepository operationRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    ContractorRepository contractorRepository;

    private WeakReference<ChoiceAccountContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private CorrespondentModel correspondentModel;


    @Inject
    public ChoiceAccountPresenter(ChoiceAccountContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(operationRepository, userRepository, contractorRepository);
        correspondentModel = contractorRepository.getCorrespondentModel();
    }

    @Override
    public void onTextChanged(String accountName, String accountNumber) {
        view.get().enableAddButton(ValidationPatterns.validate(accountNumber, ValidationPatterns.ACCOUNT_ID) && !accountName.isEmpty());
    }

    @Override
    public void onAddAccount(String accountName, String accountNumber) {
        contractorRepository.getWalletInfoByAccount(accountNumber, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(BaseResponse response) {
                WalletInfoByAccountResponse walletInfoByAccountResponse = (WalletInfoByAccountResponse) response;
                String name = walletInfoByAccountResponse.getWalletShortNameRu();
                String walletId = walletInfoByAccountResponse.getWalletId();
                String inn = walletInfoByAccountResponse.getTaxNumber();
                List<Long> correspondentIdList = contractorRepository.getCorrespondentIdList();

                if ((correspondentModel.getAccountNumber().isEmpty() || correspondentModel.getAccountNumber().equals(walletId))
                        && !getSelectedAccounts().contains(accountNumber)
                        && (!correspondentIdList.contains(Long.valueOf(walletId)) || correspondentModel.isEdit())) {
                    correspondentModel.getAccounts().add(new Account(accountName, accountNumber));
                    correspondentModel.setInn(inn);
                    correspondentModel.setAccountNumber(walletId);
                    if (correspondentModel.getCorrespondentName().isEmpty()) {
                        correspondentModel.setCorrespondentName(name);
                    }
                    exit();
                } else if (correspondentIdList.contains(Long.valueOf(walletId)) && !correspondentModel.isEdit()) {
                    view.get().showSnackbar(R.string.error_contact);
                } else {
                    view.get().showSnackbar(R.string.error_number_account);
                }
            }
        });
    }

    @Override
    public void getFilteredOperations(String accountNumber) {
        List<Operation> filteredOperations = new ArrayList<>();
        List<Operation> operations = operationRepository.getUniqueOperationList(userRepository.getUser().getWalletId());
        List<Long> correspondentIdList = contractorRepository.getCorrespondentIdList();
        List<String> accountNumberList = getSelectedAccounts();

        if (correspondentModel.isEdit() || correspondentModel.getAccounts().size() > 0) {
            for (Operation item : operations) {
                if (!item.getContractorAccount().equals(accountNumber)
                        && (!accountNumberList.contains(item.getContractorAccount()))
                        && item.getContractorId().toString().equals(correspondentModel.getAccountNumber())) {
                    filteredOperations.add(item);
                }
            }
        } else {
            for (Operation item : operations) {
                if (!item.getContractorAccount().equals(accountNumber)
                        && (!accountNumberList.contains(item.getContractorAccount()))
                        && (!correspondentIdList.contains(item.getContractorId()))) {
                    filteredOperations.add(item);
                }
            }
        }

        if (filteredOperations.size() > 0) {
            view.get().showAccountList(filteredOperations);
        } else {
            view.get().showEmptyList();
        }
    }

    private List<String> getSelectedAccounts() {
        List<String> accountNumberList = new ArrayList<>();
        for (Account item : correspondentModel.getAccounts()) {
            accountNumberList.add(item.getNumber());
        }
        return accountNumberList;
    }

    private void exit() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

    @Override
    public void onClickBack() {
        exit();
    }

}
