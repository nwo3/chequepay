package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.listofcontactoperations;

import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.contractors.ContractorRepository;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.FilterDataModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ListOfContactOperationsPresenter extends BasePresenter implements ListOfContactOperationsContract.IPresenter {

    @Inject
    OperationRepository operationRepository;

    @Inject
    ContractorRepository contractorRepository;

    @Inject
    UserRepository userRepository;

    private WeakReference<ListOfContactOperationsContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private FilterDataModel filterDataModel;
    private DetailOperationModel detailOperationModel;


    @Inject
    public ListOfContactOperationsPresenter(ListOfContactOperationsContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated() {
        addRepositories(operationRepository, contractorRepository, userRepository);
        filterDataModel = operationRepository.getFilterDataModel();
        detailOperationModel = operationRepository.getDetailOperationModel();
    }

    @Override
    public void getOperationList(long correspondentId) {
        Calendar calendar = Calendar.getInstance();
        filterDataModel.setDateEnd(calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, -1);
        filterDataModel.setDateStart(calendar.getTimeInMillis());

        Correspondent correspondent = contractorRepository.getCorrespondentFromList(correspondentId);
        filterDataModel.setContractorId(correspondent.getWalletId());

        operationRepository.getOperationList(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                if (userRepository.getUser().getFilteredOperations().size() > 0) {
                    operationRepository.getFilterDataModel().setUpdated(true);
                    view.get().showOperationList(userRepository.getUser().getFilteredOperations());
                } else {
                    view.get().showEmptyOperationList();
                }
            }
        });
    }

    @Override
    public void onClickOperation(long operationId) {
        detailOperationModel.setOperationId(operationId);
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().navigateTo(Screens.OPERATION_DETAIL_FRAGMENT, operationId);
    }

    @Override
    public void onClickBack() {
        unsubscribe();
        filterDataModel.clear();
        localCiceroneHolder.getCicerone(Screens.CONTACTS_CONTAINER).getRouter().exit();
    }

}
