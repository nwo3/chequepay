package com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

public interface PdfViewerContract {

    interface IView extends IBaseView {
        void showPdf(byte[] pdf);
        void showError();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();
        void showPdf(Long idDownload);
        void downloadPdf(Long idDownload);
    }
}
