package com.rusoft.chequepay.ui.mainscreen.fragments.more;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.authscreen.AuthActivity;
import com.rusoft.chequepay.ui.base.fragments.BaseRootFragment;
import com.rusoft.chequepay.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;


public class MoreFragment extends BaseRootFragment implements MoreContract.IView {

    @Inject
    MorePresenter presenter;

    @BindView(R.id.profile) View profile;
    @BindView(R.id.security) View security;
    @BindView(R.id.docs) View docs;
    @BindView(R.id.faq) View faq;
    @BindView(R.id.feedback) View feedback;
    @BindView(R.id.logout) View logout;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_more;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_more);
    }

    public static MoreFragment getNewInstance(String containerName, String from) {
        MoreFragment moreFragment = new MoreFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putString(Screens.KEY_DATA, from);
        moreFragment.setArguments(arguments);

        return moreFragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerMoreComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .moreModule(new MoreModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getString(Screens.KEY_DATA));

        profile.setOnClickListener(v -> presenter.onClickProfile());
        security.setOnClickListener(v -> presenter.onClickSecurity());
        docs.setOnClickListener(v -> presenter.onClickDocs());
        faq.setOnClickListener(v -> presenter.onClickFaq());
        feedback.setOnClickListener(v -> presenter.onClickFeedback());
        logout.setOnClickListener(view -> UiUtils.showQuestionDialog(getContext(), R.string.dialog_on_exit, null,
                R.string.no, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            presenter.onClickLogout();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                }));

        profile.setOnTouchListener(UiUtils.getTouchListener());
        security.setOnTouchListener(UiUtils.getTouchListener());
        docs.setOnTouchListener(UiUtils.getTouchListener());
        faq.setOnTouchListener(UiUtils.getTouchListener());
        feedback.setOnTouchListener(UiUtils.getTouchListener());
        logout.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void goToAuth() {
        startActivity(new Intent(getContext(), AuthActivity.class));
        getActivity().finish();
    }
}
