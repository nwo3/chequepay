package com.rusoft.chequepay.ui.registration.fragment.individual.account;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.account.AccountRepository;
import com.rusoft.chequepay.data.repository.registration.RegistrationRepository;
import com.rusoft.chequepay.data.repository.registration.model.AccountSubModel;
import com.rusoft.chequepay.data.repository.registration.model.RegistrationModel;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public class RegAccountPresenter extends BasePresenter implements RegAccountContract.IPresenter {

    @Inject
    RegistrationRepository registrationRepository;

    @Inject
    AccountRepository accountRepository;

    @Inject
    UserRepository userRepository;

    private WeakReference<RegAccountContract.IView> view;
    private Router router;

    private RegistrationModel model;
    private AccountSubModel subModel;

    @Inject
    public RegAccountPresenter(RegAccountContract.IView view, Router router) {
        this.view = new WeakReference<>(view);
        this.router = router;
    }

    @Override
    public void onViewCreated() {
        model = registrationRepository.getModel();
        subModel = model.account;

        updateViewControlsState();
        onCurrencySelected(CurrencyUtils.getCurrencyByCountry(model.root.getResidencyCountry()));
    }

    @Override
    public void onClickCurrency() {
        List<CurrencyUtils.Currency> currencies = CurrencyUtils.Currency.asList();
        int selectedIndex = currencies.indexOf(subModel.getCurrency());
        view.get().showCurrencySelectorDialog(currencies, selectedIndex);
    }

    @Override
    public void onCurrencySelected(CurrencyUtils.Currency currency) {
        subModel.setCurrency(currency);
        view.get().setCurrencyText(currency.name());

        accountRepository.getBankIssuers(currency.getStrCode(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                List<Bank> banks = userRepository.getUser().getBanks(subModel.getCurrency().getStrCode());
                if (banks != null && !banks.isEmpty()) {
                    Bank bank = banks.get(0);
                    onBankSelected(bank);
                    updateViewControlsState();

                    List<Issuer> issuers = userRepository.getUser().getIssuersByBank(subModel.getCurrency().getStrCode(), bank);
                    if (issuers != null && !issuers.isEmpty()) {
                        Issuer issuer = issuers.get(0);
                        onIssuerSelected(issuer);
                        updateViewControlsState();
                    }
                }
            }
        });
    }

    @Override
    public void onClickBank() {
        List<Bank> banks = userRepository.getUser().getBanks(subModel.getCurrency().getStrCode());
        if (banks != null && !banks.isEmpty()){
            int selectedIndex = banks.indexOf(subModel.getBank());
            view.get().showBankSelectorDialog(banks, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_banks_found);
        }
    }

    @Override
    public void onBankSelected(Bank bank) {
        subModel.setBank(bank);
        view.get().setBankText(bank.toString());

        if (subModel.getIssuer() != null && !userRepository.getUser().isBankAndIssuerHasRelations(subModel.getIssuer(), subModel.getBank())){
            onIssuerSelected(null);
        }

        updateViewControlsState();
    }

    @Override
    public void onClickIssuer() {
        List<Issuer> issuers = userRepository.getUser().getIssuersByBank(subModel.getCurrency().getStrCode(), subModel.getBank());
        if (issuers != null && !issuers.isEmpty()){
            int selectedIndex = issuers.indexOf(subModel.getIssuer());
            view.get().showIssuerSelectorDialog(issuers, selectedIndex);
        } else {
            view.get().showSnackbar(R.string.error_no_issuers_found);
        }
    }

    @Override
    public void onIssuerSelected(Issuer issuer) {
        subModel.setIssuer(issuer);
        view.get().setIssuerText(issuer != null ? issuer.toString() : null);
        updateViewControlsState();
    }

    @Override
    public void onClickNext() {
        if (subModel.isRequiredDataReceived()){
            registrationRepository.registration(new RequestCallbackListener() {
                @Override
                public void onPreExecute(Disposable disposable) {
                    view.get().showProgressDialog(true);
                }

                @Override
                public void onPostExecute() {
                    view.get().showProgressDialog(false);
                }

                @Override
                public void onError(Throwable throwable) {
                    view.get().showError(throwable);
                }

                @Override
                public void onSuccess() {
                    view.get().goToOtp();
                }
            });
        } else {
            view.get().showSnackbar(R.string.error_on_check_required_data_fail);
        }
    }

    private void updateViewControlsState() {
        view.get().disableNext(!subModel.isRequiredDataReceived());

        UserModel user = userRepository.getUser();
        CurrencyUtils.Currency currency = subModel.getCurrency();

        if (currency != null){
            List<Bank> banks = user.getBanks(currency.getStrCode());
            view.get().disableBankInput(banks == null || banks.isEmpty());

            List<Issuer> issuers = user.getIssuers(currency.getStrCode());
            view.get().disableIssuerInput(issuers == null || banks.isEmpty());
        } else {
            view.get().disableBankInput(true);
            view.get().disableIssuerInput(true);
        }
    }


    @Override
    public void onClickBack() {
        router.exit();
    }

    @Override
    public void onResume() {
        view.get().restoreView(subModel);
        model.clearFlowToken();
    }

}
