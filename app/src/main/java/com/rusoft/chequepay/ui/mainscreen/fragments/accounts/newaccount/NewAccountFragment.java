package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount;

import android.os.Bundle;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class NewAccountFragment extends BaseBranchFragment implements NewAccountContract.IView {

    @Inject
    NewAccountPresenter presenter;

    @BindView(R.id.currency) StaticInputView currency;
    @BindView(R.id.name) StaticInputView name;
    @BindView(R.id.bank) StaticInputView bank;
    @BindView(R.id.issuer) StaticInputView issuer;

    @BindView(R.id.proceed) TextView proceed;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_account_new;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_new_account);
    }

    public static NewAccountFragment getNewInstance(String containerName) {
        NewAccountFragment fragment = new NewAccountFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerNewAccountComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .newAccountModule(new NewAccountModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated();

        currency.setOnClickListener(view -> presenter.onClickCurrency());

        presenter.setDataInput(RxTextView.textChanges(name.getTextView()).map(CharSequence::toString));
        UiUtils.setKeyboardDoneKeyListener(getContext(), name);

        bank.setOnClickListener(view -> presenter.onClickBank());
        issuer.setOnClickListener(view -> presenter.onClickIssuer());

        proceed.setOnClickListener(view -> presenter.onClickProceed());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void showCurrencySelectorDialog(List<CurrencyUtils.Currency> currencies, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), currencies, selectedIndex,
                selected -> presenter.onCurrencySelected(currencies.get(selected)));
    }

    @Override
    public void showBankSelectorDialog(List<Bank> banks, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), banks, selectedIndex,
                selected -> presenter.onBankSelected(banks.get(selected)));
    }

    @Override
    public void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), issuers, selectedIndex,
                selected -> presenter.onIssuerSelected(issuers.get(selected)));
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
    }

    @Override
    public void setAccountNameText(String text) {
        name.setText(text);
    }

    @Override
    public void setBankText(String text) {
        bank.setText(text);
    }

    @Override
    public void setIssuerText(String text) {
        issuer.setText(text);
    }

    @Override
    public void disableBankInput(boolean disabled) {
        bank.setEnabled(!disabled);
    }

    @Override
    public void disableIssuerInput(boolean disabled) {
        issuer.setEnabled(!disabled);
    }

    @Override
    public void disableProceed(boolean disabled) {
        proceed.setEnabled(!disabled);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
