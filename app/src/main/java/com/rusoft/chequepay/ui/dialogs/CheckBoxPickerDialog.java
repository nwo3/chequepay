package com.rusoft.chequepay.ui.dialogs;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.rusoft.chequepay.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lombok.Setter;

public class CheckBoxPickerDialog<T> extends BasePickerDialog {

    public interface PickerListener<T> {
        void onPickerConfirmClicked(List<T> selectedItems);
    }

    @BindView(R.id.container) View container;
    @BindView(R.id.check_all) CheckBox checkAll;
    @BindView(R.id.checkbox_group) LinearLayout checkboxGroup;

    @Setter private String checkAllTitle;
    @Setter private List<T> selectedItems;
    @Setter protected PickerListener listener;

    @Override
    protected void initializeContent() {
        checkAll.setVisibility(View.VISIBLE);
        checkAll.setChecked(false);
        checkAll.setOnCheckedChangeListener((view, checked) -> {
            for (int i = 0; i < checkboxGroup.getChildCount(); i++) {
                ((CheckBox) checkboxGroup.getChildAt(i)).setChecked(checked);
            }
        });

        for (int i = 0; i < items.size(); i++) {
            View cbView = getActivity().getLayoutInflater().inflate(R.layout.dialog_picker_checkbox, null);
            CheckBox checkBox = cbView.findViewById(R.id.checkbox);
            checkBox.setText(getItemTitle(items.get(i)));
            checkBox.setId(i + 100);
            checkBox.setChecked(isItemCheckedByDefault((T) items.get(i)));
            checkboxGroup.addView(checkBox);
        }


        confirm.setOnClickListener(view -> {
            listener.onPickerConfirmClicked(getSelectedItems());
            dismiss();
        });
    }

    private boolean isItemCheckedByDefault(T item){
        if (selectedItems != null && !selectedItems.isEmpty()){
            for (T defItem : selectedItems){
                if (getItemTitle(item).equals(getItemTitle(defItem))){
                    return true;
                }
            }
        } else {
            checkAll.setChecked(true);
        }

        return false;
    }

    private List<T> getSelectedItems(){
        List<T> selectedItems = new ArrayList<>();
        for (int i = 0; i < checkboxGroup.getChildCount(); i++){
            CheckBox checkBox = (CheckBox) checkboxGroup.getChildAt(i);
            if (checkBox.isChecked()){
                selectedItems.add((T) items.get(i));
            }
        }

        return selectedItems;
    }
}
