package com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class OperationListModule {
    private final OperationListContract.IView view;

    public OperationListModule(OperationListContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    OperationListContract.IView provideOperationListView() {
        return view;
    }
}
