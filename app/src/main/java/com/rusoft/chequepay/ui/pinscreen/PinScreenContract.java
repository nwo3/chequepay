package com.rusoft.chequepay.ui.pinscreen;

import com.rusoft.chequepay.ui.base.IBaseActivityView;
import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface PinScreenContract {

    interface IView extends IBaseActivityView{

        void setTitleText(int titleResId);

        void onWrongPin();
        void clearPin();

        void enableFingerprint(boolean enabled);
        void showFingerprintDialog();
        void askToEnableFingerprintAuthentication();

        void startUserDataReceiving();

        void goToMain();
        void goToAuth();
        void goToHelp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated();

        void onPinEntered(String code);
        void onFingerprintAuthenticationSuccess();

        void onBackPressed();
        void onClickHelp();

        void enableFingerprintAuthentication(boolean enable);

        void onTokenExpired();
    }

}
