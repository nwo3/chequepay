package com.rusoft.chequepay.ui.registration;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class,
        modules = {RegistrationModule.class, ChequepayApiModule.class})
public interface RegistrationComponent {
    void inject(RegistrationActivity activity);
}
