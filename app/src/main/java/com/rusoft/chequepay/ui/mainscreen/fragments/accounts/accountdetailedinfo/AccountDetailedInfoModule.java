package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetailedinfo;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountDetailedInfoModule {
    private final AccountDetailedInfoContract.IView view;

    public AccountDetailedInfoModule(AccountDetailedInfoContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    AccountDetailedInfoContract.IView provideView() {
        return view;
    }
}
