package com.rusoft.chequepay.ui.mainscreen.fragments.more.help;

import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class HelpPresenter extends BasePresenter implements HelpContract.IPresenter {

    private WeakReference<HelpContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;

    private String from;

    @Inject
    public HelpPresenter(HelpContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(String from) {
        this.from = from;
    }

    @Override
    public void onClickFaq() {
        localCiceroneHolder.getCicerone(Screens.MORE_CONTAINER)
                .getRouter().newScreenChain(Screens.FAQ_FRAGMENT);
    }

    @Override
    public void onClickFeedback() {
        localCiceroneHolder.getCicerone(Screens.MORE_CONTAINER)
                .getRouter().newScreenChain(Screens.FEEDBACK_FRAGMENT);
    }

    @Override
    public void onClickBack() {
        switch (from) {
            case Screens.AUTH_ACTIVITY:
                view.get().goToAuth();
                break;

            case AppConstants.FROM_PIN_TO_HELP:
                view.get().goToPin();
                break;

            default:
                break;
        }
    }
}
