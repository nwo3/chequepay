package com.rusoft.chequepay.ui.mainscreen.fragments.more.feedback;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {FeedbackModule.class, ChequepayApiModule.class})
public interface FeedbackComponent {
    void inject(FeedbackFragment fragment);
}
