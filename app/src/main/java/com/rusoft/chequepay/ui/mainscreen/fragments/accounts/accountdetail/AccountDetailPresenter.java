package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetail;

import android.content.Context;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.account.AccountRepository;
import com.rusoft.chequepay.data.repository.operations.DetailOperationModel;
import com.rusoft.chequepay.data.repository.operations.OperationRepository;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;
import com.rusoft.chequepay.utils.ErrorUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class AccountDetailPresenter extends BasePresenter implements AccountDetailContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    AccountRepository accountRepository;

    @Inject
    OperationRepository operationRepository;

    private WeakReference<AccountDetailContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private Context appContext;

    private Account account;
    private DetailOperationModel detailOperationModel;

    @Inject
    public AccountDetailPresenter(AccountDetailContract.IView view, LocalCiceroneHolder localCiceroneHolder, Context appContext) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
        this.appContext = appContext;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, accountRepository, operationRepository);

        detailOperationModel = operationRepository.getDetailOperationModel();
        if (data instanceof Account){
            Account account = (Account) data;
            updateViewTitle(account.getAccountNumber());

            CurrencyUtils.Currency currency = CurrencyUtils.getCurrency(account.getCurrency());
            boolean isResident = userRepository.getUser().isResident();
            view.get().enableDeposit(currency != null && (isResident == currency.isEnabledForResident()));
        } else {
            throw new IllegalArgumentException(ErrorUtils.WRONG_DATA_TYPE);
        }
    }

    private void updateViewTitle(String accountNumber) {
        account = userRepository.getUser().getAccountByNum(accountNumber);

        if (account != null) {
            view.get().setDetailsData(account.getName(), account.getAccountNumber(),
                    CurrencyUtils.format(account.getAmount(), account.getCurrency()));
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void onClickOperationsTab() {
        view.get().showOperationsTab();
    }

    @Override
    public void onClickExcerptTab() {
        view.get().showExcerptTab(userRepository.getUser().getAccountOperationsList(account.getAccountNumber()));
    }

    @Override
    public void onClickDetails() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                .navigateTo(Screens.ACCOUNT_DETAILED_INFO_FRAGMENT, account);
    }

    @Override
    public void onClickDeposit() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                .navigateTo(Screens.MONEY_DEPOSIT_FRAGMENT, account);
    }

    @Override
    public void onClickTransfer() {
        if (userRepository.getUser().checkSumOfAccountsGreaterThanZero()) {
            localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                    .navigateTo(Screens.MONEY_TRANSFER_FRAGMENT, account);
        } else {
            view.get().showRefillAccountsDialog();
        }
    }

    @Override
    public void onClickWithdraw() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                .navigateTo(Screens.MONEY_WITHDRAWAL_FRAGMENT, account);
    }

    @Override
    public void onClickInvoice() {
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                .navigateTo(Screens.INVOICE_FRAGMENT, account);
    }

    @Override
    public void onClickCheque() {
        userRepository.getChequeData(account.getAccountNumber(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                        .navigateTo(Screens.CHEQUE_FRAGMENT, account);
            }
        });
    }

    @Override
    public void onAccountCloseConfirmed() {
        accountRepository.closeAccount(account.getAccountNumber(), new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                        .exitWithMessage(appContext.getString(R.string.fragment_account_detail_on_close_message, account.getName()));
            }
        });
    }

    @Override
    public void onClickOperation(long operationId) {
        detailOperationModel.setOperationId(operationId);
        localCiceroneHolder.getCicerone(Screens.ACCOUNTS_CONTAINER).getRouter()
                .navigateTo(Screens.OPERATION_DETAIL_FRAGMENT, operationId);
    }

    @Override
    public void onResume() {
        //ignore
    }

    @Override
    public void onClickBack() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
