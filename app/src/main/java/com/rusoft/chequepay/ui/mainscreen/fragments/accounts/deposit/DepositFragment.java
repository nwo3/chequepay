package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.info.Bank;
import com.rusoft.chequepay.data.entities.info.Issuer;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class DepositFragment extends BaseBranchFragment implements DepositContract.IView {

    @Inject
    DepositPresenter presenter;

    @BindView(R.id.account) StaticInputView account;
    @BindView(R.id.issuer) StaticInputView issuer;
    @BindView(R.id.bank) StaticInputView bank;
    @BindView(R.id.mode) StaticInputView mode;
    @BindView(R.id.amount) StaticInputView amount;
    @BindView(R.id.focus_handler) View focusHandler;

    @BindView(R.id.commission) TextView commission;
    @BindView(R.id.currency) TextView currency;
    @BindView(R.id.total) TextView total;

    @BindView(R.id.commission_container) View commission_container;
    @BindView(R.id.currency_container) View currency_container;
    @BindView(R.id.total_container) View total_container;

    @BindView(R.id.proceed) TextView proceed;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_deposit;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_money_deposit);
    }

    public static DepositFragment getNewInstance(String containerName, Serializable data) {
        DepositFragment fragment = new DepositFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerDepositComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .depositModule(new DepositModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        account.setOnClickListener(view -> presenter.onClickAccount());
        issuer.setOnClickListener(view -> presenter.onClickIssuer());
        bank.setOnClickListener(view -> presenter.onClickBank());
        mode.setOnClickListener(view -> presenter.onClickMode());

        amount.setOnFocusChangedListener((view, focused) -> {
            if (amount != null){
                if (focused) amount.setText("");
                else presenter.onAmountFocusLost(amount.getText());
            }
        });

        amount.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                UiUtils.hideKeyboard(getContext());
                v.setFocusableInTouchMode(false);
                v.setFocusable(false);
                v.setFocusableInTouchMode(true);
                v.setFocusable(true);
                return true;
            }
            return false;
        });

        proceed.setOnClickListener(view -> presenter.onClickProceed());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void setAccountText(String text) {
        account.setText(text);
    }

    @Override
    public void setIssuerText(String text) {
        issuer.setText(text);
    }

    @Override
    public void setBankText(String text) {
        bank.setText(text);
    }

    @Override
    public void setModeText(Integer textResId) {
        if (textResId != null) {
            mode.setText(getString(textResId));
        }
    }

    @Override
    public void setAmountText(String text) {
        amount.setText(text);
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
    }

    @Override
    public void setCommissionText(String text) {
        commission.setText(text);
        commission_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setTotalText(String text) {
        total.setText(text);
        total_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onAccountSelected(accounts.get(selected)), isResident);
    }

    @Override
    public void showIssuerSelectorDialog(List<Issuer> issuers, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), issuers, selectedIndex,
                selected -> presenter.onIssuerSelected(issuers.get(selected)));
    }

    @Override
    public void showBankSelectorDialog(List<Bank> banks, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), banks, selectedIndex,
                selected -> presenter.onBankSelected(banks.get(selected)));
    }

    @Override
    public void showModeSelectorDialog(List<BusinessConstants.RefundMode> modes, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), modes, selectedIndex,
                selected -> presenter.onModeSelected(modes.get(selected)));
    }

    @Override
    public void disableAccountInput(boolean disabled) {
        account.setEnabled(!disabled);
    }

    @Override
    public void disableIssuerInput(boolean disabled) {
        issuer.setEnabled(!disabled);
    }

    @Override
    public void disableBankInput(boolean disabled) {
        bank.setEnabled(!disabled);
    }

    @Override
    public void disableModeInput(boolean disabled) {
        mode.setEnabled(!disabled);
    }

    @Override
    public void disableAmountInput(boolean disabled) {
        amount.setEnabled(!disabled);
    }

    @Override
    public void disableProceedButton(boolean disabled) {
        proceed.setEnabled(!disabled);
    }

    @Override
    public void showAddNewAccountDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_accounts, null,
                R.string.cancel, R.string.dialog_on_empty_accounts_accept,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToNewAccount();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_DEPOSIT);
        startActivity(intent);
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
