package com.rusoft.chequepay.ui.mainscreen;


import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface MainScreenContract {

    interface IView {
        void showUnreadIndicator(int count);
        void goToAuth();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(String from);
        void onClickNavAccounts();
        void onClickNavPayments();
        void onClickNavEvents();
        void onClickNavContacts();
        void onClickNavMore();
        void getUnreadMessages();

        void onCatchTokenExpiredEvent();
        void onCatchIpRestrictionEvent();
    }
}
