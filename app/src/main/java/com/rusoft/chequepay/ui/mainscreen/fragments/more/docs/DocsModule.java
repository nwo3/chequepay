package com.rusoft.chequepay.ui.mainscreen.fragments.more.docs;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DocsModule {
    private final DocsContract.IView view;

    public DocsModule(DocsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    DocsContract.IView provideView() {
        return view;
    }
}
