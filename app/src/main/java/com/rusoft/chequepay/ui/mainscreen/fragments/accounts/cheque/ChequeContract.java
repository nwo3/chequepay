package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.cheque;

import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;
import com.rusoft.chequepay.ui.base.fragments.IBaseView;

import java.io.Serializable;

public interface ChequeContract {

    interface IView extends IBaseView {
        void setChequeIdText(String text);
        void setIssuerAddressText(String text);
        void setIssuerRequisitesText(String text);
        void setPayerText(String text);
        void setAmountWordRepresentationText(String text);
        void setBankAccountText(String text);
        void setSignText(String text);
        void setEmailText(String text);
        void setPhoneText(String text);
    }

    interface IPresenter extends IBasePresenter {

        void onViewCreated(Serializable data);

    }
}
