package com.rusoft.chequepay.ui.registration;


import com.rusoft.chequepay.ui.base.fragments.IBasePresenter;

public interface RegistrationContract {

    interface IView {
        void disableNext(boolean disabled);
        void setNextButtonTitle(int titleResId);

        void goToAuth();
        void goToMain();
        void goToPin();

        void showProgressDialog(boolean enabled);
        void showError(Throwable throwable);

        void goToOtp();
    }

    interface IPresenter extends IBasePresenter {
        void onViewCreated(int key);
        void onClickNext();
        void onDestroy();
    }
}
