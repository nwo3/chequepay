package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.withdrawal;

import android.os.Handler;

import com.rusoft.chequepay.BusinessConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.ValidationPatterns;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.templates.SaleTemplate;
import com.rusoft.chequepay.data.network.RequestCallbackListener;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalModel;
import com.rusoft.chequepay.data.repository.transactions.withdrawal.WithdrawalRepository;
import com.rusoft.chequepay.data.repository.user.UserModel;
import com.rusoft.chequepay.data.repository.user.UserRepository;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.BasePresenter;
import com.rusoft.chequepay.utils.CurrencyUtils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class WithdrawalPresenter extends BasePresenter implements WithdrawalContract.IPresenter {

    @Inject
    UserRepository userRepository;

    @Inject
    WithdrawalRepository withdrawalRepository;

    private WeakReference<WithdrawalContract.IView> view;
    private LocalCiceroneHolder localCiceroneHolder;
    private WithdrawalModel model;

    @Inject
    public WithdrawalPresenter(WithdrawalContract.IView view, LocalCiceroneHolder localCiceroneHolder) {
        this.view = new WeakReference<>(view);
        this.localCiceroneHolder = localCiceroneHolder;
    }

    @Override
    public void onViewCreated(Serializable data) {
        addRepositories(userRepository, withdrawalRepository);

        this.model = withdrawalRepository.getModel();

        UserModel user = userRepository.getUser();

        if (data == null) {
            onAccountSelected(user.getOldestAccount());
            onModeSelected(BusinessConstants.RefundMode.TRANSFER);

        } else if (data instanceof Account) {
            onAccountSelected((Account) data);
            onModeSelected(BusinessConstants.RefundMode.TRANSFER);

        } else if (data instanceof SaleTemplate) {
            applyTemplate((SaleTemplate) data);
            updateEmitterData();
            recoverViewData();
        }

        updateViewControlsState();
    }

    private void applyTemplate(SaleTemplate template) {
        UserModel user = userRepository.getUser();
        Account account = user.getAccountByNum(template.getAccountNumber());
        if (account != null) {
            model.setAccount(account);
            model.setCurrency(template.getCurrency());
            model.setAmount(template.getAmount());
            model.setIssuerId(template.getReceiverId());
            model.setMode(BusinessConstants.RefundMode.getMode(template.getRefundMode()));
            model.setBankAccount(userRepository.getUser().getFullProfile().getBankAccount()); //TODO must use data from template
            model.setTemplateUsed(true);
        } else {
            view.get().showSnackbar(R.string.error_account_not_found);
        }
    }

    private void updateUserAccounts() {
        userRepository.updateAccounts(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

        });
    }

    @Override
    public void onClickAccount() {
        List<Account> accounts = userRepository.getUser().getAccounts();
        if (accounts != null && accounts.size() > 0){
            int selectedIndex = accounts.indexOf(model.getAccount());
            boolean isResident = userRepository.getUser().isResident();
            view.get().showAccountSelectorDialog(accounts, selectedIndex, isResident);
        } else {
            view.get().showAddNewAccountDialog();
        }
    }

    @Override
    public void onAccountSelected(Account account) {
        if (account == null) {
            return;
        }

        model.setAccount(account);
        model.setCurrency(CurrencyUtils.getStrCode(account.getCurrency()));
        view.get().setAccountText(account.toString());
        view.get().setCurrencyText(CurrencyUtils.getName(account.getCurrency()));

        if (account.getAmount() == 0) {
            view.get().showDepositDialog(account, true);
            return;
        }

        updateEmitterData();
        updateViewControlsState();
    }

    @Override
    public void onClickMode() {
        List<BusinessConstants.RefundMode> modes = BusinessConstants.RefundMode.asList();
        int selectedIndex = modes.indexOf(model.getMode());
        view.get().showModeSelectorDialog(modes, selectedIndex);
    }

    @Override
    public void onModeSelected(BusinessConstants.RefundMode mode) {
        model.setMode(mode);
        view.get().setModeText(mode.getTitleResId());
        updateViewControlsState();
    }

    @Override
    public void onAmountFocusLost(String inputData) {
        if (ValidationPatterns.validate(inputData, ValidationPatterns.DOUBLE)) {
            Double amount = Double.valueOf(inputData);

            if (CurrencyUtils.isAmountInRange(model.getCurrency(), amount)){
                model.setAmount(amount);
            } else {
                view.get().showSnackbar(R.string.error_on_unacceptable_amount);
                model.setAmount(null);
            }
        } else {
            model.setAmount(null);
        }

        updateModelAndViewNumericData();
        updateViewControlsState();
    }

    private void updateEmitterData() {
        withdrawalRepository.updateEmitterData(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                if (view.get() != null) view.get().setIssuerLabelText(model.getIssuerShortName());
                updateTariff();
                updateViewControlsState();
            }
        });
    }

    private void updateTariff(){
        withdrawalRepository.getTariff(new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showLoadingIndicator(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showLoadingIndicator(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess(Double tariff) {
                model.setTariff(tariff);
                updateModelAndViewNumericData();
                updateViewControlsState();
            }
        });
    }

    @Override
    public void onClickProceed() {
        Account account = model.getAccount();
        if (account.getAmount() < model.getAmount()) {
            view.get().showDepositDialog(account, false);
            return;
        }

        withdrawalRepository.createWithdraw(null, new RequestCallbackListener() {
            @Override
            public void onPreExecute(Disposable disposable) {
                view.get().showProgressDialog(true);
            }

            @Override
            public void onPostExecute() {
                view.get().showProgressDialog(false);
            }

            @Override
            public void onError(Throwable throwable) {
                view.get().showError(throwable);
            }

            @Override
            public void onSuccess() {
                view.get().goToOtp();
            }
        });
    }

    @Override
    public void onThirdPartySwitchStateChanged(boolean state) {
        //ignore
    }

    private void updateModelAndViewNumericData() {
        if (view == null) return;

        boolean isUpdateSuccess = model.updateNumericData();
        view.get().setAmountText(isUpdateSuccess ? CurrencyUtils.format(model.getAmount(), model.getCurrency()) : null);
        view.get().setCommissionText(isUpdateSuccess ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setTotalText(isUpdateSuccess ? CurrencyUtils.format(model.getPrice(), model.getCurrency()) : null);
        view.get().showCommission(model.getCommissionAmount() != null);
    }

    private void updateViewControlsState(){
        if (view == null) return;

        view.get().disableModeInput(model.getAccount() == null);
        view.get().disableAmountInput(model.getAccount() == null || model.getTariff() == null);
        view.get().disableProceedButton(!model.isAllDataReceived());
    }

    private void recoverViewData(){
        if (view == null) return;

        view.get().setAccountText(model.getAccount() != null ? model.getAccount().toString() : null);
        view.get().setCurrencyText(model.getAccount() != null ? CurrencyUtils.getName(model.getAccount().getCurrency()) : null);
        view.get().setIssuerLabelText(model.getIssuerShortName());
        view.get().setModeText(model.getMode() != null ? model.getMode().getTitleResId() : null);
        view.get().setAmountText(model.getAmount() != null ? CurrencyUtils.format(model.getAmount()) : null);
        view.get().setCommissionText(model.getCommissionAmount() != null ? CurrencyUtils.format(model.getCommissionAmount(), model.getCurrency()) : null);
        view.get().setTotalText(model.getPrice() != null ? CurrencyUtils.format(model.getPrice(), model.getCurrency()) : null);
    }

    @Override
    public void goToDeposit(Account account) {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.MONEY_DEPOSIT_FRAGMENT, account);
    }

    @Override
    public void goToNewAccount() {
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter()
                .navigateTo(Screens.NEW_ACCOUNT_FRAGMENT);
    }

    @Override
    public void onClickBack() {
        goBack();
    }

    @Override
    public void onResume() {
        if (model.isTransactionDone()) {
            new Handler().post(this::goBack);
        } else {
            updateUserAccounts();
            model.clearFlowToken();
            recoverViewData();
        }
    }

    private void goBack() {
        withdrawalRepository.clearModel();
        localCiceroneHolder.getCicerone(view.get().getContainerName()).getRouter().exit();
    }

}
