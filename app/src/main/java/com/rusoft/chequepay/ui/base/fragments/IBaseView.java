package com.rusoft.chequepay.ui.base.fragments;

public interface IBaseView {

    void showLoadingIndicator(boolean enabled);
    void showProgressDialog(boolean enabled);
    void showError(Throwable throwable);
    void showSnackbar(int textTextResId);
    void showSnackbar(String text);
    void showRefillAccountsDialog();
    void updateToolbarTitle(String text);

    String getContainerName();

}
