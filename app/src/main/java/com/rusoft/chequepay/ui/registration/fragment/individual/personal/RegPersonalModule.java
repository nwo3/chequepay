package com.rusoft.chequepay.ui.registration.fragment.individual.personal;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegPersonalModule {
    private final RegPersonalContract.IView view;

    public RegPersonalModule(RegPersonalContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegPersonalContract.IView provideView() {
        return view;
    }
}
