package com.rusoft.chequepay.ui.mainscreen.containers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.application.invoice_3.Application;
import com.rusoft.chequepay.data.entities.correspondents.request.Account;
import com.rusoft.chequepay.di.subnavigation.LocalCiceroneHolder;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseContainerFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.deposit.DepositFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount.NewAccountFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.contacts.accountchoice.ChoiceAccountFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.contacts.addcontact.AddContactFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.EventsFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.activities.ActivitiesFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay.InvoicePayFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.operationdetail.OperationDetailFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.operationlist.OperationListFragment;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.pdfviewer.PdfViewerFragment;

import java.io.Serializable;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class EventsContainer extends BaseContainerFragment {

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    private Navigator navigator;

    public static EventsContainer getNewInstance(String name) {
        EventsContainer eventsContainer = new EventsContainer();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.EXTRA_NAME, name);
        eventsContainer.setArguments(arguments);

        return eventsContainer;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerContainersComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_container;
    }

    @Override
    protected String toolbarTitle() {
        return null;
    }

    @Override
    protected void initViews() {
        //ignore
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.container) {

                @Override
                protected Intent createActivityIntent(Context context, String screenKey, Object data) {
                    return null;
                }

                @Override
                protected Fragment createFragment(String screenKey, Object data) {
                    ((MainActivity) getActivity()).setFragmentKeyEvents(screenKey);
                    switch (screenKey) {
                        case Screens.EVENTS_FRAGMENT:
                            return EventsFragment.getNewInstance(Screens.EVENTS_CONTAINER);

                        case Screens.ACTIVITIES_FRAGMENT:
                            return ActivitiesFragment.getNewInstance(Screens.EVENTS_CONTAINER);

                        case Screens.OPERATION_DETAIL_FRAGMENT:
                            return OperationDetailFragment.getNewInstance(Screens.EVENTS_CONTAINER);

                        case Screens.OPERATION_LIST_FRAGMENT:
                            return OperationListFragment.getNewInstance(Screens.EVENTS_CONTAINER);

                        case Screens.ADD_CONTACT_FRAGMENT:
                            return AddContactFragment.getNewInstance(Screens.EVENTS_CONTAINER, (Account) data);

                        case Screens.ACCOUNT_LIST_FRAGMENT:
                            return ChoiceAccountFragment.getNewInstance(Screens.EVENTS_CONTAINER, (String) data);

                        case Screens.INVOICE_PAY_FRAGMENT:
                            return InvoicePayFragment.getNewInstance(Screens.EVENTS_CONTAINER, (Application) data);

                        case Screens.PDF_FRAGMENT:
                            return PdfViewerFragment.getNewInstance(Screens.EVENTS_CONTAINER, (long) data);

                        case Screens.MONEY_DEPOSIT_FRAGMENT:
                            return DepositFragment.getNewInstance(Screens.EVENTS_CONTAINER, (Serializable) data);

                        case Screens.NEW_ACCOUNT_FRAGMENT:
                            return NewAccountFragment.getNewInstance(Screens.EVENTS_CONTAINER);

                        default:
                            throw new RuntimeException();
                    }
                }

                @Override
                protected void showSystemMessage(String message) {
                    showSnackbar(message);
                }

                @Override
                protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
                    if (!(command instanceof Replace)) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right);
                    }
                }

                @Override
                protected void exit() {
                    //ignore
                }
            };
        }
        return navigator;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.container) == null) {
            ciceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getRouter().replaceScreen(Screens.EVENTS_FRAGMENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ciceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        ciceroneHolder.getCicerone(Screens.EVENTS_CONTAINER).getNavigatorHolder().removeNavigator();
        super.onPause();
    }
}
