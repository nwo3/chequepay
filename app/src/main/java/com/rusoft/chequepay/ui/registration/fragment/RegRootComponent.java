package com.rusoft.chequepay.ui.registration.fragment;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {RegRootModule.class, ChequepayApiModule.class})
public interface RegRootComponent {
    void inject(RegRootFragment fragment);
}
