package com.rusoft.chequepay.ui.mainscreen.fragments.payments;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentsModule {
    private final PaymentsContract.IView view;

    public PaymentsModule(PaymentsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    PaymentsContract.IView providePaymentsView() {
        return view;
    }
}
