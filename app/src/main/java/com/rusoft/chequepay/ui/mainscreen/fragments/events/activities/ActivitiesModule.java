package com.rusoft.chequepay.ui.mainscreen.fragments.events.activities;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivitiesModule {
    private final ActivitiesContract.IView view;

    public ActivitiesModule(ActivitiesContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ActivitiesContract.IView provideActivitiesView() {
        return view;
    }
}
