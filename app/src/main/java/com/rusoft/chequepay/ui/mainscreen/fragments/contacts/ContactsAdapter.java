package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.utils.UiUtils;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder> {

    interface IContactsAdapter {
        void onClickItem(long contractorId);
    }

    private IContactsAdapter callback;
    private List<Correspondent> correspondents;

    public ContactsAdapter(IContactsAdapter callback, List<Correspondent> contractors) {
        this.callback = callback;
        this.correspondents = contractors;
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_conntragent_item, parent, false);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {
        Correspondent correspondent = correspondents.get(position);
        holder.container.setOnTouchListener(UiUtils.getTouchListener());
        holder.tvTitle.setText(correspondent.getName());
        holder.tvAccount.setText(String.valueOf(correspondent.getWalletId()));
    }

    @Override
    public int getItemCount() {
        return correspondents.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder {
        View container;
        TextView tvTitle, tvAccount;
        public ContactsViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            tvTitle = itemView.findViewById(R.id.title);
            tvAccount = itemView.findViewById(R.id.account);
            itemView.setOnClickListener(v -> {
                callback.onClickItem(correspondents.get(getAdapterPosition()).getWalletId());
            });
        }
    }
}
