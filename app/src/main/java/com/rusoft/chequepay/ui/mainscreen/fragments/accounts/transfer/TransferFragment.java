package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.transfer;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.OnTextChangedWatcher;
import com.rusoft.chequepay.components.input.CommentInputView;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.entities.correspondents.response.Correspondent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.UiUtils;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class TransferFragment extends BaseBranchFragment implements TransferContract.IView {

    @Inject
    TransferPresenter presenter;

    @BindView(R.id.source) StaticInputView source;
    @BindView(R.id.switch_) SwitchCompat modeSwitch;
    @BindView(R.id.target_manual) StaticInputView target_manual;
    @BindView(R.id.target_selector) StaticInputView target_selector;
    @BindView(R.id.target_acc_selector) StaticInputView target_acc_selector;
    @BindView(R.id.amount) StaticInputView amount;
    @BindView(R.id.comment) CommentInputView comment;

    @BindView(R.id.commission) TextView commission;
    @BindView(R.id.currency) TextView currency;
    @BindView(R.id.total) TextView total;

    @BindView(R.id.commission_container) View commission_container;
    @BindView(R.id.currency_container) View currency_container;
    @BindView(R.id.total_container) View total_container;

    @BindView(R.id.proceed) TextView proceed;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_transfer;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_money_transfer);
    }

    public static TransferFragment getNewInstance(String containerName, Serializable data) {
        TransferFragment fragment = new TransferFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerTransferComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .transferModule(new TransferModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        source.setOnClickListener(view -> presenter.onClickSource());

        modeSwitch.setOnCheckedChangeListener((v, state) -> presenter.onModeSwitchStateChanged(state));

        target_manual.setOnFocusChangedListener((view, focus) -> {
            if (target_manual != null && target_manual.getText() != null && !focus){
                presenter.onTargetManualFocusLost(target_manual.getText());
            }
        });

        target_selector.setOnClickListener(view -> presenter.onClickContactSelector());
        target_acc_selector.setOnClickListener(view -> presenter.onClickContactAccountSelector());

        amount.setOnFocusChangedListener((view, focused) -> {
            if (amount != null){
                if (focused) amount.setText("");
                else presenter.onAmountFocusLost(amount.getText());
            }
        });

        amount.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                UiUtils.hideKeyboard(getContext());
                v.clearFocus();
                return true;
            }
            return false;
        });

        UiUtils.setKeyboardDoneKeyListener(getContext(), comment);
        comment.addTextWatcher((OnTextChangedWatcher) (charSequence, i, i1, i2) -> {
            if (comment != null) {
                presenter.onCommentFocusLost(comment.getText());
            }
        });

        proceed.setOnClickListener(view -> presenter.onClickProceed());
        proceed.setOnTouchListener(UiUtils.getTouchListener());
    }

    @Override
    public void changeTargetInputMode(boolean state){
        modeSwitch.setChecked(state);
        target_manual.setVisibility(state ? View.GONE : View.VISIBLE);
        target_selector.setVisibility(state ? View.VISIBLE : View.GONE);
        target_acc_selector.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    @Override
    public void applyTemplateMode() {
        modeSwitch.setVisibility(View.GONE);
        target_manual.setVisibility(View.GONE);
        target_acc_selector.setVisibility(View.GONE);
        target_selector.setVisibility(View.VISIBLE);
        target_selector.setEnabled(false);
    }

    @Override
    public void setSourceText(String text) {
        source.setText(text);
    }

    @Override
    public void setTargetManualText(String text) {
        target_manual.setText(text);
    }

    @Override
    public void setContactSelectorText(String text) {
        target_selector.setText(text);
    }

    @Override
    public void setContactAccountSelectorText(String text) {
        target_acc_selector.setText(text);
    }

    @Override
    public void setContactAccountSelectorHint(String text) {
        target_acc_selector.setHint(text);
    }

    @Override
    public void setAmountText(String text) {
        amount.setText(text);
    }

    @Override
    public void setCommentText(String text) {
        comment.setText(text);
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
    }

    @Override
    public void setCommissionText(String text) {
        commission.setText(text);
        commission_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setTotalText(String text) {
        total.setText(text);
        total_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showSourceAccountSelectorDialog(List<Account> accounts, int selectedIndex, boolean isResident) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onSourceAccountSelected(accounts.get(selected)));
    }

    @Override
    public void showContactsSelectorDialog(List<Correspondent> correspondents, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), correspondents, selectedIndex,
                selected -> presenter.onContactSelected(correspondents.get(selected)));
    }

    @Override
    public void showContactAccountsSelectorDialog(List<com.rusoft.chequepay.data.entities.correspondents.response.Account> accounts, int selectedIndex) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onContactAccountSelected(accounts.get(selected)));
    }

    @Override
    public void showTargetInputError(Integer textResId) {
        target_manual.setError(textResId != null ? getString(textResId) : null);
    }

    @Override
    public void disableSourceAccountInput(boolean disabled) {
        source.setEnabled(!disabled);
    }

    @Override
    public void disableTargetManualInput(boolean disabled) {
        target_manual.setEnabled(!disabled);
    }

    @Override
    public void disableTargetSelectorAndSwitch() {
        target_selector.setEnabled(false);
        modeSwitch.setVisibility(View.GONE);
    }

    @Override
    public void disableContactAccountSelector(boolean disabled) {
        target_acc_selector.setEnabled(!disabled);
    }

    @Override
    public void disableAmountInput(boolean disabled) {
        amount.setEnabled(!disabled);
    }

    @Override
    public void disableModeSwitch(boolean disabled) {
        modeSwitch.setEnabled(!disabled);
        modeSwitch.setAlpha(!disabled ? AppConstants.ENABLED_VIEW_ALPHA : AppConstants.DISABLED_VIEW_ALPHA);
    }

    @Override
    public void disableProceedButton(boolean disabled) {
        proceed.setEnabled(!disabled);
    }

    @Override
    public void showAddNewAccountDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_accounts, null,
                R.string.cancel, R.string.dialog_on_empty_accounts_accept,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToNewAccount();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showDepositDialog(Account account, boolean isInfoDialogType) {
        if (isInfoDialogType){
            UiUtils.showInfoDialog(getContext(), R.string.dialog_on_account_with_not_enough_funds, R.string.ok);

        } else {
            UiUtils.showQuestionDialog(getContext(),
                    R.string.dialog_on_account_with_not_enough_funds, null,
                    R.string.cancel, R.string.deposit_1,
                    true, (dialog, i) -> {
                        switch (i) {
                            case DialogInterface.BUTTON_POSITIVE:
                                presenter.goToDeposit(account);
                                dialog.dismiss();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    });
        }
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_TRANSFER);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        presenter.clearModel();
        super.onDestroyView();
    }
}
