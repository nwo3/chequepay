package com.rusoft.chequepay.ui.mainscreen.fragments.contacts.listofcontactoperations;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.data.entities.operations.response.operationListApiResponse.Operation;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.mainscreen.fragments.events.EventsAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class ListOfContactOperationsFragment extends BaseBranchFragment implements ListOfContactOperationsContract.IView, EventsAdapter.ItemClickListener {

    @Inject
    ListOfContactOperationsPresenter presenter;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.label) TextView emptyExcerptLabel;
    @BindView(R.id.btn_all_operations) TextView btnSearch;
    @BindView(R.id.filter_scrollview) ScrollView scrollView;

    private List<Operation> operationList = new ArrayList<>();
    private EventsAdapter adapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_operations_list;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.excerpt);
    }

    public static ListOfContactOperationsFragment getNewInstance(String containerKey, long contactId) {
        ListOfContactOperationsFragment fragment = new ListOfContactOperationsFragment();
        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerKey);
        arguments.putLong(Screens.KEY_DATA, contactId);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerListOfContactOperationsComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .listOfContactOperationsModule(new ListOfContactOperationsModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        scrollView.setVisibility(View.GONE);
        adapter = new EventsAdapter(getContext(), operationList, this, false);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.addItemDecoration(new EventsAdapter.DividerCustomItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        presenter.onViewCreated();
        presenter.getOperationList(getArguments().getLong(Screens.KEY_DATA));
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    protected void onNavigationClicked() {
        presenter.onClickBack();
    }

    @Override
    public void showOperationList(List<Operation> operations) {
        recyclerView.setVisibility(View.VISIBLE);
        emptyExcerptLabel.setVisibility(View.GONE);
        operationList.clear();
        operationList.addAll(operations);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyOperationList() {
        recyclerView.setVisibility(View.GONE);
        emptyExcerptLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void eventSelected(long operationId, boolean isNewOperation) {
        if (isNewOperation) {
            int countUnreadMessages = ((MainActivity) getActivity()).getCountUnreadMessages();
            ((MainActivity) getActivity()).showUnreadIndicator(--countUnreadMessages);
        }
        presenter.onClickOperation(operationId);
    }

    @Override
    public void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }
}
