package com.rusoft.chequepay.ui.mainscreen.fragments.contacts;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactsModule {
    private final ContactsContract.IView view;

    public ContactsModule(ContactsContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    ContactsContract.IView provideContactsView() {
        return view;
    }
}
