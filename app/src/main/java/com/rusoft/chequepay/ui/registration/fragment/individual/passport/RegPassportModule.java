package com.rusoft.chequepay.ui.registration.fragment.individual.passport;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class RegPassportModule {
    private final RegPassportContract.IView view;

    public RegPassportModule(RegPassportContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    RegPassportContract.IView provideView() {
        return view;
    }
}
