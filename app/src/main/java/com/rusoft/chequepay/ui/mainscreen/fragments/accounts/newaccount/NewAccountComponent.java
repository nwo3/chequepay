package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.newaccount;

import com.rusoft.chequepay.di.AppComponent;
import com.rusoft.chequepay.di.module.ChequepayApiModule;
import com.rusoft.chequepay.utils.CustomScope;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = {NewAccountModule.class, ChequepayApiModule.class})
public interface NewAccountComponent {
    void inject(NewAccountFragment fragment);
}
