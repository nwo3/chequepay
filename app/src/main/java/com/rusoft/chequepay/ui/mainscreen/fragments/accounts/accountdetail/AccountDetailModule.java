package com.rusoft.chequepay.ui.mainscreen.fragments.accounts.accountdetail;

import com.rusoft.chequepay.utils.CustomScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountDetailModule {
    private final AccountDetailContract.IView view;

    public AccountDetailModule(AccountDetailContract.IView view) {
        this.view = view;
    }

    @Provides
    @CustomScope
    AccountDetailContract.IView provideAccountDetailView() {
        return view;
    }
}
