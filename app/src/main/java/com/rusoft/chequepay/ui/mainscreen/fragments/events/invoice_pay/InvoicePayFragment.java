package com.rusoft.chequepay.ui.mainscreen.fragments.events.invoice_pay;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.rusoft.chequepay.App;
import com.rusoft.chequepay.AppConstants;
import com.rusoft.chequepay.R;
import com.rusoft.chequepay.components.input.StaticInputView;
import com.rusoft.chequepay.data.entities.accounts.Account;
import com.rusoft.chequepay.data.event.ToolbarBtnOnClickEvent;
import com.rusoft.chequepay.ui.Screens;
import com.rusoft.chequepay.ui.base.fragments.BaseBranchFragment;
import com.rusoft.chequepay.ui.mainscreen.MainActivity;
import com.rusoft.chequepay.ui.otpscreen.OtpActivity;
import com.rusoft.chequepay.utils.CommonUtils;
import com.rusoft.chequepay.utils.UiUtils;

import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.rusoft.chequepay.ui.Screens.KEY_DATA;

public class InvoicePayFragment extends BaseBranchFragment implements InvoicePayContract.IView {

    @Inject
    InvoicePayPresenter presenter;

    @BindView(R.id.account) StaticInputView account;
    @BindView(R.id.date_time) StaticInputView date_time;
    @BindView(R.id.conteragent_name) StaticInputView conteragent_name;
    @BindView(R.id.conteragent_account) StaticInputView conteragent_account;
    @BindView(R.id.validity) StaticInputView validity;
    @BindView(R.id.amount) StaticInputView amount;
    @BindView(R.id.comment) StaticInputView comment;
    @BindView(R.id.status) StaticInputView status;
    @BindView(R.id.focus_handler) View focus_handler;

    @BindView(R.id.commission) TextView commission;
    @BindView(R.id.currency) TextView currency;
    @BindView(R.id.total) TextView total;
    @BindView(R.id.commission_container) View commission_container;
    @BindView(R.id.currency_container) View currency_container;
    @BindView(R.id.total_container) View total_container;

    @BindView(R.id.proceed) View proceed;
    @BindView(R.id.add_conteragent) View add_conteragent;
    @BindView(R.id.refuse) View refuse;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_invoice_pay;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.mainscreen_title_invoice_pay);
    }

    public static InvoicePayFragment getNewInstance(String containerName, Serializable data) {
        InvoicePayFragment fragment = new InvoicePayFragment();

        Bundle arguments = new Bundle();
        arguments.putString(Screens.CONTAINER_NAME, containerName);
        arguments.putSerializable(Screens.KEY_DATA, data);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    protected void initializeDaggerComponent() {
        DaggerInvoicePayComponent.builder()
                .appComponent(App.INSTANCE.getAppComponent())
                .invoicePayModule(new InvoicePayModule(this))
                .build().inject(this);
    }

    @Override
    protected void initViews() {
        presenter.onViewCreated(getArguments().getSerializable(KEY_DATA));

        account.setOnClickListener(v -> presenter.onClickAccount());

        proceed.setOnClickListener(v -> presenter.onClickProceed());
        add_conteragent.setOnClickListener(v -> presenter.onClickAddContragent());
        refuse.setOnClickListener(v -> showRefuseDialog());

        proceed.setOnTouchListener(UiUtils.getTouchListener());
        add_conteragent.setOnTouchListener(UiUtils.getTouchListener());
        refuse.setOnTouchListener(UiUtils.getTouchListener());

    }

    @Override
    public void showAccountSelectorDialog(List<Account> accounts, int selectedIndex, Integer currencyFilter) {
        UiUtils.showRadioButtonPickerDialog(getActivity(), accounts, selectedIndex,
                selected -> presenter.onAccountSelected(accounts.get(selected)), currencyFilter);
    }

    @Override
    public void setAccountText(String text) {
        account.setText(text);
    }

    @Override
    public void setDateTimeText(String text) {
        date_time.setText(text);
    }

    @Override
    public void setConteragentText(String text) {
        conteragent_name.setText(text);
    }

    @Override
    public void setConteragentAccountText(String text) {
        conteragent_account.setText(text);
    }

    @Override
    public void setValidityText(String text) {
        validity.setText(text);
        validity.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setAmountText(String text) {
        amount.setText(text);
    }

    @Override
    public void setAmountTextColor(int textColor) {
        amount.setTextColor(textColor);
    }

    @Override
    public void setCommentText(String text) {
        comment.setText(text);
        comment.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setStatusText(String text) {
        status.setText(text);
    }

    @Override
    public void setCurrencyText(String text) {
        currency.setText(text);
        currency_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setCommissionText(String text) {
        commission.setText(text);
        commission_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setTotalText(String text) {
        total.setText(text);
        total_container.setVisibility(CommonUtils.isStringNullOrEmpty(text) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showAddContragentButton(boolean show) {
        add_conteragent.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void disableProceedButton(boolean disable) {
        proceed.setEnabled(!disable);
    }

    @Override
    public void hideProceedButton(boolean hide) {
        proceed.setVisibility(hide ? View.GONE : View.VISIBLE);
    }

    @Override
    public void hideRefuseButton(boolean hide) {
        refuse.setVisibility(hide ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showDownloadPdfButton(boolean show) {
        MainActivity activity = (MainActivity) getActivity();
        if (show) activity.showToolbarBtn(R.drawable.ic_upload_file);
        else activity.hideToolbarBtn();
    }

    @Override
    public void showAddNewAccountDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_empty_accounts, null,
                R.string.cancel, R.string.dialog_on_empty_accounts_accept,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToNewAccount();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showDepositDialog(Account account) {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_account_with_not_enough_funds, null,
                R.string.cancel, R.string.deposit_1,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.goToDeposit(account);
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void showRefuseDialog() {
        UiUtils.showQuestionDialog(getContext(),
                R.string.dialog_on_invoice_refuse_text, null,
                R.string.cancel, R.string.yes,
                true, (dialog, i) -> {
                    switch (i) {
                        case DialogInterface.BUTTON_POSITIVE:
                            presenter.onRefuseConfirmed();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            dialog.dismiss();
                            break;
                    }
                });
    }

    @Override
    public void goToOtp() {
        Intent intent = new Intent(getContext(), OtpActivity.class);
        intent.putExtra(AppConstants.KEY_ACTION_TYPE, AppConstants.ACTION_TYPE_INVOICE_TRANSFER);
        startActivity(intent);
    }

    @Subscribe
    public void onEvent(ToolbarBtnOnClickEvent event) {
        presenter.goToPdfViewer();
    }

    @Override
    protected void onNavigationClicked() {
        UiUtils.hideKeyboard(getContext());
        presenter.onClickBack();
    }

    @Override
    protected void onBackPressed() {
        presenter.onClickBack();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onStop() {
        ((MainActivity) getActivity()).hideToolbarBtn();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        presenter.clearModel();
        super.onDestroyView();
    }
}
