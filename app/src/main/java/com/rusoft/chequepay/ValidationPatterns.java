package com.rusoft.chequepay;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ValidationPatterns {
    ACCOUNT_ID("[A-Z]{2}[0-9]{14}", 16, R.string.error_incorrect_requisites),

    WALLET_ID("[0-9]{12}", 12, R.string.error_incorrect_requisites),

    PHONE_RUS("^(\\+7|7|8)?[\\s\\-]?\\(?[489][0-9]{2}\\)?[\\s\\-]?[0-9]{3}[\\s\\-]?[0-9]{2}[\\s\\-]?[0-9]{2}$",
            18, R.string.error_wrong_phone),

    EMAIL("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
            66, R.string.error_wrong_email),

    PHONE_OTHER("\\+1[\\d]{9,13}|\\+7[\\d]{10}|\\+[2345689][\\d]{9,13}", 14, R.string.error_wrong_phone),

    CREDENTIALS("^[ \\-–`'’a-zA-Zа-яА-ЯёЁ]{1,70}$", 70, R.string.error_wrong_credentials),

    PASSWORD("^[a-zA-Z0-9_\\.\\-@#\\$]{4,20}$", 20, R.string.error_wrong_pass_1),
    AMC_PASSWORD("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*", 20, R.string.error_wrong_pass_3),


    WEB_URL("^(http:\\/\\/|https:\\/\\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$", 70, R.string.error_wrong_web_url),
    WEB_SITE("^(?=.{3,70}$)[-a-zA-Zа-яА-ЯёЁ0-9-_/]+(\\.[-a-zA-Zа-яА-ЯёЁ0-9-_/]+)+$", 70, R.string.error_wrong_web_url),

    PASSPORT_NUMBER_AND_SERIAL_NUMBER("^[а-яА-ЯёЁa-zA-Z0-9]{3,16}$", 16, R.string.error_wrong_data),
    PASSPORT_SERIAL_RUS("^[0-9]{4}$", 4, R.string.error_wrong_data),
    PASSPORT_NUMBER_RUS("^[0-9]{6}$", 6, R.string.error_wrong_data),
    PASSPORT_ISSUER("^[_\\.,\\-–“”\\(\\)&#/\\+«»\" №a-zA-Zа-яА-ЯёЁ0-9]{1,70}$", 70, R.string.error_wrong_data),
    PASSPORT_DEPARTMENT_CODE_RUS("^\\d{3}\\-\\d{3}$", 7, R.string.error_wrong_passport_department_code),
    PASSPORT_DEPARTMENT_CODE_OTHER("^[_\\.,\\-“”\\(\\)&#/\\+«»\" №a-zA-Zа-яА-ЯёЁ0-9]{1,70}$", 70, R.string.error_wrong_data),


    POST_CODE_RUS("^[0-9]{6}$", 6, R.string.error_wrong_data),
    POST_CODE_OTHER("^[a-zA-Z0-9\\-]{3,10}$", 10, R.string.error_wrong_data),
    DISTRICT("^[_\\.,\\-–“”\\(\\)&#/\\+«»\" a-zA-Zа-яА-ЯёЁ0-9]{1,70}$|^$", 70, R.string.error_wrong_data),
    SETTLEMENT("^[_\\.,\\-–“”\\(\\)&#/\\+«»\" '‘`’a-zA-Zа-яА-ЯёЁ0-9]{1,70}$", 70, R.string.error_wrong_data),
    ADDRESS("^[_\\.,\\-–“”\\(\\)&#/\\+«»\" №a-zA-Zа-яА-ЯёЁ0-9]{1,70}$", 70, R.string.error_wrong_data),

    PERSONAL_INN("^[0-9]{12}$", 12, R.string.error_wrong_inn),
    ENTITY_INN("^[0-9]{10}$", 10, R.string.error_wrong_inn),

    BIC_RUS("^([0-9]{9}|)$", 9, R.string.error_wrong_bic),
    BIC_OTHER("^([a-zA-Z0-9]{8,11}|)$", 11, R.string.error_wrong_bic),
    SWIFT("^[a-zA-Z0-9]{8,11}|$", 11, R.string.error_wrong_swift),
    BANK_INFO("^[_\\.,\\-“”\\(\\)&#/\\+«»\" '‘`’@№:а-яА-ЯёЁa-zA-Z0-9]{1,512}$", 512, R.string.error_wrong_data),
    BANK_ACCOUNT_RUS("^[0-9]{20}$", 20, R.string.error_wrong_bank_account_rus),
    BANK_ACCOUNT_OTHER("^[a-zA-Z0-9]{5,34}$", 34, R.string.error_wrong_data),
    BANK_INN("^[0-9]{10}$", 10, R.string.error_wrong_bank_inn),
    BANK_KPP("^[0-9]{9}$", 9, R.string.error_wrong_bank_kpp),
    BANK_OWNER("^[a-zA-Zа-яА-ЯёЁ \\-–`'’]{1,128}$", 128, R.string.error_wrong_data),

    CORR_ACC("^(?:[\\. ]*\\d){20}$", 20, R.string.error_wrong_corr_acc),

    IP_ADDRESS("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)",
            18, R.string.error_wrong_IP),

    DOUBLE("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)" +
            "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|" +
            "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))" +
            "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*",
            20, R.string.error_wrong_double),

    EMPTY_PATTERN(".*", 20, R.string.error_wrong_data);

    @Getter private String pattern;
    @Getter private int maxLength;
    @Getter private int errorTextResId;

    public static List<ValidationPatterns> asList() {
        return new ArrayList<>(EnumSet.allOf(ValidationPatterns.class));
    }

    @Nullable
    public static ValidationPatterns getType(String s) {
        for (ValidationPatterns p : asList()) {
            if (Pattern.compile(p.getPattern(), Pattern.CASE_INSENSITIVE).matcher(s).matches()) {
                return p;
            }
        }

        return null;
    }

    public static boolean validate(String s, ValidationPatterns p) {
        Pattern pattern = Pattern.compile(p.getPattern());
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    public boolean validate(String s) {
        return validate(s, this);
    }
}
