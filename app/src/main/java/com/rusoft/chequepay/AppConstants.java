package com.rusoft.chequepay;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class AppConstants {

    public static final String PHONE_CODE_RUS = "+7";
    public static final String PHONE_FORMAT = PHONE_CODE_RUS + " ([000]) [000]-[00]-[00]";
    public static final String PHONE_RECOVERY_FORMAT = "+[000000000000000000]";
    public static final String PASSPORT_CODE_FORMAT = "[000]-[000]";
    public static final String FILE_PHONE_MASKS = "phonecodes.json";

    public static final int NO_DATA = -1;

    public static final int REG_ACTIVITY_REGULAR = 0;
    public static final int REG_ACTIVITY_PROFILE_EDIT_PERSONAL = 1;
    public static final int REG_ACTIVITY_PROFILE_EDIT_HOME_ADDRESS = 2;
    public static final int REG_ACTIVITY_PROFILE_EDIT_BANK = 3;

    public static final String KEY_ACTION_TYPE = "key_action_type";
    public static final int ACTION_TYPE_AUTH = 0;
    public static final int ACTION_TYPE_REGISTRATION = 1;
    public static final int ACTION_TYPE_TRANSFER = 2;
    public static final int ACTION_TYPE_DEPOSIT = 3;
    public static final int ACTION_TYPE_WITHDRAWAL = 4;
    public static final int ACTION_TYPE_INVOICE = 5;
    public static final int ACTION_TYPE_SECURITY_SETTINGS_CHANGE = 6;
    public static final int ACTION_TYPE_PASS_RECOVERY = 7;
    public static final int ACTION_TYPE_PROFILE_EDIT = 8;
    public static final int ACTION_TYPE_INVOICE_TRANSFER = 9;

    public static final String EVENT_USER_DATA_RECEIVING_START = "event_user_data_receiving_started";
    public static final String EVENT_USER_DATA_RECEIVING_END = "event_user_data_receiving_end";
    public static final String EVENT_USER_DATA_RECEIVING_SUCCESS = "event_user_data_receiving_success";
    public static final String EVENT_USER_DATA_RECEIVING_ERROR = "event_user_data_receiving_error";

    public static final int DURATION_INSTANT = 50;
    public static final int DURATION_VERY_FAST = 100;
    public static final int DURATION_FAST = 200;
    public static final int DURATION_MEDIUM = 500;
    public static final int DURATION_LONG = 900;

    public static final int INVISIBLE_ALPHA = 0;
    public static final int VISIBLE_ALPHA = 255;

    public static final float INVISIBLE_VIEW_ALPHA = 0f;
    public static final float DISABLED_VIEW_ALPHA_WEAK = 0.25f;
    public static final float DISABLED_VIEW_ALPHA = 0.5f;
    public static final float ENABLED_VIEW_ALPHA = 1f;

    public static final SimpleDateFormat SDF_ddMMyyyy = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    public static final SimpleDateFormat SDF_ddMMyyyy_HHmm = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());

    public static final int SUCCESS_RESPONSE = 7;

    public static final int WEEK = 7;

    public static final int REQUEST_CODE_SUCCESS = 1;

    public static final int MESSAGE_DURATION = 4000;
    public static final int UPDATE_OPERATIONS_DELAY = 4000;

    public static final String FROM_PIN_TO_HELP = "pin_to_help"; // Костыль
}
